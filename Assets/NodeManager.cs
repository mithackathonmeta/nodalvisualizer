﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeManager : MonoBehaviour {
    public int maxLayers = 2;
    int maxCount;
    public GameObject Node;

    [SerializeField]
    private List<GameObject> _Nodes;

    private static NodeManager _instance;
    public static NodeManager Instance { get { return _instance; } }

    public List<GameObject> tmp1;
    public List<GameObject> tmp2;


    void Awake () {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _instance = this;
        }

    }

    void Start()
    {
        //SpawnNode(Vector3.zero, "84988900350");

    }

    void SpawnNode(Vector3 releasePos, string id)
    {
        Debug.Log("spawn node");
        GameObject newNode = (GameObject)Instantiate(Node, releasePos, Quaternion.identity, transform);
        AddNode(newNode);
        DataNode dn = newNode.GetComponent<DataNode>();

        dn.maxLayers = maxLayers;
        dn._node = Node;
        dn.originNode = null;
        newNode.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        dn.referenceID = id;
        
        
    }

    public void AddNode(GameObject node)
    {
        _Nodes.Add(node);
    }

    public GameObject CheckForRepeats(string datum)
    {
        for (int i = 0; i < _Nodes.Count; i ++)
        {
            if (_Nodes[i].GetComponent<DataNode>().referenceID == datum)
            {
                return _Nodes[i];
            }
        }
        return null;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SpawnNode(Vector3.zero, "84988900350");
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            SpawnNode(Vector3.right * 3, "84939631826");

        }
    }
    
}
