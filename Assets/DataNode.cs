﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public struct NodeStruct
{
    public GameObject n;
    public Color c;
    public LineRenderer l;
}

public class DataNode : MonoBehaviour {
    float seperationHorizontal;
    float seperationVertical = 1.0f;
    public List<GameObject> DataPoints;
    public GameObject originNode;
    public GameObject _node;
    public List<GameObject> Connectors;
    private float _emissionStart = 0.5f;
    


    //public Color myColor = Color.blue;
    public Color nextColor = Color.cyan;


    public int maxLayers;
   // public LineRenderer lineRenderer;
    public Material lineMatDummy;

    public List<string> authors;
    public List<string> content;
    public List<string> citations;
    public string referenceID;

    public TextMesh titleText;
    public TextMesh authorText;
    public TextMesh contentText;

    public Transform corner;
    public GameObject lightFlare;

    public Transform playerHead;
    public float viewRange = 1;
    public GameObject TextStuff;


    public GameObject boxFrame;
    public GameObject cornerFrame;


    public List<NodeStruct> NSList = new List<NodeStruct>();




    void Start()
    {
        GameObject.Find("ElsevierManager").GetComponent<DataRetriever>().GetArticleData(referenceID, ReceiveMyArticleData);
        playerHead = GameObject.Find("MetaCameraRig").transform;


        nextColor = new Color(Random.value, Random.value, Random.value, 1.0f);
    }

    public LineRenderer CreateLineRenderer(Color myColor)
    {
        LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
        Material tmpmat = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.material = tmpmat;
        lineRenderer.SetColors(myColor, myColor * 2);
        lineRenderer.SetWidth(0.001F, 0.001F);
        lineRenderer.SetVertexCount(2);
        boxFrame.GetComponent<Renderer>().material = tmpmat;
        boxFrame.GetComponent<Renderer>().material.color = myColor;
        cornerFrame.GetComponent<Renderer>().material = tmpmat;
        cornerFrame.GetComponent<Renderer>().material.color = myColor;
        return lineRenderer;

    }

    public void ReceiveMyArticleData(Article article)
    {
        print(article.ID);
        print(article.Title);
        if (article.Title == null)
        {
            //gameObject.SetActive(false);
            titleText.text = ResolveTextSize("Unable to retrieve title", 50);
        }
        else
        {
            titleText.text = ResolveTextSize(article.Title, 50);

        }
        for (int i = 0; i < article.authors.Count; i ++ )
        {
            authors.Add(article.authors[i].name);
            if (i < article.authors.Count - 1)
            {
                authorText.text += article.authors[i].name + ", ";
            }
            else
            {
                authorText.text += article.authors[i].name + ".";
            }
        }

        content.Add(article.Publisher);
        contentText.text += article.Publisher;

        citations = article.referencesID;
        MakeConnections();
    }



    // GameObject connectionNode;
    public void MakeConnections()
    {
        if (maxLayers > 0)
        {
            for (int i = 0; i < citations.Count; i++)
            {
                GameObject testNode = NodeManager.Instance.CheckForRepeats(citations[i]);
                if (testNode == null)
                {
                    testNode = (GameObject)Instantiate(_node, transform.position + (transform.forward * seperationVertical), Quaternion.identity, transform);
                    testNode.transform.parent = transform.parent;
                }

                if (citations.Count < 8)
                {
                    testNode.transform.position = RandomCircle(testNode.transform.position, citations.Count * maxLayers, i, citations.Count); ;

                }
                else
                {
                    Vector2 randomInsideCircle = (Random.insideUnitCircle);
                    testNode.transform.position = transform.position + (transform.forward * seperationVertical) + new Vector3(randomInsideCircle.x, randomInsideCircle.y, 0);
                }

                NodeManager.Instance.AddNode(testNode);
                DataNode dn = testNode.GetComponent<DataNode>();
                dn.maxLayers = maxLayers - 1;
                dn._node = _node;
                dn.originNode = gameObject;
                dn.Connectors.Add(gameObject);
                dn.referenceID = citations[i];
                //dn.myColor = nextColor;

                NodeStruct newNodeStruct = new NodeStruct();
                newNodeStruct.c = nextColor;
                newNodeStruct.n = cornerFrame;
                newNodeStruct.l = dn.CreateLineRenderer(nextColor);
                dn.NSList.Add(newNodeStruct);

                //dn.CreateLineRenderer();
                dn.RefreshEmission();
                dn.MakeConnections();
            }
        }
    }

    void RefreshEmission()
    {
        Material _mat = new Material(Shader.Find("Standard"));
        _mat.CopyPropertiesFromMaterial(lineMatDummy);
        //_mat.EnableKeyword("_EMISSION");
        float emission = Mathf.PingPong(Time.time, 1.0f);
        Color baseColor = _mat.color; //Replace this with whatever you want for your base color at emission level '1'
        Color finalColor = baseColor * 1.5f * Connectors.Count;
        _mat.SetColor("_EmissionColor", finalColor);
        //GetComponent<Renderer>().material = _mat;


        lightFlare.transform.localScale = new Vector3(1 + Connectors.Count/5, 1 + Connectors.Count/5, 1 + Connectors.Count/5);
    }

    Vector3 RandomCircle(Vector3 center, float radius, int i, int total)
    {
        //float ang = Random.value * 360;
        float ang = 360 / total * i;
        Vector3 pos;
        pos.x = center.x + radius/50 * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius/50 * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }

    private string ResolveTextSize(string input, int lineLength)
    {     
        string[] words = input.Split(" "[0]);
        string result = "";
        string line = "";
        foreach (string s in words)
        {
            string temp = line + " " + s;
            if (temp.Length > lineLength)
            {
                result += line + "\n";
                line = s;
            }
            else {
                line = temp;
            }
        }      
        result += line;
        return result.Substring(1, result.Length - 1);
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, playerHead.position) > viewRange * 1.5)
        {
            TextStuff.SetActive(false);
            //transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

        }
        else
        {
            TextStuff.SetActive(true);
        }

        if (Vector3.Distance(transform.position, playerHead.position) > viewRange * 0.75)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.05f, 0.05f, 0.05f), Time.deltaTime * 5);
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.1f, 0.1f, 0.1f), Time.deltaTime * 5);
        }


        if (originNode != null)
        {
            //lineRenderer.SetPosition(0, originNode.GetComponent<DataNode>().corner.position);
            //lineRenderer.SetPosition(1, corner.position);

            for (int i = 0 ; i < NSList.Count; i ++)
            {
                NSList[i].l.SetPosition(0, NSList[i].n.transform.position);
                NSList[i].l.SetPosition(1, corner.position);
            }
        }
    }
    
}
