﻿using UnityEngine;
using System.Collections;

public class CurveRotation : MonoBehaviour {
    Vector3 randomVect;

    // Use this for initialization
    void Start () {
        randomVect = Random.insideUnitSphere;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(randomVect * Time.deltaTime * 100);
    }
}
