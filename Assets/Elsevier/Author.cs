﻿using UnityEngine;
using System.Collections;

public class Author
{
    public Author(string id, string name)
    {
        this.id = id;
        this.name = name;
    }

    public string id;
    public string name;
    //affiliation
}
