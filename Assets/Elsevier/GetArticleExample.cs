﻿using UnityEngine;
using System.Collections;

public class GetArticleExample : MonoBehaviour 
{

	void Start ()
	{
        GameObject.Find("ElsevierManager").GetComponent<DataRetriever>().GetArticleData("84980383694", ReceiveMyArticleData);
    }

    public void ReceiveMyArticleData(Article article)
    {
        print(article.ID);

    }
}
