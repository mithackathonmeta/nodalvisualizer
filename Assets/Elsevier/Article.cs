﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Article
{
    public Article(string id, string title, string publisher)
    {
        ID = id;
        Title = title;
        Publisher = publisher;
        authors = new List<Author>();
        referencesID = new List<string>();
    }

    public string ID { get; set; }
    public string Title { get; set; }
    public string Publisher { get; set; }
    public List<Author> authors;
    public List<string> referencesID;
}
