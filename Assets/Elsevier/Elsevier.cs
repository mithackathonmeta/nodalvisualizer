﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Elsevier : MonoBehaviour 
{
    [SerializeField]
    private string apiKey = "038771fde409d6a9a74399c41ef0eae8";
    private string baseURL = "http://api.elsevier.com/content/";
    private Dictionary<string, string> message = new Dictionary<string, string>();
    public delegate void Callback(JSONNode data, DataRetriever.ArticleCallback articleCallback);

    private void Awake ()
	{
        InitDictionary();
    }

    private void InitDictionary()
    {
        message.Add("search_scopus", "search/scopus/");
        message.Add("retrieve_scopus", "article/scopus_id/");
        message.Add("abstract_scopus", "abstract/scopus_id/");
    }

    public void SearchScopus(string keywords, Callback callback = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("apiKey", apiKey);
        form.AddField("httpAccept", "application/json");
        form.AddField("query", keywords);

        StartCoroutine(SendRequest(form, message["search_scopus"], "", callback));
    }

    public void RetrieveScopus(string id, Callback callback = null, DataRetriever.ArticleCallback articleCallback = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("apiKey", apiKey);
        form.AddField("httpAccept", "application/json");
        // form.AddField("view", "REF");

        StartCoroutine(SendRequest(form, message["abstract_scopus"], id, callback, articleCallback));
    }

    private IEnumerator SendRequest(WWWForm form, string message, string id = "", Callback callback = null, DataRetriever.ArticleCallback articleCallback = null)
    {
        WWW www = new WWW(baseURL + message + id, form);
        yield return www;

        //print(www.url);
        print(www.text);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
        }
        else 
        {
            JSONNode json = JSON.Parse(www.text);
            if (callback != null)
            {
                callback(json, articleCallback);
            }
        }
    }
}
