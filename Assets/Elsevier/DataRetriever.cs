﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

[RequireComponent(typeof(Elsevier))]
public class DataRetriever : MonoBehaviour 
{
    Elsevier elsevier;
    int numOfPapers = 0;
    public delegate void ArticleCallback(Article article);

	void Awake ()
	{
        elsevier = GetComponent<Elsevier>();
    }

    public void GetArticleData(string articleID, ArticleCallback articleCallback = null)
    {
        //elsevier.SearchScopus("liver+cancer");
        elsevier.RetrieveScopus(articleID, ParseArticleData, articleCallback);
    }

    public void ParseArticleData(JSONNode json, ArticleCallback articleCallback)
    {
        JSONNode coredata = json["abstracts-retrieval-response"]["coredata"];

        if (json["abstracts-retrieval-response"] == "null") return;

        string id = coredata["dc:identifier"].ToString().Replace("SCOPUS_ID:", "");
        string title = coredata["dc:title"];
        string publisher = coredata["dc:publisher"];
        string description = coredata["dc:description"];

        Article article = new Article(id, title, publisher);

        JSONNode authors = json["abstracts-retrieval-response"]["authors"]["author"];
        for (int i = 0; i < authors.Count; i++)
        {
            article.authors.Add(new Author(authors[i]["@auid"], authors[i]["ce:indexed-name"]));
        }

        JSONNode references = json["abstracts-retrieval-response"]["item"]["bibrecord"]["tail"]["bibliography"]["reference"];
        for (int i = 0; i < references.Count; i++)
        {
            numOfPapers++;
            article.referencesID.Add(references[i]["ref-info"]["refd-itemidlist"]["itemid"]["$"]);
        }

        articleCallback(article);
    }
}
