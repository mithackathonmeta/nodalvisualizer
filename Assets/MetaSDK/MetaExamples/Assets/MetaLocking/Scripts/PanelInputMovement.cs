﻿using UnityEngine;
using System.Collections;

namespace Meta.Examples
{
    /// <summary>
    /// This Monobehaviour lets the user move a game object on the X/Y plane using the arrow keys. 
    /// </summary>
    public class PanelInputMovement : MonoBehaviour
    {
        /// <summary>
        /// The constant-force component of this game object
        /// </summary>
        private ConstantForce _constantForce;

        void Start()
        {
            //Retrieve the constant-force component
            _constantForce = GetComponent<ConstantForce>();
        }

        void Update()
        {
            AddForceFromInput(15f);
        }

        /// <summary>
        /// Adds force to the game object using the constant-force component.
        /// </summary>
        /// <param name="force">The amount of force to add.</param>
        void AddForceFromInput(float force)
        {
            int x = (Input.GetKey(KeyCode.RightArrow) ? 1 : 0) - (Input.GetKey(KeyCode.LeftArrow) ? 1 : 0);
            int y = (Input.GetKey(KeyCode.UpArrow) ? 1 : 0) - (Input.GetKey(KeyCode.DownArrow) ? 1 : 0);
            _constantForce.relativeForce = new Vector3(x * force, y * force, 0);
        }
    }
}
