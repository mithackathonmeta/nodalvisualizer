using Meta.HandInput;
using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Manipulation to rotate model in an orbit ball manner.
    /// </summary>
    [RequireComponent(typeof(ModelManipulator))]
    [AddComponentMenu("Meta/Manipulation/OrbitRotateManipulation")]
    public class OrbitRotateManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 10;

        private Transform _gizmoTransform;
        private AnimationCurve _slerpCurve;
        private ModelManipulator _manipulator;
        private Quaternion _priorGizmoRotation;
        private Quaternion _deltaRotation;
        private Quaternion _priorRotation;
        private HandFeature _handFeature;
        private float _initialHandCenterDistance;
        private float _inertia;
        private bool _engaged;
        private Vector3 _priorHandPosition;

        public override int Priority
        {
            get { return _priority; }
        }

        private void Start()
        {
            _manipulator = GetComponent<ModelManipulator>();
            GameObject gizmoGameObject = new GameObject("gizmo");
            _gizmoTransform = gizmoGameObject.transform;
            _slerpCurve = new AnimationCurve();
            _slerpCurve.AddKey(new Keyframe(.5f, 0f, 0f, 0f));
            _slerpCurve.AddKey(new Keyframe(.8f, 1f, 0f, 0f));
        }

        private void Update()
        {
            //add inertia on release
            if (!_engaged)
            {
                Quaternion targetRotation = _deltaRotation * transform.rotation;
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _inertia);
                _inertia -= Time.deltaTime * 2f;
            }
        }

        public override bool TestEngage()
        {
            _handFeature = _manipulator.HandVolume.GetHand<PointHandFeature>();
            return _handFeature != null;
        }

        public override void Engage()
        {
            _engaged = true;
            _gizmoTransform.position = transform.position;
            _gizmoTransform.LookAt(_handFeature.transform.position);
            _priorGizmoRotation = _gizmoTransform.rotation;
            _initialHandCenterDistance = Vector3.Distance(transform.position, _handFeature.transform.position);
            _priorHandPosition = _handFeature.transform.position;
        }

        public override bool TestDisengage()
        {
            return !_manipulator.HandVolume.ContainsHand(_handFeature) || !_handFeature.ParentHand.BufferedIsValid();
        }

        public override void Disengage()
        {
            _inertia = 1f;
            _engaged = false;
            _handFeature = null;
        }

        public override void Manipulate()
        {
            _gizmoTransform.position = transform.position;

            _gizmoTransform.rotation = Quaternion.FromToRotation(_priorHandPosition - transform.position, _handFeature.transform.position - transform.position) * _gizmoTransform.rotation;
            Quaternion deltaGizmoRotation = Quaternion.Inverse(_priorGizmoRotation * Quaternion.Inverse(_gizmoTransform.rotation));
            Quaternion targetRotation = deltaGizmoRotation * transform.rotation;
            float centerDistance = Vector3.Distance(transform.position, _handFeature.transform.position);
            float centerRatio = centerDistance / _initialHandCenterDistance;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _slerpCurve.Evaluate(centerRatio));
            _priorGizmoRotation = _gizmoTransform.rotation;

            _deltaRotation = Quaternion.Inverse(_priorRotation * Quaternion.Inverse(transform.rotation));
            _priorRotation = transform.rotation;
            _priorHandPosition = _handFeature.transform.position;

        }
    }
}