﻿using Meta.HandInput;
using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Manipulation to scale model simply by placing
    /// two hands into the model
    /// </summary>
    [AddComponentMenu("Meta/Manipulation/TwoHandScaleManipulation")]
    public class TwoHandScaleManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 30;

        [SerializeField]
        private float _minSize = .2f;

        [SerializeField]
        private float _maxSize = 2f;

        private const float _limitResizeDamp = .1f;
        private float _priorDistance;
        private HandFeature _leftHandFeature;
        private HandFeature _rightHandFeature;
        private ModelManipulator _manipulator;
        private Vector3 _limitResizeVelocity;
        private bool _engaged;

        public override int Priority
        {
            get { return _priority; }
        }

        private void Start()
        {
            _manipulator = GetComponent<ModelManipulator>();
        }

        private void Update()
        {
            //resize model if past limits
            if (transform.localScale.x > _maxSize)
            {
                transform.localScale = new Vector3(_maxSize, _maxSize, _maxSize);
            }
            if (!_engaged)
            {
                if (transform.localScale.x < _minSize)
                {
                    transform.localScale = Vector3.SmoothDamp(transform.localScale,
                        new Vector3(_minSize, _minSize, _minSize), ref _limitResizeVelocity, _limitResizeDamp);
                }
            }
        }

        public override bool TestEngage()
        {
            //if both hands are in HandVolume engage
            _leftHandFeature = _manipulator.HandVolume.GetHand(HandType.LEFT);
            _rightHandFeature = _manipulator.HandVolume.GetHand(HandType.RIGHT);
            return _leftHandFeature != null && _rightHandFeature != null;
        }

        public override void Engage()
        {
            _engaged = true;
            _priorDistance = Vector3.Distance(_leftHandFeature.transform.position,
                                                    _rightHandFeature.transform.position);
        }

        public override bool TestDisengage()
        {
            HandFeature leftHandPart = _manipulator.HandVolume.GetHand(HandType.LEFT);
            HandFeature rightHandPart = _manipulator.HandVolume.GetHand(HandType.RIGHT);
            return leftHandPart == null || rightHandPart == null || !_leftHandFeature.ParentHand.BufferedIsValid() || !_rightHandFeature.ParentHand.BufferedIsValid();
        }

        public override void Disengage()
        {
            _engaged = false;
            _leftHandFeature = null;
            _rightHandFeature = null;
        }

        public override void Manipulate()
        {
            float currentDistance = Vector3.Distance(_leftHandFeature.transform.position,
                                                        _rightHandFeature.transform.position);
            float multiplier = currentDistance / _priorDistance;
            if (multiplier < 1.5f && multiplier > .5f)
            {
                transform.localScale *= multiplier;
            }
            _priorDistance = currentDistance;
        }
    }
}