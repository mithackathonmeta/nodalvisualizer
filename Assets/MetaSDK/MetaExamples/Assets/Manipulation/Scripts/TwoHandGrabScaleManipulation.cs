﻿using Meta.HandInput;
using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Manipulation to scale model simply by placing
    /// two hands into the model
    /// </summary>
    [AddComponentMenu("Meta/Manipulation/TwoHandGrabScaleManipulation")]
    public class TwoHandGrabScaleManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 30;

        [SerializeField]
        private float _minSize = .2f;

        [SerializeField]
        private float _maxSize = 2f;

        private const float _limitResizeDamp = .1f;
        private float _priorDistance;
        private HandFeature _leftGrabbingHandFeature;
        private HandFeature _rightGrabbingHandFeature;
        private ModelManipulator _manipulator;
        private Vector3 _limitResizeVelocity;
        private bool _engaged;

        public override int Priority
        {
            get { return _priority; }
        }

        private void Start()
        {
            _manipulator = GetComponent<ModelManipulator>();
        }

        private void Update()
        {
            //resize model if past limits
            if (transform.localScale.x > _maxSize)
            {
                transform.localScale = new Vector3(_maxSize, _maxSize, _maxSize);
            }
            if (!_engaged)
            {
                if (transform.localScale.x < _minSize)
                {
                    transform.localScale = Vector3.SmoothDamp(transform.localScale,
                        new Vector3(_minSize, _minSize, _minSize), ref _limitResizeVelocity, _limitResizeDamp);
                }
            }
        }

        public override bool TestEngage()
        {
            HandFeature leftGrabbingHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.LEFT);
            HandFeature rightGrabbingHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.RIGHT);
            //store references to hand center rather than top for more stable interaction
            if (leftGrabbingHandFeature != null && rightGrabbingHandFeature != null)
            {
                _leftGrabbingHandFeature = leftGrabbingHandFeature.ParentHand.GetChildHandFeature<CenterHandFeature>();
                _rightGrabbingHandFeature = rightGrabbingHandFeature.ParentHand.GetChildHandFeature<CenterHandFeature>();
            }
            return leftGrabbingHandFeature != null && rightGrabbingHandFeature != null;
        }

        public override void Engage()
        {
            _engaged = true;
            _leftGrabbingHandFeature.GrabbedGameObject(gameObject);
            _rightGrabbingHandFeature.GrabbedGameObject(gameObject);
            _priorDistance = Vector3.Distance(_leftGrabbingHandFeature.transform.position,
                                                    _rightGrabbingHandFeature.transform.position);
        }

        public override bool TestDisengage()
        {
            return _leftGrabbingHandFeature.ParentHand.Released() ||
                   _rightGrabbingHandFeature.ParentHand.Released();
        }

        public override void Disengage()
        {
            _engaged = false;
            _leftGrabbingHandFeature.ReleasedGameObject(gameObject);
            _rightGrabbingHandFeature.ReleasedGameObject(gameObject);
        }

        public override void Manipulate()
        {
            float currentDistance = Vector3.Distance(_leftGrabbingHandFeature.transform.position,
                                                        _rightGrabbingHandFeature.transform.position);
            float multiplier = currentDistance / _priorDistance;
            if (multiplier < 1.5f && multiplier > .5f)
            {
                transform.localScale *= multiplier;
            }
            _priorDistance = currentDistance;
        }
    }
}