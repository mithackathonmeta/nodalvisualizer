using Meta.HandInput;
using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Manipulation to rotate model in an orbit ball manner.
    /// </summary>
    [RequireComponent(typeof(ModelManipulator))]
    [AddComponentMenu("Meta/Manipulation/GrabOrbitRotateManipulation")]
    public class TwoHandGrabRotateManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 30;

        [SerializeField]
        private bool _useLocalGrabThreshold = false;

        [SerializeField]
        private float _localGrabThreshold = .4f;

        [SerializeField]
        private float _localReleaseThreshold = .5f;

        private Transform _gizmoTransform;
        private Transform _gizmoChildTransform;
        private AnimationCurve _slerpCurve;
        private ModelManipulator _manipulator;
        private HandFeature _leftHandFeature;
        private HandFeature _rightHandFeature;
        private Vector3 _priorRightHandPosition;
        private Vector3 _priorLeftHandPosition;

        public override int Priority
        {
            get { return _priority; }
        }

        private void Start()
        {
            _manipulator = GetComponent<ModelManipulator>();
            GameObject gizmoGameObject = new GameObject("gizmo");
            GameObject gizmoChildGameObject = new GameObject("gizmoChild");
            _gizmoTransform = gizmoGameObject.transform;
            _gizmoChildTransform = gizmoChildGameObject.transform;
            _gizmoChildTransform.parent = _gizmoTransform;
            _slerpCurve = new AnimationCurve();
            _slerpCurve.AddKey(new Keyframe(.5f, 0f, 0f, 0f));
            _slerpCurve.AddKey(new Keyframe(.8f, 1f, 0f, 0f));
        }

        public override bool TestEngage()
        {
            HandFeature leftHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.LEFT);
            HandFeature rightHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.RIGHT);
            if (_useLocalGrabThreshold)
            {
                leftHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.LEFT, _localGrabThreshold);
                rightHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.RIGHT, _localGrabThreshold);
            }
            else
            {
                leftHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.LEFT);
                rightHandFeature = _manipulator.HandVolume.GetGrabbingHand(HandType.RIGHT);
            }

            //store references to hand center rather than top for more stable interaction
            if (leftHandFeature != null && rightHandFeature != null)
            {
                _leftHandFeature = leftHandFeature.ParentHand.GetChildHandFeature<CenterHandFeature>();
                _rightHandFeature = rightHandFeature.ParentHand.GetChildHandFeature<CenterHandFeature>();
                return true;
            }
            return false;
        }

        public override void Engage()
        {
            _leftHandFeature.GrabbedGameObject(gameObject);
            _rightHandFeature.GrabbedGameObject(gameObject);
            _gizmoTransform.position = (_leftHandFeature.transform.position + _rightHandFeature.transform.position) / 2f;

            Vector3 leftDirection = transform.position - _leftHandFeature.transform.position;
            Vector3 rightDirection = transform.position - _rightHandFeature.transform.position;
            Vector3 averageDirection = (leftDirection + rightDirection) / 2f;
            _gizmoTransform.forward = averageDirection;

            _priorRightHandPosition = _rightHandFeature.transform.position;
            _priorLeftHandPosition = _leftHandFeature.transform.position;
        }

        public override bool TestDisengage()
        {
            if (_useLocalGrabThreshold)
            {
                return _leftHandFeature.ParentHand.Released(_localReleaseThreshold) || _rightHandFeature.ParentHand.Released(_localReleaseThreshold);
            }
            else
            {
                return _leftHandFeature.ParentHand.Released() || _rightHandFeature.ParentHand.Released();
            }
        }

        public override void Disengage()
        {
            _leftHandFeature.ReleasedGameObject(gameObject);
            _rightHandFeature.ReleasedGameObject(gameObject);
        }

        /// <summary>
        /// Continually place gizmo between two hands and average the movement vectors of the two hands
        /// in order to apply to the final object quaternion around the gizmo center point.
        /// </summary>
        public override void Manipulate()
        {
            _gizmoTransform.position = (_leftHandFeature.transform.position + _rightHandFeature.transform.position) / 2f;
            _gizmoChildTransform.position = _manipulator.RootTransform.position;
            _gizmoChildTransform.rotation = _manipulator.RootTransform.rotation;

            Vector3 priorLeftDirection = _priorLeftHandPosition - transform.position;
            Vector3 leftDirection = _leftHandFeature.transform.position - transform.position;
            Vector3 priorRightDirection = _priorRightHandPosition - transform.position;
            Vector3 rightDirection = _rightHandFeature.transform.position - transform.position;
            //Vector3 priorAverageDirection = (priorLeftDirection + priorRightDirection) / 2f;
            //Vector3 averageDirection = (leftDirection + rightDirection) / 2f;

            Quaternion leftQuaternion = Quaternion.FromToRotation(priorLeftDirection, leftDirection);
            Quaternion rightQuaternion = Quaternion.FromToRotation(priorRightDirection, rightDirection);

            _gizmoTransform.rotation = Quaternion.Slerp(leftQuaternion, rightQuaternion, .5f) * _gizmoTransform.rotation;

            _manipulator.Move(_gizmoChildTransform.transform.position);
            _manipulator.Rotate(_gizmoChildTransform.rotation);

            _priorRightHandPosition = _rightHandFeature.transform.position;
            _priorLeftHandPosition = _leftHandFeature.transform.position;
        }
    }
}