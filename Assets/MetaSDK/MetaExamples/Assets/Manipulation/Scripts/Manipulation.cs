﻿using UnityEngine;

namespace Meta
{
    /// <summary>
    /// ModelManipulator will gather all Manipulation components on the
    /// same GameObject and execute them depending on conditions described
    /// in TestEngage and TestDisengage.
    /// </summary>
    [RequireComponent(typeof(ModelManipulator))]
    public abstract class Manipulation : MonoBehaviour
    {
        public ManipulationState State { get; set; }

        /// <summary>
        /// Priority of manipulation. Manipulations with higher priority
        /// will immediatly override ones with lower priority.
        /// </summary>
        public abstract int Priority { get; }

        /// <summary>
        /// Returns true when proper conditions are met to engage this manipulation.
        /// </summary>
        public abstract bool TestEngage();

        /// <summary>
        /// Called when Engaged.
        /// </summary>
        public abstract void Engage();

        /// <summary>
        /// Returns true when proper conditions are met to disengage this manipulation.
        /// </summary>
        public abstract bool TestDisengage();

        /// <summary>
        /// Called when Disengaged.
        /// </summary>
        public abstract void Disengage();

        /// <summary>
        /// Called every frame to perform manipulation.
        /// </summary>
        public abstract void Manipulate();
    }
}