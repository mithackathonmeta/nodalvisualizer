﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using Meta.HandInput;

namespace Meta
{
    [CustomEditor(typeof(ModelManipulator))]
    [CanEditMultipleObjects]
    public class ModelManipulatorInspector : Editor
    {
        private SerializedProperty _handVolumeProperty;

        void OnEnable()
        {
            _handVolumeProperty = serializedObject.FindProperty("_handVolume");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            ModelManipulator modelManipulator = (ModelManipulator)target;
            HandVolume handVolume = (HandVolume) _handVolumeProperty.objectReferenceValue;

            if (handVolume == null)
            {
                if (GUILayout.Button("Create HandVolume"))
                {
                    modelManipulator.GenerateHandVolume();
                }
            }

            EditorGUILayout.PropertyField(_handVolumeProperty, new GUIContent("HandVolume"));

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Manipulations:");

            Manipulation[] manipulations = modelManipulator.GetComponents<Manipulation>().OrderByDescending((a) => a.Priority).ToArray();

            for (int i = 0; i < manipulations.Length; ++i)
            {
               
                EditorGUILayout.LabelField("- " + manipulations[i].GetType() + " | Priority: " + manipulations[i].Priority + " | State: " + manipulations[i].State);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}