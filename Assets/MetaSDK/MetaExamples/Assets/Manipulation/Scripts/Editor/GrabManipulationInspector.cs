﻿using UnityEditor;

namespace Meta
{
    [CustomEditor(typeof(GrabManipulation))]
    [CanEditMultipleObjects]
    public class GrabManipulationInspector : Editor
    {
        private SerializedProperty _priorityProperty;
        private SerializedProperty _useLocalThresholdsProperty;
        private SerializedProperty _localGrabThresholdProperty;
        private SerializedProperty _localReleaseThresholdProperty;
        private SerializedProperty _showEventsProperty;
        private SerializedProperty _onGrabProperty;
        private SerializedProperty _onReleaseProperty;

        void OnEnable()
        {
            _priorityProperty = serializedObject.FindProperty("_priority");
            _useLocalThresholdsProperty = serializedObject.FindProperty("_useLocalGrabThreshold");
            _localGrabThresholdProperty = serializedObject.FindProperty("_localGrabThreshold");
            _localReleaseThresholdProperty = serializedObject.FindProperty("_localReleaseThreshold");
            _showEventsProperty = serializedObject.FindProperty("_showEvents");
            _onGrabProperty = serializedObject.FindProperty("_onGrab");
            _onReleaseProperty = serializedObject.FindProperty("_onRelease");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(_priorityProperty);

            EditorGUILayout.PropertyField(_useLocalThresholdsProperty);

            if (_useLocalThresholdsProperty.boolValue)
            {
                EditorGUILayout.PropertyField(_localGrabThresholdProperty);
                EditorGUILayout.PropertyField(_localReleaseThresholdProperty);
            }

            _showEventsProperty.boolValue = EditorGUILayout.Foldout(_showEventsProperty.boolValue, "Event Callbacks");
            
            if (_showEventsProperty.boolValue)
            {
                EditorGUILayout.PropertyField(_onGrabProperty);
                EditorGUILayout.PropertyField(_onReleaseProperty);
            }

            EditorGUILayout.Space();

            serializedObject.ApplyModifiedProperties();
        }
    }
}