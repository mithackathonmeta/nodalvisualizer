﻿using Meta.HandInput;
using UnityEngine;
using UnityEngine.Events;

namespace Meta
{
    /// <summary>
    /// Manipulation to grab the model to translate it's position.
    /// </summary>
    [AddComponentMenu("Meta/Manipulation/GrabManipulation")]
    public class GrabManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 20;

        [SerializeField]
        private bool _useLocalGrabThreshold = true;

        [SerializeField]
        private float _localGrabThreshold = .1f;

        [SerializeField]
        private float _localReleaseThreshold = .2f;

        [SerializeField]
        private bool _showEvents;

        [SerializeField]
        private HandFeatureEvent _onGrab = null;

        [SerializeField]
        private HandFeatureEvent _onRelease = null;

        private ModelManipulator _manipulator;
        private HandFeature _handFeature;
        private Vector3 _grabOffset;

        public override int Priority
        {
            get { return _priority; }
        }

        private void Start()
        {
            _manipulator = GetComponent<ModelManipulator>();
        }

        public override bool TestEngage()
        {
            HandFeature handFeature;
            if (_useLocalGrabThreshold)
            {
                handFeature = _manipulator.HandVolume.GetGrabbingHand(_localGrabThreshold);
            }
            else
            {
                handFeature = _manipulator.HandVolume.GetGrabbingHand();
            }
            //store references to hand center rather than top for more stable interaction
            if (handFeature != null)
            {
                _handFeature = handFeature.ParentHand.GetChildHandFeature<CenterHandFeature>();
            }
            return handFeature != null;

        }

        public override void Engage()
        {
            _handFeature.GrabbedGameObject(gameObject);

            //rigidbody should be kinematic as to not interfere with grab translation
            if (_manipulator.Rigidbody != null)
            {
                _manipulator.Rigidbody.isKinematic = true;
            }

            _grabOffset = _manipulator.RootTransform.position - _handFeature.transform.position;
            if (_onGrab != null)
            {
                _onGrab.Invoke(_handFeature);
            }
        }

        public override bool TestDisengage()
        {
            if (_useLocalGrabThreshold)
            {
                return _handFeature.ParentHand.Released(_localReleaseThreshold);
            }
            else
            {
                return _handFeature.ParentHand.Released();
            }
        }

        public override void Disengage()
        {
            _handFeature.ReleasedGameObject(gameObject);

            if (_manipulator.Rigidbody != null)
            {
                _manipulator.Rigidbody.isKinematic = false;
            }

            if (_onRelease != null)
            {
                _onRelease.Invoke(_handFeature);
            }
        }

        public override void Manipulate()
        {
            _manipulator.RootTransform.position = _handFeature.transform.position + _grabOffset;
        }
    }
}
