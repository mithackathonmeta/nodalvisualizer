﻿using Meta.HandInput;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Meta
{
    /// <summary>
    /// Iterates through and applies manipulations from all Manipulation
    /// components on same GameObject.
    /// </summary>
    public class ModelManipulator : MonoBehaviour
    {
        [Tooltip("HandVolume which encapsulates object to detect hand interactions. If not specified it will be created automatically.")]
        [SerializeField]
        private HandVolume _handVolume;

        private List<Manipulation> _manipulationList;
        private Manipulation _currentManipulation;
        private Rigidbody _rigidbody;
        private Transform _rootTransform;

        /// <summary>
        /// HandVolume which encapsulates object to detect hand interactions. 
        /// If not specified it will be created automatically.
        /// </summary>
        public HandVolume HandVolume
        {
            get { return _handVolume; }
        }

        /// <summary>
        /// Transform to apply manipulation to. If GameObject hiearchy contains a Rigidbody
        /// this will be the Transform of the Rigidbody. Otherwise it is the transform the 
        /// ModelManipulator is placed on.
        /// </summary>
        public Transform RootTransform
        {
            get { return _rootTransform; }
        }

        /// <summary>
        /// Relevant Rigibody on this object, or first one above it in hiearchy.
        /// </summary>
        public Rigidbody Rigidbody
        {
            get { return _rigidbody; }
        }

        private void Start()
        {
            _manipulationList = GetComponents<Manipulation>().OrderByDescending((a) => a.Priority).ToList();

            _rigidbody = GetComponentInParent<Rigidbody>();
            _rootTransform = _rigidbody == null ? transform : _rigidbody.transform;

            GenerateHandVolume();
        }

        /// <summary>
        /// Move relevant Rigidbody transform if exists, otherwise will move transform.
        /// </summary>
        public void Move(Vector3 postion)
        {
            if (_rigidbody == null)
            {
                transform.position = postion;
            }
            else
            {
                _rigidbody.MovePosition(postion);
            }
        }

        /// <summary>
        /// Rotate relevant Rigidbody transform if exists, otherwise will move transform.
        /// </summary>
        public void Rotate(Quaternion rotatation)
        {
            if (_rigidbody == null)
            {
                transform.rotation = rotatation;
            }
            else
            {
                _rigidbody.MoveRotation(rotatation);
            }
        }

        /// <summary>
        /// Automatically generate a HandVolume and hook it up to this ModelManipulator
        /// </summary>
        public void GenerateHandVolume()
        {
            Collider[] colliders = GetComponentsInChildren<Collider>();
            MeshRenderer[] meshRenderer = GetComponentsInChildren<MeshRenderer>();

            if (_handVolume == null && (colliders.Length > 0 || meshRenderer.Length > 0))
            {
                //Create new HandVolume GameObject
                GameObject gameObject = new GameObject("HandVolume");
                gameObject.transform.parent = transform;
                gameObject.transform.localPosition = Vector3.zero;

                //Create bounds and fit to all contained colliders or meshes
                Bounds bounds = new Bounds(gameObject.transform.position, Vector3.zero);
                for (int i = 0; i < colliders.Length; ++i)
                {
                    bounds.Encapsulate(colliders[i].bounds);
                }
                for (int i = 0; i < meshRenderer.Length; ++i)
                {
                    bounds.Encapsulate(meshRenderer[i].bounds);
                }

                //Create and configure HandVolume collider
                BoxCollider collider = gameObject.AddComponent<BoxCollider>();
                collider.isTrigger = true;
                float expandSize = .1f;
                collider.size = new Vector3(bounds.size.x + expandSize, bounds.size.y + expandSize, bounds.size.z + expandSize);

                _handVolume = gameObject.AddComponent<HandVolume>();
            }
        }

        private void Update()
        {
            //TODO refactor to not rely on currentManipulation being null or not
            //but rather do computation based purely on the ManipulationState and also
            //allow multiple manipulations to be engaged
            if (_handVolume != null)
            {
                SetState();
                FindValidManipulation();
            }
        }

        private void SetState()
        {
            if (_currentManipulation != null &&
                _currentManipulation.State == ManipulationState.On &&
                _currentManipulation.TestDisengage())
            {
                _currentManipulation.State = ManipulationState.Disengaging;
                _currentManipulation.Disengage();
            }
            else if (_currentManipulation != null &&
                     _currentManipulation.State == ManipulationState.Engaging)
            {
                _currentManipulation.State = ManipulationState.On;
            }
            else if (_currentManipulation != null &&
                    _currentManipulation.State == ManipulationState.Disengaging)
            {
                _currentManipulation.State = ManipulationState.Off;
            }
            else if (_currentManipulation != null &&
                     _currentManipulation.State == ManipulationState.Off)
            {
                _currentManipulation = null;
            }
            else if (_currentManipulation != null)
            {
                _currentManipulation.Manipulate();
            }
        }

        private void FindValidManipulation()
        {
            //If one already in use only check manipulations above it in Priority.
            for (int i = 0; i < _manipulationList.Count; ++i)
            {
                if (_manipulationList[i] == _currentManipulation)
                {
                    break;
                }
                if (_manipulationList[i].enabled && _manipulationList[i].TestEngage())
                {
                    //if manipulation already engaged, disengage before switching to new one
                    if (_currentManipulation != null)
                    {
                        _currentManipulation.State = ManipulationState.Disengaging;
                        _currentManipulation.Disengage();
                    }
                    _currentManipulation = _manipulationList[i];
                    _currentManipulation.State = ManipulationState.Engaging;
                    _currentManipulation.Engage();
                    break;
                }
            }
        }
    }
}