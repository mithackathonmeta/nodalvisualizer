﻿namespace Meta
{
    public enum ManipulationState
    {
        /// <summary>
        /// No manipulation engaged
        /// </summary>
        Off,

        /// <summary>
        /// Manipulation is engaging this frame.
        /// </summary>
        Engaging,

        /// <summary>
        /// Manipulation is engaged
        /// </summary>
        On,

        /// <summary>
        /// Manipulation disengaging this frame.
        /// </summary>
        Disengaging,
    }
}
