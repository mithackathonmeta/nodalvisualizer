﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Meta.Physics;

namespace Meta
{

    /// <summary>
    /// Manipulation to rotate model in a touchpad manner.
    /// </summary>
    [RequireComponent(typeof(ModelManipulator))]
    [RequireComponent(typeof(BoxCollider))]
    public class RotateViaTouchesManipulation : Manipulation
    {
        [SerializeField]
        private int _priority = 10;

        public PointProvider pointProvider;
        public float Margin = 0.003f;
        public float TouchRadius = 0.01f;
        public int MinPointsForTouch = 10;
        private int _lastPointCloudId = int.MinValue;
        private List<Vector3> _pointBuffer = new List<Vector3>();

        protected List<TouchState> _touchStates = new List<TouchState>();

        public override int Priority
        {
            get { return _priority; }
        }

        private void UpdateRotation(TouchState previous, TouchState current)
        {
            if (previous.Touches.Count < 1 || current.Touches.Count < 1)
            {
                return;  // No touches, no rotates.
            }

            if (previous.Touches.Count == 1 || current.Touches.Count == 1)
            {
                UpdateRotationWithSingleTouchPoint(previous, current);
            }


            if (previous.Touches.Count == 2 || current.Touches.Count == 2)
            {
                UpdateRotationWithTwoTouchPoints(previous, current);
            }

            // Fall back to one point:
            UpdateRotationWithSingleTouchPoint(previous, current);
        }

        private void UpdateRotationWithTwoTouchPoints(TouchState previous, TouchState current)
        {
            // Compare these two single-touch sets and rotate this object to make them match as best we can.
            // Find the mapping of two touch pairs with the smallest sum-of-squares distances.
            UpdateRotationWithSingleTouchPoint(previous, current);
            return;


            //var pair = FindClosestPair(previous, current);

            //Vector3 center = this.transform.position;
            //Vector3 toP = (pair.Key.Centroid - center).normalized;
            //if (toP == Vector3.zero) return;

            //Vector3 toC = (pair.Value.Centroid - center).normalized;
            //if (toC == Vector3.zero) return;

            //Quaternion q = Quaternion.FromToRotation(toP, toC);

            //Vector3 euler = q.eulerAngles;
            ////euler = ZeroSmallerDimensions(euler);
            //q = Quaternion.Euler(euler);

            //Quaternion newRotation = this.transform.rotation * q;
            ////this.transform.rotation = newRotation;
            //this.transform.Rotate(euler, Space.World);

            //// This is not working well.
            ////  Touch points correctly determined?  Yes
            ////  Rotation between points correctly determined?  Dunno    - how to verify???
            ////  Rotation correctly applied to cube?  Dunno

        }

        private void UpdateRotationWithSingleTouchPoint(TouchState previous, TouchState current)
        {
            // Compare these two single-touch sets and rotate this object to make them match as best we can.
            // Find the two touches that are closest together.
            var pair = FindClosestPair(previous, current);

            Vector3 center = this.transform.position;
            Vector3 toP = (pair.Key.Centroid - center).normalized;
            if (toP == Vector3.zero) return;

            Vector3 toC = (pair.Value.Centroid - center).normalized;
            if (toC == Vector3.zero) return;

            Quaternion q = Quaternion.FromToRotation(toP, toC);
            Vector3 euler = q.eulerAngles;
            //euler = ZeroSmallerDimensions(euler);  // This line would restrict it to axis-aligned rotations.  Not satisfying, but handy for debugging.
            q = Quaternion.Euler(euler);

            this.transform.Rotate(euler, Space.World);

            //  Touch points correctly determined?  Yes
            //  Rotation between points correctly determined?  Yes
            //  Rotation correctly applied to cube?  Yes- it was using local space for some reason.  Decomposing into Euler and applying in world space worked to fix the issue.
        }

        private Vector3 ZeroSmallerDimensions(Vector3 euler)
        {
            if (euler.x > 180) euler.x -= 360;
            if (euler.y > 180) euler.y -= 360;
            if (euler.z > 180) euler.z -= 360;

            Vector3 abs = new Vector3(Mathf.Abs(euler.x), Mathf.Abs(euler.y), Mathf.Abs(euler.z));
            if (abs.x > abs.y)
            {
                euler.y = 0;
                if (abs.x > abs.z)
                {
                    euler.z = 0;
                }
                else
                {
                    euler.x = 0;
                }
                return euler;
            }
            else
            {
                euler.x = 0;
                if (abs.y > abs.z)
                {
                    euler.z = 0;
                }
                else
                {
                    euler.y = 0;
                }
                return euler;
            }
        }

        void OnDrawGizmos()
        {
            foreach (var touch in LatestTouchState.Touches)
            {
                Gizmos.DrawSphere(touch.Centroid, TouchRadius);
            }
        }

        private KeyValuePair<CentroidData, CentroidData> FindClosestPair(TouchState previous, TouchState current)
        {
            float minDistanceSquared = float.MaxValue;
            CentroidData bestP = new CentroidData();
            CentroidData bestC = new CentroidData();

            for (int i = 0; i < previous.Touches.Count; ++i)
            {
                CentroidData p = previous.Touches[i];
                Vector3 pCentroid = p.Centroid;

                for (int j = 0; j < current.Touches.Count; ++j)
                {
                    CentroidData c = current.Touches[j];
                    float d2 = (pCentroid - c.Centroid).sqrMagnitude;
                    if (d2 < minDistanceSquared)
                    {
                        minDistanceSquared = d2;
                        bestP = p;
                        bestC = c;
                    }
                }
            }

            return new KeyValuePair<CentroidData, CentroidData>(bestP, bestC);
        }

        float DistanceBetweenCentroidsSquared(CentroidData a, CentroidData b)
        {
            return (a.Centroid - b.Centroid).sqrMagnitude;
        }

        private void DetermineMapping(TouchState previous, TouchState current)
        {
            // Again, doing this the potentially slow way.
            int[] prevToCurrentBest = new int[previous.Touches.Count];
            for (int i = 0; i < prevToCurrentBest.Length; ++i)
            {
                prevToCurrentBest[i] = i;
            }

            // Find the largest triangle?   Does it match?

            //  If only one point, do the math.  Don't rotate too far to avoid finger swap issues.
            //      Vector from center to point, get quat between them
            //  Two points, make a triangle.  Map the triangles by side length, use the rotation of the normal to determine rotation
            //  Two+, find the biggest triangle, and do the two point thing

        }

        private float Score(int[] prevToCurrentBest, int length, TouchState previous, TouchState current)
        {
            throw new NotImplementedException();
        }

        private float Score(int[] prevToCurrentBest, TouchState previous, TouchState current)
        {
            throw new NotImplementedException();
        }

        public override void Manipulate()
        {
            if (pointProvider.Initialized && _lastPointCloudId != pointProvider.UpdateID)
            {
                // Points have changed. Parse!
                ParsePointCloudIntoTouches();
                CleanUpOldTouches();

                if (_touchStates.Count > 1)
                {
                    UpdateRotation(_touchStates[_touchStates.Count - 2], _touchStates[_touchStates.Count - 1]);
                }
            }
        }

        public override bool TestEngage()
        {
            // Engage when there are some points on my borders.
            if (pointProvider.Initialized && _lastPointCloudId != pointProvider.UpdateID)
            {
                // Points have changed. Parse!
                ParsePointCloudIntoTouches();
                CleanUpOldTouches();

                return (LatestTouchState.Touches.Count > 0);
            }

            return false;
        }

        private void CleanUpOldTouches()
        {
            float tooOld = Time.time - 0.1f;
            for (int i = _touchStates.Count - 1; i >= 0; --i)
            {
                TouchState state = _touchStates[i];
                if (state.UnityTime < tooOld)
                {
                    _touchStates.RemoveAt(i);
                }
            }
        }

        TouchState CreateTouchState()
        {
            return new TouchState();  // could reuse touch states later to avoid memory allocations
        }

        private void ParsePointCloudIntoTouches()
        {
            _lastPointCloudId = pointProvider.UpdateID;

            TouchState newState = CreateTouchState();

            _pointBuffer.Clear();
            if (_pointBuffer.Capacity < pointProvider.GetPointPoolLength())
            {
                _pointBuffer.Capacity = pointProvider.GetPointPoolLength();
            }
            pointProvider.AddPointsToPool(_pointBuffer);

            // That's the whole point cloud.
            // Filter down to points nearby.
            BoxCollider box = GetComponent<BoxCollider>();
            Matrix4x4 worldToLocal = transform.worldToLocalMatrix;
            Bounds worldBounds = box.bounds;
            worldBounds.Expand(Margin);

            Bounds localBounds = new Bounds(box.center, box.size);
            Vector3 lossyScale = transform.lossyScale;

            Bounds localBoundsLarge = localBounds;
            localBoundsLarge.extents += new Vector3(Margin / lossyScale.x, Margin / lossyScale.y, Margin / lossyScale.z);

            Bounds localBoundsSmall = localBounds;
            localBoundsSmall.extents -= new Vector3(Margin / lossyScale.x, Margin / lossyScale.y, Margin / lossyScale.z);
            localBoundsSmall.extents = Vector3.Max(localBoundsSmall.extents, Vector3.zero);

            var points = _pointBuffer.Take(pointProvider.GetPointPoolLength());
            var nearbyPoints = points.Where(p => worldBounds.Contains(p));
            var onEdgePoints = nearbyPoints.Where(p =>
            {
                Vector3 localP = worldToLocal.MultiplyPoint3x4(p);
                return localBoundsLarge.Contains(localP) && !localBoundsSmall.Contains(localP);
            }
            );

            newState.UnityTime = Time.time;
            newState.Touches = ParseEdgePointsIntoCentroids(onEdgePoints.ToList()).ToList();
            newState.worldToLocal = worldToLocal;
            _touchStates.Add(newState);
        }

        private IEnumerable<CentroidData> ParseEdgePointsIntoCentroids(List<Vector3> points)
        {
            //Debug.Log("EDGE: " + points.Count);

            // Not worrying about speed right now.  Make some touches!
            for (int i = 0; i < 100 && points.Count > 0; ++i)
            {
                CentroidData cd = FindCentroidAround(points[points.Count - 1], TouchRadius, points);
                if (cd.VectorCount < 1)
                {
                    Debug.LogError("No points in centroid!!");
                    yield break;
                }

                if (cd.VectorCount >= MinPointsForTouch)
                {
                    yield return cd;
                }

                // Remove all points in the centroid region
                RemovePointsInCentroidRegion(cd.Centroid, TouchRadius, points);
            }
        }

        private CentroidData FindCentroidAround(Vector3 center, float radius, List<Vector3> points)
        {
            CentroidData cd = new CentroidData();

            for (int i = 0; i < points.Count; ++i)
            {
                Vector3 p = points[i];
                float d2 = (p - center).sqrMagnitude;
                if (d2 <= radius)
                {
                    cd.AddPoint(p);
                }
            }

            return cd;
        }

        private void RemovePointsInCentroidRegion(Vector3 center, float radius, List<Vector3> points)
        {
            for (int i = points.Count - 1; i >= 0; --i)
            {
                Vector3 p = points[i];
                float d2 = (p - center).sqrMagnitude;
                if (d2 <= radius)
                {
                    points.RemoveAt(i);
                }
            }
        }

        public override void Engage()
        {
            Debug.Log("ENGAGE!");
        }

        public override bool TestDisengage()
        {
            return LatestTouchState.Touches.Count < 1;
        }

        public override void Disengage()
        {
            Debug.Log("DISENGAGE!");
        }

        TouchState LatestTouchState
        {
            get
            {
                if (_touchStates.Count < 1)
                {
                    _touchStates.Add(new TouchState());
                }

                return _touchStates[_touchStates.Count - 1];
            }
        }

        public class TouchState
        {
            public float UnityTime;
            public List<CentroidData> Touches = new List<CentroidData>();
            public Matrix4x4 worldToLocal;

            internal void SortByDecreasingNumberOfSamples()
            {
                throw new NotImplementedException();
            }
        }
    }
}
