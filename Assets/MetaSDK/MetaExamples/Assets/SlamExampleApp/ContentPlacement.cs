﻿using UnityEngine;
using System.Collections;
using Meta;

public class ContentPlacement : MetaBehaviour {

    /// <summary>
    /// The key to be used as the panel locking toggle.
    /// </summary>
    public KeyCode key = KeyCode.L;

    /// <summary>
    /// Current locking state.
    /// </summary>
    bool locked = false;

    /// <summary>
    /// The TextMesh that will display the SLAM Localizer state.
    /// </summary>
    public TextMesh message = null;

    /// <summary>
    /// The MetaLocking script that has the HUD property that will be toggled.
    /// </summary>
    public MetaLocking metaLocking;

    /// <summary>
    /// Change whether the SLAMPanels object is HUD Locked or not.
    /// </summary>
    /// <param name="state">The HUD Locked state to change to.</param>
    void ToggleHUDLocking(bool state)
    {
        metaLocking.hud = state;
    }

    /// <summary>
    /// Check whether the locking toggle key has been pressed.
    /// If it has, change the state of the HUD locking property.
    /// </summary>
    void CheckForToggleKey()
    {
        if (Input.GetKeyDown(key))
        {
            if (!locked)
            {
                Debug.Log("Dropped");
                locked = true;

            }
            else
            {
                Debug.Log("Picked Up");
                this.transform.parent = GameObject.Find("MetaCameraRig").transform;
                locked = false;
            }
            ToggleHUDLocking(locked);
        }
    }

    /// <summary>
    /// Updates the on-screen text with the state of the SLAM Localizer.
    /// </summary>
    void UpdateText()
    {
        ILocalizer localizer = metaContext.Get<MetaLocalization>().GetLocalizer();

        if (localizer.GetType() == typeof(Meta.SlamLocalizer))
        {
            // if (((Meta.SlamLocalizer)localizer).state == SlamInterop.SLAMLocalizerState.inTracking)

            if (((Meta.SlamLocalizer)localizer).slamFeedback.camera_ready == 0)
            {
                if(message != null) message.text = "Cameras initializing...";
            }
            else if (((Meta.SlamLocalizer)localizer).slamFeedback.tracking_ready == 0)
            {
                if (message != null) message.text = "Tracker initializing...";
            }
            else if (((Meta.SlamLocalizer)localizer).slamFeedback.scale_quality_percent == 100)
            {
                if (message != null) message.text = "";
            }
            else
            {
                if (message != null) message.text = 
                        "Scanning " + 
                        ((Meta.SlamLocalizer)localizer).slamFeedback.scale_quality_percent + 
                        "% done." + System.Environment.NewLine 
                        + "...look around...";
            }
        }
    }

	// Use this for initialization
	void Start()
    {

	}
	
	// Update is called once per frame
	void Update()
	{

	    CheckForToggleKey();
        UpdateText();


	}
}
