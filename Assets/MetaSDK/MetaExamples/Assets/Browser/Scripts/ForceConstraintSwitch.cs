﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Meta;
using Meta.Physics;

namespace Meta
{
    public class ForceConstraintSwitch : MonoBehaviour
    {
        [SerializeField]
        private List<ForceConstraint> _constraintList = null;
        [SerializeField]
        private float _initialConstraintsApplyDelay = 0.5f;
        [SerializeField]
        private float _constraintChangeDelay = 0.5f;

        private Rigidbody _rigidbody;

        private RigidbodyConstraints _initialConstraints;

        private float _constraintChangeTimer;

        private void Awake()
        {
            _rigidbody = GetComponentInParent<Rigidbody>();
            _initialConstraints = _rigidbody.constraints;
        }

        private void FixedUpdate()
        {
            if (_constraintChangeTimer <= 0f)
            {
                FindAndApplyConstraint();
            }

            if (_constraintChangeTimer > 0f)
            {
                StopAllCoroutines();
            }
            else
            {
                StartCoroutine(ApplyInitialContraintsCoroutine());
            }

            if (_constraintChangeTimer > 0f)
            {
                _constraintChangeTimer -= Time.deltaTime;
            }
        }

        private void FindAndApplyConstraint()
        {
            for (int i = 0; i < _constraintList.Count; ++i)
            {
                if (_constraintList[i].Agent.AppliedForce.magnitude > 0f)
                {
                    _rigidbody.constraints = Bool3ToContraints(_constraintList[i].FreezePosition, _constraintList[i].FreezeRotation);
                    _constraintChangeTimer = _constraintChangeDelay;

                    break;
                }
            }
        }

        private IEnumerator ApplyInitialContraintsCoroutine()
        {
            yield return new WaitForSeconds(_initialConstraintsApplyDelay);

            _rigidbody.constraints = _initialConstraints;
        }

        private RigidbodyConstraints Bool3ToContraints(Bool3 position, Bool3 rotation)
        {
            RigidbodyConstraints constraints = RigidbodyConstraints.None;

            if (position.X)
            {
                constraints |= RigidbodyConstraints.FreezePositionX;
            }

            if (position.Y)
            {
                constraints |= RigidbodyConstraints.FreezePositionY;
            }

            if (position.Z)
            {
                constraints |= RigidbodyConstraints.FreezePositionZ;
            }

            if (rotation.X)
            {
                constraints |= RigidbodyConstraints.FreezeRotationX;
            }

            if (rotation.Y)
            {
                constraints |= RigidbodyConstraints.FreezeRotationY;
            }

            if (rotation.Z)
            {
                constraints |= RigidbodyConstraints.FreezeRotationZ;
            }

            return constraints;
        }
    }
}