﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

namespace Meta
{
    [CustomEditor(typeof(ForceConstraintSwitch))]
    [CanEditMultipleObjects]
    public class ForceContraintSwitchEditor : Editor
    {
        private ReorderableList _forceConstraintReorderableList;

        private void OnEnable()
        {
            _forceConstraintReorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("_constraintList"), true, true, true, true);
            _forceConstraintReorderableList.drawElementCallback = DrawElementCallback;
            _forceConstraintReorderableList.elementHeightCallback = ElementHeightCallback;
            _forceConstraintReorderableList.drawHeaderCallback = DrawHeaderCallback;
        }

        private void DrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, "Force Constraint List");
        }

        private float ElementHeightCallback(int index)
        {
            SerializedProperty element = _forceConstraintReorderableList.serializedProperty.GetArrayElementAtIndex(index);
            SerializedProperty showFreezeProperty = element.FindPropertyRelative("_showFreezeConstraints");
            float height = EditorGUIUtility.singleLineHeight * 3f;
            if (showFreezeProperty.boolValue)
            {
                height += EditorGUIUtility.singleLineHeight * 2f;
            }
            return height;
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            rect.y += 2;
            SerializedProperty element = _forceConstraintReorderableList.serializedProperty.GetArrayElementAtIndex(index);
            SerializedProperty colliderProperty = element.FindPropertyRelative("_agent");
            SerializedProperty positionProperty = element.FindPropertyRelative("_freezePosition");
            SerializedProperty rotationProperty = element.FindPropertyRelative("_freezeRotation");
            Rect drawRect = new Rect(rect.x, rect.y, 300, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(drawRect, colliderProperty, GUIContent.none);
            drawRect.y += EditorGUIUtility.singleLineHeight;

            SerializedProperty showFreezeProperty = element.FindPropertyRelative("_showFreezeConstraints");
            drawRect.x = rect.x;
            showFreezeProperty.boolValue = EditorGUI.Foldout(drawRect, showFreezeProperty.boolValue, "Constraints");

            if (showFreezeProperty.boolValue)
            {
                drawRect.y += EditorGUIUtility.singleLineHeight;
                drawRect.width = 30;
                drawRect.x = rect.x + 30;
                SerializedProperty xPositionProperty = positionProperty.FindPropertyRelative("X");
                EditorGUI.PropertyField(drawRect, xPositionProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                SerializedProperty yPositionProperty = positionProperty.FindPropertyRelative("Y");
                EditorGUI.PropertyField(drawRect, yPositionProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                SerializedProperty zPositionProperty = positionProperty.FindPropertyRelative("Z");
                EditorGUI.PropertyField(drawRect, zPositionProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                drawRect.width = 150f;
                EditorGUI.LabelField(drawRect, "Freeze Position X Y Z");

                drawRect.y += EditorGUIUtility.singleLineHeight;
                drawRect.width = 30;
                drawRect.x = rect.x + 30;
                SerializedProperty xRotationProperty = rotationProperty.FindPropertyRelative("X");
                EditorGUI.PropertyField(drawRect, xRotationProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                SerializedProperty yRotationProperty = rotationProperty.FindPropertyRelative("Y");
                EditorGUI.PropertyField(drawRect, yRotationProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                SerializedProperty zRotationProperty = rotationProperty.FindPropertyRelative("Z");
                EditorGUI.PropertyField(drawRect, zRotationProperty, GUIContent.none);
                drawRect.x += drawRect.width;
                drawRect.width = 150f;
                EditorGUI.LabelField(drawRect, "Freeze Rotation X Y Z");
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            _forceConstraintReorderableList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}