﻿using UnityEngine;
using Meta.Physics;

namespace Meta
{
    [System.Serializable]
    public class ForceConstraint
    {
        [SerializeField]
        private MetaPhysicsAgent _agent = null;
        [SerializeField]
        private Bool3 _freezePosition = new Bool3();
        [SerializeField]
        private Bool3 _freezeRotation = new Bool3();

        public MetaPhysicsAgent Agent
        {
            get { return _agent; }
        }

        public Bool3 FreezePosition
        {
            get { return _freezePosition; }
        }

        public Bool3 FreezeRotation
        {
            get { return _freezeRotation; }
        }
    }
}