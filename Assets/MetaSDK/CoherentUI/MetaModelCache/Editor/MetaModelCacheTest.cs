﻿using UnityEngine;
using NUnit.Framework;
using System;
using System.IO;

namespace Meta.Tests
{
    [TestFixture]
    public class MetaModelCacheTest
    {
        [SetUp]
        public void SetUp()
        {
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = false;
            MetaModelCache.StartCache();
        }

        [TearDown]
        public void TearDown()
        {
            //We don't need to hard flush the cache since there are no actual models to destroy
            //MetaModelCache.HardFlushCache();
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = true;
        }

        [Test]
        public void TestAddModelToCache()
        {
            string name = "Test1";
            string url = "TestUrl1";
            int size = 1;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.ForceCleanCache();
            Assert.True(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        public void TestGettingModelNotInCache()
        {
            MetaModelCache.ForceCleanCache();
            Assert.False(MetaModelCache.IsModelInCache("ThisShouldFail") != null);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestSizeTooBig()
        {
            string name = "Test1";
            string url = "TestUrl1";
            int size = MetaModelCache.MAX_ITEM_SIZE + 1;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.ForceCleanCache();
            Assert.False(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestTotalSizeTooBigSingle()
        {
            string name = "Test1";
            string url = "TestUrl1";
            int size = MetaModelCache.MAX_TOTAL_CACHE_SIZE + 1;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.ForceCleanCache();
            Assert.False(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestTotalSizeTooBigMulti()
        {
            string name1 = "Test1";
            string url1 = "TestUrl1";
            int size1 = (MetaModelCache.MAX_TOTAL_CACHE_SIZE / 2) - 1;

            string name2 = "Test2";
            string url2 = "TestUrl2";
            int size2 = (MetaModelCache.MAX_TOTAL_CACHE_SIZE / 2) - 1;

            string name3 = "Test3";
            string url3 = "TestUrl3";
            int size3 = (MetaModelCache.MAX_TOTAL_CACHE_SIZE / 2) - 1;

            TimeSpan t = new TimeSpan(1, 1, 1);
            DateTime dt3 = DateTime.Now.Subtract(t);

            MetaModelCache.AddModelToCache(url1, name1, size1);
            MetaModelCache.AddModelToCache(url2, name2, size2);
            MetaModelCache.AddModelToCache(url3, name3, size3, dt3);
            MetaModelCache.ForceCleanCache();
            Assert.True(MetaModelCache.IsModelInCache(url1) != null);
            Assert.True(MetaModelCache.IsModelInCache(url2) != null);
            Assert.False(MetaModelCache.IsModelInCache(url3) != null);
        }

        [Test]
        [ExpectedException(typeof(DuplicateWaitObjectException))]
        public void TestDuplicateModelCached()
        {
            string name = "Test1";
            string url = "Test1Url";
            int size = 0;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.AddModelToCache(url, name, size);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestCachetimeExpired()
        {
            string name = "Test1";
            string url = "Test1Url";
            int size = 0;

            int totalSeconds = MetaModelCache.MAX_ITEM_CACHE_DURATION_SECONDS + 10;
            int hours = Mathf.FloorToInt(totalSeconds / (60f * 60f));
            totalSeconds -= hours * 60 * 60;
            int minutes = Mathf.FloorToInt(totalSeconds / 60f);
            totalSeconds -= minutes * 60;
            int seconds = totalSeconds;
            TimeSpan t = new TimeSpan(hours, minutes, seconds);
            DateTime dt = DateTime.Now.Subtract(t);

            MetaModelCache.AddModelToCache(url, name, size, dt);
            MetaModelCache.ForceCleanCache();
            Assert.False(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        public void TestNotCleanableSize()
        {
            string name = "Test1";
            string url = "Test1Url";
            int size = MetaModelCache.MAX_ITEM_SIZE + 1;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.SetCleanable(url, false);
            MetaModelCache.ForceCleanCache();
            Assert.True(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        public void TestNotCleanableFlush()
        {
            string name = "Test1";
            string url = "Test1Url";
            int size = MetaModelCache.MAX_ITEM_SIZE + 1;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.SetCleanable(url, false);
            MetaModelCache.FlushCache();
            Assert.True(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestFlush()
        {
            string name = "Test1";
            string url = "Test1Url";
            int size = 0;

            MetaModelCache.AddModelToCache(url, name, size);
            MetaModelCache.FlushCache();
            Assert.False(MetaModelCache.IsModelInCache(url) != null);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TestHardFlush()
        {
            string name1 = "Test1";
            string url1 = "Test1Url";
            int size1 = 0;

            string name2 = "Test2";
            string url2 = "Test2Url";
            int size2 = 0;

            MetaModelCache.AddModelToCache(url1, name1, size1);
            MetaModelCache.AddModelToCache(url2, name2, size2);
            MetaModelCache.SetCleanable(url2, false);
            MetaModelCache.HardFlushCache();
            Assert.False(MetaModelCache.IsModelInCache(url1) != null);
            Assert.False(MetaModelCache.IsModelInCache(url2) != null);
        }

        [Test]
        public void TestRemoveModel()
        {
            string name1 = "Test1.obj";
            string url1 = "Test1Url";
            int size1 = MetaModelCache.MAX_ITEM_SIZE + 1;

            FileStream file = File.Create(MetaModelCache.LOCAL_DOWNLOAD_LOCATION + "\\" + name1);
            file.Close();
            MetaModelCache.AddModelToCache(url1, name1, size1);
            MetaModelCache.ForceCleanCache();
            Assert.False(MetaModelCache.IsModelInCache(url1) != null);
            Assert.False(File.Exists(MetaModelCache.LOCAL_DOWNLOAD_LOCATION + "\\" + name1));
        }

        [Test]
        public void TestLoadingConfigFile()
        {
            string prevCacheName = MetaModelCache.CACHE_FILENAME;
            MetaModelCache.CACHE_FILENAME = "_testConf";
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = true;
            MetaModelCache.StartCache();

            Assert.True(File.Exists(MetaModelCache.LOCAL_DOWNLOAD_LOCATION + "\\_testConf"));

            string name1 = "Test1";
            string url1 = "Test1url";
            int size1 = 0;
            
            //Check this model was really added to the cache
            MetaModelCache.AddModelToCache(url1, name1, size1);
            Assert.True(MetaModelCache.IsModelInCache(url1) != null);

            //Force a clean to save the cache
            MetaModelCache.ForceCleanCache();
            string name2 = "Test2";
            string url2 = "Test2url";
            int size2 = 0;

            //Disable saving temporarily to allow us to add a model to the cache without it saving in the cache
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = false;
            MetaModelCache.AddModelToCache(url2, name2, size2);
            //Check this model was really added to the cache
            Assert.True(MetaModelCache.IsModelInCache(url2) != null);
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = true;

            //Reload the cache by restarting it
            MetaModelCache.StartCache();
            
            //Check that Test1 is in the cache but Test2 isn't
            Assert.True(MetaModelCache.IsModelInCache(url1) != null);
            Assert.False(MetaModelCache.IsModelInCache(url2) != null);

            //Clean up after ourselves
            MetaModelCache.CACHE_PERSISTS_BETWEEN_SESSIONS = false;
            MetaModelCache.CACHE_FILENAME = prevCacheName;
            File.Delete(MetaModelCache.LOCAL_DOWNLOAD_LOCATION + "\\_testConf");
        }
    }
}
