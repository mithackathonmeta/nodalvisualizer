﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Meta
{
    public static class MetaModelCache
    {
        [Serializable]
        private class CachedModel : IComparable<CachedModel>
        {
            public string url;
            public DateTime timeDownloaded;
            public int byteSize;
            public string fileName;
            //Whether this model can be deleted from the cache
            public bool cleanable;

            //Create a new CachedModel
            public CachedModel(string u, string n, int s)
            {
                url = u;
                fileName = n;
                byteSize = s;
                timeDownloaded = DateTime.Now;
                cleanable = true;
            }

            //Create a new CachedModel with a custom time
            public CachedModel(string u, string n, int s, DateTime d)
            {
                url = u;
                fileName = n;
                byteSize = s;
                timeDownloaded = d;
                cleanable = true;
            }

            //Compare two CachedModels based on which was downloaded earlier
            public int CompareTo(CachedModel other)
            {
                if (other == null)
                {
                    return 1;
                }

                return timeDownloaded.CompareTo(other.timeDownloaded);
            }
        }

        #region config_replaceable_variables
        //The download location
        public static string LOCAL_DOWNLOAD_LOCATION = Application.dataPath + "\\.LocalDownloads";
        //The maximum size for any single item (in bytes)
        public static int MAX_ITEM_SIZE = 1000000;
        //The maximum size for the cache in total (if exceeded, deletes oldest first) (in bytes)
        public static int MAX_TOTAL_CACHE_SIZE = 20000000;
        //The maximum time to store all items in the cache (in seconds)
        private static TimeSpan MAX_ITEM_CACHE_DURATION;
        public static int MAX_ITEM_CACHE_DURATION_SECONDS = 345600;
        public static string CACHE_FILENAME = "_cinfo";
        public static bool CACHE_PERSISTS_BETWEEN_SESSIONS = true;
        #endregion

        #region cache_refresh_variables
        private static int CACHE_REFRESH_TIME_SECONDS = 600;
        private static TimeSpan CACHE_REFRESH_TIME;
        private static DateTime CACHE_PREVIOUS_REFRESH;
        #endregion

        private static List<CachedModel> cachedModels;

        /// <summary>
        /// Boot up the cache
        /// </summary>
        public static void StartCache()
        {
            ReloadCacheFromDisk();
                
            //Convert the seconds parameters to TimeSpan parameters
            int totalSeconds = MAX_ITEM_CACHE_DURATION_SECONDS;
            int hours = Mathf.FloorToInt(totalSeconds / (60f * 60f));
            totalSeconds -= hours * 60 * 60;
            int minutes = Mathf.FloorToInt(totalSeconds / 60f);
            totalSeconds -= minutes * 60;
            int seconds = totalSeconds;
            MAX_ITEM_CACHE_DURATION = new TimeSpan(hours, minutes, seconds);

            totalSeconds = CACHE_REFRESH_TIME_SECONDS;
            hours = Mathf.FloorToInt(totalSeconds / (60f * 60f));
            totalSeconds -= hours * 60 * 60;
            minutes = Mathf.FloorToInt(totalSeconds / 60f);
            totalSeconds -= minutes * 60;
            seconds = totalSeconds;
            CACHE_REFRESH_TIME = new TimeSpan(hours, minutes, seconds);

            CleanCache();
        }

        /// <summary>
        /// Add a downloaded model to the cache
        /// </summary>
        /// <param name="url">The URL of the downloaded model</param>
        /// <param name="name">The name of the downloaded model in the file system</param>
        /// <param name="size">The size of the downloaded model</param>
        public static void AddModelToCache(string url, string name, int size)
        {
            if(getModelByUrl(url) != null)
            {
                //The model is already cached.
                //Fire an exception
                throw new DuplicateWaitObjectException(url);
            }

            CachedModel newModel = new CachedModel(url, name, size);
            cachedModels.Add(newModel);
            cachedModels.Sort();

            SaveCache();
        }

        /// <summary>
        /// Add a downloaded model to the cache
        /// </summary>
        /// <param name="url">The URL of the downloaded model</param>
        /// <param name="name">The name of the downloaded model in the file system</param>
        /// <param name="size">The size of the downloaded model</param>
        /// <param name="time">The custom time of the downloaded model</param>
        public static void AddModelToCache(string url, string name, int size, DateTime time)
        {
            if (getModelByUrl(url) != null)
            {
                //The model is already cached.
                //Fire an exception
                throw new DuplicateWaitObjectException(url);
            }

            CachedModel newModel = new CachedModel(url, name, size, time);
            cachedModels.Add(newModel);
            cachedModels.Sort();

            SaveCache();
        }

        private static CachedModel getModelByUrl(string url)
        {
            foreach (CachedModel c in cachedModels)
            {
                if (c.url.Equals(url))
                {
                    return c;
                }
            }
            return null;
        }

        /// <summary>
        /// Checks if a model has already been downloaded. Also refresh the time of the model
        /// </summary>
        /// <param name="url">The url of the model</param>
        /// <returns>The full path to the model (if it exists)</returns>
        public static string IsModelInCache(string url)
        {
            if(DateTime.Now.Subtract(CACHE_REFRESH_TIME).CompareTo(CACHE_PREVIOUS_REFRESH) > 0)
            {
                CleanCache();
            }

            CachedModel match = getModelByUrl(url);

            if (match != null)
            {
                match.timeDownloaded = DateTime.Now;
                cachedModels.Sort();
                return getFullPath(match);
            }

            return null;
        }

        /// <summary>
        /// Force a clean of the cache
        /// </summary>
        public static void ForceCleanCache()
        {
            CleanCache();
        }

        /// <summary>
        /// Clean the cache, removing models that are past their use-by time
        /// </summary>
        private static void CleanCache()
        {
            CACHE_PREVIOUS_REFRESH = DateTime.Now;

            int sizeSoFar = 0;
            CachedModel[] modelArray = cachedModels.ToArray();

            //Iterate through the models from newest to oldest
            int i = modelArray.Length - 1;
            while(i >= 0)
            {
                bool toClean = false;
                CachedModel curr = modelArray[i];

                if (curr.cleanable)
                {
                    //Check time
                    if (DateTime.Now.Subtract(MAX_ITEM_CACHE_DURATION).CompareTo(curr.timeDownloaded) > 0)
                    {
                        toClean = true;
                    }

                    //Check size
                    if(curr.byteSize > MAX_ITEM_SIZE)
                    {
                        toClean = true;
                    }
                    
                    //Check totalSize
                    if(sizeSoFar + curr.byteSize > MAX_TOTAL_CACHE_SIZE)
                    {
                        toClean = true;
                    }
                }

                if (toClean)
                {
                    //Clean the model
                    DeleteCachedItem(curr);
                    cachedModels.Remove(curr);
                }
                else
                {
                    sizeSoFar += curr.byteSize;
                }
                i -= 1;
            }

            SaveCache();
        }

        /// <summary>
        /// Flush the entire cache. All models. GONE. As long as they're cleanable.
        /// </summary>
        public static void FlushCache()
        {
            CachedModel[] modelArray = cachedModels.ToArray();
            foreach(CachedModel cm in modelArray)
            {
                if (cm.cleanable)
                {
                    cachedModels.Remove(cm);
                    DeleteCachedItem(cm);
                }
            }

            SaveCache();
        }

        /// <summary>
        /// Flush the cache, IGNORES CLEANABLE. DONT USE THIS USUALLY.
        /// </summary>
        public static void HardFlushCache()
        {
            foreach(CachedModel cm in cachedModels)
            {
                DeleteCachedItem(cm);
            }

            cachedModels = new List<CachedModel>();

            SaveCache();
        }

        /// <summary>
        /// Set whether a model can be cleaned (useful for making sure we dont delete models that are currently in the scene
        /// </summary>
        /// <param name="url"></param>
        /// <param name="b"></param>
        public static void SetCleanable(string url, bool b)
        {
            CachedModel match = getModelByUrl(url);
            if(match != null)
            {
                match.cleanable = b;

                if (b)
                {
                    //This model has had cleanable enabled, refresh its cacheTime
                    match.timeDownloaded = DateTime.Now;
                    cachedModels.Sort();
                }
            }
            else
            {
                Debug.LogWarning("Model " + url + " was not found in the cache.");
            }
        }

        /// <summary>
        /// Delete a cached item (removing the model on disk)
        /// </summary>
        private static void DeleteCachedItem(CachedModel model)
        {
            if (!File.Exists(getFullPath(model)))
            {
                throw new FileNotFoundException("File " + getFullPath(model) + "not found");
            }
            File.Delete(getFullPath(model));
        }

        private static string getFullPath(CachedModel m)
        {
            return LOCAL_DOWNLOAD_LOCATION + "\\" + m.fileName;
        }

        private static void ReloadCacheFromDisk()
        {
            cachedModels = new List<CachedModel>();

            if (!Directory.Exists(LOCAL_DOWNLOAD_LOCATION))
            {
                //If the local downloads path doesn't exist
                Directory.CreateDirectory(LOCAL_DOWNLOAD_LOCATION);
            }

            if (CACHE_PERSISTS_BETWEEN_SESSIONS)
            {
                //Check if we have the cache data on file
                //If so, load it up
                //If not, remake the directory
                //Create a new, empty, cache

                //Reload the cache from the disk
                if (File.Exists(LOCAL_DOWNLOAD_LOCATION + "\\" + CACHE_FILENAME))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = File.Open(LOCAL_DOWNLOAD_LOCATION + "\\" + CACHE_FILENAME, FileMode.Open);
                    List<CachedModel> dataModels = (List<CachedModel>)bf.Deserialize(file);
                    file.Close();

                    foreach (CachedModel cm in dataModels)
                    {
                        cm.cleanable = true;
                        cachedModels.Add(cm);
                    }
                    cachedModels.Sort();
                }
            }
        }

        private static void SaveCache()
        {
            if (CACHE_PERSISTS_BETWEEN_SESSIONS)
            {
                //Save the cache to the file in the LOCAL_FILE_DOWNLOADS directory
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(LOCAL_DOWNLOAD_LOCATION + "\\" + CACHE_FILENAME);

                bf.Serialize(file, cachedModels);
                file.Close();
            }
        }
    }
}
