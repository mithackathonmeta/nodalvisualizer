﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    public class MetaModelCachedObject : MonoBehaviour {
        private string url;

        public void Register(string u)
        {
            url = u;
            MetaModelCache.SetCleanable(url, false);
        }

        void OnDestroy()
        {
            MetaModelCache.SetCleanable(url, true);
        }
    }
}
