﻿using Meta.HandInput;
using UnityEngine;

namespace Meta
{
    public class TwoHandZoom : MonoBehaviour
    {
        [SerializeField]
        private float _minZoom = 0.25f;

        [SerializeField]
        private float _currentZoomValue = 1.5f;

        [SerializeField]
        private float _maxZoom = 4;

        [SerializeField]
        private float _zoomScale = 10f;

        [SerializeField]
        private MetaBrowserView _view = null;

        private const float ZoomDamp = .15f;
        private HandFeature _leftHand;
        private HandFeature _rightHand;
        private float _initialDistance;
        private float _initialZoomValue;
        private float _zoomVelocity;

        private void Start()
        {
            if (_view != null)
            {
                _view.Listener.ReadyForBindings += Listener_ReadyForBindings;
            }
        }

        private void Listener_ReadyForBindings(int frameId, string path, bool isMainFrame)
        {
            _view.Zoom(_currentZoomValue);
        }

        private void Update()
        {
            if (_leftHand != null && _rightHand != null &&
                _leftHand.ParentHand.Data.center.z > 0f && _rightHand.ParentHand.Data.center.z > 0f)
            {
                Scale();
            }
        }

        public void ScaleStart(HandVolume twoHandVolume)
        {
            _leftHand = twoHandVolume.GetHand(HandType.LEFT);
            _rightHand = twoHandVolume.GetHand(HandType.RIGHT);
            _initialDistance = Vector3.Distance(_leftHand.transform.position, _rightHand.transform.position);
            _initialZoomValue = _currentZoomValue;
        }

        private void Scale()
        {
            float currentDistance = Vector3.Distance(_leftHand.transform.position, _rightHand.transform.position);
            float deltaDistance = (currentDistance - _initialDistance) * _zoomScale;
            _currentZoomValue = Mathf.SmoothDamp(
                _currentZoomValue,
                Mathf.Clamp(
                    _initialZoomValue + deltaDistance, 
                    _minZoom, 
                    _maxZoom),
                ref _zoomVelocity, 
                ZoomDamp);
            _view.Zoom(_currentZoomValue);
        }

        public void ScaleEnd(HandVolume twoHandVolume)
        {
            _leftHand = null;
            _rightHand = null;
        }
    }
}