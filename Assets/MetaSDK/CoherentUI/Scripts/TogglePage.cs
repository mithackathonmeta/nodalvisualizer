﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meta.Examples
{
    public class TogglePage : MonoBehaviour
    {
        [SerializeField]
        private MetaBrowserView _view = null;

        private readonly List<String> _pages = new List<String>() { "https://demoscrollandbutton.herokuapp.com/demo_01/click.html", "https://demoscrollandbutton.herokuapp.com/demo_01/click2.html" };
        private int _index = 0;

        //Hack for double clicking issue
        private float _lastToggle = 0;

        void Start()
        {
            if (_view != null)
            {
                _view.Listener.NavigateTo += Listener_NavigateTo;
            }
        }

        private void Listener_NavigateTo(string path)
        {
            int index = _pages.IndexOf(path);

            if (index != -1)
            {
                _index = index;
            }
        }

        public void Toggle()
        {
            if (Time.time - _lastToggle > 1)
            {
                _index = (_index + 1) % _pages.Count;

                loadPage(_index);

                _lastToggle = Time.time;
            }
        }

        private void loadPage(int index)
        {
            if (_view != null)
            {
                _view.Page = _pages[index];
            }
        }
    }
}