﻿using UnityEngine;
using UnityEditor;
using Meta;

[CustomEditor(typeof(MetaBrowserSystem))]
[CanEditMultipleObjects]
public class MetaBrowserSystemEditor : Editor
{
    private MetaBrowserSystem m_Target;

    private bool general = true;

    public void OnEnable()
    {
        m_Target = (MetaBrowserSystem)target;
    }

    public override void OnInspectorGUI()
    {
        GUI.changed = false;

        if (m_Target == null)
            return;
        this.DrawDefaultInspector();

        Undo.RecordObject(target, "Modify Meta Browser System");
        if (!Application.isPlaying)
        {
            general = EditorGUILayout.Foldout(general, "General");
            if (general)
            {
                m_Target.AllowCookies = EditorGUILayout.Toggle("Cookies", m_Target.AllowCookies);
                m_Target.CookiesResource = EditorGUILayout.TextField("Cookies file", m_Target.CookiesResource);
                m_Target.CachePath = EditorGUILayout.TextField("Cache path", m_Target.CachePath);
                m_Target.HTML5LocalStoragePath = EditorGUILayout.TextField("Local storage path", m_Target.HTML5LocalStoragePath);
                m_Target.ForceDisablePluginFullscreen = EditorGUILayout.Toggle("Disable fullscreen plugins", m_Target.ForceDisablePluginFullscreen);
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(m_Target);
            foreach (UnityEngine.Object t in targets)
            {
                MetaBrowserSystem mbs = (MetaBrowserSystem)t;
                if (mbs != m_Target)
                {
                    if (!Application.isPlaying)
                    {
                        if (general)
                        {
                            mbs.AllowCookies = m_Target.AllowCookies;
                            mbs.CookiesResource = m_Target.CookiesResource;
                            mbs.CachePath = m_Target.CachePath;
                            mbs.HTML5LocalStoragePath = m_Target.HTML5LocalStoragePath;
                            mbs.ForceDisablePluginFullscreen = m_Target.ForceDisablePluginFullscreen;
                        }
                    }
                    EditorUtility.SetDirty(mbs);
                }
            }
        }
    }
}
