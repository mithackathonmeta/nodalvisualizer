﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

public class CopyCoherent
{
    [PostProcessBuild]
    public static void Process(BuildTarget target, string pathToBuiltProject)
    {
        //Debug.Log(pathToBuiltProject);

        if (target == BuildTarget.StandaloneWindows || target == BuildTarget.StandaloneWindows64)
        {
            string directory = Path.GetDirectoryName(pathToBuiltProject);
            string buildName = Path.GetFileNameWithoutExtension(pathToBuiltProject);
            string dataDirectory = Path.Combine(Path.Combine(directory, buildName + "_Data"), "CoherentUI_Host");

            //DirectoryCopy(@"Assets/CoherentUI/Binaries/CoherentUI_Host", dataDirectory, true);
            DirectoryCopy(@"Assets/MetaSDK/CoherentUI/Binaries/CoherentUI_Host", dataDirectory, true);
        }
    }

    private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
    {
        //Debug.Log("Copying " + sourceDirName + " to " + destDirName);

        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);

        if (!dir.Exists)
        {
            Debug.LogError("Could not find source directory: " + sourceDirName);
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        DirectoryInfo[] dirs = dir.GetDirectories();
        // If the destination directory doesn't exist, create it.
        if (!Directory.Exists(destDirName))
        {
            Directory.CreateDirectory(destDirName);
        }

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string temppath = Path.Combine(destDirName, file.Name);
            file.CopyTo(temppath, false);
        }

        // If copying subdirectories, copy them and their contents to new location.
        if (copySubDirs)
        {
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }
    }
}
