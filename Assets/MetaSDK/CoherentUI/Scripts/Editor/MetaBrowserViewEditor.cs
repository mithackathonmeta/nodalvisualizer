﻿using UnityEngine;
using UnityEditor;
using Meta;

[CustomEditor(typeof(MetaBrowserView))]
[CanEditMultipleObjects]
public class MetaBrowserViewEditor : Editor
{
    private MetaBrowserView m_Target;

    private bool general = true;
    private bool rendering = false;
    private bool advancedRendering = false;
    private bool input = false;
    private bool scripting = false;

    public void OnEnable()
    {
        m_Target = (MetaBrowserView)target;
    }

    // When removed, remove Coherent component
    [ExecuteInEditMode]
    void OnDestroy()
    {
        foreach (GameObject go in Selection.gameObjects)
        {
            if (!MetaBrowserViewCheck(go))
            {
                DestroyCoherentView(go);
            }
        }
    }

    /// <summary>
    /// Check whether the MetaBrowserView script is attached to the specified GameObject.
    /// </summary>
    /// <param name="obj">The GameObject to check.</param>
    /// <returns>True if the script is present.</returns>
    internal bool MetaBrowserViewCheck(GameObject obj)
    {
        if (obj == null || obj.GetComponent<MetaBrowserView>() == null)
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Destroys the CoherentUIView script (if it exists) on the specified GameObject.
    /// </summary>
    /// <param name="obj">The GameObject to check.</param>
    internal void DestroyCoherentView(GameObject obj)
    {
        CoherentUIView cView = obj.GetComponent<CoherentUIView>();
        if (cView != null)
        {
            DestroyImmediate(cView);    // Must be immediate for Editor
        }
    }

    public override void OnInspectorGUI()
    {
        GUI.changed = false;

        if (m_Target == null)
            return;
        this.DrawDefaultInspector();

        Undo.RecordObject(target, "Modify Meta Browser View");
        general = EditorGUILayout.Foldout(general, "General");
        if (general)
        {
            m_Target.Page = EditorGUILayout.TextField("URL", m_Target.Page);
            m_Target.Width = EditorGUILayout.IntField("Width", m_Target.Width);
            m_Target.Height = EditorGUILayout.IntField("Height", m_Target.Height);
            if (!Application.isPlaying)
            {
                m_Target.IsTransparent = EditorGUILayout.Toggle("Transparent", m_Target.IsTransparent);
                m_Target.Context = (MetaBrowserSystem)EditorGUILayout.ObjectField("View Context", m_Target.Context, typeof(MetaBrowserSystem), true);
            }
        }

        rendering = EditorGUILayout.Foldout(rendering, "Rendering");
        if (rendering)
        {
            if (!Application.isPlaying)
            {
                m_Target.DrawAfterPostEffects = (MetaBrowserView.DrawOrder)EditorGUILayout.EnumPopup("Draw order", m_Target.DrawAfterPostEffects);
            }
            m_Target.FlipY = EditorGUILayout.Toggle("Flip Y", m_Target.FlipY);
            m_Target.Filtering = (MetaBrowserView.CoherentFilteringModes)EditorGUILayout.EnumPopup("Texture filtering", m_Target.Filtering);
            if (!Application.isPlaying)
            {
                m_Target.ForceSoftwareRendering = EditorGUILayout.Toggle("Software only rendering", m_Target.ForceSoftwareRendering);
                m_Target.UseCameraDimensions = EditorGUILayout.Toggle("Match camera size", m_Target.UseCameraDimensions);
            }
        }

        if (!Application.isPlaying)
        {
            advancedRendering = EditorGUILayout.Foldout(advancedRendering, "Advanced rendering");
            if (advancedRendering)
            {
                m_Target.IsOnDemand = EditorGUILayout.Toggle("On-demand", m_Target.IsOnDemand);
                m_Target.ControlTimeOnDemand = EditorGUILayout.Toggle("Timer override", m_Target.ControlTimeOnDemand);
                m_Target.TargetFramerate = EditorGUILayout.IntField("Max. frame-rate", m_Target.TargetFramerate);
                m_Target.IsIndependentOfZBuffer = EditorGUILayout.Toggle("Always on top", m_Target.IsIndependentOfZBuffer);
                m_Target.CorrectGamma = EditorGUILayout.Toggle("Compensate gamma", m_Target.CorrectGamma);
            }
        }

        input = EditorGUILayout.Foldout(input, "Input");
        if (input)
        {
            if (!Application.isPlaying)
            {
                m_Target.SupportClickThrough = EditorGUILayout.Toggle("Smart input", m_Target.SupportClickThrough);
                m_Target.ClickThroughAlphaThreshold = EditorGUILayout.FloatField("Smart input alpha", m_Target.ClickThroughAlphaThreshold);
                m_Target.ClickToFocus = EditorGUILayout.Toggle("Lockable focus", m_Target.ClickToFocus);
                m_Target.DisplayVirtualKeyboard = EditorGUILayout.Toggle("Show virtual keyboard", m_Target.DisplayVirtualKeyboard);
            }
            m_Target.EnableIME = EditorGUILayout.Toggle("Enable IME", m_Target.EnableIME);
        }

        scripting = EditorGUILayout.Foldout(scripting, "Scripting");
        if (scripting)
        {
            if (!Application.isPlaying)
            {
                //m_Target.InitialScript = EditorGUILayout.TextField("Pre-load script", m_Target.InitialScript);
                //m_Target.InterceptAllEvents = EditorGUILayout.Toggle("Auto UI messages", m_Target.InterceptAllEvents);
                m_Target.EnableBindingAttribute = EditorGUILayout.Toggle("Enable [CoherentMethodAttribute]", m_Target.EnableBindingAttribute);
            }
            m_Target.ShowJavaScriptDialogs = EditorGUILayout.Toggle("Show JS dialogs", m_Target.ShowJavaScriptDialogs);
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(m_Target);
            foreach (UnityEngine.Object t in targets)
            {
                MetaBrowserView mbv = (MetaBrowserView)t;
                if (mbv != m_Target)
                {
                    if (general)
                    {
                        mbv.Page = m_Target.Page;
                        mbv.Width = m_Target.Width;
                        mbv.Height = m_Target.Height;
                        if (!Application.isPlaying)
                        {
                            mbv.IsTransparent = m_Target.IsTransparent;
                            mbv.Context = m_Target.Context;
                        }
                    }
                    if (rendering)
                    {
                        if (!Application.isPlaying)
                        {
                            mbv.DrawAfterPostEffects = m_Target.DrawAfterPostEffects;
                        }
                        mbv.FlipY = m_Target.FlipY;
                        mbv.Filtering = m_Target.Filtering;
                        if (!Application.isPlaying)
                        {
                            mbv.ForceSoftwareRendering = m_Target.ForceSoftwareRendering;
                            mbv.UseCameraDimensions = m_Target.UseCameraDimensions;
                        }
                    }
                    if (advancedRendering)
                    {
                        if (!Application.isPlaying)
                        {
                            mbv.IsOnDemand = m_Target.IsOnDemand;
                            mbv.ControlTimeOnDemand = m_Target.ControlTimeOnDemand;
                            mbv.TargetFramerate = m_Target.TargetFramerate;
                            mbv.IsIndependentOfZBuffer = m_Target.IsIndependentOfZBuffer;
                            mbv.CorrectGamma = m_Target.CorrectGamma;
                        }
                    }
                    if (input)
                    {
                        if (!Application.isPlaying)
                        {
                            mbv.SupportClickThrough = m_Target.SupportClickThrough;
                            mbv.ClickThroughAlphaThreshold = m_Target.ClickThroughAlphaThreshold;
                            mbv.ClickToFocus = m_Target.ClickToFocus;
                            mbv.DisplayVirtualKeyboard = m_Target.DisplayVirtualKeyboard;
                        }
                    }
                    if (scripting)
                    {
                        mbv.EnableIME = m_Target.EnableIME;
                        if (!Application.isPlaying)
                        {
                            mbv.EnableBindingAttribute = m_Target.EnableBindingAttribute;
                        }
                        mbv.ShowJavaScriptDialogs = m_Target.ShowJavaScriptDialogs;
                    }

                    EditorUtility.SetDirty(mbv);
                }
            }
        }
    }
}
