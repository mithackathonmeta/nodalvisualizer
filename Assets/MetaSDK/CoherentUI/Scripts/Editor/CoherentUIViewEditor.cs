﻿using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(CoherentUIView))]
public class CoherentUIViewEditor : Editor {

    private CoherentUIView m_Target;

    // Hide inspector GUI
    public override void OnInspectorGUI()
    {
        m_Target = (CoherentUIView)target;
        // Hide CoherentUIView script
        if (m_Target.GetComponent<CoherentUIView>() != null)
        {
            m_Target.GetComponent<CoherentUIView>().hideFlags = HideFlags.HideInInspector;
            //m_Target.Page = EditorGUILayout.TextField("URL", m_Target.Page);
        }
    }
}