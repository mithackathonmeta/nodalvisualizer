﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Coherent.UI;
using Meta.Events;

namespace Meta.UI
{
    [AddComponentMenu("Meta/Browser/MetaBrowserInput")]
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(AudioSource))]
    public class MetaBrowserInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IScrollHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private MetaBrowserView _view = null;

        [SerializeField]
        private AudioClip _clickSound = null;

        //Amount under which a click will register onPointerUp.
        private float _clickMoveMagnitude = 1f;

        //Time under which a click will registeronPointerUp.
        private float _clickTime = .5f;

        private float _inertiaDecay = 3.5f;

        //ScrollCurve X is driven by accumulatedMagnitude, final scroll value is multiplied by result.
        private AnimationCurve _scrollCurve = new AnimationCurve();

        //ScrollCurve X is driven by accumulatedMagnitude, final inertia value is multiplied by result.
        private AnimationCurve _inertiaCurve = new AnimationCurve();

        private int _clickRadius = 300;
        private RectTransform _rectTransform;
        private Vector2 _lastPosition;
        private Vector2 _startPosition;
        private Vector2 _scrollDelta;
        private Vector2 _intertiaOnDown;
        private float _startTime;
        private float _accumulatedMagnitude;
        private PointerEventData _hoverData;
        private bool _isDown;
        private float _scrollScale = 0.01f;
        private float _scrollXThreshold = 0.1f;
        private float _scrollYThreshold = 0.1f;

        private void Awake()
        {
            _scrollCurve.AddKey(new Keyframe(0, 0, 0, 80 * Mathf.Deg2Rad));
            _scrollCurve.AddKey(new Keyframe(2, 1, 0, 0));

            _inertiaCurve.AddKey(new Keyframe(0, 0, 0, 0));
            _inertiaCurve.AddKey(new Keyframe(3, 1, 0, 0));
        }

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();

            //Turn off browser's automatic input functionality so that mouse input isn't received twice
            if (_view != null)
            {
                _view.ReceivesInput = false;
            }

            if (GetComponent<AudioSource>() == null)
            {
                gameObject.AddComponent<AudioSource>();
            }
        }

        private void Update()
        {
            //Provide pointer state to browser so that "hover" behavior occurs
            if (!_isDown && _hoverData != null)
            {
                MouseEventData mouseEventData = new MouseEventData();
                mouseEventData.Button = MouseEventData.MouseButton.ButtonNone;
                mouseEventData.Type = MouseEventData.EventType.MouseMove;
                mouseEventData.MouseModifiers = new EventMouseModifiersState();

                Camera eventCamera = EventSystemUtility.GetMetaHandEventDataPressEventCamera(_hoverData);

                Vector2 position = _view.GetCoordinates(_hoverData.position, _rectTransform, eventCamera);
                mouseEventData.X = (int)position.x;
                mouseEventData.Y = (int)position.y;

                if (_view.View != null)
                {
                    _view.View.MouseEvent(mouseEventData);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData is MetaHandEventData)
            {
                //Hands can provide intertial scrolling. Stop inertia when the user presses.
                StopAllCoroutines();

                eventData.useDragThreshold = false;
                Camera eventCamera = EventSystemUtility.GetMetaHandEventDataPressEventCamera(eventData);

                //Get the cursor coordinates relative to the browser
                _startPosition = _view.GetCoordinates(eventData.position, _rectTransform, eventCamera);
                _startTime = Time.time;

                _accumulatedMagnitude = 0f;
                _intertiaOnDown = _scrollDelta;
            }
            else
            {
                Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventData.enterEventCamera);
                _startPosition = position;

                MouseEventData mouseEventData = new MouseEventData();
                mouseEventData.Button = MouseEventData.MouseButton.ButtonLeft;
                mouseEventData.Type = MouseEventData.EventType.MouseDown;
                mouseEventData.MouseModifiers = new EventMouseModifiersState() { IsLeftButtonDown = true, IsMiddleButtonDown = false, IsRightButtonDown = false };

                mouseEventData.X = (int)position.x;
                mouseEventData.Y = (int)position.y;

                _view.View.MouseEvent(mouseEventData);

                EventSystem.current.SetSelectedGameObject(_view.gameObject);
            }

            _isDown = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData is MetaHandEventData)
            {
                eventData.useDragThreshold = true;

                //This logic goes that the person can always click if less than clickMoveMagnitude OR
                //they can click if they are less than a certain time but have not moved a larger amount (clickMoveMagnitude * 4)
                //This is so that someone may potentially have a lot of movement in their click, but can still do it if it was quick
                //enough. But if they scroll quickly then it will not click. But not if the the inertiaOnDown was large (i.e it was scrolling on start).
                if (_intertiaOnDown.sqrMagnitude < .001f &&
                    (_accumulatedMagnitude < _clickMoveMagnitude ||
                    (Time.time - _startTime < _clickTime && _accumulatedMagnitude < _clickMoveMagnitude * 4f && eventData.delta.sqrMagnitude < 2000f)))
                {
                    HandDownEvent(eventData);
                    HandUpEvent(eventData);
                }
            }
            else
            {
                MouseEventData mouseEventData = new MouseEventData();
                mouseEventData.Button = MouseEventData.MouseButton.ButtonLeft;
                mouseEventData.Type = MouseEventData.EventType.MouseUp;
                mouseEventData.MouseModifiers = new EventMouseModifiersState() { IsLeftButtonDown = true, IsMiddleButtonDown = false, IsRightButtonDown = false };

                Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventData.enterEventCamera);

                mouseEventData.X = (int)position.x;
                mouseEventData.Y = (int)position.y;

                _view.View.MouseEvent(mouseEventData);
            }

            _isDown = false;
        }

        private void HandDownEvent(PointerEventData eventData)
        {
            TouchEventData touchEventData = new TouchEventData();
            touchEventData.Id = (uint)eventData.pointerId;
            touchEventData.RadiusX = (uint)_clickRadius;
            touchEventData.RadiusY = (uint)_clickRadius;
            touchEventData.HasRadiusInformation = true;
            touchEventData.Type = TouchEventData.EventType.TouchDown;

            touchEventData.X = (int)_startPosition.x;
            touchEventData.Y = (int)_startPosition.y;

            _view.View.TouchEvent(touchEventData, 1);
        }

        private void HandUpEvent(PointerEventData eventData)
        {
            TouchEventData touchEventData = new TouchEventData();
            touchEventData.Id = (uint)eventData.pointerId;
            touchEventData.RadiusX = (uint)_clickRadius;
            touchEventData.RadiusY = (uint)_clickRadius;
            touchEventData.HasRadiusInformation = true;
            touchEventData.Type = TouchEventData.EventType.TouchUp;

            touchEventData.X = (int)_startPosition.x;
            touchEventData.Y = (int)_startPosition.y;

            _view.View.TouchEvent(touchEventData, 1);

            GetComponent<AudioSource>().PlayOneShot(_clickSound);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData is MetaHandEventData)
            {
                _view.ReceivesInput = false;
                _view.View.SetFocus();

                Camera eventCamera = EventSystemUtility.GetMetaHandEventDataPressEventCamera(eventData);
                Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventCamera);

                _lastPosition = position;

                EventSystem.current.SetSelectedGameObject(_view.gameObject);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData is MetaHandEventData)
            {
                Camera eventCamera = EventSystemUtility.GetMetaHandEventDataPressEventCamera(eventData);
                Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventCamera);
                _scrollDelta = position - _lastPosition;

                _accumulatedMagnitude += _scrollDelta.sqrMagnitude / 1000f;

                if (Mathf.Abs(_scrollDelta.x) > _scrollXThreshold)
                {
                    _scrollDelta.x *= _scrollScale;

                    MouseEventData mouseEventData = GenerateScrollEvent(_scrollDelta.x, 0, position.x, position.y);
                    _view.View.MouseEvent(mouseEventData);

                    _lastPosition.x = position.x;
                }

                if (Mathf.Abs(_scrollDelta.y) > _scrollYThreshold)
                {
                    _scrollDelta.y *= _scrollScale;

                    MouseEventData mouseEventData = GenerateScrollEvent(0, _scrollDelta.y, position.x, position.y);
                    _view.View.MouseEvent(mouseEventData);

                    _lastPosition.y = position.y;
                }
            }
            else
            {
                _view.ReceivesInput = false;
                _view.View.SetFocus();

                MouseEventData mouseEventData = new MouseEventData();
                mouseEventData.Button = MouseEventData.MouseButton.ButtonLeft;
                mouseEventData.Type = MouseEventData.EventType.MouseMove;
                mouseEventData.MouseModifiers = new EventMouseModifiersState() { IsLeftButtonDown = true, IsMiddleButtonDown = false, IsRightButtonDown = false }; ;

                //Get the correct coordinates
                Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventData.enterEventCamera);
                mouseEventData.X = (int)position.x;
                mouseEventData.Y = (int)position.y;

                _view.View.MouseEvent(mouseEventData);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData is MetaHandEventData)
            {
                _scrollDelta *= _inertiaCurve.Evaluate(_accumulatedMagnitude);

                StartCoroutine(ProvideInertia());
            }
        }

        public void OnScroll(PointerEventData eventData)
        {
            Camera eventCamera = EventSystemUtility.GetMetaHandEventDataPressEventCamera(eventData);
            Vector2 position = _view.GetCoordinates(eventData.position, _rectTransform, eventCamera);
            MouseEventData mouseEventData = GenerateScrollEvent(eventData.scrollDelta.x, 0, position.x, position.y);

            _view.View.MouseEvent(mouseEventData);

            mouseEventData = GenerateScrollEvent(0, eventData.scrollDelta.y, position.x, position.y);

            _view.View.MouseEvent(mouseEventData);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _hoverData = eventData;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _hoverData = null;
        }

        private static MouseEventData GenerateScrollEvent(float xChange, float yChange)
        {
            MouseEventData mouseEventData = new MouseEventData();
            mouseEventData.Type = MouseEventData.EventType.MouseWheel;
            mouseEventData.WheelX = xChange;
            mouseEventData.WheelY = yChange;

            return mouseEventData;
        }

        private static MouseEventData GenerateScrollEvent(float xChange, float yChange, float xPosition, float yPosition)
        {
            MouseEventData mouseEventData = new MouseEventData();
            mouseEventData.Type = MouseEventData.EventType.MouseWheel;
            mouseEventData.WheelX = xChange;
            mouseEventData.WheelY = yChange;
            mouseEventData.X = (int)xPosition;
            mouseEventData.Y = (int)yPosition;

            return mouseEventData;
        }

        private IEnumerator ProvideInertia()
        {
            if (_view != null && _view.View != null)
            {
                Vector2 threshold = 2 * _scrollScale * new Vector2(_scrollXThreshold, _scrollYThreshold);

                bool scrollX = Mathf.Abs(_scrollDelta.x) > threshold.x;
                bool scrollY = Mathf.Abs(_scrollDelta.y) > threshold.y;

                while (_scrollDelta.sqrMagnitude > Mathf.Epsilon)
                {
                    _scrollDelta = Vector2.Lerp(_scrollDelta, Vector2.zero, _inertiaDecay * Time.deltaTime);

                    if (scrollX)
                    {
                        MouseEventData mouseEventData = GenerateScrollEvent(_scrollDelta.x, 0);
                        _view.View.MouseEvent(mouseEventData);
                    }

                    if (scrollY)
                    {
                        MouseEventData mouseEventData = GenerateScrollEvent(0, _scrollDelta.y);
                        _view.View.MouseEvent(mouseEventData);
                    }

                    yield return null;
                }
            }
        }
    }
}