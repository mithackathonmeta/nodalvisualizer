﻿namespace Meta
{
    internal class MetaViewListener
    {

        private MetaBrowserView m_View = null;

        //private TextAsset coherentJS = null;

        public MetaViewListener(MetaBrowserView mbv)
        {
            m_View = mbv;

            m_View.Listener.ReadyForBindings += HandleReadyForBindings;
            m_View.Listener.StartLoading += HandleStartLoad;

            //coherentJS = Resources.Load("CoherentResources/coherent") as TextAsset;
        }

        public void RemoveEvents()
        {
            if (m_View != null)
            {
                m_View.Listener.ReadyForBindings -= HandleReadyForBindings;
                m_View.Listener.StartLoading -= HandleStartLoad;
            }
        }

        private void HandleStartLoad()
        {
            //Automatically add the CoherentJS script to all web pages loaded
            //m_View.View.ExecuteScript(coherentJS.text);
        }

        void HandleReadyForBindings(int frameId, string path, bool isMainFrame)
        {
            if (isMainFrame)
            {
                m_View.View.BindCall("LoadMetaModel", (System.Action<string>)this.LoadMetaModel);
                m_View.Listener.ReadyForBindings -= HandleReadyForBindings;
            }
        }

        //Model loading code
        private void LoadMetaModel(string s)
        {
            //Initiate a model load request
            //Debug.Log("loading model from " + s);
            m_View.View.DownloadUrl(s);
        }
    }
}