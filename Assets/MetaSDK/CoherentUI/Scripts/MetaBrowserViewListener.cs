﻿using UnityEngine;
using System.Collections;
using Coherent.UI;
using System;

namespace Meta
{
    /// <summary>
    /// Wrapper class for UnityViewListener.
    /// </summary>
    public class MetaBrowserViewListener : MonoBehaviour
    {
        internal UnityViewListener m_Listener;

        /// <summary>
        /// Fired when a WebAudio stream is closed. This is usually called when the page is unloading.
        /// </summary>
        /// <remarks>Signature: OnAudioStreamClose(int streamId);</remarks>
        public event BrowserViewListener.CoherentUI_OnAudioStreamClose AudioStreamClose
        {
            add
            {
                m_Listener.AudioStreamClose += value;
            }
            remove
            {
                m_Listener.AudioStreamClose -= value;
            }
        }
        /// <summary>
        /// Fired when a new WebAudio stream is created. This is usually when the page is loading (for <audio> HTML tags).
        /// </summary>
        /// <remarks>Signature: OnAudioStreamCreated(int streamId, int channels, int bitDepth, int frequency);</remarks>
        public event BrowserViewListener.CoherentUI_OnAudioStreamCreated AudioStreamCreated
        {
            add
            {
                m_Listener.AudioStreamCreated += value;
            }
            remove
            {
                m_Listener.AudioStreamCreated -= value;
            }
        }
        /// <summary>
        /// Fired when a WebAudio stream is stopped.
        /// </summary>
        /// <remarks>Signature: OnAudioStreamPause(int streamId);</remarks>
        public event BrowserViewListener.CoherentUI_OnAudioStreamPause AudioStreamPause
        {
            add
            {
                m_Listener.AudioStreamPause += value;
            }
            remove
            {
                m_Listener.AudioStreamPause -= value;
            }
        }
        /// <summary>
        /// Fired when a WebAudio stream is played.
        /// </summary>
        /// <remarks>Signature: OnAudioStreamPlay(int streamId);</remarks>
        public event BrowserViewListener.CoherentUI_OnAudioStreamPlay AudioStreamPlay
        {
            add
            {
                m_Listener.AudioStreamPlay += value;
            }
            remove
            {
                m_Listener.AudioStreamPlay -= value;
            }
        }
        /// <summary>
        /// Fired when the bindings for frame are released.
        /// </summary>
        /// <remarks>Signature: OnBindingsReleased(int frameId, string path, bool isMainFrame);</remarks>
        public event BrowserViewListener.CoherentUI_OnBindingsReleased BindingsReleased
        {
            add
            {
                m_Listener.BindingsReleased += value;
            }
            remove
            {
                m_Listener.BindingsReleased -= value;
            }
        }
        /// <summary>
        /// Fired by the UI when there is no registered handler for this event.
        /// </summary>
        /// <remarks>Signature: OnCallback(string eventName, CallbackArguments arguments);</remarks>
        public event BrowserViewListener.CoherentUI_OnCallback Callback
        {
            add
            {
                m_Listener.Callback += value;
            }
            remove
            {
                m_Listener.Callback -= value;
            }
        }
        /// <summary>
        /// Fired when the caret changes during IME composition. You can use this method to 
        /// correctly position a custom IME control & candidate list.
        /// </summary>
        /// <remarks>Signature: OnCaretRectChanged(uint x, uint y, uint width, uint height);</remarks>
        public event BrowserViewListener.CoherentUI_OnCaretRectChanged CaretRectChanged
        {
            add
            {
                m_Listener.CaretRectChanged += value;
            }
            remove
            {
                m_Listener.CaretRectChanged -= value;
            }
        }
        /// <summary>
        /// Fired when there is an error with the certificate of a particular URL. the certificate 
        /// and response pointers are valid only for the duration of this call
        /// </summary>
        /// <remarks>Signature: OnCertificateError(string url, CertificateStatus status, Certificate certificate, CertificateErrorResponse response);</remarks>
        public event BrowserViewListener.CoherentUI_OnCertificateError CertificateError
        {
            add
            {
                m_Listener.CertificateError += value;
            }
            remove
            {
                m_Listener.CertificateError -= value;
            }
        }
        /// <summary>
        /// Fired when the client is requesting a certificate.
        /// </summary>
        /// <remarks>Signature: OnClientCertificateRequested(string url, ClientCertificateResponse response);</remarks>
        public event BrowserViewListener.CoherentUI_OnClientCertificateRequested ClientCertificateRequested
        {
            add
            {
                m_Listener.ClientCertificateRequested += value;
            }
            remove
            {
                m_Listener.ClientCertificateRequested -= value;
            }
        }
        /// <summary>
        /// Called when the cursor has changed internally in the View.
        /// </summary>
        /// <remarks>Signature: OnCursorChanged(CursorTypes cursor);</remarks>
        public event BrowserViewListener.CoherentUI_OnCursorChanged CursorChanged
        {
            add
            {
                m_Listener.CursorChanged += value;
            }
            remove
            {
                m_Listener.CursorChanged -= value;
            }
        }
        /// <summary>
        /// Fired when a new surface has been drawn and is ready to use by the client.
        /// </summary>
        /// <remarks>Signature: OnDraw(CoherentHandle handle, bool usesSharedMemory, int width, int height);</remarks>
        public event BrowserViewListener.CoherentUI_OnDraw Draw
        {
            add
            {
                m_Listener.Draw += value;
            }
            remove
            {
                m_Listener.Draw -= value;
            }
        }
        /// <summary>
        /// Fired when an error occurs for this specific View.
        /// </summary>
        /// <remarks>Signature: OnError(ViewError error);</remarks>
        public event BrowserViewListener.CoherentUI_OnError Error
        {
            add
            {
                m_Listener.Error += value;
            }
            remove
            {
                m_Listener.Error -= value;
            }
        }
        /// <summary>
        /// Fired when a frame has been failed loading.
        /// </summary>
        /// <remarks>Signature: OnFailLoad(int frameId, string validatedPath, bool isMainFrame, string error);</remarks>
        public event BrowserViewListener.CoherentUI_OnFailLoad FailLoad
        {
            add
            {
                m_Listener.FailLoad += value;
            }
            remove
            {
                m_Listener.FailLoad -= value;
            }
        }
        /// <summary>
        /// Fired when the view requests file selection. It could be either single file, directory or multiple files.
        /// </summary>
        /// <remarks>Signature: OnFileSelectRequest(FileSelectRequest request);</remarks>
        public event BrowserViewListener.CoherentUI_OnFileSelectRequest FileSelectRequest
        {
            add
            {
                m_Listener.FileSelectRequest += value;
            }
            remove
            {
                m_Listener.FileSelectRequest -= value;
            }
        }
        /// <summary>
        /// Fired when a frame has been successfully loaded.
        /// </summary>
        /// <remarks>Signature: OnFinishLoad(int frameId, string validatedPath, bool isMainFrame, int statusCode, HTTPHeader[] headers);</remarks>
        public event BrowserViewListener.CoherentUI_OnFinishLoad FinishLoad
        {
            add
            {
                m_Listener.FinishLoad += value;
            }
            remove
            {
                m_Listener.FinishLoad -= value;
            }
        }
        /// <summary>
        /// Fired when a view requires authentication credentials.
        /// </summary>
        /// <remarks>Signature: OnGetAuthCredentials(bool isProxy, string host, uint port, string realm, string scheme);</remarks>
        public event BrowserViewListener.CoherentUI_OnGetAuthCredentials GetAuthCredentials
        {
            add
            {
                m_Listener.GetAuthCredentials += value;
            }
            remove
            {
                m_Listener.GetAuthCredentials -= value;
            }
        }
        /// <summary>
        /// Fired when the browser history is obtained.
        /// </summary>
        /// <remarks>Signature: OnHistoryObtained(string[] previous, string[] next);</remarks>
        internal event BrowserViewListener.CoherentUI_OnHistoryObtained HistoryObtained
        {
            add
            {
                m_Listener.HistoryObtained += value;
            }
            remove
            {
                m_Listener.HistoryObtained -= value;
            }
        }
        /// <summary>
        /// Fired when the user must cancel the IME composition due to an event in the View.
        /// </summary>
        /// <remarks>Signature: OnIMEShouldCancelComposition();</remarks>
        public event BrowserViewListener.CoherentUI_OnIMEShouldCancelComposition IMEShouldCancelComposition
        {
            add
            {
                m_Listener.IMEShouldCancelComposition += value;
            }
            remove
            {
                m_Listener.IMEShouldCancelComposition -= value;
            }
        }
        /// <summary>
        /// Fired when the view triggered a javascript message box, i.e. an alert, confirmation dialog or a prompt dialog.
        /// </summary>
        /// <remarks>Signature: OnJavaScriptMessage(string message, string defaultPrompt, string frameUrl, int messageType);</remarks>
        public event BrowserViewListener.CoherentUI_OnJavaScriptMessage JavaScriptMessage
        {
            add
            {
                m_Listener.JavaScriptMessage += value;
            }
            remove
            {
                m_Listener.JavaScriptMessage -= value;
            }
        }
        /// <summary>
        /// Fired when the view starts navigation to a new path.
        /// </summary>
        /// <remarks>Signature: OnNavigateTo(string path);</remarks>
        public event BrowserViewListener.CoherentUI_OnNavigateTo NavigateTo
        {
            add
            {
                m_Listener.NavigateTo += value;
            }
            remove
            {
                m_Listener.NavigateTo -= value;
            }
        }
        /// <summary>
        /// Fired when pdf data is needed.
        /// </summary>
        /// <remarks>OnPrintToPDFReady(IntPtr buffer, uint bufferSize);</remarks>
        public event BrowserViewListener.CoherentUI_OnPrintToPDFReady PrintToPDFReady
        {
            add
            {
                m_Listener.PrintToPDFReady += value;
            }
            remove
            {
                m_Listener.PrintToPDFReady -= value;
            }
        }
        /// <summary>
        /// Fired when a frame is ready for bindings.
        /// </summary>
        /// <remarks>Signature: OnReadyForBindings(int frameId, string path, bool isMainFrame);</remarks>
        public event BrowserViewListener.CoherentUI_OnReadyForBindings ReadyForBindings
        {
            add
            {
                m_Listener.ReadyForBindings += value;
            }
            remove
            {
                m_Listener.ReadyForBindings -= value;
            }
        }
        /// <summary>
        /// Fired when the view requests access to a media stream. Media streams are the audio capture 
        /// (microphone) and video capture (camera) devices on the system.
        /// </summary>
        /// <remarks>Signature: OnRequestMediaStream(MediaStreamRequest request);</remarks>
        public event BrowserViewListener.CoherentUI_OnRequestMediaStream RequestMediaStream
        {
            add
            {
                m_Listener.RequestMediaStream += value;
            }
            remove
            {
                m_Listener.RequestMediaStream -= value;
            }
        }
        /// <summary>
        /// Fired when a message is sent from a script running in this specific View.
        /// </summary>
        /// <remarks>Signature: OnScriptMessage(MessageLevel level, string message, string sourceId, int line);</remarks>
        public event BrowserViewListener.CoherentUI_OnScriptMessage ScriptMessage
        {
            add
            {
                m_Listener.ScriptMessage += value;
            }
            remove
            {
                m_Listener.ScriptMessage -= value;
            }
        }
        /// <summary>
        /// Fired when a new path has started loading.
        /// </summary>
        /// <remarks>Signature: OnStartLoading();</remarks>
        public event BrowserViewListener.CoherentUI_OnStartLoading StartLoading
        {
            add
            {
                m_Listener.StartLoading += value;
            }
            remove
            {
                m_Listener.StartLoading -= value;
            }
        }
        /// <summary>
        /// Fired when all load operations have completed.
        /// </summary>
        /// <remarks>Signature: OnStopLoading();</remarks>
        public event BrowserViewListener.CoherentUI_OnStopLoading StopLoading
        {
            add
            {
                m_Listener.StopLoading += value;
            }
            remove
            {
                m_Listener.StopLoading -= value;
            }
        }
        /// <summary>
        /// Fired when the current text input control changes (i.e. the user click an edit-box). Use this 
        /// method to decide when to allow for IME input. The method will be called ONLY if IME is activated 
        /// on this View.
        /// </summary>
        /// <remarks>Signature: OnTextInputTypeChanged(TextInputControlType type, bool canComposeInline);</remarks>
        public event BrowserViewListener.CoherentUI_OnTextInputTypeChanged TextInputTypeChanged
        {
            add
            {
                m_Listener.TextInputTypeChanged += value;
            }
            remove
            {
                m_Listener.TextInputTypeChanged -= value;
            }
        }
        /// <summary>
        /// Fired before an URL request is made. The default implementation allows all requests.
        /// </summary>
        /// <remarks>Signature: OnURLRequest(URLRequest request);</remarks>
        public event BrowserViewListener.CoherentUI_OnURLRequest URLRequest
        {
            add
            {
                m_Listener.URLRequest += value;
            }
            remove
            {
                m_Listener.URLRequest -= value;
            }
        }
        /// <summary>
        /// Fired when the requested View is created.
        /// </summary>
        /// <remarks>Signature: OnViewCreated(View view);</remarks>
        public event BrowserViewListener.CoherentUI_OnViewCreated ViewCreated
        {
            add
            {
                m_Listener.ViewCreated += value;
            }
            remove
            {
                m_Listener.ViewCreated -= value;
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}