﻿using UnityEngine;
using System.Collections.Generic;

namespace Meta
{
    /// <summary>
    /// Wrapper class for CoherentUISystem. Acts to define contexts for CoherentUIView.
    /// </summary>
    public class MetaBrowserSystem : MonoBehaviour
    {
        private static MetaBrowserSystem m_Instance = null;
        public static MetaBrowserSystem DefaultContext
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = MetaBrowserSystem.Create();
                    if (m_Instance == null)
                    {
                        throw new System.ApplicationException("Unable to create "
                            + "Meta Browser System");
                    }
                }
                return m_Instance;
            }
        }

        private CoherentUISystem _coherentSystem;
        /// <summary>
        /// Reference to a CoherentSystem ("Context")
        /// </summary>
        internal CoherentUISystem CoherentSystem
        {
            get
            {
                if (_coherentSystem != null)
                {
                    return _coherentSystem;
                }
                // If this is not the MetaBrowserSystem Default Context
                if (gameObject.name != "MetaBrowserSystemDC")
                {
                    // Custom context, need to create a new CoherentUISystem context
                    _coherentSystem = gameObject.GetComponent<CoherentUISystem>();
                    if (_coherentSystem == null)
                    {
                        _coherentSystem = gameObject.AddComponent<CoherentUISystem>();
                    }
                    return _coherentSystem;
                }
                else
                {
                    return CoherentUISystem.DefaultContext;
                }
            }
            set
            {
                _coherentSystem = value;
            }
        }

        private List<MetaBrowserView> m_Views = new List<MetaBrowserView>();
        /// <summary>
        /// List of MetaBrowserViews that are within this context
        /// </summary>
        internal List<MetaBrowserView> UIViews
        {
            get
            {
                return m_Views;
            }
        }

        internal void AddView(MetaBrowserView view)
        {
            m_Views.Add(view);
        }

        internal bool RemoveView(MetaBrowserView view)
        {
            return m_Views.Remove(view);
        }

        public static MetaBrowserSystem Create()
        {
            if (GameObject.Find("MetaBrowserSystemDC") != null)
            {
                Debug.LogWarning("Unable to create MetaBrowserSystemDC because a GameObject with the same name already exists!");
                return null;
            }

            var go = new GameObject("MetaBrowserSystemDC");
            MetaBrowserSystem system = go.AddComponent<MetaBrowserSystem>();
            return system;
        }

        /// <summary>
        /// enable proxy support for loading web pages
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool m_EnableProxy = false;
        private bool EnableProxy
        {
            get
            {
                return m_EnableProxy;
            }
            set
            {
                CoherentSystem.EnableProxy = value;
                m_EnableProxy = value;
            }
        }

        /// <summary>
        /// allow cookies
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool m_AllowCookies = true;
        public bool AllowCookies
        {
            get
            {
                return m_AllowCookies;
            }
            set
            {
                CoherentSystem.AllowCookies = value;
                m_AllowCookies = value;
            }
        }

        /// <summary>
        /// URL for storing persistent cookies
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private string m_CookiesResource = "cookies.dat";
        public string CookiesResource
        {
            get
            {
                return m_CookiesResource;
            }
            set
            {
                CoherentSystem.CookiesResource = value;
                m_CookiesResource = value;
            }
        }

        /// <summary>
        /// path for browser cache
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private string m_CachePath = "cui_cache";
        public string CachePath
        {
            get
            {
                return m_CachePath;
            }
            set
            {
                CoherentSystem.CachePath = value;
                m_CachePath = value;
            }
        }

        /// <summary>
        /// path for HTML5 localStorage
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private string m_HTML5LocalStoragePath = "cui_app_cache";
        public string HTML5LocalStoragePath
        {
            get
            {
                return m_HTML5LocalStoragePath;
            }
            set
            {
                CoherentSystem.HTML5LocalStoragePath = value;
                m_HTML5LocalStoragePath = value;
            }
        }

        /// <summary>
        /// disable fullscreen for plugins like Flash and Silverlight
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool m_ForceDisablePluginFullscreen = true;
        public bool ForceDisablePluginFullscreen
        {
            get
            {
                return m_ForceDisablePluginFullscreen;
            }
            set
            {
                CoherentSystem.ForceDisablePluginFullscreen = value;
                m_ForceDisablePluginFullscreen = value;
            }
        }

        /// <summary>
        /// disable web security like cross-domain checks
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private bool m_DisableWebSecurity = false;
        private bool DisableWebSecurity
        {
            get
            {
                return m_DisableWebSecurity;
            }
            set
            {
                CoherentSystem.DisableWebSecurity = value;
                m_DisableWebSecurity = value;
            }
        }

        /// <summary>
        /// port for debugging Views, -1 to disable
        /// </summary>
        [HideInInspector]
        [SerializeField]
        private int m_DebuggerPort = -1;
        public int DebuggerPort
        {
            get
            {
                return m_DebuggerPort;
            }
            set
            {
                CoherentSystem.DebuggerPort = value;
                m_DebuggerPort = value;
            }
        }

        /*/// <summary>
        /// The main camera. Used for obtaining mouse position over the HUD and raycasting in the world.
        /// </summary>
        public Camera m_MainCamera = null;

        [HideInInspector]
        public bool DeviceSupportsSharedTextures = false;*/

        [HideInInspector]
        [SerializeField]
        private bool m_UseURLCache = true;
        /// <summary>
        /// Sets if the system should use the URL Cache. NOTE: This should almost always be enabled.
        /// Disable it if you already set the URL cache yourself for the app
        /// </summary>
        /// <value>
        /// If to set the cache
        /// </value>
        private bool UseURLCache
        {
            get
            {
                return m_UseURLCache;
            }
            set
            {
                CoherentSystem.UseURLCache = value;
                m_UseURLCache = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_MemoryCacheSize = 4 * 1024 * 1024;
        /// <summary>
        /// Sets the in-memory size of the URL cache
        /// </summary>
        /// <value>
        /// The maximum size of the in-memory cache
        /// </value>
        private int MemoryCacheSize
        {
            get
            {
                return m_MemoryCacheSize;
            }
            set
            {
                CoherentSystem.MemoryCacheSize = value;
                m_MemoryCacheSize = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_DiskCacheSize = 32 * 1024 * 1024;
        /// <summary>
        /// Sets the on-disk size of the URL cache
        /// </summary>
        /// <value>
        /// The maximum size of the on-disk cache
        /// </value>
        private int DiskCacheSize
        {
            get
            {
                return m_DiskCacheSize;
            }
            set
            {
                CoherentSystem.DiskCacheSize = value;
                m_DiskCacheSize = value;
            }
        }

        /// <summary>
        /// Determines whether this instance is ready.
        /// </summary>
        /// <returns>
        /// <c>true</c> if this instance is ready; otherwise, <c>false</c>.
        /// </returns>
        public bool IsReady()
        {
            return CoherentSystem.IsReady();
        }

        /// <summary>
        /// Determines whether there is an focused Click-to-focus view
        /// </summary>
        /// <value>
        /// <c>true</c> if there is an focused Click-to-focus view; otherwise, <c>false</c>.
        /// </value>
        public bool HasFocusedView
        {
            get { return CoherentSystem.HasFocusedView; }
        }

        /// <summary>
        /// Occurs when a Click-to-focus view gains or loses focus
        /// </summary>
        /*public event CoherentUISystem.OnViewFocusedDelegate OnViewFocused
        {
            add
            {
                CoherentSystem.OnViewFocused += value;
            }
            remove
            {
                CoherentSystem.OnViewFocused -= value;
            }
        }*/

        void Awake()
        {
            
        }

        // Use this for initialization
        void Start()
        {
            CoherentSystem.enabled = true;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}