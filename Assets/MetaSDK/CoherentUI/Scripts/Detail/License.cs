/*
This file is part of Coherent UI, modern user interface library for
games. Release $RELEASE$. Build $VERSION$ for $LICENSEE$.

Copyright (c) 2012-2015 Coherent Labs AD and/or its licensors. All
rights reserved in all media.

The coded instructions, statements, computer programs, and/or related
material (collectively the "Data") in these files contain confidential
and unpublished information proprietary Coherent Labs and/or its
licensors, which is protected by United States of America federal
copyright law and by international treaties.

This software or source code is supplied under the terms of a license
agreement and nondisclosure agreement with Coherent Labs Limited and may
not be copied, disclosed, or exploited except in accordance with the
terms of that agreement. The Data may not be disclosed or distributed to
third parties, in whole or in part, without the prior written consent of
Coherent Labs Limited.

COHERENT LABS MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS
SOURCE CODE FOR ANY PURPOSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER, ITS AFFILIATES,
PARENT COMPANIES, LICENSORS, SUPPLIERS, OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OR PERFORMANCE OF THIS SOFTWARE OR SOURCE CODE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Coherent.UI
{
static class License
{
    public const string COHERENT_KEY = "AAEAAFSHC4mHJpUJk5fv86B5lLOembV/lIjDrYlL+NRgrR/Pq1hJbwBd3aPBtdp0JH1+1JjJqsy2JwAmQvmmqQhLJsBE2oaZcLdfoFYcrSD31/1xwXFSQOwjijnJfrps4pPjB/K0ntfWSwgflj+OydZPlHWObACIgrtxiSzMgiveIAY5C2bue3hdzDGV5vgRYh0Z68ljIiATE49sJthWZ1kaNBRPWhinxOEwgZS+Psx0gfYVPxLh8DMDo6KWHoMbqu2d4MO5KWQ0V4WGQuxxH+yotqXmWT+FnwkZgeazvaPoUSs/nMV0PqD0gVkOuUcian9VxJ121mBnFE+o5Vuoic3RXDIJW/8Cg53vWFZfsR7PHYepo+NJPTofTRXrC8kNATAQIx4oayVQ0JVKEPKyIu/EqZEi9aVMXqiImdIUn+mDkpd+OV8AeuU7if/o2qlz9eS45UdqVm8zNBlMS81i4jENGGhA1VxAT8ikdrFXa0k/FikJx1xgVqxBhaZOjsGgwSP/g3KitPbItIltp2HeIPMLZ++gfZEGbgLuq7SQh3aCpXwM7xouV/V0hRH0Eua8X1RPc5CqWMeouMayYhFv+IU7C4w=";
}
}
