#if UNITY_STANDALONE || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
#define COHERENT_UNITY_STANDALONE
#endif

#if UNITY_NACL || UNITY_WEBPLAYER
#define COHERENT_UNITY_UNSUPPORTED_PLATFORM
#endif

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE || COHERENT_UNITY_UNSUPPORTED_PLATFORM
using System;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine;
using Meta;

namespace Coherent.UI
#elif UNITY_IPHONE || UNITY_ANDROID
namespace Coherent.UI.Mobile
#endif
{
	public class SystemListener : ContextListener
	{
        private static string LOCAL_DOWNLOAD_FILE_PATH = MetaModelCache.LOCAL_DOWNLOAD_LOCATION;

        public SystemListener(System.Action onSystemReady)
		{
			m_OnSystemReady = onSystemReady;
            MetaModelCache.StartCache();
        }
		
		public override void ContextReady()
		{
			IsReady = true;
			m_OnSystemReady();
		}
	
		public override void OnError(ContextError arg0)
		{
			UnityEngine.Debug.Log(string.Format("An error occured! {0} (#{1})", arg0.Error, arg0.ErrorCode));
		}
	
		public bool IsReady
		{
			get;
			private set;
		}

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
		public override bool OnDownloadStarted (Download downloadItem)
		{
            UnityEngine.Debug.Log("Download started");

            //Check if this is a model
            if (downloadItem.GetURL().EndsWith(".obj"))
            {
                //If so, check if we have already downloaded it
                string path = MetaModelCache.IsModelInCache(downloadItem.GetURL());
                if(path != null)
                {
                    //If so, simply import it and cancel the download
                    ObjectImporter.Import(path);
                    return false;
                }
            }

            return true;
		}

        public override void OnDownloadComplete(Download downloadItem, IntPtr data, uint size)
        {
            Debug.Log(downloadItem.GetFilename());
            //base.OnDownloadComplete(downloadItem, data, size);


            byte[] bytes = new byte[size];
            try
            {
                int sizeInt = Convert.ToInt32(size);
                Marshal.Copy(data, bytes, 0, sizeInt);
                string path = LOCAL_DOWNLOAD_FILE_PATH + "\\" + downloadItem.GetFilename();
                System.IO.File.WriteAllBytes(path, bytes);
                Debug.Log("File Written");

                //Code to initiate loading of .obj files
                if (downloadItem.GetFilename().EndsWith(".obj"))
                {
                    //This file is a .obj, load it appropriately
                    Debug.Log("File is .obj. Initiating load");
                    GameObject g = ObjectImporter.Import(path);
                    MetaModelCachedObject gMMCO = g.AddComponent<MetaModelCachedObject>();
                    gMMCO.Register(downloadItem.GetURL());

                    //Add the model to the cache
                    MetaModelCache.AddModelToCache(downloadItem.GetURL(), downloadItem.GetFilename(), sizeInt);
                }

                downloadItem.Destroy();
            }
            catch (OverflowException e)
            {
                throw new OverflowException("Size of data is greater than maximum allowed value " + e.StackTrace);
            }

        }
#endif

#if !UNITY_EDITOR && (UNITY_ANDROID)
		public static string GetTouchScreenKbdInitialText()
		{
			return new string('-', 64);
		}

		public override void OnSoftwareKeyboardVisibilityChanged(bool visible)
		{
			UnityEngine.TouchScreenKeyboard kbd = null;

			bool isHidden = (!UnityEngine.TouchScreenKeyboard.visible)
			|| (CoherentUISystem.DefaultContext.TouchscreenKeyboard == null);

			if (visible)
			{
				if (!isHidden)
				{
					return;
				}

				UnityEngine.TouchScreenKeyboard.hideInput = true;
				string initialString = GetTouchScreenKbdInitialText();
				kbd = UnityEngine.TouchScreenKeyboard.Open(initialString);

				if (kbd != null && kbd.text.Length != initialString.Length)
				{
					kbd.text = initialString;
				}
			}
			CoherentUISystem.DefaultContext.TouchscreenKeyboard = kbd;
		}
#endif

        private System.Action m_OnSystemReady;
	}
}
