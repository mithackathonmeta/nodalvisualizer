﻿#define COHERENT_UI_PRO_UNITY3D
#if UNITY_STANDALONE || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
#define COHERENT_UNITY_STANDALONE
#endif

#if UNITY_2_6 || UNITY_2_6_1 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5
#define COHERENT_UNITY_PRE_4_0
#endif

#if UNITY_NACL || UNITY_WEBPLAYER
#define COHERENT_UNITY_UNSUPPORTED_PLATFORM
#endif

#if UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
#define COHERENT_SIMULATE_MOBILE_IN_EDITOR
#endif

using UnityEngine;
using System.Collections;
using Coherent.UI;

namespace Meta
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Wrapper class for CoherentUIView.
    /// </summary>
    [RequireComponent(typeof(CoherentUIView))]
    [DisallowMultipleComponent]
    public class MetaBrowserView : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField]
        private CoherentUIView _coherentView;
        internal CoherentUIView CoherentView
        {
            get
            {
                if (_coherentView == null)
                {
                    _coherentView = gameObject.GetComponent<CoherentUIView>();
                    if (_coherentView == null)
                    {
                        _coherentView = gameObject.AddComponent<CoherentUIView>();

                    }
                }
                return _coherentView;
            }
            set
            {
                _coherentView = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private string m_Page = "http://www.metavision.com";
        /// <summary>
        /// Gets or sets the URL of the view
        /// </summary>
        /// <value>
        /// The loaded URL of view
        /// </value>
        public string Page
        {
            get
            {
                if (_coherentView != null)
                {
                    m_Page = _coherentView.Page;
                }
                return m_Page;
            }
            set
            {
                CoherentView.Page = value;
                m_Page = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_Width = 1024;
        /// <summary>
        /// Gets or sets the width of the view.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width
        {
            get
            {
                if (_coherentView != null)
                {
                    m_Width = _coherentView.Width;
                }
                return m_Width;
            }
            set
            {
                CoherentView.Width = value;
                m_Width = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_Height = 512;
        /// <summary>
        /// Gets or sets the height of the view.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height
        {
            get
            {
                if (_coherentView != null)
                {
                    m_Height = _coherentView.Height;
                }
                return m_Height;
            }
            set
            {
                CoherentView.Height = value;
                m_Height = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_XPos = 0;
        /// <summary>
        /// Gets or sets the X position of the overlay view.
        /// </summary>
        /// <value>
        /// The X position.
        /// </value>
        private int XPos
        {
            get
            {
                if (_coherentView != null)
                {
                    m_XPos = _coherentView.XPos;
                }
                return m_XPos;
            }
            set
            {
                CoherentView.XPos = value;
                m_XPos = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_YPos = 0;
        /// <summary>
        /// Gets or sets the Y position of the overlay view.
        /// </summary>
        /// <value>
        /// The Y position.
        /// </value>
        private int YPos
        {
            get
            {
                if (_coherentView != null)
                {
                    m_YPos = _coherentView.YPos;
                }
                return m_YPos;
            }
            set
            {
                CoherentView.YPos = value;
                m_YPos = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_ScaleToFit = false;
        /// <summary>
        /// Gets or sets if the view will scale it's content to fit it's size.
        /// </summary>
        /// <value>
        /// The scale-to-fit property.
        /// </value>
        private bool ScaleToFit
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ScaleToFit = _coherentView.ScaleToFit;
                }
                return m_ScaleToFit;
            }
            set
            {
                CoherentView.ScaleToFit = value;
                m_ScaleToFit = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_EnableWebGLSupport = false;
        /// <summary>
        /// If enabled, WebGL will be supported in the view.
        /// </summary>
        /// <value>
        /// The EnableWebGLSupport property.
        /// </value>
        private bool EnableWebGLSupport
        {
            get
            {
                if (_coherentView != null)
                {
                    m_EnableWebGLSupport = _coherentView.EnableWebGLSupport;
                }
                return m_EnableWebGLSupport;
            }
            set
            {
                CoherentView.EnableWebGLSupport = value;
                m_EnableWebGLSupport = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private string m_InitialScript;
        /// <summary>
        /// Gets or sets the initial JavaScript code to be executed when the view JavaScript engine is created.
        /// </summary>
        /// <value>
        /// The script.
        /// </value>
        public string InitialScript
        {
            get
            {
                if (_coherentView != null)
                {
                    m_InitialScript = _coherentView.InitialScript;
                }
                return m_InitialScript;
            }
            set
            {
                CoherentView.InitialScript = value;
                m_InitialScript = value;
            }
        }

        public enum CoherentViewInputState
        {
            TakeAll = 0,
            TakeNone,
            Transparent
        }
        [HideInInspector]
        [SerializeField]
        private CoherentViewInputState m_ViewInputState = CoherentViewInputState.Transparent;
        /// <summary>
        /// Gets or sets the input state of the overlay view.
        /// </summary>
        /// <value>
        /// The new input state.
        /// </value>
        public CoherentViewInputState InputState
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ViewInputState = (CoherentViewInputState)_coherentView.InputState;
                }
                return m_ViewInputState;
            }
            set
            {
                CoherentView.InputState = (CoherentUIView.CoherentViewInputState)value;
                m_ViewInputState = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_IsTransparent = false;
        /// <summary>
        /// Gets or sets a value indicating whether the view is transparent.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is transparent; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the property is modified and the view has already been created
        /// </exception>
        public bool IsTransparent
        {
            get
            {
                if (_coherentView != null)
                {
                    m_IsTransparent = _coherentView.IsTransparent;
                }
                return m_IsTransparent;
            }
            set
            {
                CoherentView.IsTransparent = value;
                m_IsTransparent = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_SupportClickThrough = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view <see cref="MetaBrowserView"/> supports click through.
        /// </summary>
        /// <value>
        /// <c>true</c> if supports click through; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the property is modified and the view has already been created
        /// </exception>
        public bool SupportClickThrough
        {
            get
            {
                if (_coherentView != null)
                {
                    m_SupportClickThrough = _coherentView.SupportClickThrough;
                }
                return m_SupportClickThrough;
            }
            set
            {
                CoherentView.SupportClickThrough = value;
                m_SupportClickThrough = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private float m_ClickThroughAlphaThreshold = 0;
        /// <summary>
        /// Gets or sets the alpha threshold for click through checks.
        /// </summary>
        /// <value>
        /// The alpha threshold for click through checks.
        /// </value>
        public float ClickThroughAlphaThreshold
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ClickThroughAlphaThreshold = _coherentView.ClickThroughAlphaThreshold;
                }
                return m_ClickThroughAlphaThreshold;
            }
            set
            {
                CoherentView.ClickThroughAlphaThreshold = value;
                m_ClickThroughAlphaThreshold = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_ClickToFocus = false;
        /// <summary>
        /// When enabled, allows a view to take input focus when clicked with the left mouse button.
        /// </summary>
        /// <value>
        /// <c>true</c> if this view takes input focus when clicked; otherwise, <c>false</c>.
        /// </value>
        public bool ClickToFocus
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ClickToFocus = _coherentView.ClickToFocus;
                }
                return m_ClickToFocus;
            }
            set
            {
                CoherentView.ClickToFocus = value;
                m_ClickToFocus = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_DisplayVirtualKeyboard = true;
        /// <summary>
        /// When enabled, allows a view to show virtual keyboard when a text input
        /// widget is focused on touch enabled devices running Windows 8 and later.
        /// </summary>
        /// <value>
        /// <c>true</c> if this view should show the virtual keyboard; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayVirtualKeyboard
        {
            get
            {
                if (_coherentView != null)
                {
                    m_DisplayVirtualKeyboard = _coherentView.DisplayVirtualKeyboard;
                }
                return m_DisplayVirtualKeyboard;
            }
            set
            {
                CoherentView.DisplayVirtualKeyboard = value;
                m_DisplayVirtualKeyboard = value;
            }
        }

#if COHERENT_UI_PRO_UNITY3D && (UNITY_EDITOR || COHERENT_UNITY_STANDALONE)
        [HideInInspector]
        [SerializeField]
        private bool m_IsOnDemand = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view is an "on demand" view
        /// </summary>
        /// <value>
        /// <c>true</c> if this view is an "on demand" view; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the property is modified and the view has already been created
        /// </exception>
        public bool IsOnDemand
        {
            get
            {
                if (_coherentView != null)
                {
                    m_IsOnDemand = _coherentView.IsOnDemand;
                }
                return m_IsOnDemand;
            }
            set
            {
                CoherentView.IsOnDemand = value;
                m_IsOnDemand = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_ControlTimeOnDemand = false;
        /// <summary>
        /// Defines if the client controls the time of on-demand view himself. If not Coherent Browser will update
        /// the timers in 'View::RequestFrame'. This option allows for perfect time-sync between the game and the UI.
        /// </summary>
        /// <value>
        /// <c>true</c> if this view is time-controlled by the user; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the property is modified and the view has already been created
        /// </exception>
        public bool ControlTimeOnDemand
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ControlTimeOnDemand = _coherentView.ControlTimeOnDemand;
                }
                return m_ControlTimeOnDemand;
            }
            set
            {
                CoherentView.ControlTimeOnDemand = value;
                m_ControlTimeOnDemand = value;

            }
        }

        [HideInInspector]
        [SerializeField]
        private int m_TargetFramerate = 60;
        /// <summary>
        /// Gets or sets the target framerate for the view.
        /// </summary>
        /// <value>
        /// The target framerate.
        /// </value>
        public int TargetFramerate
        {
            get
            {
                if (_coherentView != null)
                {
                    m_TargetFramerate = _coherentView.TargetFramerate;
                }
                return m_TargetFramerate;
            }
            set
            {
                CoherentView.TargetFramerate = value;
                m_TargetFramerate = value;
            }
        }
#endif

        public enum DrawOrder
        {
            Normal,
            AfterPostEffects
        }
        [HideInInspector]
        [SerializeField]
        private DrawOrder m_DrawAfterPostEffects = DrawOrder.Normal;
        /// <summary>
        /// Gets or sets a value indicating whether this view is drawn after post effects.
        /// </summary>
        /// <value>
        /// <c>AfterPostEffects</c> if the view is drawn after post effects; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the application exception.
        /// </exception>
        public DrawOrder DrawAfterPostEffects
        {
            get
            {
                if (_coherentView != null)
                {
                    m_DrawAfterPostEffects = (DrawOrder)_coherentView.DrawAfterPostEffects;
                }
                return m_DrawAfterPostEffects;
            }
            set
            {
                CoherentView.DrawAfterPostEffects = (CoherentUIView.DrawOrder)value;
                m_DrawAfterPostEffects = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_FlipY = false;
        /// <summary>
        /// Gets or sets a value indicating whether the Y axis of this view should be flipped.
        /// </summary>
        /// <value>
        /// <c>true</c> if the Y axis is flipped; otherwise, <c>false</c>.
        /// </value>
        public bool FlipY
        {
            get
            {
                if (_coherentView != null)
                {
                    m_FlipY = _coherentView.FlipY;
                }
                return m_FlipY;
            }
            set
            {
                CoherentView.FlipY = value;
                m_FlipY = value;
            }
        }

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
        public enum CoherentFilteringModes
        {
            PointFiltering = 1,
            LinearFiltering = 2
        };

        [HideInInspector]
        [SerializeField]
        private CoherentFilteringModes m_Filtering =
            CoherentFilteringModes.PointFiltering;
        public CoherentFilteringModes Filtering
        {
            get
            {
                if (_coherentView != null)
                {
                    m_Filtering = (CoherentFilteringModes)_coherentView.Filtering;
                }
                return m_Filtering;
            }
            set
            {
                CoherentView.Filtering = (CoherentUISystem.CoherentFilteringModes)value;
                m_Filtering = value;
            }
        }
#endif

        // Invert flipping on camera-attached views on OpenGL
        /*internal bool ForceInvertY()
        {
            return GetComponent<Camera>() != null
                && SystemInfo.graphicsDeviceVersion.Contains("Open")
                && Application.platform != RuntimePlatform.WindowsEditor;
        }*/

        [HideInInspector]
        [SerializeField]
        private bool m_ReceivesInput = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view receives input.
        /// All automatic processing and reading of this property is done in the
        /// `LateUpdate()` / `OnGUI()` callbacks in Unity, letting you do all your logic
        /// for View focus in `Update()`.
        /// </summary>
        /// <value>
        /// <c>true</c> if this view receives input; otherwise, <c>false</c>.
        /// </value>
        public virtual bool ReceivesInput
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ReceivesInput = _coherentView.ReceivesInput;
                }
                return m_ReceivesInput;
            }
            set
            {
                CoherentView.ReceivesInput = value;
                m_ReceivesInput = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private MetaBrowserViewListener m_Listener;
        /// <summary>
        /// Gets the underlying MetaBrowserViewListener for this view.
        /// </summary>
        /// <value>
        /// The listener.
        /// </value>
        internal MetaBrowserViewListener Listener
        {
            get
            {
                if (m_Listener == null)
                {
                    m_Listener = gameObject.GetComponent<MetaBrowserViewListener>();
                    if (m_Listener == null)
                    {
                        m_Listener = gameObject.AddComponent<MetaBrowserViewListener>();
                    }
                }
                return m_Listener;
            }
            set
            {
                m_Listener = value;
            }
        }

        //Handle OnStartLoad/OnFinishLoad/etc?

        /*protected UnityViewListener m_Listener;

        [HideInInspector]
        [SerializeField]
        protected CoherentUISystem m_Context;
        internal CoherentUISystem Context
        {
            get
            {
                if (CoherentView != null)
                {
                    return CoherentView.Context;
                }
                return null;
            }
            set
            {
                if (CoherentView != null)
                {
                    CoherentView.Context = value;
                }
            }
        }*/

        [HideInInspector]
        [SerializeField]
        private MetaBrowserSystem m_Context;
        public MetaBrowserSystem Context
        {
            get
            {
                return m_Context;
            }
            set
            {
                // Cannot change context when running
                if (!Application.isPlaying)
                {
                    // If removing view context, must also reset context of CoherentUIView
                    if (value == null)
                    {
                        if (_coherentView != null)
                        {
                            _coherentView.Context = null;
                        }
                    }
                    // Otherwise, set the view context of CoherentUIView
                    else if (CoherentView != null && value.CoherentSystem != null)
                    {
                        CoherentView.Context = value.CoherentSystem;
                    }
                    m_Context = value;
                }
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_InterceptAllEvents = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view intercepts all events and sends a message for each event.
        /// </summary>
        /// <value>
        /// <c>true</c> if view intercepts all events; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref='System.ApplicationException'>
        /// Is thrown when the property is modified and the view has already been created
        /// </exception>
        private bool InterceptAllEvents
        {
            get
            {
                if (_coherentView != null)
                {
                    m_InterceptAllEvents = _coherentView.InterceptAllEvents;
                }
                return m_InterceptAllEvents;
            }
            set
            {
#if !UNITY_EDITOR
			        if(View != null) {
				        throw new System.ApplicationException("Intercepting all events can't be changed if the view has already been created");
			        }
#endif
                CoherentView.InterceptAllEvents = value;
                m_InterceptAllEvents = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_EnableBindingAttribute = true;
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MetaBrowserView"/> enables usage of the CoherentMethod attribute
        /// in components in the host GameObject.
        /// When true, the all components in the host GameObject are inspected for the CoherentMethod attribute (in the Awake() function)
        /// and the decorated methods are automatically bound when the ReadyForBindings event is received.
        /// When false, the attribute does nothing.
        /// </summary>
        /// <value>
        /// <c>true</c> if usage of the CoherentMethod is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool EnableBindingAttribute
        {
            get
            {
                if (_coherentView != null)
                {
                    m_EnableBindingAttribute = _coherentView.EnableBindingAttribute;
                }
                return m_EnableBindingAttribute;
            }
            set
            {
                CoherentView.EnableBindingAttribute = value;
                m_EnableBindingAttribute = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_IsIndependentOfZBuffer = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view is z-buffer independent. If it is set to true, the view is rendered on top of everything.
        /// </summary>
        /// <value>
        /// <c>true</c> if it is independent; otherwise <c>false</c>.
        /// </value>
        public bool IsIndependentOfZBuffer
        {
            get
            {
                if (_coherentView != null)
                {
                    m_IsIndependentOfZBuffer = _coherentView.IsIndependentOfZBuffer;
                }
                return m_IsIndependentOfZBuffer;
            }
            set
            {
                CoherentView.IsIndependentOfZBuffer = value;
                m_IsIndependentOfZBuffer = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_ShowJavaScriptDialogs = true;
        /// <summary>
        /// Gets or sets a value indicating whether dialogs will be drawn when a JavaScript message is received (e.g. alert, prompt, auth credentials).
        /// </summary>
        /// <value>
        /// <c>true</c> if a dialog is to be shown automatically; otherwise <c>false</c>.
        /// </value>
        public bool ShowJavaScriptDialogs
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ShowJavaScriptDialogs = _coherentView.ShowJavaScriptDialogs;
                }
                return m_ShowJavaScriptDialogs;
            }
            set
            {
                CoherentView.ShowJavaScriptDialogs = value;
                m_ShowJavaScriptDialogs = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_ForceSoftwareRendering = false;
        /// <summary>
        /// Forces the view to use software rendering. For GPU-bound applications software views might be a good choice.
        /// CSS 3D transforms, WebGL and accelerated Canvas don't work with software views. This option doesn't work with OnDemand.
        ///
        /// Not available for iOS.
        /// </summary>
        /// <value>
        /// <c>true</c> if the view is to use software rendering; otherwise <c>false</c>.
        /// </value>
        public bool ForceSoftwareRendering
        {
            get
            {
                if (_coherentView != null)
                {
                    m_ForceSoftwareRendering = _coherentView.ForceSoftwareRendering;
                }
                return m_ForceSoftwareRendering;
            }
            set
            {
                CoherentView.ForceSoftwareRendering = value;
                m_ForceSoftwareRendering = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_IgnoreDisplayDensity = false;
        /// <summary>
        /// Defines whether the Android device's display density will affect the
        /// scale of the View. When set to *true*, the displayed content in the view
        /// will match its pixel size.
        /// For example, a device with 1.5 scale factor will display a div of 100px
        /// in a 200px View on about 75% of surface if IgnoreDisplayDensity
        /// is set to false.
        /// If IgnoreDisplayDensity is set to true, the same div of 100px displayed
        /// in a 200px View will occupy exactly 50% of the View.
        /// </summary>
        /// <value>
        /// <c>true</c> if the view ignores the device's display density;
        /// otherwise <c>false</c>.
        /// </value>
        private bool IgnoreDisplayDensity
        {
            get
            {
                if (_coherentView != null)
                {
                    m_IgnoreDisplayDensity = _coherentView.IgnoreDisplayDensity;
                }
                return m_IgnoreDisplayDensity;
            }
            set
            {
                CoherentView.IgnoreDisplayDensity = value;
                m_IgnoreDisplayDensity = value;
            }
        }

        /*private Camera m_Camera;

        private bool m_QueueCreateView = false;*/

        [HideInInspector]
        [SerializeField]
        private bool m_UseCameraDimensions = true;
        /// <summary>
        /// If checked, the view will use the camera's width and height
        /// </summary>
        /// <value>
        /// <c>true</c> if we want to use camera's width and height; otherwise <c>false</c>.
        /// </value>
        public bool UseCameraDimensions
        {
            get
            {
                if (_coherentView != null)
                {
                    m_UseCameraDimensions = _coherentView.UseCameraDimensions;
                }
                return m_UseCameraDimensions;
            }
            set
            {
                CoherentView.UseCameraDimensions = value;
                m_UseCameraDimensions = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_CorrectGamma = false;
        /// <summary>
        /// Gets or sets a value indicating whether this view should have gamma
        /// corrected.
        /// </summary>
        /// <value>
        /// <c>true</c> if gamma is corrected; otherwise, <c>false</c>.
        /// </value>
        public bool CorrectGamma
        {
            get
            {
                if (_coherentView != null)
                {
                    m_CorrectGamma = _coherentView.CorrectGamma;
                }
                return m_CorrectGamma;
            }
            set
            {
                CoherentView.CorrectGamma = value;
                m_CorrectGamma = value;
            }
        }

        [HideInInspector]
        [SerializeField]
        private bool m_EnableIME = false;
        /// <summary>
        /// Gets or sets a value indicating whether the view should have IME enabled
        /// </summary>
        /// <value>
        /// <c>true</c> if IME is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool EnableIME
        {
            get
            {
                if (_coherentView != null)
                {
                    m_EnableIME = _coherentView.EnableIME;
                }
                return m_EnableIME;
            }
            set
            {
#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
                CoherentView.EnableIME = value;
                m_EnableIME = value;
#endif
            }
        }

        /*internal int MouseX { get; set; }
        internal int MouseY { get; set; }

        public bool IsReadyForBindings { get; private set; }

        private List<CoherentMethodBindingInfo> m_CoherentMethods;*/

        /// <summary>
        /// Sets the mouse position. Note Coherent Browser (0,0) is the upper left corner of the screen.
        /// </summary>
        /// <param name='x'>
        /// X coordinate of the mouse.
        /// </param>
        /// <param name='y'>
        /// Y coordinate of the mouse.
        /// </param>
        public void SetMousePosition(int x, int y)
        {
            CoherentView.SetMousePosition(x, y);
        }

        //private string InitialScript;

        //private bool InterceptAllEvents;

        //[SerializeField]
        private float _dpi = 40f;
        private float _slowResizeIntervalInSeconds = 0.1f;
        private Queue<Vector2> _sizes = new Queue<Vector2>();
        private Coroutine _resizeCoroutine = null;

        private string _javaScript;
        private bool _isJsInjectionInitialized = false;

        private bool _hideScrollbars = false;

        internal MetaBrowserSecurity _metaSecurity;

        internal MetaViewListener _modelEvents;

        void Awake()
        {
            CoherentView.enabled = true;

            // Get the listener from the Coherent view
            if (_coherentView != null)
            {
                Listener.m_Listener = _coherentView.m_Listener;
            }
            // If no context specified, use default context
            if (m_Context == null)
            {
                m_Context = MetaBrowserSystem.DefaultContext;
            }
            m_Context.AddView(this); // Add to MetaBrowserSystem

            // Add model loading functions
            _modelEvents = new MetaViewListener(this);

            // Add SSL checking functions
            _metaSecurity = new MetaBrowserSecurity(m_Listener);
        }

        // Use this for initialization
        void Start()
        {
            _javaScript = JSCode.SCROLLING_EVENT_SCRIPT + JSCode.COHERENT_SCRIPT + JSCode.DOCUMENT_SIZE_SCRIPT + JSCode.IS_CLICKABLE_SCRIPT;

            initialize();
        }

        // Update is called once per frame
        void Update()
        {
            // Keep Page up to date with what is in browser view
            if (_coherentView != null)
            {
                m_Page = _coherentView.Page;
            }

            if (!_isJsInjectionInitialized)
            {
                initialize();
            }
        }

        void OnDestroy()
        {
            _modelEvents.RemoveEvents();
            _metaSecurity.RemoveSecurityCheck(m_Listener);
            m_Context.RemoveView(this);
            DestroyCoherentView();
        }

        /// <summary>
        /// Removes the CoherentView from the GameObject.
        /// </summary>
        internal void DestroyCoherentView()
        {
            if (_coherentView != null)
            {
                Destroy(_coherentView);
            }
        }

        /// <summary>
        /// Destroy this view. Destroys the Meta Browser view and removes the MetaBrowserView
        /// component from its game object. Any usage of the view after this method is
        /// undefined behaviour.
        /// </summary>
        public void DestroyView()
        {
            CoherentView.DestroyView();
            OnDestroy();
            UnityEngine.Object.Destroy(this);
        }

        /// <summary>
        /// Occurs when the view has been destroyed and the MetaBrowserView component
        /// is going to be removed from the game object.
        /// </summary>
        internal event CoherentUIView.ViewDestroyedHandler OnViewDestroyed
        {
            add
            {
                if (_coherentView != null)
                {
                    _coherentView.OnViewDestroyed += value;
                }
            }
            remove
            {
                if (_coherentView != null)
                {
                    _coherentView.OnViewDestroyed -= value;
                }
            }
        }


        /// <summary>
        /// Resize the view to the specified width and height.
        /// </summary>
        /// <param name='width'>
        /// New width for the view.
        /// </param>
        /// <param name='height'>
        /// New height for the view.
        /// </param>
        public void Resize(int width, int height)
        {
            if (m_Width != width || m_Height != height)
            {
                m_Width = width;
                m_Height = height;

                if (CoherentView.View != null)
                {
                    //Debug.Log("Resize to " + width + "," + height);
                    CoherentView.Resize(width, height);
                }
                else
                {
                    StartCoroutine(waitToResize(width, height));
                }
            }
        }

        private IEnumerator waitToResize(int width, int height)
        {
            int numFramesWaited = 0;

            while (CoherentView.View == null)
            {
                yield return null;

                numFramesWaited++;

                if (numFramesWaited > 60)
                {
                    break;
                }
            }

            CoherentView.Resize(width, height);
        }

        public void Resize(Vector2 size)
        {
            Resize((int)(size.x * _dpi), (int)(size.y * _dpi));
        }

        public void SlowResize(Vector2 size)
        {
            _sizes.Enqueue(size);

            if (_resizeCoroutine == null)
            {
                _resizeCoroutine = StartCoroutine(resizeEveryNSeconds(_slowResizeIntervalInSeconds));
            }
        }

        private IEnumerator resizeEveryNSeconds(float nSeconds)
        {
            while (_sizes.Count > 0)
            {
                yield return new WaitForSeconds(nSeconds);

                int numToDequeue = _sizes.Count - 1;

                for (int i = 0; i < numToDequeue; i++)
                {
                    //Debug.Log("Throwing out resize to: " + );
                    _sizes.Dequeue();
                }

                Vector2 size = _sizes.Dequeue();
                Resize(size);
            }

            _resizeCoroutine = null;
        }

        public Vector2 GetCoordinates(Vector2 screenPosition, RectTransform rectTransform, Camera camera, bool invertVertical = true)
        {
            Vector2 rectPoint = new Vector2();

            if (rectTransform != null && camera != null)
            {
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPosition, camera, out rectPoint))
                {
                    Vector2 normedRectPoint = new Vector2((rectPoint.x + rectTransform.rect.width / 2) / rectTransform.rect.width,
                                                          (rectPoint.y + rectTransform.rect.height / 2) / rectTransform.rect.height);

                    rectPoint.x = (int)(normedRectPoint.x * Width);
                    rectPoint.y = (int)((1 - normedRectPoint.y) * Height);
                }
            }

            return rectPoint;
        }

        private void initialize()
        {
            if (Listener != null)
            {
                //Debug.Log("Initializing JS");
                injectCoherentJS();

                Listener.FinishLoad += Listener_FinishLoad;
                Listener.ReadyForBindings += Listener_ReadyForBindings;
                Listener.NavigateTo += Listener_NavigateTo;

                _isJsInjectionInitialized = true;
            }
        }

        private void Listener_ReadyForBindings(int frameId, string path, bool isMainFrame)
        {
            if (isMainFrame)
            {
                View.RegisterForEvent("OnScrollChanged", (Action<float, float>)UpdateScrollbars);
                View.RegisterForEvent("OnPageResized", (Action<float, float>)UpdatePageSize);
                View.RegisterForEvent("OnPageLoaded", (Action<float, float>)DoPageLoaded);
            }
        }

        private void Listener_NavigateTo(string path)
        {
            JSHideScrollBars();
        }

        private void Listener_FinishLoad(int frameId, string validatedPath, bool isMainFrame, int statusCode, Coherent.UI.HTTPHeader[] headers)
        {
            if (isMainFrame)
            {
                View.ExecuteScript(JSCode.GETPAGESIZE_EXEC_SCRIPT);
            }
        }

        private void injectCoherentJS()
        {
            if (CoherentView != null)
            {
                CoherentView.InitialScript = _javaScript;
            }
        }

        private void JSHideScrollBars()
        {
            if (_hideScrollbars)
            {
                View.ExecuteScript(JSCode.HIDE_SCROLLBARS_EXEC_SCRIPT);
            }
        }

        public void UpdatePageSize(float horizontalRatio, float verticalRatio) { }

        public void UpdateScrollbars(float horizontalPercent, float verticalPercent) { }

        public void DoPageLoaded(float horizontalRatio, float verticalRatio) { }


#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
	    /// <summary>
	    /// Move the overlay view to the specified x and y position.
	    /// </summary>
	    /// <param name='x'>
	    /// New X position of the view.
	    /// </param>
	    /// <param name='y'>
	    /// New Y position of the view.
	    /// </param>
	    public void Move(int x, int y)
	    {
		    CoherentView.Move(x, y);
	    }

	    /// <summary>
	    /// Show/hide the overlay view.
	    /// </summary>
	    /// <param name='show'>
	    /// Whether to show or to hide the view
	    /// </param>
	    public void SetShow(bool show)
	    {
		    CoherentView.SetShow(show);
	    }

	    /// <summary>
	    /// Sets the input state of the overlay view.
	    /// </summary>
	    /// <param name='state'>
	    /// The new state of the input
	    /// </param>
	    public void SetInputState(CoherentUIView.CoherentViewInputState state)
	    {
		    CoherentView.SetInputState(state);
	    }
#endif

        /// <summary>
        /// Gets the underlying Coherent.UI.View instance.
        /// </summary>
        /// <value>
        /// The underlying Coherent.UI.View instance.
        /// </value>
        internal View View
        {
            get
            {
                return CoherentView.View;
            }
        }

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
        /// <summary>
        /// Request redraw of this view.
        /// </summary>
        public void Redraw()
        {
            CoherentView.Redraw();
        }
#endif

        /// <summary>
        /// Request reload of this view.
        /// </summary>
        /// <param name='ignoreCache'>
        /// Ignore cache for the reload.
        /// </param>
        public void Reload(bool ignoreCache)
        {
            CoherentView.Reload(ignoreCache);
        }

        /// <summary>
        /// Returns the camera dimensions of the current view.
        /// </summary>
        public bool GetCamDimensions(out int x, out int y)
        {
            return CoherentView.GetCamDimensions(out x, out y);
        }

        public void Zoom(float zoomScale)
        {
            if (View != null)
            {
                View.ExecuteScript(string.Format("document.body.style.zoom={0};", zoomScale));
            }
        }

        /// <summary>
        /// Occurs when the underlying Coherent.UI.View is created
        /// </summary>
        public event UnityViewListener.CoherentUI_OnViewCreated OnViewCreated
        {
            add
            {
                m_Listener.ViewCreated += value;
            }
            remove
            {
                m_Listener.ViewCreated -= value;
            }
        }

        /// <summary>
        /// Occurs when the view bindings should be registered.
        /// </summary>
        public event UnityViewListener.CoherentUI_OnReadyForBindings OnReadyForBindings
        {
            add
            {
                m_Listener.ReadyForBindings += value;
            }
            remove
            {
                m_Listener.ReadyForBindings -= value;
            }
        }

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
        public float WidthToCamWidthRatio(float camWidth)
        {
            return CoherentView.WidthToCamWidthRatio(camWidth);
        }

        public float HeightToCamHeightRatio(float camHeight)
        {
            return CoherentView.HeightToCamHeightRatio(camHeight);
        }
#endif

#if UNITY_EDITOR || COHERENT_UNITY_STANDALONE
        /// <summary>
        /// Handler for calculating the screen position of the IME candidate
        /// list window.
        /// </summary>
        /// <param name='x'>
        /// X coordinate of the current caret position in view space.
        /// </param>
        /// <param name='y'>
        /// Y coordinate of the current caret position in view space.
        /// </param>
        /// <param name='width'>
        /// The width of the text input control, on which the caret is positioned.
        /// </param>
        /// <param name='height'>
        /// The height of the text input control, on which the caret is positioned.
        /// </param>
        /// <returns>
        /// The position of the candaidate list window in screen space coordinates.
        /// </returns>
        public event IMEHandler.CalculateIMECandidateListPositionHandler
            CalculateIMECandidateListPosition
        {
            add
            {
                CoherentView.CalculateIMECandidateListPosition += value;
            }
            remove
            {
                CoherentView.CalculateIMECandidateListPosition -= value;
            }
        }
#endif

        internal event CoherentUIView.CalculateArbitraryTimeHandler CalculateArbitraryTime
        {
            add
            {
                CoherentView.CalculateArbitraryTime += value;
            }
            remove
            {
                CoherentView.CalculateArbitraryTime -= value;
            }
        }

        /// <summary>
        /// whether this is a mobile surface view
        /// </summary>
        /*public virtual bool IsSurfaceView
        {
            get { return false; }
        }*/
    }
}