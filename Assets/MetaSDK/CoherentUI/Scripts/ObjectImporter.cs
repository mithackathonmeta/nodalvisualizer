﻿using UnityEngine;
using System.Collections;
using Coherent.UI.Binding;

namespace Meta
{
    [RequireComponent(typeof(ObjReader))]
    public class ObjectImporter : MonoBehaviour
    {
        private GameObject _importedObject;

        private static ObjectImporter _inst;
        private static ObjectImporter Inst
        {
            get
            {
                if (_inst == null)
                {
                    _inst = FindObjectOfType<ObjectImporter>();
                }

                return _inst;
            }
        }

        /// <summary>
        /// Calls the import method to be used (eg synchronous, mixed synchronous).
        /// </summary>
        /// <param name="objectLocation">The path location of the file.</param>
        public static GameObject Import(string objectLocation, bool isReceivedFile = false)
        {
            return Inst.ImportObject(objectLocation, isReceivedFile);
        }

        private GameObject ImportObject(string objectLocation, bool isReceivedFile = false)
        {
            StartCoroutine(ImportObjectCoroutine(objectLocation, isReceivedFile));
            
            //_importedObject is now the importing object
            //We can now call functions to scale it or do whatever to it based on some abstract data

            return _importedObject;
        }

        /// <summary>
        /// Imports the object in a coroutine, using ObjReader to generate the gameobjects 
        /// and then applying model handling and manipulation. 
        /// </summary>
        /// <param name="objectLocation">The path location of the file.</param>
        private IEnumerator ImportObjectCoroutine(string objectLocation, bool isReceivedFile = false)
        {
            _importedObject = new GameObject();
            _importedObject.name = "ImportedObj";

            //Sets the file path for the importing model
            string fullFileName = objectLocation;

            //Imports the model
            var obj = ObjReader.use.ConvertFileAsync("file://" + fullFileName, true);
            while (obj.gameObjects == null)
            {
                yield return null;
            }
            var foo = obj.gameObjects;

            // If the file being imported was not received from the host (ie I am the file owner)
            //_importedObject = new GameObject();
            //_importedObject.AddComponent<uLink.NetworkView>().AllocateViewID();

            //Parent each mesh component to another gameobject
            foreach (GameObject g in foo)
            {
                g.transform.SetParent(_importedObject.transform);
                g.layer = LayerMask.NameToLayer("Default");
            }
        }
    }

}
