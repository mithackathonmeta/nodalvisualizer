﻿using UnityEngine;
using Coherent.UI;
using UnityEngine.EventSystems;

namespace Meta.UI
{
    [RequireComponent(typeof(MetaBrowserView))]
    public class MetaBrowserKeyboardInput : MonoBehaviour, ISelectHandler, IDeselectHandler, IUpdateSelectedHandler
    {
        private MetaBrowserView _view;
        private bool _selected = false;

        public MetaBrowserView View { get { return _view; } }

        void Start()
        {
            _view = GetComponent<MetaBrowserView>();
        }

        public void OnSelect(BaseEventData eventData)
        {
            _selected = true;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            _selected = false;
        }

        public void OnUpdateSelected(BaseEventData eventData)
        {
            if (_selected)
            {
                Event currentEvent = new Event();

                while (Event.PopEvent(currentEvent))
                {
                    if (currentEvent.rawType == EventType.KeyDown)
                    {
                        // Consumed event!
                        if (currentEvent.keyCode != KeyCode.None)
                        {
                            KeyEventData keyEventData = Coherent.UI.InputManager.ProcessKeyEvent(currentEvent);
                            keyEventData.Type = KeyEventData.EventType.KeyDown;

                            if (keyEventData.KeyCode == 0)
                            {
                                keyEventData = null;
                            }

                            View.View.KeyEvent(keyEventData);
                        }

                        if (currentEvent.character != 0)
                        {
                            KeyEventData keyEventData = Coherent.UI.InputManager.ProcessCharEvent(currentEvent);

                            if (keyEventData.KeyCode == 10)
                            {
                                keyEventData.KeyCode = 13;
                            }

                            View.View.KeyEvent(keyEventData);
                        }
                    }
                    else if (currentEvent.rawType == EventType.KeyUp)
                    {
                        KeyEventData keyEventData = Coherent.UI.InputManager.ProcessKeyEvent(currentEvent);
                        keyEventData.Type = KeyEventData.EventType.KeyUp;

                        if (keyEventData.KeyCode == 0)
                        {
                            keyEventData = null;
                        }

                        View.View.KeyEvent(keyEventData);
                    }
                }

                eventData.Use();
            }
        }

        protected KeyEventData KeyPressed(Event evt)
        {
            KeyEventData keyEventData = new KeyEventData();

            var currentEventModifiers = evt.modifiers;
            RuntimePlatform rp = Application.platform;
            bool isMac = (rp == RuntimePlatform.OSXEditor || rp == RuntimePlatform.OSXPlayer);
            bool ctrl = isMac ? (currentEventModifiers & EventModifiers.Command) != 0 : (currentEventModifiers & EventModifiers.Control) != 0;
            bool shift = (currentEventModifiers & EventModifiers.Shift) != 0;
            bool alt = (currentEventModifiers & EventModifiers.Alt) != 0;
            bool ctrlOnly = ctrl && !alt && !shift;

            switch (evt.keyCode)
            {
                case KeyCode.Backspace:
                    {
                        //Backspace();
                    }
                    break;
                case KeyCode.Delete:
                    {
                        //ForwardSpace();
                    }
                    break;
                case KeyCode.Home:
                    {
                        //MoveTextStart(shift);
                    }
                    break;
                case KeyCode.End:
                    {
                        //MoveTextEnd(shift);
                    }
                    break;
                // Select All
                case KeyCode.A:
                    {
                        if (ctrlOnly)
                        {
                            //SelectAll();
                        }
                    }
                    break;
                // Copy
                case KeyCode.C:
                    {
                        if (ctrlOnly)
                        {
                            //if (inputType != InputType.Password)
                            //{
                            //    clipboard = GetSelectedString();
                            //}
                            //else
                            //{
                            //    clipboard = "";
                            //}
                        }
                    }
                    break;
                // Paste
                case KeyCode.V:
                    {
                        if (ctrlOnly)
                        {
                            //Append(clipboard);
                        }
                    }
                    break;
                // Cut
                case KeyCode.X:
                    {
                        if (ctrlOnly)
                        {
                            //if (inputType != InputType.Password)
                            //{
                            //    clipboard = GetSelectedString();
                            //}
                            //else
                            //{
                            //    clipboard = "";
                            //}

                            //Delete();
                            //SendOnValueChangedAndUpdateLabel();
                        }
                    }
                    break;
                case KeyCode.LeftArrow:
                    {
                        //MoveLeft(shift, ctrl);
                    }
                    break;
                case KeyCode.RightArrow:
                    {
                        //MoveRight(shift, ctrl);
                    }
                    break;
                case KeyCode.UpArrow:
                    {
                        //MoveUp(shift);
                    }
                    break;
                case KeyCode.DownArrow:
                    {
                        //MoveDown(shift);
                    }
                    break;
                // Submit
                case KeyCode.Return:
                case KeyCode.KeypadEnter:
                    {
                        //if (lineType != LineType.MultiLineNewline)
                        //{
                        //}
                    }
                    break;
                case KeyCode.Escape:
                    {
                        //m_WasCanceled = true;
                    }
                    break;
            }

            char c = evt.character;

            // Convert carriage return and end-of-text characters to newline.
            if (c == '\r' || (int)c == 3)
            {
                c = '\n';
            }

            return keyEventData;
        }
    }
}