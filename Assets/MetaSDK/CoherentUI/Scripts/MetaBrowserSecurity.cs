﻿using Coherent.UI;

namespace Meta
{
    /// <summary>
    /// Security-related functionality for the Meta Browser.
    /// </summary>
    internal class MetaBrowserSecurity
    {
        internal enum HTTPSState
        {
            Secure,
            Nonsecure,
            NonHTTPS,
            Unknown
        }

        private bool _mainFrameLoaded = false;
        /// <summary>
        /// Stores whether the main frame has loaded or not.
        /// </summary>
        /// <remarks>
        /// The SSH state of a page cannot be determined until the main frame
        /// has been loaded.
        /// </remarks>
        private bool mainFrameLoaded
        {
            get
            {
                return _mainFrameLoaded;
            }
            set
            {
                OnSSLChange(state);
                _mainFrameLoaded = value;
            }
        }

        private HTTPSState _state = HTTPSState.Unknown;
        /// <summary>
        /// Stores the security state of the current page.
        /// </summary>
        internal HTTPSState state
        {
            get
            {
                return _state;
            }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    if (mainFrameLoaded)
                    {
                        OnSSLChange(_state);    // Fire event
                    }
                }
            }
        }

        /// <summary>
        /// An event that is triggered when the SSL state changes.
        /// </summary>
        internal static event MetaBrowserSecurity_OnSSLChange SSLChange;

        internal delegate void MetaBrowserSecurity_OnSSLChange(HTTPSState SSLState);

        /// <summary>
        /// Method that triggers the SSLChange event.
        /// </summary>
        /// <param name="SSLState">The current detected SSL state of the page.</param>
        private void OnSSLChange(HTTPSState SSLState)
        {
            if (SSLChange != null)
            {
                SSLChange(SSLState);
            }
        }

        /// <summary>
        /// Constructor for MetaBrowserSecurity class.
        /// </summary>
        /// <param name="m_Listener">The listener to subscribe events to.</param>
        public MetaBrowserSecurity(MetaBrowserViewListener m_Listener)
        {
            AddSecurityCheck(m_Listener);
        }

        /// <summary>
        /// Subscribes the security checking functions to the events.
        /// </summary>
        /// <param name="m_Listener">The MetaBrowserViewListener of the view.</param>
        /// <returns>True if successful.</returns>
        internal bool AddSecurityCheck(MetaBrowserViewListener m_Listener)
        {
            m_Listener.NavigateTo += ResetSSLStatus;
            m_Listener.FinishLoad += SSLCheck;
            m_Listener.CertificateError += CertificateInvalid;

            return true;
        }

        /// <summary>
        /// Unsubscribes the security checking functions to the events.
        /// </summary>
        /// <param name="m_Listener">The MetaBrowserViewListener of the view.</param>
        /// <returns>True if successful.</returns>
        internal bool RemoveSecurityCheck(MetaBrowserViewListener m_Listener)
        {
            m_Listener.NavigateTo -= ResetSSLStatus;
            m_Listener.FinishLoad -= SSLCheck;
            m_Listener.CertificateError -= CertificateInvalid;

            return true;
        }

        /// <summary>
        /// Resets the SSL status when navigating to a new page.
        /// </summary>
        /// <param name="path">Path being navigated to (not used).</param>
        private void ResetSSLStatus(string path)
        {
            state = HTTPSState.Unknown;
            mainFrameLoaded = false;
        }

        /// <summary>
        /// Check if the page being accessed is secure (https://).
        /// </summary>
        private void SSLCheck(int frameId, string validatedPath, bool isMainFrame, int statusCode, HTTPHeader[] headers)
        {
            //Debug.Log("Validated path = " + validatedPath + ", ismain = " + isMainFrame);
            if (isMainFrame)
            {
                if (!validatedPath.ToLower().StartsWith("https://"))
                {
                    state = HTTPSState.NonHTTPS;    // Not meant to be a secure website
                }
                else if (state != HTTPSState.Nonsecure)
                {
                    state = HTTPSState.Secure;
                }
                mainFrameLoaded = true;
            }
            else if (!validatedPath.ToLower().StartsWith("https://") 
                && !validatedPath.ToLower().StartsWith("about:blank") 
                && state != HTTPSState.NonHTTPS)
            {
                state = HTTPSState.Nonsecure;
            }
            return;
        }

        /// <summary>
        /// Cancel the request if the certificate is invalid.
        /// </summary>
        private void CertificateInvalid(string url, CertificateStatus status, Certificate certificate, CertificateErrorResponse response)
        {
            //Debug.Log("Certificate invalid: " + certificate.Issuer.CommonName);
            //Debug.Log("Invalid: " + certificate.Issuer.OrganizationNames[0]);
            //Show Certificate Invalid?
            response.CancelRequest();
            state = HTTPSState.Nonsecure;
            return;
        }
    }
}