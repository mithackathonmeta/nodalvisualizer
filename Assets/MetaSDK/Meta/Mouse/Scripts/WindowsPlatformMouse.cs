﻿using UnityEngine;
using System.Runtime.InteropServices;

namespace Meta
{

    internal class WindowsPlatformMouse : IPlatformMouse
    {

        [DllImport("user32.dll")]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out Vector2Int pos);

        public void SetGlobalCursorPos(Vector2 pos)
        {
            SetCursorPos((int)pos.x, (int)pos.y);
        }

        public Vector2 GetGlobalCursorPos()
        {
            Vector2Int pos;
            GetCursorPos(out pos);
            return new Vector2(pos.X, pos.Y);
        }

    }

}