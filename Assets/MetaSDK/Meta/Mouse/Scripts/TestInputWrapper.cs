﻿using UnityEngine;
using System.Collections;

namespace Meta
{

    internal class TestInputWrapper : IInputWrapper
    {

        public Vector3 MousePosition { get; set; }

        public Vector2 MouseScrollDelta { get; set; }

        public float Axis { get; set; }

        public bool MouseButton { get; set; }

        public bool MouseButtonUp { get; set; }

        public bool MouseButtonDown { get; set; }

        public int ScreenHeight { get; set; }

        public int ScreenWidth { get; set; }

        public Rect ScreenRect { get; set; }

        public Vector3 GetMousePosition()
        {
            return MousePosition;
        }

        public Vector2 GetMouseScrollDelta()
        {
            return MousePosition;
        }

        public float GetAxis(string axisName)
        {
            return Axis;
        }

        public bool GetMouseButton(int button)
        {
            return MouseButton;
        }

        public bool GetMouseButtonUp(int button)
        {
            return MouseButtonUp;
        }

        public bool GetMouseButtonDown(int button)
        {
            return MouseButtonDown;
        }

        public int GetScreenHeight()
        {
            return ScreenHeight;
        }

        public int GetScreenWidth()
        {
            return ScreenWidth;
        }

        public Rect GetScreenRect()
        {
            return ScreenRect;
        }

    }

}