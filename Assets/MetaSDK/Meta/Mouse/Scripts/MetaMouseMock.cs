﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Reflection;

namespace Meta
{

    [System.Serializable]
    internal class MetaMouseMock
    {

        [SerializeField]
        PlayState _playState;

        [SerializeField]
        private string _playbackName = null;

        [SerializeField]
        private string _recordingName = null;

        private InputWrapperRecorder _inputWrapperRecorder;

        private PlaybackInputWrapper _playbackInputWrapper;

        private enum PlayState
        {
            Editor,
            EditorPlayback,
            EditorRecord,
            BuiltGame,
        }

        private string recordingDir
        {
            get
            {
                if(Assembly.GetAssembly(GetType()).GetType("MetaInternal", false, true) == null)
                {
                    return Application.dataPath + "/Meta/Mouse/Recordings/";
                }
                else
                {
                    return Application.dataPath + "/MetaInternal/Tests/MouseRecordings/";
                }
            }
        }

        private string playbackRecordingPath
        {
            get { return recordingDir + _playbackName + ".mouserecord"; }
        }

        public string recordingName
        {
            get { return _recordingName; }
        }

        public void Init(MetaMouseConfig metaMouseConfig, out IInputWrapper inputWrapper, out MetaMouse metaMouse)
        {
            if (!Application.isEditor)
            {
                _playState = PlayState.BuiltGame;
            }
            if (_playState == PlayState.EditorPlayback || _playState == PlayState.EditorRecord)
            {
                Application.targetFrameRate = 30;
            }
            switch (_playState)
            {
                case PlayState.Editor:
                    inputWrapper = new UnityInputWrapper();
                    metaMouse = new MetaMouse(metaMouseConfig, inputWrapper);
                    break;
                case PlayState.BuiltGame:
                    inputWrapper = new UnityInputWrapper();
                    metaMouse = new MetaMouse(metaMouseConfig, inputWrapper);
                    break;
                case PlayState.EditorPlayback:
                    if (string.IsNullOrEmpty(_playbackName))
                    {
                        Debug.LogError("PlaybackName is empty.");
                    }
                    BinaryReader reader = new BinaryReader(File.Open(playbackRecordingPath, FileMode.Open));
                    _playbackInputWrapper = new PlaybackInputWrapper(reader);
                    inputWrapper = (IInputWrapper)_playbackInputWrapper;
                    metaMouse = new MetaMouse(metaMouseConfig, inputWrapper);
                    metaMouseConfig.sensitivity = _playbackInputWrapper.MouseSensitivity;
                    break;
                case PlayState.EditorRecord:
                    if (string.IsNullOrEmpty(recordingName))
                    {
                        Debug.LogError("RecordingName name is empty.");
                    }
                    inputWrapper = new UnityInputWrapper();
                    if (!Directory.Exists(recordingDir))
                    {
                        Directory.CreateDirectory(recordingDir);
                    }
                    BinaryWriter writer = new BinaryWriter(File.Open(recordingDir + recordingName + ".mouserecord", FileMode.Create));
                    _inputWrapperRecorder = new InputWrapperRecorder(writer, inputWrapper, metaMouseConfig);
                    metaMouse = new MetaMouse(metaMouseConfig, inputWrapper);
                    break;
                default:
                    inputWrapper = new UnityInputWrapper();
                    metaMouse = new MetaMouse(metaMouseConfig, inputWrapper);
                    break;
            }
        }

        public bool UpdateRecordingAndPlayback(Vector3 virutalPointerPos)
        {
            if (_playState == PlayState.EditorRecord)
            {
                _inputWrapperRecorder.Record(virutalPointerPos);
            }
            else if (_playState == PlayState.EditorPlayback)
            {
                if (_playbackInputWrapper.framesRead >= _playbackInputWrapper.numFrames)
                {
                    return false;
                }
                else
                {
                    _playbackInputWrapper.ReadFrame();
                }
            }
            return true;
        }

        public void FinalizeRecordingAndPlayback()
        {
            FinalizeRecording();
            FinalizePlayback();
        }

        private void FinalizePlayback()
        {
            if (_playbackInputWrapper != null)
            {
                _playbackInputWrapper.CloseFile();
                _playbackInputWrapper = null;
            }
        }

        private void FinalizeRecording()
        {
            if (_inputWrapperRecorder != null)
            {
                _inputWrapperRecorder.CloseFile();
                _inputWrapperRecorder = null;
            }
        }

    }

}
