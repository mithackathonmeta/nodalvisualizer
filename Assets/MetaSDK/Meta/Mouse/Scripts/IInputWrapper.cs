﻿using UnityEngine;


namespace Meta
{

    /// <summary>
    /// Interface for wrapper around input
    /// </summary>
    internal interface IInputWrapper
    {

        /// <summary>
        /// Gets the local mouse position relative to the Unity game window
        /// </summary>
        Vector3 GetMousePosition();

        /// <summary>
        /// Gets scrolling of mouse wheel
        /// </summary>
        Vector2 GetMouseScrollDelta();

        /// <summary>
        /// Gets the movement of a mouse axis
        /// </summary>
        float GetAxis(string axisName);

        /// <summary>
        /// Gets whether the mouse button is currently pressed
        /// </summary>
        bool GetMouseButton(int button);

        /// <summary>
        /// Gets whether a mouse button was unclicked that frame
        /// </summary>
        bool GetMouseButtonUp(int button);

        /// <summary>
        /// Gets whether a mouse button was clicked that frame
        /// </summary>
        bool GetMouseButtonDown(int button);

        /// <summary>
        /// Gets the rect of the Unity game window in pixels
        /// </summary>
        Rect GetScreenRect();

    }

}