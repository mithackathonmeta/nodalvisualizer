﻿using UnityEngine;

namespace Meta
{

    internal interface IPlatformMouse
    {

        void SetGlobalCursorPos(Vector2 pos);

        Vector2 GetGlobalCursorPos();

    }

}