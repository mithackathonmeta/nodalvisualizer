﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Meta
{
    /// <summary>
    /// Extends the built-in StandAloneInputModule to process events for
    /// MetaMouse and offer ability to play and record through MetaMouseMock.
    /// </summary>
    [RequireComponent(typeof(EventSystem))]
    internal class MetaMouseInputModule : StandaloneInputModule
    {

        [SerializeField]
        private MetaMouseConfig _metaMouseConfig = null;

        [SerializeField]
        private MetaMouseMock _metaMouseMock = null;

        private MouseState _mouseStateRecycable = new MouseState();

        private IInputWrapper _inputWrapper;

        private MetaMouse _metaMouse;

        private MetaContext _metaContext;

        /// <summary>
        /// Whether the cursor state has been registered.
        /// </summary>
        private bool _cursorStateRegistered = false;

        /// <summary>
        /// Whether the MetaMouse has started.
        /// </summary>
        private bool _hasStarted = false;


        /// <summary>
        /// Called on play
        /// </summary>
        protected override void OnEnable()
        {

            MouseStart();
            base.OnEnable(); //important: will not work without this.
        }

        /// <summary>
        /// Not called on play
        /// </summary>
        protected override void OnDisable()
        {
            MouseHalt();
            base.OnDisable();
        }

        protected override void Awake()
        {
            MouseStart();
            base.Awake();
        }

        protected void Start()
        {
            //This must occur after the metacontext has been created, hence not in 'Awake' where contention could occur.
            _metaContext = Object.FindObjectOfType<MetaManager>().metaContext;
            //Change the metaContext's default cursor state so that other users of the mouse (i.e  MouseLocalizer) may use it if needed when releasing the mouse
            MouseStart();
        }

        private void LateUpdate()
        {
            _metaMouseMock.UpdateRecordingAndPlayback(_metaMouseConfig.cursorTransform.position);
            if (ShouldProcessMouse())
            {
                _metaMouse.Process();
            }

            _metaMouse.Process();
        }

        protected override void OnDestroy()
        {
            MouseHalt();
            base.OnDestroy();
        }

        /// Copied from PointerInputModule and altered to use _inputWrapper
        /// and forward information to _metaMouse. If issues ever arise in 
        /// this method, check for a new version of this method on Unity's 
        /// UI bitbucket repo. This is called from inside the inherited 
        /// PointerInputModule.
        protected override MouseState GetMousePointerEventData(int id)
        {
            // Populate the left button...
            PointerEventData leftData;
            var created = GetPointerData(kMouseLeftId, out leftData, true);

            leftData.Reset();

            if (created)
                leftData.position = _metaMouse.screenPosition;

            Vector2 pos = _metaMouse.screenPosition;
            leftData.delta = pos - leftData.position;
            leftData.position = pos;
            leftData.scrollDelta = _inputWrapper.GetMouseScrollDelta();
            leftData.button = PointerEventData.InputButton.Left;
            eventSystem.RaycastAll(leftData, m_RaycastResultCache);
            var raycast = FindFirstRaycast(m_RaycastResultCache);
            leftData.pointerCurrentRaycast = raycast;

            _metaMouse.raycastHit = raycast.isValid;
            _metaMouse.raycastDistance = raycast.distance;

            m_RaycastResultCache.Clear();

            // copy the apropriate data into right and middle slots
            PointerEventData rightData;
            GetPointerData(kMouseRightId, out rightData, true);
            CopyFromTo(leftData, rightData);
            rightData.button = PointerEventData.InputButton.Right;

            PointerEventData middleData;
            GetPointerData(kMouseMiddleId, out middleData, true);
            CopyFromTo(leftData, middleData);
            middleData.button = PointerEventData.InputButton.Middle;

            _mouseStateRecycable.SetButtonState(PointerEventData.InputButton.Left, StateForMouseButton(0), leftData);
            _mouseStateRecycable.SetButtonState(PointerEventData.InputButton.Right, StateForMouseButton(1), rightData);
            _mouseStateRecycable.SetButtonState(PointerEventData.InputButton.Middle, StateForMouseButton(2), middleData);

            return _mouseStateRecycable;
        }

        protected new PointerEventData.FramePressState StateForMouseButton(int buttonId)
        {
            var pressed = _inputWrapper.GetMouseButtonDown(buttonId);
            var released = _inputWrapper.GetMouseButtonUp(buttonId);
            if (pressed && released)
                return PointerEventData.FramePressState.PressedAndReleased;
            if (pressed)
                return PointerEventData.FramePressState.Pressed;
            if (released)
                return PointerEventData.FramePressState.Released;
            return PointerEventData.FramePressState.NotChanged;
        }

        /// <summary>
        /// Whether processing of the mouse should occur. 
        /// </summary>
        /// <returns></returns>
        private bool ShouldProcessMouse()
        {
            GameObject cursorGameObject = _metaMouseConfig.cursorTransform.gameObject;
            System.Type localizerType = _metaContext.Get<MetaLocalization>().GetLocalizer().GetType();
            //When the MouseLocalizer is being used and holding right-mouse, the MetaMouse should not be visible. 
            if (Input.GetKey(KeyCode.Mouse1) && localizerType == typeof(MouseLocalizer))
            {
                if (cursorGameObject.activeSelf)
                {
                    cursorGameObject.SetActive(false);
                }
                return false;
            }

            if (Input.GetKeyUp(KeyCode.Mouse1) && localizerType == typeof(MouseLocalizer))
            {
                cursorGameObject.SetActive(true);
            }
            return true;
        }

        /// <summary>
        /// Change the metacontext's cursor release behaviour.
        /// The cursor should not be released because the MetaMouse is
        /// using it.
        /// </summary>
        private void RegisterReleaseCursorState()
        {
            Debug.Log("Adding cursor state");
            if (_metaContext != null && !_cursorStateRegistered)
            {
                _metaContext.Add<CursorState>(new CursorState { IsCursorVisible = false, CursorLockMode = CursorLockMode.Locked });
                
                _cursorStateRegistered = true;
            }
        }

        /// <summary>
        /// Change the metacontext's cursor release behaviour.
        /// The cursor should be released.
        /// </summary>
        private void UnregisterReleaseCursorState()
        {
            if (_metaContext != null && _cursorStateRegistered)
            {
                //Change the default cursor state such that other users of the mouse (i.e  MouseLocalizer) may use it if needed when releasing the mouse.
                _metaContext.Add<CursorState>(new CursorState { IsCursorVisible = true, CursorLockMode = CursorLockMode.None });
                _cursorStateRegistered = false;
            }
        }

        /// <summary>
        /// Start the MetaMouse
        /// </summary>
        private void MouseStart()
        {
            if (!_hasStarted)
            {
                _metaMouseConfig.cursorTransform.gameObject.SetActive(true);
                if (_metaMouseConfig.eventCamera == null)
                {
                    _metaMouseConfig.eventCamera = Camera.main;
                }
                _metaMouseMock.Init(_metaMouseConfig, out _inputWrapper, out _metaMouse);
                //GameViewMetaMouse gameViewMetaMouse = (GameViewMetaMouse)_metaMouse;
               //gameViewMetaMouse

                _hasStarted = true;
            }
            RegisterReleaseCursorState();
        }

        /// <summary>
        /// Halt the MetaMouse
        /// </summary>
        private void MouseHalt()
        {
            if (_hasStarted)
            {
                _metaMouseMock.FinalizeRecordingAndPlayback();
                //If destroyed at the end of a simulation, the metaContext could have been destroyed beforehand.
                
            }
            UnregisterReleaseCursorState();
        }

    }

}