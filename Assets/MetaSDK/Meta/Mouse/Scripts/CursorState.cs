﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This data structure is used by the MetaMouse to store state 
/// information, which is used by the MouseLocalizer.
/// </summary>
public struct CursorState
{
    public CursorLockMode CursorLockMode;
    public bool IsCursorVisible;
}
