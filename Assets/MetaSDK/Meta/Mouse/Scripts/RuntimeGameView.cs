﻿using UnityEngine;
using System;

namespace Meta
{

    internal class RuntimeGameView
    {
        public event Action onPointerEnter;

        public event Action onPointerExit;

        public event Action onPointerHover;

        private readonly IPlatformMouse _platformMouse;

        private readonly IInputWrapper _inputWrapper;

        private bool _priorPointerInGameView;

        private Rect _globalGameViewRect;

        /// <summary>
        /// The rect of the RuntimeGameView in global, system, coordinates.
        /// </summary>
        public Rect GlobalGameViewRect
        {
            get { return _globalGameViewRect; }
        }

        public RuntimeGameView(IInputWrapper inputWrapper)
        {
            _inputWrapper = inputWrapper;
            _platformMouse = PlatformMouseFactory.GetPlatformMouse();
            _priorPointerInGameView = true;
            //Must be locked initially because CheckVisibility relies on cursor being inside
            //game view intiaily calculate proper globalGameViewRect.
            Cursor.lockState = CursorLockMode.Locked;
        }

        /// <summary>
        /// Runs logic to determine pointer enter, exit and hover events.
        /// </summary>
        public void ProcessEvents(Vector2 pointerPosition)
        {
            bool currentCursorInGameView = false;
            if (_priorPointerInGameView)
            {
                currentCursorInGameView = _inputWrapper.GetScreenRect().Contains(pointerPosition);
                CalculateGlobalGameViewRect();
            }
            else
            {
                //This is how you determine if the mouse is hovering over the game view
                //and what the rect is of the render window in the game view. Unfortunately
                //there is no way to determine the mouse position inside the game view as well.
                //However this may still turn to be useful in a more ideal way in the future.

                //Assembly assembly = typeof(EditorWindow).Assembly;
                //Type type = assembly.GetType("UnityEditor.RuntimeGameView");
                //EditorWindow gameview = EditorWindow.GetWindow(type);
                //MethodInfo gameViewRenderRectMethod = type.GetMethod("GetMainGameViewRenderRect", BindingFlags.NonPublic | BindingFlags.Static);
                //Rect gameViewRenderRect = (Rect)gameViewRenderRectMethod.Invoke(null, null);
                //if (EditorWindow.mouseOverWindow.GetType() == type)
                //{
                //    currentCursorInGameView = true;
                //}

                currentCursorInGameView = PointerInGameView();
            }
            if (currentCursorInGameView && !_priorPointerInGameView)
            {
                if (onPointerEnter != null)
                {
                    onPointerEnter();
                }
                _priorPointerInGameView = true;
            }
            else if (currentCursorInGameView && _priorPointerInGameView)
            {
                if (onPointerHover != null)
                {
                    onPointerHover();
                }
            }
            else if (!currentCursorInGameView && _priorPointerInGameView)
            {
                if (onPointerExit != null)
                {
                    onPointerExit();
                }
                _priorPointerInGameView = false;
            }
            else if (!currentCursorInGameView && !_priorPointerInGameView)
            {
               
            }
        }

        /// <summary>
        /// Set the pointer position relative to the RuntimeGameView.
        /// </summary>
        public void SetGameViewPointerPos(Vector2 position)
        {
            position.x += _globalGameViewRect.x;
            position.y = (_globalGameViewRect.height - position.y) + _globalGameViewRect.y;
            _platformMouse.SetGlobalCursorPos(position);
        }

        /// <summary>
        /// Get the pointer position relative to the RuntimeGameView.
        /// </summary>
        public Vector2 GetGameViewPointerPos()
        {
            Vector2 pos = new Vector2();
            Vector2 globalPosition = _platformMouse.GetGlobalCursorPos();
            pos.x = globalPosition.x - _globalGameViewRect.x;
            pos.y = _globalGameViewRect.height - (globalPosition.y - _globalGameViewRect.y);
            return pos;
        }

        /// <summary>
        /// Determines whether or not pointer is in RuntimeGameView.
        /// </summary>
        private bool PointerInGameView()
        {
            return _globalGameViewRect.Contains(_platformMouse.GetGlobalCursorPos());
        }

        /// <summary>
        /// Calculates the global game view rect.
        /// </summary>
        /// <remarks>
        /// This works on the assumption that GetGlobalCursorPos and _inputWrapper.GetMousePosition()
        /// are on the exact same point on the screen, just one in global space and the other in local.
        /// Thus _globalScreenRect must be stored as a class variable as it can only be calculated when
        /// the point is inside the scene view. This does mean that if the pointer never enters the scene
        /// view then it cannot properly calculate, this is alleviated by starting the scene with the cursor
        /// locked which will initially force the pointer to the center of the scene view.
        /// </remarks>
        private void CalculateGlobalGameViewRect()
        {
            Vector2 globalPosition = _platformMouse.GetGlobalCursorPos();
            Vector2 localPosition = _inputWrapper.GetMousePosition();
            _globalGameViewRect.x = globalPosition.x - localPosition.x;
            _globalGameViewRect.y = globalPosition.y - (_inputWrapper.GetScreenRect().height - localPosition.y);
            _globalGameViewRect.width = _inputWrapper.GetScreenRect().width;
            _globalGameViewRect.height = _inputWrapper.GetScreenRect().height;
        }

    }

}