﻿using UnityEngine;

namespace Meta
{

    internal class UnityInputWrapper : IInputWrapper
    {

        public Vector3 GetMousePosition()
        {
            return Input.mousePosition;
        }

        public Vector2 GetMouseScrollDelta()
        {
            return Input.mouseScrollDelta;
        }

        public float GetAxis(string axisName)
        {
            return Input.GetAxis(axisName);
        }

        public bool GetMouseButton(int button)
        {
            return Input.GetMouseButton(button);
        }

        public bool GetMouseButtonUp(int button)
        {
            return Input.GetMouseButtonUp(button);
        }

        public bool GetMouseButtonDown(int button)
        {
            return Input.GetMouseButtonDown(button);
        }

        public Rect GetScreenRect()
        {
            return new Rect(0, 0, Screen.width, Screen.height);
        }

    }

}