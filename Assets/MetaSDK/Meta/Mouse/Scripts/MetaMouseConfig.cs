﻿using UnityEngine;
using System.Collections;

namespace Meta
{

    [System.Serializable]
    public class MetaMouseConfig
    {

        [SerializeField]
        private Camera _eventCamera;

        [SerializeField]
        private Transform _cursorTransform = null;

        [SerializeField]
        private Material _cursorMaterial = null;

        [SerializeField]
        private float _sensitivity = 1.0f;

        [SerializeField]
        private Color _normalColor = new Color(234 / 255f, 138 / 255f, 128 / 255f, 1f);

        [SerializeField]
        private Color _highlightedColor = new Color(241 / 255f, 90 / 255f, 36 / 255f, 1f);

        [SerializeField]
        private Color _pressedColor = new Color(1f, 45 / 255f, 0f, 1f);

        [Tooltip("Distance pointer will float away from camera when not hovering over an item.")]
        [SerializeField]
        private float _floatDistance = 100f;

        [Tooltip("Whether the cursor should be locked onto the screen when the Meta2 rotates.")]
        [SerializeField]
        private bool _lockCursor;

        public bool LockCursor
        {
            get {return _lockCursor; }
            private set { _lockCursor = value; }
        }

        [Tooltip("Damping on depth changes of pointer.")]
        [SerializeField]
        private float _distanceDamp = .2f;

        public Camera eventCamera
        {
            get { return _eventCamera; }
            set { _eventCamera = value; }
        }

        public Transform cursorTransform
        {
            get { return _cursorTransform; }
        }

        public Material cursorMaterial
        {
            get { return _cursorMaterial; }
        }

        public float floatDistance
        {
            get { return _floatDistance; }
        }

        public float distanceDamp
        {
            get { return _distanceDamp; }
        }

        public float sensitivity
        {
            get { return _sensitivity; }
            set { _sensitivity = value; }
        }

        public Color normalColor
        {
            get { return _normalColor; }
        }

        public Color highlightedColor
        {
            get { return _highlightedColor; }
        }

        public Color pressedColor
        {
            get { return _pressedColor; }
        }

    }

}