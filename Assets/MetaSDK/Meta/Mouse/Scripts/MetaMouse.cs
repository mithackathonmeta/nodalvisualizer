﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    /// <summary>
    /// Base functionality needed for MetaMouse in actual build
    /// </summary>
    internal class MetaMouse
    {

        protected readonly IInputWrapper _inputWrapper;

        protected readonly MetaMouseConfig _metaMouseConfig;

        protected Vector3 _screenPosition;

        private readonly RuntimeGameView _gameView;

        private float _cursorDistance;

        private float _cursorDistanceDampVelocity;

        /// <summary>
        /// Distance of raycast which hit interactable component.
        /// </summary>
        public float raycastDistance { get; set; }

        /// <summary>
        /// Did this raycast hit something interactable?
        /// </summary>
        public bool raycastHit { get; set; }

        public Vector3 screenPosition { get { return _screenPosition; } }

        protected Vector3 _screenPositionDelta;

        protected bool _bIsCursorOffPaddedScreen;

        public MetaMouse(MetaMouseConfig metaMouseConfig, IInputWrapper inputWrapper)
        {
            _metaMouseConfig = metaMouseConfig;
            _inputWrapper = inputWrapper;
            _screenPosition.x = Screen.width / 2f;
            _screenPosition.y = Screen.height / 2f;
            Cursor.lockState = CursorLockMode.Locked;
            Ray startRay =
                _metaMouseConfig.eventCamera.ScreenPointToRay(new Vector3(_screenPosition.x, _screenPosition.y, 0f));
            _cursorDistance = _metaMouseConfig.floatDistance;
            Vector3 startPosition = startRay.GetPoint(_metaMouseConfig.floatDistance);
            _metaMouseConfig.cursorTransform.position = startPosition;

            _gameView = new RuntimeGameView(_inputWrapper);
            _gameView.onPointerEnter += OnGameViewEnter;
            _gameView.onPointerExit += OnGameViewExit;
            _gameView.onPointerHover += OnGameViewHover;
        }

        public void Process()
        {
            _gameView.ProcessEvents(_screenPosition);
            UpdatePointerPositionAndColor();
        }

        protected void UpdatePointerPositionAndColor()
        {
            float targetDistance = _metaMouseConfig.floatDistance;
            Camera camera = _metaMouseConfig.eventCamera;
            if (!_bIsCursorOffPaddedScreen)
            {
                Ray inputRay = camera.ScreenPointToRay(_screenPosition);
                if (raycastHit)
                {
                    targetDistance = raycastDistance;
                    _metaMouseConfig.cursorMaterial.color = Input.GetMouseButton(0) ? _metaMouseConfig.pressedColor : _metaMouseConfig.highlightedColor;
                }
                else
                {
                    _metaMouseConfig.cursorMaterial.color = Input.GetMouseButton(0) ? _metaMouseConfig.pressedColor : _metaMouseConfig.normalColor;
                }
                _cursorDistance = Mathf.SmoothDamp(_cursorDistance, targetDistance, ref _cursorDistanceDampVelocity, _metaMouseConfig.distanceDamp);
                _metaMouseConfig.cursorTransform.transform.position = inputRay.GetPoint(_cursorDistance);
            }
            
            _metaMouseConfig.cursorTransform.transform.rotation = camera.transform.rotation;
        }

        protected void ProcessAxisInput()
        {
            float inputX = _inputWrapper.GetAxis("Mouse X") * _metaMouseConfig.sensitivity;
            float inputY = _inputWrapper.GetAxis("Mouse Y") * _metaMouseConfig.sensitivity;
            _screenPositionDelta = new Vector3(inputX, inputY, 0f);
        }

        protected void HandleCursorScreenBehaviour()
        {
            Camera camera = _metaMouseConfig.eventCamera;
            Vector3 cursorPos = _metaMouseConfig.cursorTransform.position;
            Vector3 screenPos = _screenPosition;
            var oldOffScreenFlag = _bIsCursorOffPaddedScreen;

            var projectedPos = camera.WorldToScreenPoint(cursorPos);
            //Work out the new screen position of the cursor depending on which mode lock-mode is used.
            if (_metaMouseConfig.LockCursor || (IsOffPaddedScreen(screenPos, 1.5f) && IsOffPaddedScreen(projectedPos, 1.5f)))
            {
                screenPos += _screenPositionDelta;
            }
            else
            {
                screenPos = projectedPos + _screenPositionDelta;
                screenPos = new Vector3(Mathf.Clamp(screenPos.x, 1, camera.pixelWidth - 1), Mathf.Clamp(screenPos.y, 1, camera.pixelHeight - 1), 0f);
            }

            //Hard-exited the screen through movement of the mouse
            if (!_bIsCursorOffPaddedScreen && IsOffPaddedScreen(screenPos + _screenPositionDelta, 0))
            {
                Cursor.lockState = CursorLockMode.None;
            }

            _bIsCursorOffPaddedScreen = IsOffPaddedScreen(screenPos, 1);

            //The is-cursor-hidden flag has changed has changed since last frame.
            if (_bIsCursorOffPaddedScreen != oldOffScreenFlag)
            {
                _metaMouseConfig.cursorTransform.gameObject.SetActive(!_bIsCursorOffPaddedScreen);
            }
            _screenPosition = screenPos;
        }

        protected bool IsOffPaddedScreen(Vector3 vec, float pad)
        {
            Camera camera = _metaMouseConfig.eventCamera;
            return (vec.x <= pad || vec.x >= (camera.pixelWidth - pad) || vec.y <= pad || vec.y >= (camera.pixelHeight - pad) || vec.z < 0f);
        }

        private void OnGameViewEnter()
        {
            _screenPosition = _gameView.GetGameViewPointerPos();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            _metaMouseConfig.cursorTransform.gameObject.SetActive(true);
        }

        private void OnGameViewExit()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            _gameView.SetGameViewPointerPos(_screenPosition);
            _metaMouseConfig.cursorTransform.gameObject.SetActive(false);
        }

        private void OnGameViewHover()
        {
            ProcessAxisInput();
            HandleCursorScreenBehaviour();
        }
    }


}