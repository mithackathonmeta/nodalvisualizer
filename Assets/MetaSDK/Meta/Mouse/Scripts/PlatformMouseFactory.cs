﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta
{

    static internal class PlatformMouseFactory
    {
       
        static public IPlatformMouse GetPlatformMouse()
        {
            if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.WindowsPlayer)
            {
                return new WindowsPlatformMouse();
            }
            throw new PlatformNotSupportedException("Unsupported platform for MetaMouse");
        }

    }

}