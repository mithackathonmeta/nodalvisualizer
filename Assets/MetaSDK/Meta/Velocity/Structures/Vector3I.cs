﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Integer equivalent of Unity's floating point Vector3.
/// </summary>
public struct Vector3I : IEquatable<Vector3I> {

    public int x;
    public int y;
    public int z;
    internal static readonly Vector3I zero = new Vector3I(0,0,0);

    public Vector3I(int _x, int _y, int _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    public static int RountToInt(float f)
    {
        if (f >= 0)
        {
            return (int)(f + 0.5f);
        }
        else
        {
            return (int)(f - 0.5f);
        }
    }

    public static Vector3I Round(Vector3 v)
    {
        return new Vector3I(RountToInt(v.x), RountToInt(v.y), RountToInt(v.z));

        // Oddly slow:
        //return new Vector3I(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
    }

    public IEnumerable<Vector3I> EnumerateTaxiCabNeighbors()
    {
        yield return new Vector3I(x, y, z+1);
        yield return new Vector3I(x, y, z-1);

        yield return new Vector3I(x, y+1, z);
        yield return new Vector3I(x, y-1, z);

        yield return new Vector3I(x+1, y, z);
        yield return new Vector3I(x-1, y, z);
    }

    public static Vector3 operator*(Vector3I vi, Vector3 vf)
    {
        return new Vector3(vi.x * vf.x, vi.y * vf.y, vi.z * vf.z);
    }

    public static Vector3 operator *(Vector3I vi, float f)
    {
        return new Vector3(vi.x * f, vi.y * f, vi.z * f);
    }

    public override string ToString()
    {
        return string.Format("[v3i {0} {1} {2}]", x, y, z);
    }

    public bool Equals(Vector3I other)
    {
        return this.x == other.x && this.y == other.y && this.z == other.z;
    }

    public static Vector3I operator +(Vector3I a, Vector3I b)
    {
        return new Vector3I(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3I operator-(Vector3I a, Vector3I b)
    {
        return new Vector3I(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static bool operator==(Vector3I a, Vector3I b)
    {
        return a.x == b.x && a.y == b.y && a.z == b.z;
    }

    public static bool operator !=(Vector3I a, Vector3I b)
    {
        return a.x != b.x || a.y != b.y || a.z != b.z;
    }

    public override bool Equals(object obj)
    {
        if (obj is Vector3I)
        {
            return this == (Vector3I)obj;
        }
        else
        {
            return base.Equals(obj);
        }
    }

    public override int GetHashCode()
    {
        const int y_shift = 10;
        const int z_shift = 20;

        return x ^ (y << y_shift) ^ (y >> (32 - y_shift)) ^ (z << z_shift) ^ (z >> (32 - z_shift));
    }

    internal Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }

    internal static Vector3I FloorOf(Vector3 vector3)
    {
        return new Vector3I((int)vector3.x, (int)vector3.y, (int)vector3.z);
    }
}
