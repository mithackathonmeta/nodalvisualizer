﻿using UnityEngine;
using System.Collections;

/// <summary>
/// An integer Bounds struct, similar to Unity's Bounds struct.
/// </summary>

public struct Bounds3I {

    public Vector3I Min;
    public Vector3I Max;

    public bool Contains(Vector3I coordinate)
    {
        return Min.x <= coordinate.x &&
               Min.y <= coordinate.y &&
               Min.z <= coordinate.z &&
               Max.x >= coordinate.x &&
               Max.y >= coordinate.y &&
               Max.z >= coordinate.z; 
    }

    public int Volume
    {
        get
        {
            Vector3I size = Max - Min;
            return size.x * size.y * size.z;
        }
    }
}
