﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// A struct that expresses an interval on the floating point number line.
/// </summary>
[Serializable]
public struct RangeF
{

    public float Min;
    public float Max;

    public RangeF(float min, float max)
    {
        Min = min;
        Max = max;
    }

    public static RangeF Create(float min, float max)
    {
        RangeF r;
        r.Min = min;
        r.Max = max;
        return r;
    }

    /// <summary>
    /// Retrieve a random value inside this range.
    ///  Uniform distribution
    /// </summary>
    public float Random
    {
        get
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }

    public void ExpandToInclude(float f)
    {
        if (f < Min)
        {
            Min = f;
        }
        else if (f > Max)
        {
            Max = f;
        }
    }

    public float Size ()
    {
        return (Max - Min);
    }

    /// <summary>
    /// Normalizes "f" to the range interval.  If "f" is inside the range, the result will be in [0,1]
    ///  If  f == Min, returns 0.
    ///  If  f == Max, returns 1.
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public float Normalize(float f)
    {
        float delta = f - Min;
        return delta / (Max - Min);
    }

    public bool Contains(float v)
    {
        return Min <= v && Max >= v;
    }

    public float Clamp(float value)
    {
        value = Mathf.Max(value, Min);
        value = Mathf.Min(value, Max);
        return value;
    }

    public override string ToString()
    {
        return "[" + Min.ToString() + "-" + Max.ToString() + "]";
    }
}

