﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Meta.Internal.Velocity
{
    /// <summary>
    /// A snapshot of the point cloud that includes Velocity field data.
    /// 
    /// This class is the output of the Velocity system, and holds the "Velocity field"
    /// </summary>
    public class PointCloudAndVelocitySnapshot : PointCloudStorageBase<VelocityAPI.PointCloudReport>
    {
        internal PointCloudAndVelocitySnapshot(PointCloudSnapshot snapshot)
        {
            Init(snapshot);
        }

        protected static string detailString(Vector3 v)
        {
            return v.ToString("G");  // magic format string!
        }

        static float timeToNextInteger(float currentValue, float speed)
        {
            if (speed == 0)
            {
                return float.MaxValue;
            }
            else if (speed > 0)
            {
                return (Mathf.Ceil(currentValue) - currentValue) / speed;
            }
            else
            {
                return (Mathf.Floor(currentValue) - currentValue) / speed;
            }
        }


        internal IEnumerable<Vector3I> EnumerateBucketCoordinatesIntersecting(Ray rayArg, float distance, float radius)
        {
            // Convert into bucket space.
            Ray rayBucketSpace = new Ray(rayArg.origin / bucketSideInRealMeters, rayArg.direction);
            distance /= bucketSideInRealMeters;

            float t = 0;
            float epsilon = 0.00001f;

            while (t < distance)
            {
                Vector3 currentPosition = rayBucketSpace.GetPoint(t);
                yield return Vector3I.Round(currentPosition);

                float timeUntilBorder = float.MaxValue;
                for (int i = 0; i < 3; ++i)
                {
                    float iTime = timeToNextInteger(currentPosition[i], rayBucketSpace.direction[i]);
                    if (iTime < timeUntilBorder)
                    {
                        timeUntilBorder = iTime;
                    }
                }

                timeUntilBorder += epsilon; // avoid rounding confusions
                t += timeUntilBorder;
            }
        }


        internal Vector3 GetScaledPointCloudVelocityAt(Vector3 unityWorldPoint)
        {
            return GetPointCloudVelocityAt(unityWorldPoint * _unityUnitsToRealMetersScale) * _realMetersToUnityUnitsScale;
        }

        internal Vector3 GetPointCloudVelocityAt(Vector3 realWorldPoint)
        {
            Vector3I bucket = this.toBucketCoordinate(realWorldPoint);
            Vector3? velocity = this.GetStoredVelocity(bucket);
            if (velocity.HasValue)
            {
                return velocity.Value;
            }
            else
            {
                return Vector3.zero;
            }
        }

        internal IEnumerable<VelocityAPI.VelocityReport> EnumerateScaledVelocities()
        {
            foreach (var data in EnumerateRealPointCloudReports())
            {
                if (data.Velocity != Vector3.zero)
                {
                    yield return new VelocityAPI.VelocityReport()
                    {
                        Velocity = data.Velocity * _realMetersToUnityUnitsScale,
                        WorldPosition = data.WorldBounds.center * _realMetersToUnityUnitsScale,
                        WhenUtc = this.snapshotTimeUtc,
                    };
                }
            }
        }
        
        internal IEnumerable<VelocityAPI.PointCloudReport> EnumerateRealPointCloudReports()
        {
            return dBucketToData.Values;
            /*

            foreach (var kvp in this.dBucketToData)
            {
                Vector3 velocity = kvp.Value.Velocity.HasValue ? kvp.Value.Velocity.Value : Vector3.zero;

                VelocityAPI.PointCloudReport report = new VelocityAPI.PointCloudReport()
                {
                    WorldBounds = this.worldBoundsFromBucketCoordinate(kvp.Key),
                    Centroid = kvp.Value.CentroidData,
                    Points = kvp.Value.Points.AsReadOnly(),
                    Velocity = velocity
                };

                // HACK HACK HACK TODO: actually report a smoothed velocity (probably cache it in the snapshot when the snapshot is created)
                report.VelocityTimeSmoothed = report.Velocity; //  this._history.CalculateTimeSmoothedVelocityAt(report.WorldBounds.center);
                yield return report;
            }

            foreach (var kvp in this.dBucketToVelocityOnlyData)
            {
                VelocityAPI.PointCloudReport report = new VelocityAPI.PointCloudReport()
                {
                    WorldBounds = this.worldBoundsFromBucketCoordinate(kvp.Key),
                    Centroid = new CentroidData(),
                    Points = null,
                    Velocity = kvp.Value,
                };

                // HACK HACK HACK TODO: actually report a smoothed velocity (probably cache it in the snapshot when the snapshot is created)
                report.VelocityTimeSmoothed = report.Velocity; // this._history.CalculateTimeSmoothedVelocityAt(report.WorldBounds.center);
                yield return report;
            }
            */
        }
        

        internal IEnumerable<VelocityAPI.PointCloudReport> EnumerateScaledPointCloudReports()
        {
            foreach (var report in EnumerateRealPointCloudReports())
            {
                var scaledReport = new VelocityAPI.PointCloudReport();
                scaledReport.Centroid = report.Centroid.ScaledBy(_realMetersToUnityUnitsScale);
                scaledReport.Velocity = report.Velocity * _realMetersToUnityUnitsScale;
                scaledReport.VelocityTimeSmoothed = report.VelocityTimeSmoothed * _realMetersToUnityUnitsScale;
                scaledReport.WorldBounds = report.WorldBounds.ScaledBy(_realMetersToUnityUnitsScale);

                if (report.Points != null)
                {
                    scaledReport.Points = report.Points.Select(vector => vector * _realMetersToUnityUnitsScale).ToList().AsReadOnly();
                }

                yield return scaledReport;
            }
        }
        /*
        internal VelocityAPI.VelocityReport GetVelocityReport(Vector3 world_position)
        {
            Vector3I bucket = toBucketCoordinate(world_position);
            Vector3? velocity = GetStoredVelocity(bucket);
            return new VelocityAPI.VelocityReport()
            {
                WorldPosition = fromBucketCoordinate(bucket),
                Velocity = velocity.HasValue ? velocity.Value : Vector3.zero,
                WhenUtc = this.snapshotTimeUtc,
            };
        }*/

        internal Nullable<Vector3> GetStoredVelocity(Vector3I bucket)
        {
            VelocityAPI.PointCloudReport data;
            if (dBucketToData.TryGetValue(bucket, out data))
            {
                return data.Velocity;
            }

            return new Nullable<Vector3>();
        }


        /// <summary>
        /// Stores the given velocity in the given bucket, IF it is larger than the current stored velocity for that bucket
        /// </summary>
        /// <param name="bucket_index"></param>
        /// <param name="velocity"></param>
        internal void StoreVelocityIfLarger(Vector3I bucket_index, Vector3 velocity)
        {
            Nullable<Vector3> storedVelocity = GetStoredVelocity(bucket_index);

            if (storedVelocity.HasValue)
            {
                if (storedVelocity.Value.sqrMagnitude > velocity.sqrMagnitude)
                {
                    return;
                }
            }

            StoreVelocity(bucket_index, velocity);
        }

        private void StoreVelocity(Vector3I bucket, Vector3 velocity)
        {
            VelocityAPI.PointCloudReport data;
            if (dBucketToData.TryGetValue(bucket, out data))
            {
                data.Velocity = velocity;
            }
            else
            {
                data = new VelocityAPI.PointCloudReport()
                {
                    Velocity = velocity,
                    WorldBounds = worldBoundsFromBucketCoordinate(bucket),
                };

                dBucketToData[bucket] = data;
                _cachedDataAsList = null;
            }
        }

        private void Init(PointCloudSnapshot snapshot)
        {
            Init(snapshot.bucketSideInRealMeters, snapshot.snapshotTimeUtc, snapshot._unityUnitsToRealMetersScale);
            FromPointCloudId = snapshot.FromPointCloudId;

            foreach( var kvp in snapshot.dBucketToData)
            {
                var pointData = kvp.Value;
                VelocityAPI.PointCloudReport report = new VelocityAPI.PointCloudReport()
                {
                    Centroid = pointData.m_CentroidData,
                    Points = pointData.m_Points.AsReadOnly(),
                    Velocity = Vector3.zero,
                    VelocityTimeSmoothed = Vector3.zero,
                    WorldBounds = worldBoundsFromBucketCoordinate(kvp.Key),
                };

                dBucketToData[kvp.Key] = report;
            }

            _cachedDataAsList = null;
        }

        public Vector3 GetLargestVelocity()
        {
            ///DEBUG
            float maxSpeedSquared = float.MinValue;
            Vector3 largestVelocity = Vector3.zero;

            foreach (var report in this.dBucketToData.Values)
            {
                float magnitudeSquared = report.Velocity.sqrMagnitude;
                if (magnitudeSquared > maxSpeedSquared)
                {
                    maxSpeedSquared = magnitudeSquared;
                    largestVelocity = report.Velocity;
                }
            }

            return largestVelocity;
        }


        private static VelocitySystemSettings Settings { get { return VelocitySystemSettings.Global; } }


    }


}