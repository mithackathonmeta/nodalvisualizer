﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Meta.Internal.Velocity
{

    /// <summary>
    /// Debugging monobehaviour that draws the velocity field in the Unity Scene view.
    /// The collider should be a trigger, and it defines the region in which we'll draw the field.
    /// </summary>
    public class DrawVelocityInRegion : MonoBehaviour, VelocityAPI.IPointCloudListener
    {
        new public Collider collider;
        public float VelocityLineScale = 0.1f;
        public Color LineColor = Color.red;
        public Color OriginColor = Color.blue;
        public float OriginFraction = 0.2f;
        public bool LogLargestVelocity = false;

        private List<VelocityAPI.PointCloudReport> _reportsToDraw = new List<VelocityAPI.PointCloudReport>();

        public void OnPointCloudUpdate(IEnumerable<VelocityAPI.PointCloudReport> reports)
        {
            _reportsToDraw.Clear();
            _reportsToDraw.AddRange(reports);
        }

        void Start()
        {
            VelocityAPI.AddPointCloudListener(this, collider, false);
        }

        void Update()
        {
            float maxSpeedSquared = float.MinValue;
            Vector3 largestVelocity = Vector3.zero;

            foreach (var report in _reportsToDraw)
            {
                Vector3 start = report.WorldBounds.center;
                Vector3 end = start + report.Velocity * VelocityLineScale;

                Debug.DrawLine(start, end, LineColor);
                Debug.DrawLine(start, Vector3.Lerp(start, end, OriginFraction), OriginColor);

                float magnitudeSquared = report.Velocity.sqrMagnitude;
                if (magnitudeSquared > maxSpeedSquared)
                {
                    maxSpeedSquared = magnitudeSquared;
                    largestVelocity = report.Velocity;
                }
            }

            if (LogLargestVelocity && _reportsToDraw.Count > 0)
            {
                Debug.LogFormat("mag={1} BiggestV={0}", largestVelocity.ToString("G"), largestVelocity.magnitude);
            }
        }
    }
}