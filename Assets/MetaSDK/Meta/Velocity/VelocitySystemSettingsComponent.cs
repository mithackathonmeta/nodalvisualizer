﻿using UnityEngine;

/// <summary>
/// MonoBehaviour that allows devs to control velocity system settings on a per-scene basis, and in the inspector in real time.
/// 
/// TODO:  Make sure things don't break when there is more than one of these in a single scene simultaneously.
/// </summary>
public class VelocitySystemSettingsComponent : MonoBehaviour {

    public VelocitySystemSettings Settings = new VelocitySystemSettings();

	void Update () {
        VelocitySystemSettings.Global = Settings;
	}
}
