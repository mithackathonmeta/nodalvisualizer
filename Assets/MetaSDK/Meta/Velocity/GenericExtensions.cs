﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Meta
{

    /// <summary>
    /// Lots of extension functions developed to make the Velocity system code easier.
    /// TODO:  Move these extension functions into more appropriate and better organized locations.
    /// </summary>
    public static class GenericExtensions
    {
        public const float Sqrt2 = 1.41421356237f;
        public const float Sqrt3 = 1.73205080757f;


        public static bool ContainsPoint(this Collider collider, Vector3 world_point)
        {
            if (world_point.IsNaN())
            {
                //If the point is not valid, ContainsPoint will say that the point is in the collider
                return false;
            }

            //var temp = collider.ClosestPointOnBounds(world_point);

            //if (collider.ClosestPointOnBounds(world_point) != world_point)
            //{
            //    // Point outside world axis aligned bounding box.
            //    return false;
            //}

            SphereCollider sphere = collider as SphereCollider;

            if (sphere)
            {
                return ContainsPoint(sphere, world_point);
            }

            BoxCollider box = collider as BoxCollider;

            if (box)
            {
                return ContainsPoint(box, world_point);
            }

            // Unsolved collider type.
            return true;
        }

        public static bool ContainsPoint(this SphereCollider collider, Vector3 world_point)
        {
            // Could be faster.
            Vector3 local_point = collider.transform.InverseTransformPoint(world_point);
            float radius_squared = collider.radius * collider.radius;
            return local_point.sqrMagnitude <= radius_squared;
        }

        public static bool ContainsPoint(this BoxCollider collider, Vector3 world_point)
        {
            // Could be faster.
            Vector3 local_point = collider.transform.InverseTransformPoint(world_point);
            Bounds local_box = new Bounds(collider.center, collider.size);
            return local_box.Contains(local_point);
        }


        public static IEnumerable<Vector3> ToVector3s(this IEnumerable<float> floats)
        {
            var floats_enumerator = floats.GetEnumerator();
            while (true)
            {
                if (floats_enumerator.MoveNext())
                {
                    float x = floats_enumerator.Current;
                    if (floats_enumerator.MoveNext())
                    {
                        float y = floats_enumerator.Current;
                        if (floats_enumerator.MoveNext())
                        {
                            float z = floats_enumerator.Current;
                            yield return new Vector3(x, y, z);
                        }
                        else
                        {
                            yield break;
                        }
                    }
                    else
                    {
                        yield break;
                    }
                }
                else
                {
                    yield break;
                }
            }
        }

        public static int AddRange<T>(this HashSet<T> set, IEnumerable<T> stuff_to_add)
        {
            int added = 0;
            foreach (T t in stuff_to_add)
            {
                if (set.Add(t))
                {
                    ++added;
                }
            }

            return added;
        }

        /// <summary>
        /// Returns the element-wise max of two Vector3's.
        /// For example, the Max of {1,2,3} and {3,2,1} is {3,2,3}
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector3 VectorMax(this Vector3 a, Vector3 b)
        {
            return new Vector3(Mathf.Max(a.x, b.x), Mathf.Max(a.y, b.y), Mathf.Max(a.z, b.z));
        }

        /// <summary>
        /// Returns the element-wise min of two Vector3's.
        /// For example, the Min of {1,2,3} and {3,2,1} is {1,2,1}
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector3 VectorMin(this Vector3 a, Vector3 b)
        {
            return new Vector3(Mathf.Min(a.x, b.x), Mathf.Min(a.y, b.y), Mathf.Min(a.z, b.z));
        }

        public struct OrientedBoundingBox
        {
            public Vector3 halfSize;
            public Vector3 position;
            public Vector3[] basis;
        }

        public static float dot(this Vector3 a, Vector3 b)
        {
            return Vector3.Dot(a, b);
        }

        public static Vector3 ScaledBy(this Vector3 a, Vector3 b)
        {
            a.Scale(b);
            return a;
        }

        [System.ThreadStatic]
        private static float[,] _R = new float[3, 3];
        [System.ThreadStatic]
        private static float[,] _Rabs = new float[3, 3];


        public static OrientedBoundingBox toOBB(this Bounds worldBounds)
        {
            GenericExtensions.OrientedBoundingBox obb_world_bounds = new GenericExtensions.OrientedBoundingBox()
            {
                basis = new Vector3[] { new Vector3(1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, 1) },
                position = worldBounds.center,
                halfSize = worldBounds.extents,
            };

            return obb_world_bounds;
        }

        public static OrientedBoundingBox toOBB(this BoxCollider box)
        {
            Transform t = box.transform;

            // How to calculate the extents in my basis?
            //  
            // Need extents IN terms of basis vectors!!!!

            OrientedBoundingBox obb = new OrientedBoundingBox()
            {
                basis = new Vector3[] { new Vector3(1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, 1) },
                position = t.TransformPoint(box.center),
                halfSize = t.lossyScale.ScaledBy(box.size / 2),
            };

            for (int i = 0; i < obb.basis.Length; ++i)
            {
                obb.basis[i] = t.TransformDirection(obb.basis[i]);
            }

            return obb;
        }

        public static bool Overlaps(this BoxCollider a, BoxCollider b)
        {
            return OverlapBoxes(a.toOBB(), b.toOBB());
        }

        /// <summary>
        /// Tests two oriented boxes for overlap.
        /// Based on algorithm found at http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool OverlapBoxes(this OrientedBoundingBox a, OrientedBoundingBox b)
        {
            Assert.IsTrue(a.basis != null);
            Assert.IsTrue(a.basis.Length == 3);
            Assert.IsTrue(b.basis != null);
            Assert.IsTrue(b.basis.Length == 3);

            // Position offset in global frame
            Vector3 worldPositionOffset = b.position - a.position;

            // Position offset in A's frame
            Vector3 T = new Vector3(worldPositionOffset.dot(a.basis[0]), worldPositionOffset.dot(a.basis[1]), worldPositionOffset.dot(a.basis[2]));
            Vector3 TAbs = new Vector3(Mathf.Abs(T.x), Mathf.Abs(T.y), Mathf.Abs(T.z));

            float[,] R = GenericExtensions._R;
            float[,] Rabs = GenericExtensions._Rabs;

            // B's basis in A's frame is _R
            for (int i = 0; i < 3; ++i)
            {
                for (int k = 0; k < 3; ++k)
                {
                    R[i, k] = a.basis[i].dot(b.basis[k]);
                    Rabs[i, k] = Mathf.Abs(R[i, k]);
                }
            }

            float ra;
            float rb;
            float t;

            for (int i = 0; i < 3; ++i)
            {
                // Test for separating axes with A basis vectors
                ra = a.halfSize[i];
                rb = b.halfSize[0] * Rabs[i, 0] + b.halfSize[1] * Rabs[i, 1] + b.halfSize[2] * Rabs[i, 2];
                t = TAbs[i];

                if (t > ra + rb)
                {
                    return false;
                }
            }

            for (int k = 0; k < 3; ++k)
            {
                // Test for separating axes with B basis vectors
                ra = a.halfSize[0] * Rabs[0, k] + a.halfSize[1] * Rabs[1, k] + a.halfSize[2] * Rabs[2, k];
                rb = b.halfSize[k];
                t = Mathf.Abs(T[0] * R[0, k] + T[1] * R[1, k] + T[2] * R[2, k]);

                if (t > ra + rb)
                {
                    return false;
                }
            }

            // Cross products

            // L = A0xB0
            ra = a.halfSize[1] * Rabs[2, 0] + a.halfSize[2] * Rabs[1, 0];
            rb = b.halfSize[1] * Rabs[0, 2] + b.halfSize[2] * Rabs[0, 1];
            t = Mathf.Abs(T[2] * R[1, 0] - T[1] * R[2, 0]);

            if (t > ra + rb)
            {
                return false;
            }

            // L = A0xB1
            ra = a.halfSize[1] * Rabs[2, 1] + a.halfSize[2] * Rabs[1, 1];
            rb = b.halfSize[0] * Rabs[0, 2] + b.halfSize[2] * Rabs[0, 0];
            t = Mathf.Abs(T[2] * R[1, 1] - T[1] * R[2, 1]);

            if (t > ra + rb)
            {
                return false;
            }

            // L = A0xB2
            ra = a.halfSize[1] * Rabs[2, 2] + a.halfSize[2] * Rabs[1, 2];
            rb = b.halfSize[0] * Rabs[0, 1] + b.halfSize[1] * Rabs[0, 0];
            t = Mathf.Abs(T[2] * R[1, 2] - T[1] * R[2, 2]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A1 x B0
            ra = a.halfSize[0] * Rabs[2, 0] + a.halfSize[2] * Rabs[0, 0];
            rb = b.halfSize[1] * Rabs[1, 2] + b.halfSize[2] * Rabs[1, 1];

            t = Mathf.Abs(T[0] * R[2, 0] - T[2] * R[0, 0]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A1 x B1
            ra = a.halfSize[0] * Rabs[2, 1] + a.halfSize[2] * Rabs[0, 1];
            rb = b.halfSize[0] * Rabs[1, 2] + b.halfSize[2] * Rabs[1, 0];

            t = Mathf.Abs(T[0] * R[2, 1] - T[2] * R[0, 1]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A1 x B2
            ra = a.halfSize[0] * Rabs[2, 2] + a.halfSize[2] * Rabs[0, 2];
            rb = b.halfSize[0] * Rabs[1, 1] + b.halfSize[1] * Rabs[1, 0];

            t = Mathf.Abs(T[0] * R[2, 2] - T[2] * R[0, 2]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A2 x B0
            ra = a.halfSize[0] * Rabs[1, 0] + a.halfSize[1] * Rabs[0, 0];

            rb =
            b.halfSize[1] * Rabs[2, 2] + b.halfSize[2] * Rabs[2, 1];

            t = Mathf.Abs(T[1] * R[0, 0] - T[0] * R[1, 0]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A2 x B1
            ra = a.halfSize[0] * Rabs[1, 1] + a.halfSize[1] * Rabs[0, 1];
            rb = b.halfSize[0] * Rabs[2, 2] + b.halfSize[2] * Rabs[2, 0];
            t = Mathf.Abs(T[1] * R[0, 1] - T[0] * R[1, 1]);

            if (t > ra + rb)
            {
                return false;
            }

            //L = A2 x B2
            ra = a.halfSize[0] * Rabs[1, 2] + a.halfSize[1] * Rabs[0, 2];
            rb = b.halfSize[0] * Rabs[2, 1] + b.halfSize[1] * Rabs[2, 0];
            t = Mathf.Abs(T[1] * R[0, 2] - T[0] * R[1, 2]);

            if (t > ra + rb)
            {
                return false;
            }

            /*no separating axis found,
            the two boxes overlap */

            return true;
        }


        public static Vector3 RandomPointInside(this Bounds bounds)
        {
            Vector3 min = bounds.min;
            Vector3 max = bounds.max;

            return new Vector3(
                Random.Range(min.x, max.x),
                Random.Range(min.y, max.y),
                Random.Range(min.z, max.z)
                );
        }

        public static bool OverlapsSphere(this Bounds bounds, Vector3 center, float radius)
        {
            Vector3 pointInBoundsNearestCenter = Vector3.Min(Vector3.Max(center, bounds.min), bounds.max);
            return (pointInBoundsNearestCenter - center).sqrMagnitude < radius * radius;
        }

        public static Bounds ScaledBy(this Bounds bounds, float scale)
        {
            return new Bounds(bounds.center * scale, bounds.size * scale);
        }

        /// <summary>
        /// Yields the counting numbers starting with zero, evenly skipping integers so that the total number yielded is no bigger than the specified number.
        /// For example, if want 5 numbers out of the first 10 counting numbers, you get { 0, 2, 4, 6, 8 }
        /// </summary>
        /// <param name="numIndicesAvailable">Consider the counting numbers up to numIndicesAvailable-1</param>
        /// <param name="numIndicesDesired">Return this many of those counting numbers.</param>
        /// <returns></returns>
        public static IEnumerable<int> SampleMIntegersDownToN(int numIndicesAvailable, int numIndicesDesired)
        {
            if (numIndicesAvailable <= numIndicesDesired)
            {
                for (int i = 0; i < numIndicesAvailable; ++i)
                {
                    yield return i;
                }
            }
            else
            {
                float stride = numIndicesAvailable / (float)numIndicesDesired;
                float fi = 0;
                int sent = 0;
                for (; sent < numIndicesDesired; fi += stride, ++sent)
                {
                    int index = (int)fi;
                    if (index < numIndicesAvailable)  // floating point math is squirrelly, so double checking for out of bounds
                    {
                        yield return index;
                    }
                }
            }
        }

        public static IEnumerable<T> EvenlySampleDownTo<T>(this T[] input, int numSamples)
        {
            foreach (int index in SampleMIntegersDownToN(input.Length, numSamples))
            {
                yield return input[index];
            }
        }

        public static T GetComponentInAncestor<T>(this GameObject go, bool checkSelf = true) where T : Component
        {
            if (go != null)
            {
                if (checkSelf)
                {
                    T component = go.GetComponent<T>();

                    if (component != null)
                    {
                        return component;
                    }
                }

                Transform parent = go.transform.parent;

                if (parent != null)
                {
                    return GetComponentInAncestor<T>(parent.gameObject);
                }
            }

            return default(T);
        }

        public static bool IsNaN(this Vector3 vector)
        {
            return float.IsNaN(vector.x) || float.IsNaN(vector.y) || float.IsNaN(vector.z);
        }
    }
}