﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections.Generic;
using Meta.Internal.HandsPhysics;
using System.Linq;
using Meta.Physics;

namespace Meta.Internal.Velocity
{

    /// <summary>
    /// This is the publicly facing class for the Velocity system.
    /// It creates a CloudInterpolator behinds the scenes and handles threading for that class.
    /// 
    /// If one of these is in a scene, the Velocity system can do its work.
    /// 
    /// Settings are controlled by a VelocitySystemSettings monobehaviour, so end users might not need to see this class.
    /// </summary>
    internal class CloudInterpolatorWrapper : MetaBehaviour, IDisposable
    {
        public bool IsVerbose = false;

        [SerializeField]
        internal PointProvider _pointProvider = null;

        internal PointCloudAndVelocitySnapshot _lastSnapshot_UnityThread;   // designed to be read-only and accessed from the Unity main thread

        private DateTime _dtLastListenerUpdate_UnityThread = DateTime.MinValue;
        
        private CloudInterpolator _interpolator_InterpolationThread;
        private Thread _interpolationThread;

        private VelocitySystemSettings Settings { get { return VelocitySystemSettings.Global; } }

        private List<Vector3> _pointPool = new List<Vector3>();

        private int _priorCloudID;

        private void Awake()
        {
            _interpolator_InterpolationThread = new CloudInterpolator(this);
        }

        public void OnEnable()
        {
            StartThread();
        }

        public void OnDisable()
        {
            StopThread();
        }

        public void Dispose()
        {
            StopThread();
        }

        public void OnDestroy()
        {
            StopThread();
        }

        protected void StartThread()
        {
            if (null != _interpolator_InterpolationThread)
            {
                _interpolator_InterpolationThread.KeepRunning = true;
            }

            if (null == _interpolationThread)
            {
                _interpolationThread = new Thread(_interpolator_InterpolationThread.ThreadFunction);
                _interpolationThread.IsBackground = true;
                _interpolationThread.Start();
            }
        }

        protected void StopThread()
        {
            if (null != _interpolator_InterpolationThread)
            {
                _interpolator_InterpolationThread.KeepRunning = false;
            }

            if (null != _interpolationThread)
            {
                _interpolationThread.Abort();
                _interpolationThread = null;
            }
        }



        private void LateUpdate()
        {
            if (!Settings.calculateVelocity || !_pointProvider || !_pointProvider.Initialized)
            { return; }

            if (_pointProvider.UpdateID != _priorCloudID)
            {
                _pointPool.Clear();
                _pointProvider.AddPointsToPool(_pointPool);

                var cloud = new PointCloudSnapshot();
                DateTime utcNow = Settings.UseUnityTime ? DateTime.MinValue + TimeSpan.FromSeconds(Time.time) : DateTime.UtcNow;
                //Debug.LogFormat("Updating wrapper calling NOW={0}.{1}  Time.time={2}", utcNow.Second, utcNow.Millisecond, Time.time);
                cloud.Init(Settings.bucketSize, _pointPool, utcNow, _unityUnitsToRealMetersScale);
                cloud.FromPointCloudId = _pointProvider.UpdateID;

                if (null != _interpolator_InterpolationThread)
                {
                    _interpolator_InterpolationThread._ThreadShared_snapshotFromSensor = cloud;
                }
                _priorCloudID = _pointProvider.UpdateID;
            }
        }

        void FixedUpdate()
        {
            if (!Settings.calculateVelocity || !_pointProvider || !_pointProvider.Initialized)
            { return; }
            
            _lastSnapshot_UnityThread = _interpolator_InterpolationThread._UnityThread_lastInterpolatedSnapshot;

            var snapshot = _lastSnapshot_UnityThread;
            if (null != snapshot)
            {
                if (snapshot.snapshotTimeUtc > _dtLastListenerUpdate_UnityThread)
                {
                    _dtLastListenerUpdate_UnityThread = snapshot.snapshotTimeUtc;

                    if (IsVerbose)
                    {
                        Debug.LogFormat("New point cloud {1} with {0} results {2}", snapshot.dBucketToData.Count, snapshot.FromPointCloudId, snapshot.GetLargestVelocity());
                        if (snapshot.dBucketToData.Count < 3)
                        {
                            foreach (var kvp in snapshot.dBucketToData)
                            {
                                Debug.LogFormat("v{0}", kvp.Value.Velocity);
                            }
                        }
                    }
                    VelocityAPI.InformPointCloudListeners(this);
                }

                if (Settings.ApplyVelocityToColliders)
                {
                    ApplyVelocityFieldToColliders();
                }

                if (Settings.DrawVelocityField)
                {
                    DrawVelocityField();
                }
            }
        }

        private void DrawVelocityField()
        {
            int numDrawn = 0;
            foreach (var report in EnumerateScaledVelocities())
            {
                Debug.DrawLine(report.WorldPosition, report.WorldPosition + report.Velocity / 4, Color.red);
                Debug.DrawLine(report.WorldPosition, report.WorldPosition + report.Velocity / 16, Color.blue);
                ++numDrawn;
            }
        }

        private void ApplyVelocityFieldToColliders()
        {
            var recentCloud = _lastSnapshot_UnityThread;
            if (null == recentCloud) { return; }

            // Since we apply velocity forces per-bucket, we need to scale the force by the bucket size.
            float bucket_scale_from_base = recentCloud.bucketRadiusInRealMeters;

            // cube the multiplier so that a bucket with twice the radius applies 8 times the force (since force should be the same for the same volume)
            float multiplier_for_bucket_size = (bucket_scale_from_base * bucket_scale_from_base * bucket_scale_from_base) * 10000f;

            foreach (var report in EnumerateScaledVelocities())
            {
                ApplyVelocityToColliders(report.WorldPosition, recentCloud.bucketRadiusInRealMeters, report.Velocity, multiplier_for_bucket_size);
            }
        }

        private Collider[] _colliderBuffer = new Collider[10000];
        private void ApplyVelocityToColliders(Vector3 center, float radius, Vector3 velocity, float extra_multiplier)
        {
            int numHits = UnityEngine.Physics.OverlapSphereNonAlloc(center, radius, _colliderBuffer, Settings.GetColliderQueryMask());
            for (int i = 0; i < numHits; ++i)
            {
                Collider collider = _colliderBuffer[i];
                ApplyVelocityToCollider(collider, center, velocity, extra_multiplier);
            }
        }

        private void ApplyVelocityToCollider(Collider collider, Vector3 position, Vector3 appliedVelocity, float extraMultiplier)
        {
            if (collider.isTrigger) { return; }

            Rigidbody rigidbody = collider.attachedRigidbody;
            if (!rigidbody) { return; }

            // -- Setup Settings -- //
            VelocityOverride override_settings = collider.gameObject.GetComponentInParent<VelocityOverride>();
            ForceMode ForceMode = override_settings ? override_settings.forceMode : Settings.velocityForceMode;
            float ForceMultiplier = override_settings ? override_settings.forceMultiplier : Settings.velocityForceMultiplier;

            // -- Calculate & Apply Force -- //
            Vector3 vForce = (appliedVelocity) * (ForceMultiplier) * extraMultiplier;
            Vector3 hit_point = position;

            rigidbody.AddForceAtPosition(vForce, hit_point, ForceMode);
        }

        internal IEnumerable<VelocityAPI.VelocityReport> EnumerateScaledVelocities()
        {
            var snapshot = _lastSnapshot_UnityThread;

            if (null == snapshot)
            {
                yield break;
            }

            foreach (var data in EnumerateRealPointCloudReports())
            {
                if (data.Velocity != Vector3.zero)
                {
                    yield return new VelocityAPI.VelocityReport()
                    {
                        Velocity = data.Velocity * _realMetersToUnityUnitsScale,
                        WorldPosition = data.WorldBounds.center * _realMetersToUnityUnitsScale,
                        WhenUtc = snapshot.snapshotTimeUtc,
                    };
                }
            }
        }

        internal IEnumerable<VelocityAPI.VelocityReport> EnumerateRealVelocities()
        {
            var snapshot = _lastSnapshot_UnityThread;
            if (null == snapshot)
            {
                yield break;
            }

            foreach (var data in EnumerateRealPointCloudReports())
            {
                if (data.Velocity != Vector3.zero)
                {
                    yield return new VelocityAPI.VelocityReport()
                    {
                        Velocity = data.Velocity,
                        WorldPosition = data.WorldBounds.center,
                        WhenUtc = snapshot.snapshotTimeUtc,
                    };
                }
            }
        }

        private VelocityAPI.PointCloudReport[] noReports = new VelocityAPI.PointCloudReport[0];
        internal IEnumerable<VelocityAPI.PointCloudReport> EnumerateRealPointCloudReports()
        {
            var snapshot = _lastSnapshot_UnityThread;

            if (null == snapshot) { return noReports; }

            return snapshot.dBucketToData.Values;
        }
        internal IEnumerable<VelocityAPI.PointCloudReport> EnumerateScaledPointCloudReports()
        {
            foreach (var report in EnumerateRealPointCloudReports())
            {
                var scaledReport = new VelocityAPI.PointCloudReport();
                scaledReport.Centroid = report.Centroid.ScaledBy(_realMetersToUnityUnitsScale);
                scaledReport.Velocity = report.Velocity * _realMetersToUnityUnitsScale;
                scaledReport.VelocityTimeSmoothed = report.VelocityTimeSmoothed * _realMetersToUnityUnitsScale;
                scaledReport.WorldBounds = report.WorldBounds.ScaledBy(_realMetersToUnityUnitsScale);

                if (report.Points != null)
                {
                    scaledReport.Points = report.Points.Select(vector => vector * _realMetersToUnityUnitsScale).ToList().AsReadOnly();
                }

                yield return scaledReport;
            }
        }

        private void ApplyVelocity(Ray ray, RaycastHit spherecastHit, Vector3 velocity, float duration)
        {
            if (!spherecastHit.rigidbody) { return; }

            float real_speed = velocity.magnitude;
            float applied_speed = Settings.realVelocityToAppliedVelocity.Evaluate(real_speed);

            if (applied_speed <= 0) { return; }

            // -- Setup Settings -- //

            VelocityOverride override_settings = spherecastHit.collider.gameObject.GetComponentInParent<VelocityOverride>();
            ForceMode ForceMode = override_settings ? override_settings.forceMode : Settings.velocityForceMode;
            float ForceMultiplier = override_settings ? override_settings.forceMultiplier : Settings.velocityForceMultiplier;

            // -- Calculate & Apply Force -- //
            Vector3 vForce = (velocity) * (ForceMultiplier);
            Vector3 hit_point = ray.GetPoint(spherecastHit.distance);

            spherecastHit.rigidbody.AddForceAtPosition(vForce, hit_point, ForceMode);
        }

        internal float _unityUnitsToRealMetersScale
        {
            get { return 1f; }
        }

        internal float _realMetersToUnityUnitsScale
        {
            get
            {
                return 1f;
            }
        }
    }
}