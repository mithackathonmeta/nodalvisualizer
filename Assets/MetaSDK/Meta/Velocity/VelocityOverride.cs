﻿using UnityEngine;
using System;
using System.Collections;

namespace Meta.Internal.HandsPhysics
{
    /// <summary>
    /// Used to override behaviour of colliders on rigidbodies when they react to Velocity forces directly
    ///  (via the VelocitySystemSettings.ApplyVelocityToColliders settings)
    ///  
    /// TODO: We should integrate proximity forces (ForcePoints or HPx2) with Velocity more directly. 
    ///   I think that this kind of override would not be needed if we get that done in a rigorous mathematical way, rather than our current "tweak it until it feels good" way
    /// </summary>
    public class VelocityOverride : MonoBehaviour
    {
        public ForceMode forceMode;
        public float forceRadius = 0.05f;
        public float forceMultiplier = 1;
        public AnimationCurve normalizedForceCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(0.2f, 0.5f), new Keyframe(1, 0));
    }

}