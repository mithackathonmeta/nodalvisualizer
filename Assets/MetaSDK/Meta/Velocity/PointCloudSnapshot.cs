﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Meta.Internal.Velocity
{
    /*
 Possible optimizations:

Cut the allocations
  Cache the bucket kvp in an array if I'm going to iterate the whole thing anyway

Walk an area-
  Instead of centroid on demand, do all centroids of same radius at once
     By looking at bucket coord, can find buckets in sphere
     By looking at delta to next bucket, can add & subtract the buckets that enter/leave sphere

Better results idea-
 To calc velocity, don't compare centroids in same spot
   Find a velocity that old way, then look at bucket in PREVIOUS BEST GUESS SPOT
      Repeat a few times
      */




    /// <summary>
    /// Keeps a record of real point data in a fast-ish spatial format.
    /// Used heavily by PointCloudInterpolator to deduce velocity information.
    /// </summary>
    [Serializable]
    public class PointCloudSnapshot : PointCloudStorageBase<PointCloudSnapshot.BucketData>
    {
        internal VelocityAPI.VelocityReport GetScaledVelocityReport(Vector3 world_position)
        {
            throw new NotImplementedException();
        }

        /*
        internal PointCloudAndVelocitySnapshot ConvertToVelocitySnapshot()
        {
            // Take care of the simple stuff.
            PointCloudAndVelocitySnapshot copy = (PointCloudSnapshot) this.MemberwiseClone();

            // Now the deep copies
            copy.dBucketToData = new Dictionary<Vector3I, BucketData>();
            foreach (var kvp in this.dBucketToData)
            {
                copy.dBucketToData[kvp.Key] = kvp.Value.Copy();
            }

            copy._cachedBucketsAsList = null;  // regenerate on demand

            return copy;
    }*/

        internal void Init(float bucket_side_real_meters, List<Vector3> pointsInRealMeters, DateTime utc_cloud_sample_time, float unity_unity_to_real_meters)
        {
            base.Init(bucket_side_real_meters, utc_cloud_sample_time, unity_unity_to_real_meters);

            foreach (int index in GenericExtensions.SampleMIntegersDownToN(pointsInRealMeters.Count, Settings.MaxPointsInSnapshot))
            {
                AddPoint(pointsInRealMeters[index]);
            }
        }

        protected static string detailString(Vector3 v)
        {
            return string.Format("({0} {1} {2})", v.x, v.y, v.z);
        }

        private static bool exampled = false;
        private void AddPoint(Vector3 position)
        {
            if (float.IsNaN(position.sqrMagnitude))
            {
                Debug.LogWarningFormat("Trying to add bogus position in PointCloudSnapshot.AddPoint()  {0}", position);
                return;
            }

            _cachedDataAsList = null;

            Vector3I rounded = toBucketCoordinate(position);

            Vector3 center_of_bucket = rounded * _bucketSideInRealMeters;
            Vector3 to_corner_of_bucket = Vector3.one * (_bucketSideInRealMeters / 2);

            // Is the bucketing working correctly?
            if (!exampled)
            {
                if ((position - center_of_bucket).sqrMagnitude > to_corner_of_bucket.sqrMagnitude)
                {
                    exampled = true; // only report this once per run, otherwise it gets tedious.
                    Debug.LogWarningFormat("Point not in bucket!  {0} moved to {1} in bucket {2} with size {3}",
                                            detailString(position),
                                            detailString(center_of_bucket),
                                            rounded,
                                            _bucketSideInRealMeters
                                            );
                }
            }

            BucketData data;
            if (!dBucketToData.TryGetValue(rounded, out data))
            {
                // Init a new bucket
                data = new BucketData(position);
                dBucketToData[rounded] = data;
            }
            else
            {
                // Update the existing bucket
                data.AddPoint(position);
            }
        }

        static float timeToNextInteger(float currentValue, float speed)
        {
            if (speed == 0)
            {
                return float.MaxValue;
            }
            else if (speed > 0)
            {
                return (Mathf.Ceil(currentValue) - currentValue) / speed;
            }
            else
            {
                return (Mathf.Floor(currentValue) - currentValue) / speed;
            }
        }


        internal IEnumerable<Vector3I> EnumerateBucketCoordinatesIntersecting(Ray rayArg, float distance, float radius)
        {
            // Convert into bucket space.
            Ray rayBucketSpace = new Ray(rayArg.origin / bucketSideInRealMeters, rayArg.direction);
            distance /= bucketSideInRealMeters;

            float t = 0;
            float epsilon = 0.001f;

            Vector3I lastBucket = new Vector3I(int.MaxValue, int.MinValue, int.MinValue);

            while (t < distance)
            {
                Vector3 currentPosition = rayBucketSpace.GetPoint(t);
                Vector3I currentBucket = Vector3I.Round(currentPosition);

                if (currentBucket != lastBucket)
                {
                    yield return currentBucket;
                    lastBucket = currentBucket;
                }
                else
                {
                    // Trying to repeat the bucket.  Not a good idea.  Try again.
                    //Debug.LogFormat("Trying to repeat bucket! {0}  {1}/{2}  ray={3}", currentBucket, t, distance, rayArg);
                }

                float timeUntilBorder = float.MaxValue;
                for (int i = 0; i < 3; ++i)
                {
                    float iTime = timeToNextInteger(currentPosition[i], rayBucketSpace.direction[i]);
                    if (iTime > 0 && iTime < timeUntilBorder)
                    {
                        timeUntilBorder = iTime;
                    }
                }

                timeUntilBorder += epsilon; // avoid rounding confusions
                t += timeUntilBorder;
            }
        }

        /// <summary>
        /// Not really needed for the centroid velocity method.
        /// Queries the pair of spatial databases for corresponding buckets.
        /// </summary>
        /// <param name="buckets_previous"></param>
        /// <param name="buckets_now"></param>
        /// <param name="bucket_coord"></param>
        /// <returns></returns>
        protected static BucketDiff GetDiff(Dictionary<Vector3I, BucketData> buckets_previous, Dictionary<Vector3I, BucketData> buckets_now, Vector3I bucket_coord)
        {
            BucketDiff diff = new BucketDiff();
            buckets_now.TryGetValue(bucket_coord, out diff.newData);
            buckets_previous.TryGetValue(bucket_coord, out diff.previousData);

            return diff;
        }

        static IEnumerable<KEY> Union<KEY>(IEnumerable<KEY> a, IEnumerable<KEY> b)
        {
            // Linq Union seems really slow for this, so using HashSet.
            HashSet<KEY> set = new HashSet<KEY>();
            AddRange(set, a);
            AddRange(set, b);

            return set;
        }

        static void AddRange<KEY>(HashSet<KEY> set, IEnumerable<KEY> values)
        {
            foreach (KEY k in values)
            {
                set.Add(k);
            }
        }

        public static float SecondsBetween(PointCloudSnapshot previous, PointCloudSnapshot now)
        {
            return (float)(now.snapshotTimeUtc - previous.snapshotTimeUtc).TotalSeconds;
        }

        /// <summary>
        /// As of 2016-03-03, the best prototype velocity guesser.
        ///  Uses the motion of the centroid of all points in a given region to deduce velocity.
        ///  Currently using all buckets that interest the given sphere.
        ///    Does not actually do a distance test on those points, 
        ///      which is fast,
        ///      but the behaviour will change a lot depending on radius and bucket size.
        /// </summary>
        /// <param name="center">Center of region to examine</param>
        /// <param name="radius">Radius of region to examine</param>
        /// <param name="history">A set of points clouds with timestamps, ordered by increasing timestamp</param>
        /// <returns></returns>
        public static VelocityResult GuessVelocityAt(Vector3 center, float radius, List<PointCloudSnapshot> history)
        {
            VelocityResult result = new VelocityResult();
            result.IsValid = true;

            // Simple average of velocities for now.
            //  Might want to weight them by some confidence measure?
            int velocity_count = 0;
            Vector3 velocity_sum = Vector3.zero;

            // Deduce all centroids up front so we don't do it twice for each cloud-region pair.
            CentroidData[] point_centroids = new CentroidData[history.Count];
            for (int i = 0; i < history.Count; ++i)
            {
                point_centroids[i] = history[i].GetCentroidExact(center, radius);
            }

            // Iterate consecutive (in time) pairs of point clouds
            for (int i = 0; i < history.Count - 1; ++i)
            {
                CentroidData prev = point_centroids[i];
                CentroidData now = point_centroids[i + 1];

                bool enough_prev_points = prev.VectorCount > 0;
                bool enough_now_points = now.VectorCount > 0;

                if (enough_prev_points && enough_now_points)
                {
                    // Both point clouds are valid.
                    Vector3 offset = now.Centroid - prev.Centroid;
                    float dt = SecondsBetween(history[i], history[i + 1]);
                    Vector3 velocity = offset / dt;

                    velocity_count++;
                    velocity_sum += velocity;

                    //Debug.LogFormat("CENTER= {4} dt={5} Centroid offset = {0}  v={1},  prevPoints={2}  nowPoints={3}",offset.ToString("G"), velocity.ToString("G"), prev, now, center.ToString("G"), dt);
                }
                else
                {
                    if (enough_now_points)
                    {
                        result.numPointsNowInPreviouslyEmptyRegions += now.VectorCount;
                        result.IsValid = false;
                        //Debug.Log("Not enough now points " + radius);
                    }
                    else
                    {
                        result.numPointsGoneFromNowEmptyRegions += prev.VectorCount;
                        result.IsValid = false;
                        //Debug.Log("Not enough prev points" + radius);
                        //foreach(var kvp in history[i].dBucketToData)
                        //{
                        //    Debug.LogFormat("-- {0} has {1}", kvp.Key, kvp.Value.m_Points.Count);
                        //}
                    }
                }
            }

            if (velocity_count > 0)
            {
                Vector3 average_velocity = velocity_sum / velocity_count;
                result.Velocity = average_velocity;
                result.IsValid = !float.IsNaN(average_velocity.sqrMagnitude);

                //Debug.LogFormat("Using v={0} from radius {1}.", average_velocity, radius );

            }
            else
            {
                result.Velocity = Vector3.zero;
                result.IsValid = false;
            }

            return result;
        }

        /// <summary>
        /// Draws a debug line demonstrating the deduced velocity near "center"
        /// See "GuessVelocityAt()"
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="history"></param>
        /// <param name="min_points_to_consider"></param>
        public static void DrawVelocityAt(Vector3 center, float radius, List<PointCloudSnapshot> history)
        {
            var velocity_result = GuessVelocityAt(center, radius, history);
            if (velocity_result.IsValid)
            {
                Debug.DrawLine(center, center + velocity_result.Velocity, Color.red);
            }
        }

        /// <summary>
        /// Approximates the centroid of all points in the given sphere.
        ///   Will include all points in any bucket intersecting the sphere, without a true distance test.
        ///   This is faster, but less accurate.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public CentroidData GetCentroidApprox(Vector3 center, float radius)
        {
            CentroidData cd = new CentroidData();
            float radiusSquared = radius * radius;
            int bucketsSampled = 0;

            Vector3I max_bucket = toBucketCoordinate(center + Vector3.one * radius);
            Vector3I min_bucket = toBucketCoordinate(center - Vector3.one * radius);
            Bounds3I sphere_bounds = new Bounds3I()
            {
                Min = min_bucket,
                Max = max_bucket,
            };

            if (sphere_bounds.Volume < dBucketToData.Count / 8)
            {
                Vector3I bucket;
                for (bucket.x = min_bucket.x; bucket.x <= max_bucket.x; ++bucket.x)
                {
                    for (bucket.y = min_bucket.y; bucket.y <= max_bucket.y; ++bucket.y)
                    {
                        for (bucket.z = min_bucket.z; bucket.z <= max_bucket.z; ++bucket.z)
                        {
                            Vector3 bucket_center = fromBucketCoordinate(bucket);
                            Vector3 offset = bucket_center - center;
                            if (offset.sqrMagnitude <= radiusSquared)
                            {
                                BucketData bucketData;
                                if (dBucketToData.TryGetValue(bucket, out bucketData))
                                {
                                    cd += bucketData.m_CentroidData;
                                    ++bucketsSampled;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var kvp in bucketsAsList)
                {
                    Vector3I bucket = kvp.Key;
                    //Debug.LogFormat("Testing {0}", bucket);
                    if (sphere_bounds.Contains(bucket))
                    {
                        //Debug.LogFormat("Bounds passed {0}", bucket);

                        Vector3 bucket_center = fromBucketCoordinate(bucket);
                        Vector3 offset = bucket_center - center;
                        if (offset.sqrMagnitude <= radiusSquared)
                        {
                            //Debug.LogFormat("Sphere passed {0}", bucket);
                            cd += kvp.Value.m_CentroidData;
                            ++bucketsSampled;
                        }
                        else
                        {
                            //Debug.LogFormat("Sphere reject {0} {1} {2}", bucket_center, offset, radius);
                        }
                    }
                }
            }

            if (!cd.IsValid)
            {
                //Debug.LogFormat("{4} CentroidApprox sampled {3} buckets at {0} rad:{1} revealed {2}", center, radius, cd, bucketsSampled, sphere_bounds.Volume < dBucketToData.Count / 8);
                //Debug.LogFormat("SphereBounds {0} {1}", sphere_bounds.Min, sphere_bounds.Max);
            }
            return cd;
        }

        /// <summary>
        /// Calculates the centroid of all points in the given sphere.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public CentroidData GetCentroidExact(Vector3 center, float radius)
        {
            CentroidData cd = new CentroidData();
            float r2 = radius * radius;

            foreach (var bucket_and_overlap in EnumeratePointBucketsOverlappingSphere(center, radius))
            {
                Vector3I bucket = bucket_and_overlap.Key;
                BucketOverlap overlap = bucket_and_overlap.Value;

                BucketData bd;
                if (dBucketToData.TryGetValue(bucket, out bd))
                {
                    // Is the bucket entirely with the sphere?
                    switch (overlap)
                    {
                        case BucketOverlap.CompletelyOutside:
                            //Debug.Log("OUTSIDE");
                            break;
                        case BucketOverlap.CompletelyInside:
                            {
                                //Debug.Log("INSIDE");
                                cd += bd.m_CentroidData;
                                break;
                            }
                        case BucketOverlap.Partial:
                            {
                                int preCount = cd.VectorCount;
                                for (int i = 0; i < bd.m_Points.Count; ++i)
                                {
                                    Vector3 point = bd.m_Points[i];
                                    float d2 = (point - center).sqrMagnitude;
                                    if (d2 <= r2)
                                    {
                                        cd.AddPoint(point);
                                    }
                                }
                                int postCount = cd.VectorCount;
                                if (postCount > preCount)
                                {

                                    //Debug.LogFormat("PARTIAL, kept {0}/{1}", postCount-preCount, bd.m_Points.Count);
                                }
                                break;
                            }
                    }
                }
            }

            return cd;
        }

        protected enum BucketOverlap
        {
            CompletelyInside,
            CompletelyOutside,
            Partial,
        };

        private BucketOverlap GetBucketOverlap(Vector3I bucket, Vector3 sphere_center, float radius)
        {
            Vector3 bucket_center = fromBucketCoordinate(bucket);
            float d2 = (bucket_center - sphere_center).sqrMagnitude;
            float d = Mathf.Sqrt(d2);

            if (d + _bucketRadiusInRealMeters <= radius)
            {
                return BucketOverlap.CompletelyInside;
            }
            else if (d - _bucketRadiusInRealMeters > radius)
            {
                return BucketOverlap.CompletelyOutside;
            }
            else
            {
                return BucketOverlap.Partial;
            }
        }


        /// <summary>
        /// Slow way to iterate all points in the given sphere.
        ///   Performs accurate distance test.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public IEnumerable<Vector3> PointsInSphere(Vector3 center, float radius)
        {
            float r2 = radius * radius;

            foreach (Vector3I bucket in EnumeratePointBucketsInSphereApprox(center, radius))
            {
                BucketData bd;
                if (dBucketToData.TryGetValue(bucket, out bd))
                {
                    foreach (Vector3 point in bd.m_Points)
                    {
                        Vector3 to_point = point - center;
                        if (to_point.sqrMagnitude <= r2)
                        {
                            yield return point;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clumsily Enumerates buckets overlapping the given sphere.
        /// TODO: cull the corner buckets when appropriate
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        protected IEnumerable<KeyValuePair<Vector3I, BucketOverlap>> EnumeratePointBucketsOverlappingSphere(Vector3 center, float radius)
        {
            Vector3I max_bucket = toBucketCoordinate(center + Vector3.one * radius);
            Vector3I min_bucket = toBucketCoordinate(center - Vector3.one * radius);
            Bounds3I sphere_bounds = new Bounds3I()
            {
                Min = min_bucket,
                Max = max_bucket,
            };

            foreach (var kvp in bucketsAsList)
            {
                if (sphere_bounds.Contains(kvp.Key))
                {
                    BucketOverlap overlap = GetBucketOverlap(kvp.Key, center, radius);
                    if (overlap != BucketOverlap.CompletelyOutside)
                    {
                        yield return new KeyValuePair<Vector3I, BucketOverlap>(kvp.Key, overlap);
                    }
                    else
                    {
                        //Debug.LogFormat("discarding {0} from {1} r{2} for no overlap", kvp.Key, center, radius);
                    }
                }
                else
                {
                    //Debug.LogFormat("discarding {0} for sphere bounds {1} {2} r{3}", kvp.Key, min_bucket, max_bucket, radius);
                }
            }
        }



        /// <summary>
        /// Clumsily Enumerates buckets overlapping the given sphere.
        /// TODO: cull the corner buckets when appropriate
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public IEnumerable<Vector3I> EnumeratePointBucketsInSphereApprox(Vector3 center, float radius)
        {
            //Vector3I center_bucket = BucketCoord(center);
            Vector3I max_bucket = toBucketCoordinate(center + Vector3.one * radius);
            Vector3I min_bucket = toBucketCoordinate(center - Vector3.one * radius);
            Bounds3I sphere_bounds = new Bounds3I()
            {
                Min = min_bucket,
                Max = max_bucket,
            };

            foreach (var kvp in bucketsAsList)
            {
                if (sphere_bounds.Contains(kvp.Key))
                {
                    yield return kvp.Key;
                }
            }
        }

        /// <summary>
        /// Enumerates buckets overlapping the given sphere.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public IEnumerable<Vector3I> EnumeratePointBucketsInSphereExact(Vector3 center, float radius)
        {
            float radiusSquared = radius * radius;

            foreach (Vector3I bucket in EnumeratePointBucketsInSphereApprox(center, radius))
            {
                // Slow radius test.
                Vector3 bucket_center = fromBucketCoordinate(bucket);
                if ((bucket_center - center).sqrMagnitude <= radiusSquared)
                {
                    yield return bucket;
                }
            }
        }

        /// <summary>
        /// Enumerates bucket coords overlapping the given sphere, whether or not they contain points.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public IEnumerable<Vector3I> EnumerateBucketCoordsOverlappingSphereExact(Vector3 center, float radius)
        {
            //Vector3I center_bucket = BucketCoord(center);
            Vector3I max_bucket = toBucketCoordinate(center + Vector3.one * radius);
            Vector3I min_bucket = toBucketCoordinate(center - Vector3.one * radius);

            float diameter = radius * 2;
            Bounds sphereBounds = new Bounds(center, Vector3.one * diameter);

            Vector3I bucket;
            for (bucket.x = min_bucket.x; bucket.x <= max_bucket.x; ++bucket.x)
            {
                for (bucket.y = min_bucket.y; bucket.y <= max_bucket.y; ++bucket.y)
                {
                    for (bucket.z = min_bucket.z; bucket.z <= max_bucket.z; ++bucket.z)
                    {
                        Bounds bucketBounds = new Bounds(fromBucketCoordinate(bucket), bucketVectorSizeInRealMeters);
                        if (sphereBounds.Intersects(bucketBounds))
                        {
                            // Slowish radius test.
                            if (bucketBounds.OverlapsSphere(center, radius))
                            {
                                yield return bucket;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Describes the general characteristics of a small point cloud
        ///   Obsolete, see GuessVelocityAt()
        /// </summary>
        protected struct CloudShapeData
        {
            // Describe the shape of the point cloud inside a bucket so that we can examine edges to get a velocity.
            //  Most cases are easy- just take the world bounds, and pretend its bounds are edges.  See how those edges move.

            // An empty bucket is a special case.  We claim a cloud

            // One weird case will seem to have no real data- if the bucket has points near the bucket sides,
            //   but there is a hole in the cloud (between fingers, for example)
            //   The movement of that hole has velocity information that a bounds analysis will miss.

            private Bounds CloudBounds;
            private Vector3 CloudCentroid;

            public static Nullable<CloudShapeData> Create(BucketData data)
            {
                if (data == null || data.m_Points.Count < 1)
                {
                    return null;
                }

                Vector3 max_point = data.m_Points.Aggregate(GenericExtensions.VectorMax);
                Vector3 min_point = data.m_Points.Aggregate(GenericExtensions.VectorMin);

                Vector3 sum = data.m_Points.Aggregate((a, b) => a + b);

                CloudShapeData cd = new CloudShapeData();
                cd.CloudBounds = new Bounds((max_point + min_point) / 2, max_point - min_point);
                cd.CloudCentroid = sum / data.m_Points.Count;
                return cd;
            }

            /// <summary>
            /// Tries to guess the velocity in a particular bucket.
            ///   Obsolete, see GuessVelocityAt() instead
            /// </summary>
            internal static Vector3 GuessVelocity(CloudShapeData? new_data, CloudShapeData? prev_data, float delta_time, Vector3 bucket_min, Vector3 bucket_max)
            {
                if (new_data.HasValue && prev_data.HasValue)
                {
                    //return (new_data.Value.CloudCentroid - prev_data.Value.CloudCentroid) / delta_time;

                    // Average the velocities of both "edges"
                    Vector3 min_offset = new_data.Value.CloudBounds.min - prev_data.Value.CloudBounds.min;
                    Vector3 max_offset = new_data.Value.CloudBounds.max - prev_data.Value.CloudBounds.max;
                    Vector3 average = (max_offset + min_offset) / 2;
                    return average / delta_time;
                }

                // Otherwise, we have no reliable basis for comparison.
                return Vector3.zero;
            }

            public override string ToString()
            {
                return string.Format("CD Min:{0} Max:{1} Cent:{2}", detailString(CloudBounds.min), detailString(CloudBounds.max), detailString(CloudCentroid));
            }
        }

        protected struct BucketDiff
        {
            public int PreviousCount
            {
                get { return previousData != null ? previousData.m_Points.Count : 0; }
            }

            public int NewCount
            {
                get { return newData != null ? newData.m_Points.Count : 0; }
            }

            public int DiffCount { get { return NewCount - PreviousCount; } }

            public BucketData previousData;
            public BucketData newData;
        }

        /// <summary>
        /// Data for spatial database.
        ///   m_Points is not currently necessary for GuessVelocityAt()
        ///     because we are not doing a true distance test for performance reasons.
        /// </summary>
        public class BucketData
        {
            public List<Vector3> m_Points;
            public CentroidData m_CentroidData;
            public Nullable<Vector3> m_Velocity;
            public Nullable<Vector3> m_VelocityTimeSmoothed;

            public BucketData()
            { }

            public BucketData(Vector3 point)
            {
                m_Points = new List<Vector3>();
                AddPoint(point);
            }

            public void AddPoint(Vector3 p)
            {
                m_Points.Add(p);
                m_CentroidData.VectorSum += p;
                ++m_CentroidData.VectorCount;
            }

            internal BucketData Copy()
            {
                BucketData copy = new BucketData();
                copy.m_Points = this.m_Points.ToList();
                copy.m_CentroidData = this.m_CentroidData;
                copy.m_Velocity = this.m_Velocity;

                return copy;
            }
        }

        private static VelocitySystemSettings Settings { get { return VelocitySystemSettings.Global; } }

    }


}