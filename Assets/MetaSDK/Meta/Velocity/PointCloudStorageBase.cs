﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meta.Internal.Velocity
{
    /// <summary>
    /// Basis for converting floating point 3d space into fast bucketed data.
    /// </summary>
    public class PointCloudStorageBase<TData>
    {
        public long Id;
        public long FromPointCloudId;
        public Dictionary<Vector3I, TData> dBucketToData;

        protected KeyValuePair<Vector3I, TData>[] _cachedDataAsList;
        protected KeyValuePair<Vector3I, TData>[] bucketsAsList
        {
            get
            {
                if (null == _cachedDataAsList)
                {
                    _cachedDataAsList = dBucketToData.ToArray();
                }

                return _cachedDataAsList;
            }
        }

        public DateTime snapshotTimeUtc;  // using real system clock because Unity FixedUpdate is not actually pinned to real time, but the point data is pinned to real time
        internal float _unityUnitsToRealMetersScale { private set; get; }
        protected float _realMetersToUnityUnitsScale { private set; get; }
        protected static long nextId = 0;

        public float bucketSideInRealMeters
        {
            get { return _bucketSideInRealMeters; }
            set
            {
                _bucketSideInRealMeters = value;
                _bucketRadiusInRealMeters = _bucketSideInRealMeters * GenericExtensions.Sqrt3 / 2; // cube largest "radius" 
                _bucketSideInverse = 1 / _bucketSideInRealMeters;
            }
        }
        protected float _bucketSideInRealMeters { get; private set; }
        protected float _bucketSideInverse { get; private set; }
        protected float _bucketRadiusInRealMeters { get; private set; }
        public float bucketRadiusInRealMeters { get { return _bucketRadiusInRealMeters; } }
        public Vector3 bucketVectorSizeInRealMeters { get { return new Vector3(bucketSideInRealMeters, bucketSideInRealMeters, bucketSideInRealMeters); } }

        public void Init(float bucket_side_real_meters, DateTime snapshot_time_utc, float unityUnitsToRealMetersScale)
        {
            Id = nextId;
            ++nextId;

            snapshotTimeUtc = snapshot_time_utc;
            bucketSideInRealMeters = bucket_side_real_meters;
            _unityUnitsToRealMetersScale = unityUnitsToRealMetersScale;
            _realMetersToUnityUnitsScale = 1 / unityUnitsToRealMetersScale;

            if (null == dBucketToData)
            {
                dBucketToData = new Dictionary<Vector3I, TData>();
            }
            dBucketToData.Clear();
        }

        public Bounds worldBoundsFromBucketCoordinate(Vector3I bucket)
        {
            return new Bounds(fromBucketCoordinate(bucket), bucketVectorSizeInRealMeters);
        }


        /// <summary>
        /// Converts a world space vector into a bucket coordinate
        /// </summary>
        /// <param name="point_location"></param>
        /// <returns>the bucket coordinate</returns>
        public Vector3I toBucketCoordinate(Vector3 point_location)
        {
            Vector3 coord = point_location * _bucketSideInverse;
            return Vector3I.Round(coord);
        }

        /// <summary>
        /// Converts a bucket coordinate into a world space vector
        /// </summary>
        /// <param name="point_location"></param>
        /// <returns>the bucket coordinate</returns>
        public Vector3 fromBucketCoordinate(Vector3I bucket)
        {
            return bucket * _bucketSideInRealMeters;
        }

        static IEnumerable<KEY> Union<KEY>(IEnumerable<KEY> a, IEnumerable<KEY> b)
        {
            // Linq Union seems really slow for this, so using HashSet.
            HashSet<KEY> set = new HashSet<KEY>();
            AddRange(set, a);
            AddRange(set, b);

            return set;
        }

        static void AddRange<KEY>(HashSet<KEY> set, IEnumerable<KEY> values)
        {
            foreach (KEY k in values)
            {
                set.Add(k);
            }
        }

        public static float SecondsBetween(PointCloudStorageBase<TData> previous, PointCloudStorageBase<TData> now)
        {
            return (float)(now.snapshotTimeUtc - previous.snapshotTimeUtc).TotalSeconds;
        }




    }
}