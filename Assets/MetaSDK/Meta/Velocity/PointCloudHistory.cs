﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Meta.Internal.Velocity
{
    /// <summary>
    /// Keeps a record of previous real point data.
    /// Used to deduce velocity information.
    /// 
    /// The history is a series of PointCloudSnapshot objects.
    /// </summary>
    public class PointCloudHistory 
    {
        internal List<PointCloudSnapshot> clouds = new List<PointCloudSnapshot>();  // In chronological order
        private Dictionary<VelocityQuery, VelocityResult> _dQueryToResultCache = new Dictionary<VelocityQuery, VelocityResult>();

        internal void AddSnapshot(PointCloudSnapshot cloud)
        {
            clouds.Add(cloud);

            // New cloud, so our cached data is now invalid.
            _dQueryToResultCache.Clear();

            // And an older cloud might now be too old to bother keeping.
            RemoveOldClouds();
        }

        public VelocityResult GuessVelocityAt(Vector3 point)
        {
            VelocityResult result = GuessVelocityAt(point, Settings.VelocityAveragingRadius.Min);
            if (result.IsValid)
            {
                result.NumTries = 1;
                return result;
            }

            float step = 1.0f / (Settings.NumVelocityTries - 1);
            for (int i = 1; i < Settings.NumVelocityTries; ++i)
            {
                float radius = Mathf.Lerp(Settings.VelocityAveragingRadius.Min, Settings.VelocityAveragingRadius.Max, i * step);
                result = GuessVelocityAt(point, radius);
                result.NumTries = i + 1;

                if (result.IsValid)
                {
                    return result;
                }
            }

            return result;
        }

        public VelocityResult GuessVelocityAt(Vector3 point, float radius)
        {
            VelocityQuery query = new VelocityQuery()
            {
                WorldPosition = point,
                Radius = radius,
            };

            VelocityResult result;
            if (_dQueryToResultCache.TryGetValue(query, out result))
            {
                return result;
            }

            result = PointCloudSnapshot.GuessVelocityAt(point, radius, clouds);
            _dQueryToResultCache[query] = result;
            return result;
        }

        private void RemoveOldClouds()
        {
            int max_to_remove = clouds.Count - 2;
            if (max_to_remove > 0)
            {
                // Remove old clouds.
                TimeSpan history_duration = TimeSpan.FromSeconds(Settings.sHistoryDuration);
                DateTime latest = clouds[clouds.Count - 1].snapshotTimeUtc;
                for (int i = clouds.Count - 1; i >= 0; --i)
                {
                    if (clouds[i].snapshotTimeUtc + history_duration < latest)
                    {
                        clouds.RemoveRange(0, Mathf.Min(i + 1, max_to_remove));
                        break;
                    }
                }
            }
        }

        private VelocitySystemSettings Settings { get { return VelocitySystemSettings.Global; } }

        internal struct VelocityQuery
        {
            public Vector3 WorldPosition;
            public float Radius;

            public override int GetHashCode()
            {
                return WorldPosition.GetHashCode() ^ Radius.GetHashCode();
            }
        }
    }
}