﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Helper struct for accumulating centroids of Vector3's.
/// </summary>
public struct CentroidData
{
    public Vector3 VectorSum;
    public int VectorCount;

    public static CentroidData operator +(CentroidData a, CentroidData b)
    {
        return new CentroidData()
        {
            VectorSum = a.VectorSum + b.VectorSum,
            VectorCount = a.VectorCount + b.VectorCount,
        };
    }

    public bool IsValid
    {
        get
        {
            return VectorCount > 0;
        }
    }

    public CentroidData ScaledBy(float scale)
    {
        return new CentroidData()
        {
            VectorCount = this.VectorCount,
            VectorSum = this.VectorSum * scale,
        };
    }

    public Vector3 Centroid
    {
        get { return VectorSum / VectorCount; }
    }

    internal void AddPoint(Vector3 point)
    {
        VectorCount++;
        VectorSum += point;
    }

    public override string ToString()
    {
        if (VectorCount <= 0)
        {
            return string.Format("[CentroidData {0}/{1}]", VectorSum.ToString("G"), VectorCount);
        }
        else
        {
            return string.Format("[CentroidData {0} from {1}]", (VectorSum/VectorCount).ToString("G"), VectorCount);
        }
    }
}