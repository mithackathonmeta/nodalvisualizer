﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Result summary from the Velocity algorithm.
/// 
/// The end-user important results is .Velocity and .IsValid
/// 
/// The other fields have been used internally at various points.
/// TODO: Remove unneeded fields, or create a new struct for user-facing results, or document the fields if they are actually useful to end-users.
/// </summary>
public struct VelocityResult  {

    public bool IsValid;
    public int NumTries;
    public Vector3 Velocity;
    public int numPointsNowInPreviouslyEmptyRegions;
    public int numPointsGoneFromNowEmptyRegions;


}
