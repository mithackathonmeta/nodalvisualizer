﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// A class that holds settings for the velocity system.
/// </summary>
[Serializable]
public class VelocitySystemSettings  {

    public static VelocitySystemSettings Global = new VelocitySystemSettings();

    [Header("Master Switches")]
    [Tooltip("Master switch to turn on/off velocity calculations entirely.  True means to make the calculations.")]
    public bool calculateVelocity = false;
    [Tooltip("Master switch to turn velocity force on/off.  True means apply the forces.")]
    public bool ApplyVelocityToColliders = true;

    [Header("Details")]
    [Tooltip("True if we should apply velocities outside of colliders, up to the velocity force radius")]
    public bool pointsOutsideApplyVelocity = false;
    [Tooltip("Which layer of colliders should we apply velocity forces against?")]
    public LayerMask RealPointInteractableLayer = 10;

    [Tooltip("A scale factor for velocity forces")]
    public float velocityForceMultiplier = 1;

    [Tooltip("When applying velocity to colliders, apply it if the field is nonzero this close to a collider")]
    public float velocityForceRadius = 0.03f;
    [Tooltip("When determining velocity field between interpolation buckets, how wide an area should get painted with the velocity between buckets?")]
    public float interpolationBucketCastRadiusMultiplier = 0.01f;
    public ForceMode velocityForceMode = ForceMode.Force;

    [Tooltip("Allows user to specify a minimum velocity to consider and a maximum velocity to consider, and tweak what happens in between.  This affects INPUTS to the velocity system, so the units are in real-world meters per second.  The global scaling factor is applied LATER.")]
    public AnimationCurve realVelocityToAppliedVelocity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.1f, 0), new Keyframe(1, 1), new Keyframe(10, 10));

    [Tooltip("Determines the volumes used to calculate velocity, in real-world meters.  Starts small, and gets big if it cannot determine a velocity in the small region.")]
    public RangeF VelocityAveragingRadius = RangeF.Create(0.04f, 0.2f);
    [Tooltip("Determines how many times we will requery velocty volumes if the smaller queries aren't good enough.")]
    public int NumVelocityTries = 3;

    [Tooltip("True if we should use Unity time instead of real time to guess velocity.")]
    public bool UseUnityTime = false;

    [Tooltip("Size of bucket used for point cloud history. Larger buckets are faster, but less accurate.  Units are in real-world meters.")]
    public float bucketSize = 0.03f;
    [Tooltip("How long should we keep point histories?  (We'll keep at least 2 snapshots so we can still calculate velocity")]
    public float sHistoryDuration = 0.2f;
    public float sMinimumSecondsBetweenHistorySnapshots = 0.05f;
    public int MaxPointsInSnapshot = 5000;

    public float sVelocitySmoothingDuration = 0.2f;

    public bool DrawVelocityField = false;

    public int GetColliderQueryMask()
    {
        return RealPointInteractableLayer.value;
    }


}
