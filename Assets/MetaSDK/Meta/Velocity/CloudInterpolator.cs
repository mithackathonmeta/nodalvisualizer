﻿using UnityEngine;
using System.Linq;
using System;
using System.Threading;
using System.Diagnostics;

namespace Meta.Internal.Velocity
{

    /// <summary>
    /// Threaded workhorse class for Velocity system.
    /// On the Unity main thread, this class is controlled by CloudInterpolatorWrapper.
    /// A user should not need to use this class directly.
    /// 
    /// The CloudInterpolator looks at successive point cloud snapshots and deduces a Velocity field from that.
    /// This process can be slow, but it doesn't require any Unity calls, so it can be threaded.
    /// As soon as a new Velocity field is complete, the CloudInterpolator sends that information back to the CloudInterpolatorWrapper and the VelocityAPI.
    /// </summary>
    public class CloudInterpolator
    {
        public bool KeepRunning = true;
        public bool IsVerbose = false;

        private PointCloudHistory _history;
        internal PointCloudSnapshot _ThreadShared_snapshotFromSensor;
        internal PointCloudAndVelocitySnapshot _UnityThread_lastInterpolatedSnapshot;
        private Stopwatch _timer;

        internal CloudInterpolator(CloudInterpolatorWrapper wrapper)
        {
            _history = new PointCloudHistory();
            _timer = new Stopwatch();
        }

        internal void ThreadFunction()
        {
            while (KeepRunning)
            {
                try
                {
                    Thread.Sleep(1);
                    if (!Settings.calculateVelocity) continue;

                    // Look for a new snapshot.
                    //  Snapshot variable might be updated to a new snapshot object by wrapper, but should never be nulled by anyone but us, right here.
                    if (null != _ThreadShared_snapshotFromSensor)
                    {
                        var inputSnapshot = _ThreadShared_snapshotFromSensor;
                        _ThreadShared_snapshotFromSensor = null;

                        RunInterpolationInThread(inputSnapshot);
                    }
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log("CloudInterpolator exception in thread: " + e.Message);
                }
            }
        }

        private void RunInterpolationInThread(PointCloudSnapshot inputSnapshot)
        {
            var utc_cloud_sample_time = inputSnapshot.snapshotTimeUtc;

            if (_history.clouds.Count > 0)
            {
                TimeSpan timeSinceLastSnapshot = utc_cloud_sample_time - _history.clouds.Last().snapshotTimeUtc;
                if (timeSinceLastSnapshot.TotalSeconds < Settings.sMinimumSecondsBetweenHistorySnapshots)
                {
                    return;  // ignore this sample.  It's too soon.
                }
            }

            _history.AddSnapshot(inputSnapshot);

            RunInterpolationInThread();
        }

        private void RunInterpolationInThread()
        {
            _timer.Reset();
            _timer.Start();

            PointCloudSnapshot recent_cloud;
            PointCloudSnapshot previous_cloud;

            if (_history.clouds.Count < 2)
            { return; }

            recent_cloud = _history.clouds[_history.clouds.Count - 1];
            previous_cloud = _history.clouds[_history.clouds.Count - 2];

            if (IsVerbose)
            {
                TimeSpan betweenClouds = recent_cloud.snapshotTimeUtc - previous_cloud.snapshotTimeUtc;
                UnityEngine.Debug.LogFormat("STARTING RunInterpolationInThread() {2}  prev {0}  recent {1}", previous_cloud.snapshotTimeUtc, recent_cloud.snapshotTimeUtc, betweenClouds.TotalSeconds);
            }

            var velocityResult = new PointCloudAndVelocitySnapshot(recent_cloud);

            float seconds_between_clouds = PointCloudSnapshot.SecondsBetween(previous_cloud, recent_cloud);
            int num_failed_velocity_queries = 0;

            foreach (var kvp in recent_cloud.dBucketToData)
            {
                Vector3 bucket_center = recent_cloud.fromBucketCoordinate(kvp.Key);
                VelocityResult result = _history.GuessVelocityAt(bucket_center);
                if (!result.IsValid)
                {
                    ++num_failed_velocity_queries;
                    if (IsVerbose)
                    {
                        UnityEngine.Debug.LogFormat("INVALID interp for bucket {0}", kvp.Key);
                    }
                    continue;
                }

                Vector3 applied_velocity = result.Velocity;
                float velocity_magnitude = applied_velocity.magnitude;

                float distance_traveled = velocity_magnitude * seconds_between_clouds;
                Vector3 previous_center = bucket_center - (result.Velocity * seconds_between_clouds);
                Ray ray_prev_to_now = new Ray(previous_center, -result.Velocity.normalized);
                float raycastRadiusInRealMeters = recent_cloud.bucketSideInRealMeters * Settings.interpolationBucketCastRadiusMultiplier;

                foreach (var bucket_index in recent_cloud.EnumerateBucketCoordinatesIntersecting(ray_prev_to_now, distance_traveled, raycastRadiusInRealMeters))
                {
                    velocityResult.StoreVelocityIfLarger(bucket_index, applied_velocity);

                    if (IsVerbose)
                    {
                        UnityEngine.Debug.LogFormat("STORING {2} for bucket {0}, largest saved={1}",
                            bucket_index, velocityResult.GetLargestVelocity(), applied_velocity);
                    }
                }
            }

            if (num_failed_velocity_queries > 0 && IsVerbose)
            {
                UnityEngine.Debug.LogFormat("Failed velocity queries in interpolator: {0} out of {1}", num_failed_velocity_queries, recent_cloud.dBucketToData.Count);
            }

            // Store a copy of this most recent snapshot
            this._UnityThread_lastInterpolatedSnapshot = velocityResult;
            VelocityAPI.SetLatestSnapshotFromThread(velocityResult);


            _timer.Stop();
            if (IsVerbose)
            {
                double dt = (recent_cloud.snapshotTimeUtc - previous_cloud.snapshotTimeUtc).TotalSeconds;
                UnityEngine.Debug.LogFormat("Finished interp of point clouds {0}-{1}, dt={2}", previous_cloud.FromPointCloudId, recent_cloud.FromPointCloudId, dt);
                UnityEngine.Debug.LogFormat("Vel interpolation took {0}ms", _timer.ElapsedMilliseconds);
            }
        }
        
        private VelocitySystemSettings Settings { get { return VelocitySystemSettings.Global; } }

    }

}