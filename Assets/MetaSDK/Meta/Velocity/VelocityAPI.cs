﻿using UnityEngine;
using System.Collections;
using Meta.Internal.Velocity;
using System.Collections.Generic;
using Meta;
using System;
using System.Linq;

/// <summary>
/// Convenience API for accessing Velocity field information.
/// 
/// Supports subscribers that listen for new velocity field info, optionally restricted to a particular (collider-defined) region.
/// Supports direct queries into the Velocity field at particular points in space.
/// Supports subscribers that listen for point cloud changes.
/// 
/// TODO:  Optimize the point cloud info. It is slow and allocates too much memory per frame, causing too many GC's
/// </summary>
public class VelocityAPI
{
    public static bool IsVerbose = false;
    
    // This reference set from a thread.  The contents of that refernce should never change from a thread.
    //  When using it inside VelocityAPI, first make a local variable copy of the reference so it isn't replaced in the middle of the operation.
    //    C# claims that reference assignments are atomic, so this should be safe.
    private static volatile PointCloudAndVelocitySnapshot _LatestSnapshotVolatile;

    internal static void SetLatestSnapshotFromThread(PointCloudAndVelocitySnapshot snapshot)
    {
        if (IsVerbose)
        {
            Vector3 largestVelocity = snapshot.GetLargestVelocity();
            
            Debug.LogFormat("mag={1} SetLatestSnapshotFromThread   BiggestV={0} point cloud id {2}",
                largestVelocity.ToString("G"), largestVelocity.magnitude, snapshot.FromPointCloudId);
        }
        _LatestSnapshotVolatile = snapshot;
    }

    public static bool GetPointCloudVelocityAt(Vector3 world_position, out Vector3 velocity)
    {
        var snapshot = _LatestSnapshotVolatile;
        if (null == snapshot || !VelocitySystemSettings.Global.calculateVelocity)
        {
            velocity = Vector3.zero;
            return false;
        }
        else
        {
            velocity = snapshot.GetScaledPointCloudVelocityAt(world_position);
            return velocity != Vector3.zero;
        }
    }

    public static IEnumerable<VelocityReport> EnumerateVelocities()
    {
        var snapshot = _LatestSnapshotVolatile;
        if (null == snapshot || !VelocitySystemSettings.Global.calculateVelocity)
        {
            return new VelocityReport[0];
        }
        else
        {
            return snapshot.EnumerateScaledVelocities();
        }
    }

    static List<PointCloudListenerRecord> _pointCloudListeners = new List<PointCloudListenerRecord>();
    public static void AddPointCloudListener(IPointCloudListener listener, Collider collider, bool isExact)
    {
        PointCloudListenerRecord record = new PointCloudListenerRecord()
        {
            listener = listener,
            collider = collider,
            isExactPoints = isExact,
        };
        _pointCloudListeners.Add(record);
    }

    public static bool RemovePointCloudListener(IPointCloudListener listener)
    {
        for(int i = 0; i < _pointCloudListeners.Count; ++i)
        {
            if ( _pointCloudListeners[i].listener == listener )
            {
                _pointCloudListeners.RemoveAt(i);
                return true;
            }
        }

        return false;
    }

    internal static void InformPointCloudListeners(CloudInterpolatorWrapper interpolator)
    {
        if (!VelocitySystemSettings.Global.calculateVelocity)
        { return; }

        RemoveInvalidListeners(_pointCloudListeners);

        foreach (var record in _pointCloudListeners)
        {
            record.InformListener(interpolator);
        }
    }

    private static void RemoveInvalidListeners(List<PointCloudListenerRecord> records)
    {
        for (int i = records.Count - 1; i >= 0; --i)
        {
            if (!records[i].collider)
            {
                records.RemoveAt(i);
            }
        }
    }
    
    internal struct PointCloudListenerRecord
    {
        public IPointCloudListener listener;
        public Collider collider;
        public bool isExactPoints;   // true if we need to do PERFECT intersection tests against the collider 

        internal void InformListener(CloudInterpolatorWrapper interpolator)
        {
            // Turning the enumerator into an array so that the event receiver can iterate multiple times without incurring large
            //  performance cost.  This costs memory, instead.
            listener.OnPointCloudUpdate(EnumerateRecords(interpolator).ToArray());
        }

        internal IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator, BoxCollider box)
        {
            var box_obb = box.toOBB();
            Matrix4x4 worldToColliderLocal = box.transform.worldToLocalMatrix;
            Bounds colliderLocalBounds = new Bounds(box.center, box.size);

            foreach (PointCloudReport report in interpolator.EnumerateScaledPointCloudReports())
            {
                var bucket_obb = report.WorldBounds.toOBB();
                if (bucket_obb.OverlapBoxes(box_obb))
                {
                    if (!isExactPoints || report.Points == null || report.Points.Count == 0)
                    {
                        yield return report;
                    }
                    else
                    {
                        // Not efficient, but we will make a new point array.
                        List<Vector3> exactPoints = new List<Vector3>();
                        foreach(Vector3 point in report.Points)
                        {
                            Vector3 localPoint = worldToColliderLocal.MultiplyPoint3x4(point);
                            if (colliderLocalBounds.Contains(localPoint))
                            {
                                exactPoints.Add(point);
                            }
                        }

                        var reportCopy = report;
                        reportCopy.Points = exactPoints.AsReadOnly();
                        yield return reportCopy;
                    }
                }
            }
        }

        internal IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator, SphereCollider box)
        {
            // Unknown, or unimplemented collider type!
            Bounds collider_world_bounds = collider.bounds;
            return EnumerateRecords(interpolator, collider_world_bounds);
        }

        internal IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator, CapsuleCollider box)
        {
            // Unknown, or unimplemented collider type!
            Bounds collider_world_bounds = collider.bounds;
            return EnumerateRecords(interpolator, collider_world_bounds);
        }

        internal IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator, MeshCollider box)
        {
            // Unknown, or unimplemented collider type!
            Bounds collider_world_bounds = collider.bounds;
            return EnumerateRecords(interpolator, collider_world_bounds);
        }

        internal IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator)
        {
            BoxCollider box = collider as BoxCollider;
            if (box)
            {
                return EnumerateRecords(interpolator, box);
            }

            SphereCollider sphere = collider as SphereCollider;
            if (sphere)
            {
                return EnumerateRecords(interpolator, sphere);
            }

            CapsuleCollider capsule = collider as CapsuleCollider;
            if (capsule)
            {
                return EnumerateRecords(interpolator, capsule);
            }

            MeshCollider mesh = collider as MeshCollider;
            if (mesh)
            {
                return EnumerateRecords(interpolator, mesh);
            }

            // Unknown, or unimplemented collider type!
            Bounds collider_world_bounds = collider.bounds;
            return EnumerateRecords(interpolator, collider_world_bounds);
        }

        private static IEnumerable<PointCloudReport> EnumerateRecords(CloudInterpolatorWrapper interpolator, Bounds collider_world_bounds)
        {
            foreach (PointCloudReport report in interpolator.EnumerateScaledPointCloudReports())
            {
                if (collider_world_bounds.Intersects(report.WorldBounds))
                {
                    yield return report;
                }
            }
        }

    }

    public class PointCloudReport
    {
        public Bounds WorldBounds;
        public CentroidData Centroid;
        public System.Collections.ObjectModel.ReadOnlyCollection<Vector3> Points;
        public Vector3 Velocity;
        public Vector3 VelocityTimeSmoothed;
    }

    public struct VelocityReport
    {
        public Vector3 WorldPosition;
        public Vector3 Velocity;
        public DateTime WhenUtc;
    }

    public interface IPointCloudListener
    {
        void OnPointCloudUpdate(IEnumerable<PointCloudReport> reports);
    }
}
