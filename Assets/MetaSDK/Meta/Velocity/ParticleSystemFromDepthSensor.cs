﻿using UnityEngine;
using Meta;
using System.Collections.Generic;
using Meta.Physics;

/// <summary>
/// MonoBehaviour which translates point cloud info into a particle system for visualization.
/// 
/// Was most useful for older code in RealBodyFromSensor; might be obsolete if we are adopting PointProvider (or something like it)
///   In particular, was able to draw points from any point cloud source.
/// 
/// TODO: Delete this class if it is truly obsolete.
/// </summary>
public class ParticleSystemFromDepthSensor : MetaBehaviour {

    public int MaxNumParticles = 5000;

    protected ParticleSystem.Particle[] m_cloudThreaded;  // lock on this to do thread transfer
    protected int m_numCloudParticles;
    protected bool m_applyCloudParticles = false;
    protected List<Vector3> _pointProviderBuffer;

    /// <summary>
    /// Particle system used to render the cloud.
    /// </summary>
    private ParticleSystem m_particleSystem;

    public Transform scaleParticlesWithDistanceTo;
    public PointProvider pointProvider;
    protected PointProvider _lastPointProvider = null;
    protected int _lastPointProviderId = int.MinValue;

    /// <summary>
    /// Color to render particles as.
    /// </summary>
    public Color _particleColor = Color.red;

    public Color particleColor
    {
        get { return _particleColor; }
        set { _particleColor = value; }
    }

    private float _scaledParticleSize;

    /// <summary>
    /// Size of particles.
    /// </summary>
    [SerializeField]
    private float _particleSize = .03f;

    public float particleSize
    {
        get { return _particleSize; }
        set { _particleSize = value; }
    }

    // Use this for initialization
    void Start () {
        //m_cloudThreaded = new ParticleSystem.Particle[MAX_POINTS];
        m_particleSystem = GetComponent<ParticleSystem>();
        m_particleSystem.simulationSpace = ParticleSystemSimulationSpace.World;
    }

    private void CopyFromPointProvider()
    {
        if (_pointProviderBuffer == null)
        {
            _pointProviderBuffer = new List<Vector3>(Mathf.Min(pointProvider.GetPointPoolLength(), MaxNumParticles));
        }

        _pointProviderBuffer.Clear();
        pointProvider.AddPointsToPool(_pointProviderBuffer);

        if (m_cloudThreaded == null || m_cloudThreaded.Length < _pointProviderBuffer.Count)
        {
            m_cloudThreaded = new ParticleSystem.Particle[_pointProviderBuffer.Count];
        }

        lock(m_cloudThreaded)
        {
            int outputIndex = 0;

            ParticleSystem.Particle particle = new ParticleSystem.Particle();
            particle.startColor = _particleColor;
            particle.startSize = _scaledParticleSize;

            for (int inputIndex = 0; inputIndex < _pointProviderBuffer.Count && outputIndex < m_cloudThreaded.Length; ++inputIndex)
            {
                Vector3 realWorldMetersPosition = _pointProviderBuffer[inputIndex];

                if (!float.IsNaN(realWorldMetersPosition.sqrMagnitude))
                {
                    particle.position = realWorldMetersPosition;
                    m_cloudThreaded[outputIndex] = particle;

                    ++outputIndex;
                }
            }

            m_numCloudParticles = outputIndex;
            m_applyCloudParticles = true;
        }

    }
    
    // Update is called once per frame
    void Update () {
        //TODO IS THIS RIGHT SCALE?
        _scaledParticleSize = _particleSize * metaContext.meterToUnityScale;
        if (pointProvider)
        {
            if (_lastPointProvider != pointProvider || _lastPointProviderId != pointProvider.UpdateID)
            {
                _lastPointProvider = pointProvider;
                _lastPointProviderId = pointProvider.UpdateID;

                CopyFromPointProvider();
            }
        }

        if (m_applyCloudParticles && null != m_cloudThreaded)
        {
            lock(m_cloudThreaded)
            {
                if (m_applyCloudParticles)  // test again while locked
                {
                    m_applyCloudParticles = false;

                    if (scaleParticlesWithDistanceTo)
                    {
                        Vector3 center = scaleParticlesWithDistanceTo.position;
                        //TODO IS THIS RIGHT SCALE?
                        float normalize = 1.0f / metaContext.meterToUnityScale;

                        for(int i = 0; i < m_numCloudParticles && i < m_cloudThreaded.Length; ++i)
                        {
                            float d = (m_cloudThreaded[i].position - center).magnitude;
                            if (d >= 0)
                            {
                                m_cloudThreaded[i].startSize *= d * normalize;
                            }
                            else
                            {
                                m_cloudThreaded[i].startSize = 0;
                            }
                        }
                    }

                    m_particleSystem.SetParticles(m_cloudThreaded, m_numCloudParticles);
                }
            }
        }
	}
}
