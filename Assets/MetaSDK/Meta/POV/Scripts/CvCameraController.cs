using UnityEngine;
using System.Collections;

namespace Meta {


    // This script is used to emulate calibrated cameras in Unity.

    // [2*K00/width,  -2*K01/width,   (width - 2*K02 + 2*x0)/width,                            0]
    // [          0, -2*K11/height, (height - 2*K12 + 2*y0)/height,                            0]
    // [          0,             0, (-zfar - znear)/(zfar - znear), -2*zfar*znear/(zfar - znear)]
    // [          0,             0,                             -1,                            0]


    internal class CvCameraController : MonoBehaviour {

        public RgbCameraParameters rgbParameters = null;
        public CameraParametersPoly parameters = null;
        private float fx_, fy_, tx_, ty_, width_, height_, zfar_, znear_;

        void UpdateParameters()
        {
            parameters = rgbParameters.parameters;

            fx_ = parameters.fX / parameters.zoom;
            fy_ = parameters.fY / parameters.zoom;
            tx_ = parameters.cX;
            ty_ = parameters.cY;
            width_ = parameters.maxX;
            height_ = parameters.maxY;
            zfar_ = parameters.zFar;
            znear_ = parameters.zNear;
        }

        // Update is called once per frame
        void Update() {
            UpdateParameters();

            Matrix4x4 openGLMatrix =
                CalculateOpenGLMatrixFromIntrinsics(fx_, fy_, tx_, ty_, width_, height_, zfar_, znear_);

            GetComponent<Camera>().projectionMatrix = openGLMatrix;
        }


        // The fx, fy, tx, ty are assumed to be in pixel units.
        public Matrix4x4 CalculateOpenGLMatrixFromIntrinsics(float fx, float fy, float tx, float ty, float width, float height, float zfar, float znear)
        {
            // x 0 a 0
            // 0 y b 0
            // 0 0 c d
            // 0 0 e 0

            float x = 2.0f * fx / width;
            float y = 2.0f * fy / height;
            float a = (width - 2.0f * tx) / width;
            float b = (height - 2.0f * ty) / height;
            float c = -(zfar + znear) / (zfar - znear);
            float d = -(2.0f * zfar * znear) / (zfar - znear);  // if far is too big, then: -(2.0 * near)
            float e = -1.0f;

            Matrix4x4 m = new Matrix4x4();
            m[0, 0] = x;
            m[0, 1] = 0;
            m[0, 2] = a;
            m[0, 3] = 0;

            m[1, 0] = 0;
            m[1, 1] = y;
            m[1, 2] = b;
            m[1, 3] = 0;

            m[2, 0] = 0;
            m[2, 1] = 0;
            m[2, 2] = c;
            m[2, 3] = d;

            m[3, 0] = 0;
            m[3, 1] = 0;
            m[3, 2] = e;
            m[3, 3] = 0;

            return m;
        }

    }

}  // namespace meta