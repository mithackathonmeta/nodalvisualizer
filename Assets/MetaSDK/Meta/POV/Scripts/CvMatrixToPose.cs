﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
internal class CvMatrixToPose : MonoBehaviour {
    //  0.9996919, 0.01348558, -0.02083933, 0.02340652;
    // -0.01492281, 0.9974054, -0.07042588, -0.00443981;
    // 0.01983552, 0.07071516, 0.9972993, -0.003295652 
    public Vector4 row0 = new Vector4(0.9996919f, 0.01348558f, -0.02083933f, 0.02340652f);
    public Vector4 row1 = new Vector4(-0.01492281f, 0.9974054f, -0.07042588f, -0.00443981f);
    public Vector4 row2 = new Vector4(0.01983552f, 0.07071516f, 0.9972993f, -0.003295652f);
    public Vector4 row3 = new Vector4(0, 0, 0, 1.0f);

    public Matrix4x4 poseMatrix = new Matrix4x4();
    // Use this for initialization
    void Awake() {
        UpdatePose();
    }

    public void UpdatePose()
    {
        poseMatrix.SetRow(0, row0);
        poseMatrix.SetRow(1, row1);
        poseMatrix.SetRow(2, row2);
        poseMatrix.SetRow(3, row3);

        // Get translation.
        Vector3 translation = ExtractTranslationFromCvMatrix(ref poseMatrix);
        GetComponent<Transform>().transform.localPosition = translation;

        // Get rotation.
        Quaternion rotation = ExtractRotationFromCvMatrix(ref poseMatrix);
        GetComponent<Transform>().transform.localRotation = rotation;
    }

    void Update()
    {
        UpdatePose();
    }

    // Cv matrix means that the rows are:
    // x = Right
    // y = Down
    // z = Forward
    public static Vector3 ExtractTranslationFromCvMatrix(ref Matrix4x4 matrix)
    {
        Vector3 translate;
        translate.x = matrix.m03;
        translate.y = matrix.m13;
        translate.z = matrix.m23;
        return translate;
    }

    // Cv matrix means that the rows are:
    // x = Right
    // y = Down
    // z = Forward
    //
    // Signs applied on the original matrix:
    // [ 1   -1   1 ;
    //  -1    1  -1 ;
    //   1   -1   1   ]

    public static Quaternion ExtractRotationFromCvMatrix(ref Matrix4x4 matrix)
    {
        Vector3 forward;
        forward.x = matrix.m02;
        forward.y = -matrix.m12;
        forward.z = matrix.m22;

        Vector3 upwards;
        upwards.x = -matrix.m01;
        upwards.y = matrix.m11;
        upwards.z = -matrix.m21;

        return Quaternion.LookRotation(forward, upwards);
    }
}
