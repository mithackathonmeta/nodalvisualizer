﻿using System;
using UnityEngine;
using Meta;

namespace Meta
{



    // Default parameters for TED Rgb camera
    // Parameters
    //public float cx = 640.2231f;
    //public float cy = 368.5472f;
    //public float k1 = -0.3940473f;
    //public float k2 = 0.1805779f;
    //public float k3 = -0.04361237f;
    //public float fx = 861.0092f;
    //public float fy = 861.2428f;
    //public float MAX_X = 1280.0f;
    //public float MAX_Y = 720.0f;
    //public float ZOOM = 1.0f;


    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    //[AddComponentMenu("Image Effects/Displacement/Fisheye")]
    internal class UndistortRgbTextureIF : PostEffectsBaseMeta
    {
        // Publics
        public Shader fishEyeShader = null;
        public CvCameraController RgbCameraController = null;

        // Privates
        private Material fisheyeMaterial = null;
        private CameraParametersPoly pars = null;


        public override bool CheckResources()
        {
            if (RgbCameraController == null)
            {
                Debug.LogError("No camera controller attached to Rgb Undistort module.");
            }

            pars = RgbCameraController.parameters;  // Add a reference for readability.


            CheckSupport(false);
            fisheyeMaterial = CheckShaderAndCreateMaterial(fishEyeShader, fisheyeMaterial);

            if (!isSupported)
                ReportAutoDisable();
            return isSupported;
        }


        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            // This script REQUIRES an Rgb Camera Controller in order to fetch the
            // undistort parameters.
            if (RgbCameraController == null) return;

            if (CheckResources() == false)
            {
                Graphics.Blit(source, destination);
                return;
            }

            ExposeParametersToShader();

            Graphics.Blit(source, destination, fisheyeMaterial);
        }


        void ExposeParametersToShader()
        {
            fisheyeMaterial.SetFloat("cX", pars.cX);
            fisheyeMaterial.SetFloat("cY", pars.cY);

            fisheyeMaterial.SetFloat("fX", pars.fX);
            fisheyeMaterial.SetFloat("fY", pars.fY);

            fisheyeMaterial.SetFloat("k1", pars.k1);
            fisheyeMaterial.SetFloat("k2", pars.k2);
            fisheyeMaterial.SetFloat("k3", pars.k3);

            fisheyeMaterial.SetFloat("maxX", pars.maxX);
            fisheyeMaterial.SetFloat("maxY", pars.maxY);

            fisheyeMaterial.SetFloat("zoom", pars.zoom);
        }
    }
}