﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    [System.Serializable]
    internal class CameraParametersPoly
    {
        // fX, fY : focal lengths
        // cX, cY : principal point
        // k1,2,3 : radial distortion parameters
        // maxX, maxY : image dimensions in pixels
        // zFar, zNear: near and far clipping planes (for CG rendering)
        // zoom : zoom factor. 1-> no zoom, >1 -> zoom out, <1 -> zoom in.
        public float fX = 0;
        public float fY = 0;
        public float cX = 0;
        public float cY = 0;
        public float k1 = 0;
        public float k2 = 0;
        public float k3 = 0;
        public float maxX = 0;
        public float maxY = 0;
        public float zFar = 0;
        public float zNear = 0;
        public float zoom = 0;

    }
}
