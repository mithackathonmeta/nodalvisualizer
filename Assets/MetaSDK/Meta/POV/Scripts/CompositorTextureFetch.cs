﻿using UnityEngine;
using System.Collections;
using Meta;

// Default parameters for TED Rgb camera
// Parameters
//public float cx = 640.2231f;
//public float cy = 368.5472f;
//public float k1 = -0.3940473f;
//public float k2 = 0.1805779f;
//public float k3 = -0.04361237f;
//public float fx = 861.0092f;
//public float fy = 861.2428f;
//public float MAX_X = 1280.0f;
//public float MAX_Y = 720.0f;
//public float ZOOM = 1.0f;


///<summary> An example to use camera feed via code.</summary>
///
///<seealso cref="T:UnityEngine.MonoBehaviour"/>
internal class CompositorTextureFetch : MonoBehaviour
{
    // Publics
    public RenderTexture rgbTexture = null;
    public RenderTexture occlusionTexture = null;
    public RenderTexture contentTexture = null;
    public RenderTexture occlusionDepthTexture = null;
    public RenderTexture contentDepthTexture = null;

    // Privates
    [SerializeField]
    private Material material = null;
    

    // Use this for initialization
    void Start()
    {
        material = GetComponent<Renderer>().material;
    }


    void Update()
    {
        material.SetTexture("_CameraFeed", rgbTexture);

        material.SetTexture("_Content", contentTexture);
        material.SetTexture("_Occlusion", occlusionTexture);
        material.SetTexture("_ContentDepth", contentDepthTexture);
        material.SetTexture("_OcclusionDepth", occlusionDepthTexture);
    }

    void OnDestroy()
    {
    }
}