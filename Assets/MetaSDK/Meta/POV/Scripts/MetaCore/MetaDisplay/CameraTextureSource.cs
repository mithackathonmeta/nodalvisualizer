﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;

namespace Meta
{
    /* <summary> A camera texture source. </summary> */
    internal class CameraTextureSource
    {
        /* <summary> An image texture. </summary> */
        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct ImageTexture
        {
            /// <summary> The rgb data.</summary>
            public IntPtr data;
            public int height, width;

        };

        private GCHandle _textureHandle;
        private ImageTexture _texture = new ImageTexture();
        private byte[] _imageData;

        /*
            <summary> Gets texture data handle. </summary>
        
            <param name="imageTexture"> [in,out] The image texture. </param>
        */
        public delegate void GetTextureDataHandle(ref ImageTexture imageTexture);

        /* <summary> Information describing the get texture. </summary> */
        GetTextureDataHandle _getTextureData;
        int _imageHeight, _imageWidth;
        Texture2D _texture2D;
        private System.Object thisLock = new System.Object();
        int _numberOfClients = 0;

        /*
            <summary> Constructor. </summary>
        
            <param name="width">            The width. </param>
            <param name="height">           The height. </param>
            <param name="getTextureHandle"> Handle of the get texture. </param>
        */
        public CameraTextureSource(int width, int height, GetTextureDataHandle getTextureHandle)
        {
            //Debug.Log("Texture w " + width + " h " + height);
            _imageData = new byte[width * height * 4];
            _imageHeight = _texture.height = height;
            _imageWidth = _texture.width= width;
            _textureHandle = GCHandle.Alloc(_imageData, GCHandleType.Pinned);
            _texture.data = _textureHandle.AddrOfPinnedObject();
            _textureHandle.Free();
            _getTextureData = getTextureHandle;
            _texture2D = new Texture2D(_imageWidth, _imageHeight, TextureFormat.RGBA32, false);
            _texture2D.filterMode = FilterMode.Point;
            _texture2D.mipMapBias = 0f;
        }

        /* <summary> Registers the client. </summary> */
        public void registerClient()
        {
            _numberOfClients++;
        }

        /* <summary> Unregisters the client. </summary> */
        public void unregisterClient()
        {
            if(_numberOfClients > 0)
            {
                _numberOfClients--;
            }
        }

        public ImageTexture GetTextureData()
        {
            return _texture;
        }

        /* <summary> Updates this Meta.CameraTextureSource. </summary> */
        public void Update()
        {
            if(_numberOfClients > 0)
            {
                lock(thisLock)
                {
                    _getTextureData(ref _texture);
                    Marshal.Copy(_texture.data, _imageData, 0, _texture.height * _texture.width * 4);
                    _texture2D.LoadRawTextureData(_imageData);
                    _texture2D.Apply();
                    //Debug.Log(_texture2D.GetPixel(400, 400));
                }
            }

        }

        /*
            <summary> Gets or sets information describing the texture. </summary>
        
            <value> Information describing the texture. </value>
        */
        public Texture2D textureData
        {
            get
            {
                lock(thisLock)
                {
                    return _texture2D;
                }
            }
            set { }
        }

        public int GetTextureSize()
        {
            return _texture.height * _texture.width * 4;
        }

    }
}