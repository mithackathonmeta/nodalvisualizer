﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading;

namespace Meta
{
    /// <summary>
    /// A device texture source. Used to display data from the camera.
    /// </summary>
    internal class DeviceTextureSource : IEventReceiver
    {
        private MetaSensors _metaSensors;
        private DeviceInfo _deviceInfo;
        private RgbExposure _rgbExposure;

        /// <summary>
        /// The texture sources.
        /// </summary>
        private CameraTextureSource[] textureSources = new CameraTextureSource[4];

        int _colorIndex = 0, _depthIndex = 1, _irIndex = 2;

        /// <summary>
        /// Gets device texture.
        /// </summary>
        /// <param name="device"> 0 for color/RGB feed, 1 for depth, 2 for infrared. </param>
        /// <returns> The device texture as a Texture2D. </returns>
        public Texture2D GetDeviceTexture(int device)
        {
            if (device >= 0 && device < textureSources.Length && textureSources[device] != null)
            {
                return textureSources[device].textureData;
            }
            else
            {
                return Texture2D.whiteTexture;
            }
        }

        public CameraTextureSource GetRGBSource()
        {
            return textureSources[0];
        }

        /// <summary>
        /// Queries if a device texture is registered.
        /// </summary>
        /// <param name="device"> The device.</param>
        /// <returns> True if a device texture is registered, false if not.</returns>
        public bool IsDeviceTextureRegistered(int device)
        {
            return (textureSources[device] != null);
        }

        /// <summary>
        /// Gets number of sources.
        /// </summary>
        /// <returns> The number of sources.</returns>
        public int GetNumberOfSources()
        {
            return textureSources.Length;
        }

        /// <summary>
        /// Registers the display in DLL.
        /// </summary>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "registerCameraTexture")]
        internal static extern void registerCameraTexture();

        /// <summary>
        /// Gets IR texture. 
        /// </summary>
        /// <param name="irTexture"> [in,out] The IR texture.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "getIRTexture")]
        internal static extern void getIRTexture(ref CameraTextureSource.ImageTexture irTexture);

        /// <summary>
        /// Gets depth texture.
        /// </summary>
        /// <param name="depthTexture"> [in,out] The depth texture.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "getDepthTexture")]
        internal static extern void getDepthTexture(ref CameraTextureSource.ImageTexture depthTexture);

        /// <summary>
        /// Gets RGB texture.
        /// </summary>
        /// <param name="irTexture"> [in,out] The ir texture.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "getRGBTexture")]
        internal static extern void getRGBTexture(ref CameraTextureSource.ImageTexture rgbTexture);

        /// <summary>
        /// Information describing the byte.
        /// </summary>
        byte[] byteData;

        /// <summary>
        /// Gets fake image texture.
        /// </summary>
        /// <param name="fakeImage"> [in,out] The fake image. </param>
        internal void getFakeImageTexture(ref CameraTextureSource.ImageTexture fakeImage)
        {

            Marshal.Copy(byteData, 0, fakeImage.data, byteData.Length);
        }

        /// <summary>
        /// Registers the color data.
        /// </summary>
        private void registerColorData()
        {
            //Debug.Log("rcd: w " + _deviceInfo.colorWidth + " h " + _deviceInfo.colorHeight);
            if (textureSources[_colorIndex] == null)
            {
                if (!_metaSensors.GetDeviceInfoValid()) return;
                initializeTexture();
                CameraTextureSource.GetTextureDataHandle colorDataHandle = new CameraTextureSource.GetTextureDataHandle(getRGBTexture);
                CameraTextureSource colorTextureSource = new CameraTextureSource(_deviceInfo.colorWidth, _deviceInfo.colorHeight, colorDataHandle);
                textureSources[_colorIndex] = colorTextureSource;
            }
        }

        /// <summary>
        /// Registers the depth data.
        /// </summary>
        private void registerDepthData()
        {
            //Debug.Log("rdd: w " + MetaCore.Instance.DeviceInformation.depthWidth + " h " + MetaCore.Instance.DeviceInformation.depthHeight);
            if (textureSources[_depthIndex] == null)
            {
                if (!_metaSensors.GetDeviceInfoValid()) return;
                initializeTexture();
                CameraTextureSource.GetTextureDataHandle depthDataHandle = new CameraTextureSource.GetTextureDataHandle(getDepthTexture);
                CameraTextureSource depthTextureSource = new CameraTextureSource(_deviceInfo.depthWidth, _deviceInfo.depthHeight, depthDataHandle);
                textureSources[_depthIndex] = depthTextureSource;
            }
        }

        /// <summary>
        /// Registers the IR data.
        /// </summary>
        private void registerIRData()
        {
            //Debug.Log("rird: w " + MetaCore.Instance.DeviceInformation.depthWidth + " h " + MetaCore.Instance.DeviceInformation.depthHeight);
            if (textureSources[_irIndex] == null)
            {
                if (!_metaSensors.GetDeviceInfoValid()) return;
                initializeTexture();
                CameraTextureSource.GetTextureDataHandle irDataHandle = new CameraTextureSource.GetTextureDataHandle(getIRTexture);
                CameraTextureSource irTextureSource = new CameraTextureSource(_deviceInfo.depthWidth, _deviceInfo.depthHeight, irDataHandle);
                textureSources[_irIndex] = irTextureSource;
            }

        }

        /// <summary>
        /// Registers the image data.
        /// </summary>
        private void registerImageData()
        {
            if ((textureSources[3] == null) && (Assembly.GetAssembly(GetType()).GetType("MetaInternalEditor", false, true) == null))
            {
                Color32[] colorbytes = new Color32[128 * 128];
                byte[] fileData = new byte[128 * 128 * 4];
                byteData = new byte[128 * 128 * 4];

                Texture2D tex = null;
                string filePath = "Assets\\Meta\\CameraFeed\\Textures\\distorted_img.png";
                if (System.IO.File.Exists(filePath))
                {
                    fileData = System.IO.File.ReadAllBytes(filePath);
                    tex = new Texture2D(128, 128);
                    tex.LoadImage(fileData);
                    colorbytes = tex.GetPixels32();
                    GCHandle handle;
                    try
                    {
                        handle = GCHandle.Alloc(colorbytes, GCHandleType.Pinned);
                        IntPtr ptr = handle.AddrOfPinnedObject();
                        Marshal.Copy(ptr, byteData, 0, colorbytes.Length * 4);
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }

                CameraTextureSource.GetTextureDataHandle fakeDataHandle = new CameraTextureSource.GetTextureDataHandle(getFakeImageTexture);
                CameraTextureSource fakeDataSource = new CameraTextureSource(128, 128, fakeDataHandle);
                textureSources[3] = fakeDataSource;
            }
        }

        /// <summary>
        /// Registers the texture device described by device.
        /// </summary>
        /// <remarks>
        /// 0 - color
        /// 1 - depth
        /// 2 - IR
        /// 3 - fakeImage
        /// </remarks>
        /// <param name="device"> The device. </param>
        public void registerTextureDevice(int device)
        {
            if (device < 0)
                return;
            switch (device)
            {
                case 0: registerColorData(); break;
                case 1: registerDepthData(); break;
                case 2: registerIRData(); break;
                case 3: registerImageData(); break;
                default: return;
            }
            if (textureSources[device] != null) textureSources[device].registerClient();

        }

        /// <summary>
        /// Unregisters the texture device described by device.
        /// </summary>
        /// <param name="device">The device. </param>
        public void unregisterTextureDevice(int device)
        {
            if (device >= 0 && device < textureSources.Length && textureSources[device] != null)
                textureSources[device].unregisterClient();
        }

        /// <summary>
        /// Flag to mark if build workaround has evaded MetaInit.
        /// </summary>
        private static bool _textureInit = false;

        /// <summary>
        /// Register the texture client.  This is a hack.  It awaits a more permenant fix.  Note difference
        /// betweeen Unity editor (MetaInit) and build (which invokes registureTextureDevice).
        /// </summary>
        /// <see cref="https://meta-view.atlassian.net/browse/MU-256"/>
        private void initializeTexture()
        {
            if (_textureInit) return;

            //Debug.Log("About to register camera texture.");
            registerCameraTexture();
            _textureInit = true;
        }

        public DeviceTextureSource(MetaSensors metaSensors, DeviceInfo deviceInfo)
        {
            _metaSensors = metaSensors;
            _deviceInfo = deviceInfo;
        }

        /// <summary>
        /// Starts this Meta.DeviceTextureSource.
        /// </summary>
        //public void MetaInit() - from IMetaEventReceiver
        public void Init(ref EventHandlers eventHandlers)
        {
            _rgbExposure = new RgbExposure(ref eventHandlers);

            eventHandlers.updateEvent += Update;
            eventHandlers.onDestroyEvent += OnDestroy;
        }

        /// <summary>
        /// Updates this Meta.DeviceTextureSource.
        /// </summary>
        public void Update()
        {
            if (_metaSensors.GetDeviceInfoValid())
            {
                if (!_textureInit)
                {
                    registerCameraTexture();
                    _textureInit = true;
                }
                

                for (int i = 0; i < textureSources.Length; i++)
                {
                    if (textureSources[i] != null)
                    {

                        textureSources[i].Update();
                    }
                }
                _rgbExposure.UpdateColorExposure();
                _rgbExposure.UpdateColorGain();
            }
        }
       
        public void OnDestroy()
        {
            _textureInit = false;
        }

    }

}