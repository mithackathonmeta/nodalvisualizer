﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace Meta
{
    internal class RgbExposure
    {
        /// <summary>
        /// Set the color camera gain.
        /// </summary>
        /// <param name="iGain">Color camera gain.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "SetColorGain")]
        internal static extern void SetColorGain(int iGain);

        /// <summary>
        /// Set the color camera exposure.
        /// </summary>
        /// <param name="iExpose">Color camera exposure.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "SetColorExposure")]
        internal static extern void SetColorExposure(int iExpose);

        internal RgbExposure(ref EventHandlers eventHandlers)
        {
            LoadValues();

            eventHandlers.onDestroyEvent += OnDestroy;
        }
        internal void OnDestroy()
        {
            SaveValues();
        }

        /// <summary>
        /// Load the saved gain and exposure values from PlayerPrefs.
        /// TODO: Refactor to use user settings API (when implemented)
        /// </summary>
        private void LoadValues()
        {
            int gain = PlayerPrefs.GetInt("MetaRGBColorGain");
            int exposure = PlayerPrefs.GetInt("MetaRGBColorExposure");

            if (gain != 0)
            {
                ColorGain = gain;
            }
            if (exposure != 0)
            {
                ColorExposure = exposure;
            }
        }

        /// <summary>
        /// Save the gain and exposure values to PlayerPrefs.
        /// TODO: Refactor to use user settings API (when implemented)
        /// </summary>
        private void SaveValues()
        {
            PlayerPrefs.SetInt("MetaRGBColorGain", ColorGain);
            PlayerPrefs.SetInt("MetaRGBColorExposure", ColorExposure);
        }

        /// <summary>
        /// Color camera gain
        /// </summary>
        private int m_colorGain = 32;
        public int ColorGain
        {
            get { return m_colorGain; }
            set { m_colorGain = value; }
        }
        internal void UpdateColorGain()
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                m_colorGain++;
                SetColorGain(m_colorGain);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                m_colorGain--;
                if (m_colorGain < 1) m_colorGain = 1;
                SetColorGain(m_colorGain);
            }

        }

        /// <summary>
        /// Color camera exposure
        /// </summary>
        private int m_colorExposure = -4;
        public int ColorExposure
        {
            get { return m_colorExposure; }
            set { m_colorExposure = value; }
        }
        internal void UpdateColorExposure()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                m_colorExposure++;
                if (m_colorExposure > -3) m_colorExposure = -3;
                SetColorExposure(m_colorExposure);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                m_colorExposure--;
                SetColorExposure(m_colorExposure);
            }


        }
    }
}