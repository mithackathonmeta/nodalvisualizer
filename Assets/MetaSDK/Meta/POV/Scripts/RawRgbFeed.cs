﻿using UnityEngine;
using System.Collections;
using Meta;

///<summary> An example to use camera feed via code.</summary>
///
///<seealso cref="T:UnityEngine.MonoBehaviour"/>
internal class RawRgbFeed : MetaBehaviour 
{

    public int sourceDevice = 1;  // in inspector, for color feed texture set value = 0, for depth set value = 1, for ir set value = 2;
    /*WARNING: the depthdata is converted to rgb space for display purposes. The values in the depth texture do not represent the actual depth value*/

    public MeshRenderer renderTarget = null;

    public Texture2D cameraTexture = null;

    private bool registered = false;
  
    void Start()
    {
        
    }

    void Update()
    {
        if (!registered && metaContext.Get<DeviceTextureSource>() != null)
        {
            metaContext.Get<DeviceTextureSource>().registerTextureDevice(sourceDevice);
            //get the texture
            if (metaContext.Get<DeviceTextureSource>().IsDeviceTextureRegistered(sourceDevice))
            {
                registered = true;

                cameraTexture = metaContext.Get<DeviceTextureSource>().GetDeviceTexture(sourceDevice);

                //Debug.Log("height" + cameraTexture.height);
                //Debug.Log("width" + cameraTexture.width);

                // if a rendering target is set. Display it
                if (renderTarget != null && renderTarget.material != null)
                {
                    if (metaContext.Get<DeviceTextureSource>() != null) // && metaContext.Get<DeviceTextureSource>().enabled)
                    {
                        renderTarget.material.mainTexture = cameraTexture;
                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        if (metaContext.Get<DeviceTextureSource>() != null)
        {
            metaContext.Get<DeviceTextureSource>().unregisterTextureDevice(sourceDevice);
        }
    }




}
