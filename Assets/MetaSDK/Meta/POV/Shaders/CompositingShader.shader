Shader "Meta/CompositingShader" {
	Properties {
		// _MainTex ("Base (RGB)", 2D) = "black" {}
        _Content("Content", 2D) = "black" {}
        _CameraFeed("Camera Feed", 2D) = "black" {}
        _Occlusion("Occlusion", 2D) = "black" {}
        _OcclusionDepth("Occlusion Depth", 2D) = "black" {}
        _ContentDepth("Content Depth", 2D) = "black" {}
        _Hulkness("Hulkness", int) = 0
        _RgbFeedIntensity("RgbFeedIntensity", float) = 1.0
        _ContentIntensity("ContentIntensity", float) = 1.0
        _AlphaThreshold("AlphaThreshold", float) = 1.0
        _ContentAlpha("ContentAlpha", float) = 0.8
	}
	
	// Shader code pasted into all further CGPROGRAM blocks
	CGINCLUDE
	
	#include "UnityCG.cginc"
	
	struct v2f {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};
	
	// sampler2D _MainTex;
    sampler2D _CameraFeed;

    sampler2D _Content;
    sampler2D _ContentDepth;
    
    sampler2D _Occlusion;
    sampler2D _OcclusionDepth;
    
    int _Hulkness = 0;
    float _RgbFeedIntensity = 1.0f;
    float _ContentIntensity = 1.0f;
    float _AlphaThreshold = 1.0f;
    float _ContentAlpha = 1.0f;

	
    // Distortion factor
    float cX = 640.2231f;
    float cY = 368.5472f;

    float k1 = -0.04361237f;
    float k2 = 0.1805779f;
    float k3 = -0.3940473f;

    float fX = 861.0092f;
    float fY = 861.2428f;

    float maxX = 1280.0f;
    float maxY = 720.0f;

    float zoom = 1.0f;

    int _Radius = 10;


    float SampleOcclusionMask(half2 uv) {
        float depth_occlusion = tex2D(_OcclusionDepth, uv);
        float depth_content = tex2D(_ContentDepth, uv);
        half4 color_occlusion = tex2D(_Occlusion, uv);

        if (color_occlusion.x == 0) {
            return 0.0;
        }

        if (depth_occlusion > depth_content) {
            return 0.0;
        }
        else {
            return 1.0;
        }
    }

    // Returns a value that increases from 0 to 1 with proximity to pixels of near-zero value
    float proximityToZero(half2 uv) {
        const int SEARCH_RADIUS = 8;                          // Radius of search footprint
        const float ZERO_THRESHOLD = 0.001;                    // Cutoff intensity for zero detection
        const float step_x = 0.0005, step_y = 0.0005;
        float MAX_R2 = SEARCH_RADIUS*SEARCH_RADIUS;   // Cutoff distance for zero detection


        float min_r2 = MAX_R2;
        int i, j, dx2, dy2;
        half2 uv_neighbor;
        half4 color_content;
        for (i = -SEARCH_RADIUS; i <= SEARCH_RADIUS; i++) {
            dx2 = i*i;
            uv_neighbor.x = uv.x + i*step_x;
            for (j = -SEARCH_RADIUS; j <= SEARCH_RADIUS; j++) {
                dy2 = j*j;
                uv_neighbor.y = uv.y + j*step_y;
                color_content = tex2D(_Content, uv_neighbor);
                if (abs(color_content.r + color_content.g + color_content.b) < ZERO_THRESHOLD)
                    min_r2 = min(min_r2, dx2 + dy2);
            }
        }

        return min(min_r2, MAX_R2) / MAX_R2;
    }


    // Returns a value that increases from 0 to 1 with proximity to pixels of near-zero value
    float occlusionKernel(half2 uv) {
        const int SEARCH_RADIUS = 5;                          // Radius of search footprint
        const float ZERO_THRESHOLD = 0.001;                    // Cutoff intensity for zero detection
        const float step_x = 0.001, step_y = 0.001;
        float SIGMA2 = 9;   // Cutoff distance for zero detection
        int SQUANCH = 9;

        float sum = 0;
        int i, j, dx2, dy2;
        half2 uv_neighbor;
        half4 color_content;
        for (i = -SEARCH_RADIUS; i <= SEARCH_RADIUS; i++) {
            dx2 = i*i;
            uv_neighbor.x = uv.x + i*step_x;
            for (j = -SEARCH_RADIUS; j <= SEARCH_RADIUS; j++) {
                dy2 = j*j;
                uv_neighbor.y = uv.y + j*step_y;
                color_content = tex2D(_Content, uv_neighbor);
                if (abs(color_content.r + color_content.g + color_content.b) < ZERO_THRESHOLD)
                    sum += exp(-(dx2 + dy2) / SIGMA2);
            }
        }

        return 1-min(1,sum/SQUANCH);
    }

    float GaussianOcclusion(half2 uv) {

        float sum = 0;
        int i = 0;
        int j = 0;

        // float coef = .0f * (float)_Radius * (float)_Radius + 4.0f * (float)_Radius + 1.0f;

        for (i = -_Radius; i <= _Radius; ++i) {
            for (j = -_Radius; j <= _Radius; ++j) {
                half2 pos = half2(uv.x + i, uv.y + j);
                sum += SampleOcclusionMask(pos);
            }
        }

        sum /= 2;

        return sum;
    }


	v2f vert( appdata_img v ) 
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;
		return o;

        /*
        v2f o;
        o.pos = float4(2.0 * v.texcoord.x - 1, 2.0 * v.texcoord.y - 1.0, 0.0, 1.0);
        o.uv = v.texcoord;
        return o;
        */
	} 
	
    // UNDISTORTING CAMERA IMAGE
	half4 frag(v2f i) : SV_Target 
	{
		half2 coords = i.uv;
        
        half4 color_feed = tex2D(_CameraFeed, coords);
        half4 color_content = tex2D(_Content, coords);
        half4 color_occlusion = tex2D(_Occlusion, coords);

        half depth_occlusion = tex2D(_OcclusionDepth, coords);
        half depth_content = tex2D(_ContentDepth, coords);

#ifdef HULK
        // if (apply_hulk == 1) {
        //     return color_feed + half4(0, color_occlusion.x, 0, 0);
        // }
#endif

    //    return (_RgbFeedIntensity * color_feed + _ContentIntensity * color_content) / 2.0;
        float alph = occlusionKernel(coords)*.9;

        /*
        if (depth_occlusion > depth_content)
            return half4(1, 0, 0, 0);
        else
            return half4(0, 0, 0, 0);
            */

        //float alph = proximityToZero(coords);
        
        //return lerp(_RgbFeedIntensity * color_feed + _ContentIntensity * color_content, color_content, alph);


        // --------------------------------
        // FINAL SHADER ----- DO NOT CHANGE
        // FINAL SHADER ----- DO NOT CHANGE
        // FINAL SHADER ----- DO NOT CHANGE
        // --------------------------------
        //if (length(color_content.rgb) < _AlphaThreshold)
        //    return _RgbFeedIntensity * color_feed;
        //else

        // return color_content;

        return lerp(_RgbFeedIntensity * color_feed + _ContentIntensity * color_content, color_content, _ContentAlpha);
        
        
        
        // --------------------------------
        // FINAL SHADER ----- DO NOT CHANGE
        // FINAL SHADER ----- DO NOT CHANGE
        // FINAL SHADER ----- DO NOT CHANGE
        // --------------------------------






        // return pow(pow(_RgbFeedIntensity*color_feed, 1 / 1.8) + pow(_ContentIntensity * color_content, 1 / 1.2), 1.8);

        /*

        if (depth_occlusion > depth_content) {  // Not occluded areas
        // if (depth_occlusion_blurred > depth_content) {  // Not occluded areas
        //  if (depth_content < 2.0f) {         // Content exists
        //     return color_content;
        //}
        //return color_feed;

            return color_content;

        }
        else {                  // Occluded areas
            return color_feed;
        }

        
        return half4(0, 0, 1, 0);

        */
	}


	ENDCG 
	
Subshader {
 Pass {
	  ZTest Always Cull Off ZWrite Off

      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      ENDCG
  }
  
}

Fallback off
	
} // shader
