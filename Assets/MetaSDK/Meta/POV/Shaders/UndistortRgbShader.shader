Shader "Meta/UndistortRgbShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "black" {}
        // _CameraFeed("Camera Feed", 2D) = "black" {}
	}
	
	// Shader code pasted into all further CGPROGRAM blocks
	CGINCLUDE
	
	#include "UnityCG.cginc"
	
	struct v2f {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};
	
	sampler2D _MainTex;
    // sampler2D _CameraFeed;
	
    // Distortion factor
    float cX = 640.2231f;
    float cY = 368.5472f;

    float k1 = -0.04361237f;
    float k2 = 0.1805779f;
    float k3 = -0.3940473f;

    float fX = 861.0092f;
    float fY = 861.2428f;

    float maxX = 1280.0f;
    float maxY = 720.0f;

    float zoom = 1.0f;
	
	v2f vert( appdata_img v ) 
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;
		return o;

        /*
        v2f o;
        o.pos = float4(2.0 * v.texcoord.x - 1, 2.0 * v.texcoord.y - 1.0, 0.0, 1.0);
        o.uv = v.texcoord;
        return o;
        */
	} 
	
    // UNDISTORTING CAMERA IMAGE
	half4 frag(v2f i) : SV_Target 
	{
		half2 coords = i.uv;
		
        // OUR SDK FLIPS THE TEXTURE
        // THIS IS A HACK TO FIX THIS
        // -Agis
        // coords.y = 1.0 - coords.y;

        float2 pix;
        pix.x = (coords.x * maxX - cX) / fX;
        pix.y = (coords.y * maxY - cY) / fY;

        // get more FOV in
        pix.x = pix.x * zoom;
        pix.y = pix.y * zoom;

        float r2 = (pix.y * pix.y) + (pix.x * pix.x);
        float r4 = r2 * r2;

        
        float2 dpix;
        dpix.x = pix.x * (1.0f + k1 * r2 + k2 * r4 + k3 * r4 * r2);
        dpix.y = pix.y * (1.0f + k1 * r2 + k2 * r4 + k3 * r4 * r2);

        float2 di;
        di.x = (dpix.x * fX + cX) / maxX;
        di.y = (dpix.y * fY + cY) / maxY;

        if (di.x < 0 || di.x > 1 || di.y < 0 || di.y > 1) {
            return half4(0, 0, 0, 0);
        }

		// half4 color = tex2D (_CameraFeed, di) - half4(0.1, 0.15, 0.1, 0);
        half4 color = pow((pow(tex2D(_MainTex, di), 1 / 1.2) - pow(half4(0.05, 0.08, 0.05, 0), 1 / 1.2)), 1.4);

		return pow(color, 1.0);
	}

	ENDCG 
	
Subshader {
 Pass {
	  ZTest Always Cull Off ZWrite Off

      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      ENDCG
  }
  
}

Fallback off
	
} // shader
