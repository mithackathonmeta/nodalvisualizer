﻿using UnityEngine;
using System.Collections;

namespace Meta.HandInput
{
    [RequireComponent(typeof(SphereCollider))]
    public abstract class PointHandFeature : HandFeature
    {
        private readonly AnimationCurve _dampCurve = new AnimationCurve();
        private Transform _handsTransform;
        private Vector3 _handVelocity;

        /// <summary>
        /// Transform in Meta2 Prefab to transform local hand data into world space.
        /// </summary>
        protected Transform HandsTransform
        {
            get { return _handsTransform; }
        }

        protected override void Start()
        {
            base.Start();
            //Debug.Log(GetType().AssemblyQualifiedName);
            _handsTransform = FindObjectOfType<MetaManager>().transform.Find("MetaCameras/DepthOcclusion");

            //construct curve, through internal testing these values were determined to produce ideal results
            _dampCurve.AddKey(new Keyframe(0f, .15f, 0f, -200f * Mathf.Deg2Rad));
            _dampCurve.AddKey(new Keyframe(.06f, .015f, 0f, 0f));
        }

        private void Update()
        {
            if (ParentHand.Data != null)
            {
                UpdateHandPosition();
            }
        }

        protected abstract Vector3 GetLocalPosition();

        private Vector3 GetWorldPosition()
        {
            Vector3 localPosition = GetLocalPosition();
            Vector3 FlipXY = new Vector3(-1, -1, 1);
            return HandsTransform.localToWorldMatrix.MultiplyPoint3x4(Vector3.Scale(localPosition, FlipXY));
        }

        private void UpdateHandPosition()
        {
            Vector3 worldPosition = GetWorldPosition();

            if (ParentHand.BufferedIsValid() && ParentHand.Data.isValid)
            {
                float deltaMagnitude = (transform.position - worldPosition).sqrMagnitude * 100f;
                float deltaDamp = _dampCurve.Evaluate(deltaMagnitude);
                Rigidbody.MovePosition(Vector3.SmoothDamp(transform.position, worldPosition, ref _handVelocity, deltaDamp));
            }
            else if (ParentHand.BufferedIsValid())
            {
                
            }
            else
            {
                transform.position = worldPosition;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(transform.position, .005f);
            //Enforce proper setup in editor
            HandUtility.ValidateCollider(gameObject);
        }
    }
}