﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Meta.HandInput
{
    /// <summary>
    /// Will gather Meta HandFeature GameObjects when they enter and exit the Trigger on this GameObject.
    /// Use whenver you need to determine the entry and exit of the HandFeature into a particular area.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class HandVolume : MonoBehaviour
    {
        [SerializeField]
        private HandVolumeEvent _twoHandEnterEvent = new HandVolumeEvent();

        [SerializeField]
        private HandFeatureEvent _handFeatureEnterEvent = new HandFeatureEvent();

        [SerializeField]
        private HandFeatureEvent _firstHandFeatureEnterEvent = new HandFeatureEvent();

        [SerializeField]
        private HandVolumeEvent _twoHandExitEvent = new HandVolumeEvent();

        [SerializeField]
        private HandFeatureEvent _handFeatureExitEvent = new HandFeatureEvent();

        [SerializeField]
        private HandFeatureEvent _lastHandFeatureExitEvent = new HandFeatureEvent();

        /*TODO Write UI for this not need these strings typed in
        "Meta.HandInput.TopHandFeature, Assembly-CSharp",
        "Meta.HandInput.CenterHandFeature, Assembly-CSharp",
        "Meta.HandInput.LinkHandFeature, Assembly-CSharp",
        */
        [SerializeField]
        private List<string> _allowedTypeList = new List<string>();

        [SerializeField]
        private Vector3 _expandOnEntry = new Vector3(1.1f, 1.1f, 1.1f);

        [SerializeField]
        private bool _showCursor = true;

        private readonly List<HandFeature> _handFeatureList = new List<HandFeature>();
        private bool _twoHandsEntered;
        private Type[] _allowedTypes;
        private Vector3 _initialScale;

        public bool ShowCursor
        {
            get { return _showCursor; }
        }

        public HandVolumeEvent TwoHandEnterEvent
        {
            get { return _twoHandEnterEvent; }
        }

        public HandFeatureEvent HandFeatureEnterEvent
        {
            get { return _handFeatureEnterEvent; }
        }

        public HandFeatureEvent FirstHandFeatureEnterEvent
        {
            get { return _firstHandFeatureEnterEvent; }
        }

        public HandVolumeEvent TwoHandExitEvent
        {
            get { return _twoHandExitEvent; }
        }

        public HandFeatureEvent HandFeatureExitEvent
        {
            get { return _handFeatureExitEvent; }
        }

        public HandFeatureEvent LastHandFeatureExitEvent
        {
            get { return _lastHandFeatureExitEvent; }
        }

        private void Start()
        {
            HandUtility.ValidateCollider(gameObject);
            HandUtility.ValidateLayer(gameObject);
            _allowedTypes = new Type[_allowedTypeList.Count];
            for (int i = 0; i < _allowedTypes.Length; ++i)
            {
                _allowedTypes[i] = Type.GetType(_allowedTypeList[i]);
            }
            _initialScale = transform.localScale;
        }

        private void OnTriggerEnter(Collider collider)
        {
            HandFeature handFeature = collider.GetComponent<HandFeature>();

            if (handFeature != null && IsAllowedType(handFeature.GetType()))
            {
                if (_firstHandFeatureEnterEvent != null && _handFeatureList.Count == 0)
                {
                    transform.localScale = Vector3.Scale(transform.localScale, _expandOnEntry);
                    _firstHandFeatureEnterEvent.Invoke(handFeature);
                }
                if (_handFeatureEnterEvent != null)
                {
                    _handFeatureEnterEvent.Invoke(handFeature);
                }

                _handFeatureList.Add(handFeature);
                handFeature.EnterHandVolume(this);

                //if both HandPart enter for first time
                if (!_twoHandsEntered && GetHand(HandType.LEFT) != null && GetHand(HandType.RIGHT) != null)
                {
                    _twoHandsEntered = true;

                    if (_twoHandEnterEvent != null)
                    {
                        _twoHandEnterEvent.Invoke(this);
                    }
                }
            }
        }

        private void OnTriggerExit(Collider collider)
        {
            HandFeature handFeature = collider.GetComponent<HandFeature>();

            if (handFeature != null && IsAllowedType(handFeature.GetType()))
            {
                if (_lastHandFeatureExitEvent != null && _handFeatureList.Count == 1)
                {
                    transform.localScale = _initialScale;
                    _lastHandFeatureExitEvent.Invoke(handFeature);
                }
                if (_handFeatureExitEvent != null)
                {
                    _handFeatureExitEvent.Invoke(handFeature);
                }

                _handFeatureList.Remove(handFeature);
                handFeature.ExitHandVolume(this);

                //if both hands have exited
                if (_twoHandsEntered && GetHand(HandType.LEFT) == null && GetHand(HandType.RIGHT) == null)
                {
                    _twoHandsEntered = false;

                    if (_twoHandExitEvent != null)
                    {
                        _twoHandExitEvent.Invoke(this);
                    }
                }
            }
        }

        private bool IsAllowedType(Type type)
        {
            //if no types specified, allow all
            return _allowedTypes.Length == 0 || _allowedTypes.Contains(type);
        }

        public int HandCount()
        {
            int count = 0;

            if (GetHand(HandType.LEFT) != null)
            {
                count++;
            }

            if (GetHand(HandType.RIGHT) != null)
            {
                count++;
            }

            return count;
        }

        public bool ContainsHand(HandFeature handFeature)
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i] == handFeature)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieves any hand without condition.
        /// </summary>
        public HandFeature GetHand()
        {
            if (_handFeatureList.Count > 0)
            {
                return _handFeatureList[0];
            }

            return null;
        }

        /// <summary>
        /// Retrieves HandFeature if it has passed the grab threshold.
        /// </summary>
        public HandFeature GetGrabbingHand()
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i].ParentHand.Grabbing())
                {
                    return _handFeatureList[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves HandFeature if it has passed the grab threshold.
        /// </summary>
        public HandFeature GetGrabbingHand(float threshold)
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i].ParentHand.Grabbing(threshold))
                {
                    return _handFeatureList[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves HandFeature of type if it has passed the global grab threshold.
        /// </summary>
        public HandFeature GetGrabbingHand(HandType type)
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i].ParentHand.Data.type == type && _handFeatureList[i].ParentHand.Grabbing())
                {
                    return _handFeatureList[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Retrieves HandFeature of type if it has passed the provided grab threshold.
        /// </summary>
        public HandFeature GetGrabbingHand(HandType type, float threshold)
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i].ParentHand.Data.type == type && _handFeatureList[i].ParentHand.Grabbing(threshold))
                {
                    return _handFeatureList[i];
                }
            }
            return null;
        }

        public HandFeature GetHand<THandFeature>(HandType type) where THandFeature : HandFeature
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i] is THandFeature && _handFeatureList[i].ParentHand.Data.type == type)
                {
                    return _handFeatureList[i];
                }
            }

            return null;
        }

        public HandFeature GetHand<THandFeature>() where THandFeature : HandFeature
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i] is THandFeature)
                {
                    return _handFeatureList[i];
                }
            }

            return null;
        }

        public HandFeature GetHand(HandType type)
        {
            for (int i = 0; i < _handFeatureList.Count; ++i)
            {
                if (_handFeatureList[i].ParentHand.Data.type == type)
                {
                    return _handFeatureList[i];
                }
            }

            return null;
        }

        private void OnDrawGizmos()
        {
            //Enforce proper setup in editor
            HandUtility.ValidateCollider(gameObject);
        }
    }
}