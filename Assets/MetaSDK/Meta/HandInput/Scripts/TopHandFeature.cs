﻿using UnityEngine;

namespace Meta.HandInput
{
    public class TopHandFeature : PointHandFeature
    {
        protected override Vector3 GetLocalPosition()
        {
            return ParentHand.Data.top;
        }
    }
}