﻿using UnityEngine;

namespace Meta.HandInput
{
    public class CenterHandFeature : PointHandFeature
    {
        protected override Vector3 GetLocalPosition()
        {
            return ParentHand.Data.center;
        }
    }
}