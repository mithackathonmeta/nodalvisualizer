﻿using UnityEngine;

namespace Meta.HandInput
{
    [RequireComponent(typeof(CapsuleCollider))]
    public class LinkHandFeature : HandFeature
    {
        [SerializeField]
        private PointHandFeature[] _pointHandFeatures = null;
        private CapsuleCollider _collider;

        protected override void Start()
        {
            base.Start();

            _collider = GetComponent<CapsuleCollider>();
        }

        private void FixedUpdate()
        {
            if (ParentHand.Data != null && ParentHand.BufferedIsValid())
            {
                float height = Vector3.Distance(_pointHandFeatures[0].transform.position, _pointHandFeatures[1].transform.position);
                _collider.height = Mathf.Clamp(height, 0f, .5f) * 2f;
                Rigidbody.MovePosition(_pointHandFeatures[0].transform.position);
                Vector3 forward = _pointHandFeatures[1].transform.position - _pointHandFeatures[0].transform.position;

                if (forward != Vector3.zero)
                {
                    Rigidbody.MoveRotation(Quaternion.LookRotation(forward));
                }
            }
            else
            {
                transform.position = _pointHandFeatures[1].transform.position;
                _collider.height = 0f;
            }
        }
    }
}