﻿using UnityEngine;

namespace Meta.HandInput
{
    /// <summary>
    /// Global settings for both Hands and all HandFeatures
    /// </summary>
    public class HandsSettings : MonoBehaviour
    {
        [SerializeField]
        private AudioClip _grabAudioClip = null;

        [SerializeField]
        private AudioClip _releaseAudioClip = null;

        [SerializeField]
        private float _grabThreshold = .2f;

        [SerializeField]
        private float _releaseThreshold = .5f;

        public AudioClip GrabAudioClip
        {
            get { return _grabAudioClip; }
        }

        public AudioClip ReleaseAudioClip
        {
            get { return _releaseAudioClip; }
        }

        public float GrabThreshold
        {
            get { return _grabThreshold; }
        }

        public float ReleaseThreshold
        {
            get { return _releaseThreshold; }
        }

        private void Start()
        {
            transform.parent = null;
        }
    }
}