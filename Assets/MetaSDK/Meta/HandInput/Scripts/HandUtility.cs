﻿using UnityEngine;
using System.Collections;

namespace Meta.HandInput
{
    public static class HandUtility
    {
        public const string HandFeatureLayerName = "HandFeature";

        public static void ValidateCollider(GameObject gameObject)
        {
            if (!gameObject.GetComponent<Collider>().isTrigger)
            {
                Debug.LogWarning("Setting Collider associated with " + gameObject.name + " to HandFeature layer.This is required to interact with the HandFeature GameObjects in the MetaHands prefab.");
                gameObject.GetComponent<Collider>().isTrigger = true;
            }
        }

        public static void ValidateLayer(GameObject gameObject)
        {
            int layer = LayerMask.NameToLayer(HandFeatureLayerName);
            if (layer != -1)
            {
                Debug.LogWarning("Setting " + gameObject.name + " to HandFeature layer.");
                gameObject.layer = layer;
            }
        }
    }
}
