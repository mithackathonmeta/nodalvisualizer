﻿using UnityEngine;

namespace Meta.HandInput
{
    public class FarthestZHandFeature : PointHandFeature
    {
        protected override Vector3 GetLocalPosition()
        {
            return ParentHand.Data.farthestZ;
        }
    }
}