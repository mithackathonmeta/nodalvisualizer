﻿using System;
using UnityEngine;

namespace Meta.HandInput
{
    /// <summary>
    /// Represents a piece of the sdk HandData in the world.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public abstract class HandFeature : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private Hand _parentHand;
        private HandVolume _inHandVolume;

        /// <summary>
        /// Hand which this feature belongs to.
        /// </summary>
        public Hand ParentHand
        {
            get { return _parentHand; }
        }

        public Rigidbody Rigidbody
        {
            get { return _rigidbody; }
        }

        public HandVolume InHandVolume
        {
            get { return _inHandVolume; }
        }

        protected virtual void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _parentHand = GetComponentInParent<Hand>();
        }

        public void GrabbedGameObject(GameObject gameObject)
        {
            _parentHand.GrabbedGameObject(gameObject);
        }

        public void ReleasedGameObject(GameObject gameObject)
        {
            _parentHand.ReleasedGameObject(gameObject);
        }

        public void EnterHandVolume(HandVolume volume)
        {
            _inHandVolume = volume;
            _parentHand.EnterHandVolume(volume, this);
        }

        public void ExitHandVolume(HandVolume volume)
        {
            _inHandVolume = null;
            _parentHand.ExitHandVolume(volume, this);
        }
    }
}
