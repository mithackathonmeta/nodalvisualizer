﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Meta.UI;

namespace Meta.HandInput
{
    public class FixedSizeQueue<T> : Queue<T>
    {
        public int QueueSize { get; set; }
        /*
        public void FixedSizeQueue<T>(int queueSize)
        {
            QueueSize = queueSize;
        }
        */
        public new void Enqueue(T obj)
        {
            base.Enqueue(obj);

            while (base.Count > QueueSize)
            {
                base.Dequeue();
            }
        }

        public bool QueueFull()
        {
            return base.Count == QueueSize;
        }
    }

    /// <summary>
    /// Represents sdk HandData in the game world.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class Hand : MetaBehaviour
    {
        [SerializeField]
        private HandType _type = HandType.UNKNOWN;

        [SerializeField]
        private GrabEvent _grabEvent = new GrabEvent();
        [SerializeField]
        private GrabEvent _releaseEvent = new GrabEvent();

        private const int _bufferCount = 32;
        private HandCursor _cursor;
        private const float GrabDamp = .1f;
        private readonly List<HandFeature> _handFeatureInHandVolumeList = new List<HandFeature>();
        private readonly List<GameObject> _grabbingGameObjectList = new List<GameObject>();
        private List<HandFeature> _childHandFeatureList = new List<HandFeature>();
        private InteractionEngine _interaction;
        private HandsSettings _settings;
        private AudioSource _audioSource;
        private HandData _data;
        private float _grabVelocity;
        private float _dampedGrabResidual;

        private bool _setPriorGrabResidual = true;
        private float _priorDampedGrabResidual;
        private float _grabResidualChangeTolerance = .1f;
        private int _bufferedNotValidCount;
        private FixedSizeQueue<float> _grabResidualQueue;
        private int _lastFrameId;

        public HandData Data
        {
            get { return _data; }
        }

        public float DampedGrabResidual
        {
            get { return _dampedGrabResidual; }
        }

        public GrabEvent onGrabEvent
        {
            get { return _grabEvent; }
            set { SetPropertyUtility.SetClass(ref _grabEvent, value); }
        }

        public GrabEvent onReleaseEvent
        {
            get { return _releaseEvent; }
            set { SetPropertyUtility.SetClass(ref _releaseEvent, value); }
        }

        private IEnumerator Start()
        {
            _settings = GetComponentInParent<HandsSettings>();
            _audioSource = GetComponent<AudioSource>();
            _interaction = metaContext.Get<InteractionEngine>();
            _childHandFeatureList = GetComponentsInChildren<HandFeature>().ToList();
            _cursor = GetComponentInChildren<HandCursor>();

            _lastFrameId = -1;

            _grabResidualQueue = new FixedSizeQueue<float>();
            _grabResidualQueue.QueueSize = 10;

            //Information from SDK is not immediately available, wait until avaialble
            while (_interaction.leftHand == null && _interaction.rightHand == null)
            {
                yield return null;
            }

            if (_type == HandType.LEFT)
            {
                _data = _interaction.leftHand;
            }
            else if (_type == HandType.RIGHT)
            {
                _data = _interaction.rightHand;
            }
        }

        private void Update()
        {
            ShouldShowHandCursor();

            if (Data != null && Data.frameID > _lastFrameId)
            {
                if (_cursor != null)
                {
                    _cursor.NormalizedGrabResidual = Mathf.InverseLerp(
                        _settings.ReleaseThreshold,
                        _settings.GrabThreshold,
                        _dampedGrabResidual);
                }
                /*
                CalculateBufferedIsValid();
                FilterGrabResidual();
                */

                RollingAverageGrabResidual();

                _lastFrameId = Data.frameID;
            }

            if (Input.GetKeyUp(KeyCode.G) && _type == HandType.RIGHT)
            {
                onGrabEvent.Invoke(this, gameObject);
            }

            if (Input.GetKeyUp(KeyCode.R) && _type == HandType.RIGHT)
            {
                onReleaseEvent.Invoke(this, gameObject);
            }
        }

        private void RollingAverageGrabResidual()
        {
            if (Data.isValid)
            {
                _grabResidualQueue.Enqueue(Data.grabResidual);
                _dampedGrabResidual = _grabResidualQueue.Average();
            }
            else
            {
                _dampedGrabResidual = _settings.ReleaseThreshold + 1f;
                _grabResidualQueue.Clear();
            }
        }

        private void CalculateBufferedIsValid()
        {
            if (Data.isValid)
            {
                if (_bufferedNotValidCount > 0)
                {
                    _bufferedNotValidCount--;
                }
            }
            else
            {
                if (_bufferedNotValidCount < _bufferCount)
                {
                    _bufferedNotValidCount++;
                }
            }
        }

        private void FilterGrabResidual()
        {
            //when hand enters for first time grab its residual so that the tolerance
            //of allowed changed is within range of where it started
            if (!BufferedIsValid())
            {
                _setPriorGrabResidual = true;
            }
            else if (_setPriorGrabResidual)
            {
                _setPriorGrabResidual = false;
                _dampedGrabResidual = Data.grabResidual;
                _priorDampedGrabResidual = Data.grabResidual;
            }

            //this is a hack to try and filter out the times when the grab residual will erroneously jump .5 to 2 for 3 to 8 frames.
            float newDampedGrabResidual = Mathf.SmoothDamp(_dampedGrabResidual, Data.grabResidual, ref _grabVelocity, GrabDamp);
            if (Mathf.Abs(newDampedGrabResidual - _priorDampedGrabResidual) < _grabResidualChangeTolerance && BufferedIsValid())
            {
                _dampedGrabResidual = newDampedGrabResidual;
                _priorDampedGrabResidual = _dampedGrabResidual;
            }
        }

        /// <summary>
        /// Returns an IsValid which watches has error checking on top of what comes from HandData directly
        /// </summary>
        public bool BufferedIsValid()
        {
            return _bufferedNotValidCount < _bufferCount / 2;
        }

        public HandFeature GetChildHandFeature<THandFeature>() where THandFeature : HandFeature
        {
            return _childHandFeatureList.Find(h => h is THandFeature);
        }

        public bool Grabbing()
        {
            return Grabbing(_settings.GrabThreshold);
        }

        public bool Grabbing(float threshold)
        {
            return _dampedGrabResidual < threshold && BufferedIsValid();
        }

        public bool Released()
        {
            return Released(_settings.ReleaseThreshold);
        }

        public bool Released(float threshold)
        {
            return _dampedGrabResidual > threshold || !BufferedIsValid();
        }

        public void GrabbedGameObject(GameObject gameObject)
        {
            if (onGrabEvent != null)
            {
                onGrabEvent.Invoke(this, gameObject);
            }

            _grabbingGameObjectList.Add(gameObject);

            if (_settings.GrabAudioClip != null)
            {
                _audioSource.PlayOneShot(_settings.GrabAudioClip);
            }

            if (_cursor != null)
            {
                _cursor.Grabbed();
            }
        }

        public void ReleasedGameObject(GameObject gameObject)
        {
            if (onReleaseEvent != null)
            {
                onReleaseEvent.Invoke(this, gameObject);
            }

            _grabbingGameObjectList.Remove(gameObject);

            if (_settings.ReleaseAudioClip != null)
            {
                _audioSource.PlayOneShot(_settings.ReleaseAudioClip);
            }

            if (_cursor != null)
            {
                _cursor.Released();
            }
        }

        public void EnterHandVolume(HandVolume volume, HandFeature feature)
        {
            _handFeatureInHandVolumeList.Add(feature);
            _cursor.Visible(ShouldShowHandCursor());
        }

        public void ExitHandVolume(HandVolume volume, HandFeature feature)
        {
            _handFeatureInHandVolumeList.Remove(feature);
            _cursor.Visible(ShouldShowHandCursor());
        }

        private bool ShouldShowHandCursor()
        {
            for (int i = 0; i < _handFeatureInHandVolumeList.Count; ++i)
            {
                if (_handFeatureInHandVolumeList[i].InHandVolume != null &&
                    _handFeatureInHandVolumeList[i].InHandVolume.ShowCursor)
                {
                    return true;
                }
            }
            return false;
        }
    }
}