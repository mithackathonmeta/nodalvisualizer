﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    /// <summary>
    /// Toggles and positions the left and right hand light components.
    /// </summary>
    public class HandLightController : MonoBehaviour
    {
        public GameObject leftLight;
        public GameObject rightLight;

        private GameObject leftHand;
        private GameObject rightHand;

        void OnEnable()
        {
            leftLight.SetActive(true);
            rightLight.SetActive(true);
        }

        void OnDisable()
        {
            leftLight.SetActive(false);
            rightLight.SetActive(false);
        }

        void Start()
        {
            leftHand = GameObject.Find("LeftHand");
            rightHand = GameObject.Find("RightHand");

            // Set light culling mask to match Grid
            if (LayerMask.NameToLayer("Grid") != -1)
            {
                leftLight.GetComponent<Light>().cullingMask = 1 << LayerMask.NameToLayer("Grid");
                rightLight.GetComponent<Light>().cullingMask = 1 << LayerMask.NameToLayer("Grid");
            }
            else
            {
                leftLight.GetComponent<Light>().cullingMask = 1 << 11;   // Default Grid layer is 11
                rightLight.GetComponent<Light>().cullingMask = 1 << 11;
            }
        }

        /// <summary>
        /// Move position of light to the position of the hands
        /// </summary>
        void Update()
        {
            leftLight.transform.position = leftHand.transform.position;
            rightLight.transform.position = rightHand.transform.position;
        }
    }
}