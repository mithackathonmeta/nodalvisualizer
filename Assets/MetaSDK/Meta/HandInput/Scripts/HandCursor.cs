﻿using UnityEngine;
using System.Collections;
using Meta.Extensions;

namespace Meta.HandInput
{
    /// <summary>
    /// Cursor placed on back of hand will display when it has entered a HandVolume
    /// and will provide feedback for when it is grabbing inside a HandVolume.
    /// </summary>
    public class HandCursor : MonoBehaviour
    {
        [SerializeField]
        private Transform _ringSprite = null;

        [SerializeField]
        private Transform _centerIdleSprite = null;

        [SerializeField]
        private Transform _centerActiveSprite = null;

        public float NormalizedGrabResidual { get; set; }

        private readonly Vector3 _targetRingScale = new Vector3(.005f, .005f, .005f);
        private Vector3 _initalRingScale;
        private float _targetAlpha;
        private float _alphaVelocity;
        private float _alpha;
        private SpriteRenderer _ringSpriteRenderer;
        private SpriteRenderer _centerIdleSpriteRenderer;
        private SpriteRenderer _centerActiveSpriteRenderer;

        private void Awake()
        {
            _initalRingScale = _ringSprite.localScale;
            _centerActiveSprite.gameObject.SetActive(false);
            _ringSpriteRenderer = _ringSprite.GetComponent<SpriteRenderer>();
            _centerIdleSpriteRenderer = _centerIdleSprite.GetComponent<SpriteRenderer>();
            _centerActiveSpriteRenderer = _centerActiveSprite.GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            transform.LookAt(Camera.main.transform);
            if (!float.IsNaN(NormalizedGrabResidual))
            {
                _ringSprite.localScale = Vector3.Lerp(_initalRingScale, _targetRingScale, Mathf.Clamp(NormalizedGrabResidual, 0f, 1f));
                _alpha = Mathf.SmoothDamp(_alpha, _targetAlpha, ref _alphaVelocity, .1f);
                _ringSpriteRenderer.color = _ringSpriteRenderer.color.SetAlpha(_alpha);
                _centerIdleSpriteRenderer.color = _centerIdleSpriteRenderer.color.SetAlpha(_alpha);
                _centerActiveSpriteRenderer.color = _centerActiveSpriteRenderer.color.SetAlpha(_alpha);
            }
        }

        public void Visible(bool state)
        {
            if (state)
            {
                _targetAlpha = 1f;
            }
            else
            {
                _targetAlpha = 0f;
            }
        }

        public void Grabbed()
        {
            _centerActiveSprite.gameObject.SetActive(true);
            _centerIdleSprite.gameObject.SetActive(false);
        }

        public void Released()
        {
            _centerActiveSprite.gameObject.SetActive(false);
            _centerIdleSprite.gameObject.SetActive(true);
        }
    }
}