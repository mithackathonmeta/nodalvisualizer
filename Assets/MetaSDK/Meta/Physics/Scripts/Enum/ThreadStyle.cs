﻿namespace Meta.Physics
{ 
    public enum ThreadStyle
    {
        NoThreads,

        /// <summary>
        /// Use C# ThreadPool
        /// </summary>
        ThreadPool,

        /// <summary>
        /// Custom thread pool that has fewer allocations.
        /// </summary>
        CustomThreadPool,
    };
}