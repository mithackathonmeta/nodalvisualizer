﻿namespace Meta.Physics
{
    public enum SimulationSpace
    {
        Local,
        World,
    }
}