﻿namespace Meta.Physics
{
    public enum SurfaceCalculationsType
    {
        ClosestPointOnBounds,
        ClosestPointOnBox,
        Core,
        ClosestPointOnSphere,
        ClosestPointOnLine
    }
}