﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Collections;

namespace Meta.Physics
{
    /// <summary>
    /// MetaPhysics is an expirimental physics sytem to project forces directly
    /// from the pointcloud.
    /// 
    /// Central object which controls the whole MetaPhysics system. 
    /// Will scan from the hand pointcloud via OverlapSphere and then forward
    /// appropriate information onto the prospective MetaPhysicsAgent
    /// </summary>
    public class MetaPhysicsManager : MetaBehaviour
    {
        [SerializeField]
        internal PointProvider _pointProvider;

        [SerializeField]
        private LayerMask _collideWithLayers = 1;

        [SerializeField]
        private PhysicsProperties _physicsProperties = null;

        [SerializeField]
        private DebugInfo _generalInfo = null;

        [SerializeField]
        private int _maximumNumCloudPointsConsidered = 5000;

        [SerializeField]
        private int _bufferLength = 25;

        [Header("Threading")]
        [SerializeField]
        private ThreadStyle Threading = ThreadStyle.NoThreads;

        [SerializeField]
        private int CustomPoolNumThreads = 8;

        private static List<AgentWorker> _rigidbodyWorkers = new List<AgentWorker>();
        private static readonly ManualResetEvent[] _manualResetEventsBuffer = new ManualResetEvent[2];  // magic max # of waithandles
        private readonly Dictionary<int, List<MetaPhysicsAgent>> _dRigidBodyIdToAttachedAgents = new Dictionary<int, List<MetaPhysicsAgent>>();
        private readonly List<MetaPhysicsAgent> _agents = new List<MetaPhysicsAgent>();

        private int _priorUpdateId;
        private List<int> _rigidbodyIdsBuffer = new List<int>();
        private Collider[] _colliderBuffer;
        private RaycastHit[] _rayCastHitBuffer;
        private List<ScanPoint> _pointPool;

        private Queue _threadWorkQueue;
        private List<Thread> _threads;
        private volatile bool _killCustomThreads = true;

        public DebugInfo GeneralInfo
        {
            get { return _generalInfo; }
        }

        private void Awake()
        {
            _pointPool = new List<ScanPoint>();
            _colliderBuffer = new Collider[_bufferLength];
            _rayCastHitBuffer = new RaycastHit[_bufferLength];
            _managersCache = null;  // clear the managers cache
            _threadWorkQueue = System.Collections.Queue.Synchronized(new Queue());
            _threads = new List<Thread>();
        }

        private void Start()
        {
            StartCoroutine(SimulateLateFixedUpdateCoroutine());
        }

        private void OnDestroy()
        {
            _managersCache = null;
            DestroyCustomThreadPool();
        }

        private void FixedUpdate()
        {
            if (_pointProvider != null && _pointProvider.Initialized)
            {
                HandleHandsPhysics();
            }
        }

        public void RegisterAgent(MetaPhysicsAgent agent)
        {
            if (!_agents.Contains(agent))
            {
                _agents.Add(agent);
            }
        }

        private void HandleHandsPhysics()
        {
            if (_colliderBuffer.Length != _bufferLength)
            {
                _colliderBuffer = new Collider[_bufferLength];
            }

            if (_rayCastHitBuffer.Length != _bufferLength)
            {
                _rayCastHitBuffer = new RaycastHit[_bufferLength];
            }

            RefreshPointPool();
            RefreshPointsAffectingVirtualObjects();
            CalculateForcesFromPointsAffectingAgents();
        }

        private void CalculateForcesFromPointsAffectingAgents()
        {
            // Because colliders can come and go, let's recreate our mapping of rigidbody->Agent every time.
            foreach (var voList in _dRigidBodyIdToAttachedAgents.Values)
            {
                voList.Clear();
            }

            // Map rigidbody to its VOs
            for (int i = 0; i < _agents.Count; ++i)
            {
                MetaPhysicsAgent agent = _agents[i];
                if (agent == null || !agent.isActiveAndEnabled)
                { continue; }
                Collider coll = agent.GetComponent<Collider>();
                Rigidbody rb = coll ? coll.attachedRigidbody : null;
                if (!rb || rb.isKinematic)
                {
                    //UnityEngine.Debug.LogWarningFormat("No rigidbody for VO {0}", vo.name);
                    continue;
                }
                int rbKey = rb.GetInstanceID();

                List<MetaPhysicsAgent> voList;
                if (!_dRigidBodyIdToAttachedAgents.TryGetValue(rbKey, out voList))
                {
                    voList = new List<MetaPhysicsAgent>();
                    _dRigidBodyIdToAttachedAgents[rbKey] = voList;
                }

                voList.Add(agent);
            }

            if (null == _rigidbodyIdsBuffer)
            {
                _rigidbodyIdsBuffer = new List<int>();
            }
            _rigidbodyIdsBuffer.Clear();
            foreach (int i in _dRigidBodyIdToAttachedAgents.Keys)
            {
                _rigidbodyIdsBuffer.Add(i);
            }

            for (int i = 0; i < _rigidbodyIdsBuffer.Count; ++i)
            {
                int rbID = _rigidbodyIdsBuffer[i];
                if (_dRigidBodyIdToAttachedAgents[rbID].Count < 1)
                {
                    _dRigidBodyIdToAttachedAgents.Remove(rbID);
                }
            }

            // Is there any work to do??
            if (_dRigidBodyIdToAttachedAgents.Count < 1)
            {
                // No!  Get out of here.
                return;
            }

            UpdateCustomThreadPool();

            if (null == _rigidbodyWorkers)
            {
                _rigidbodyWorkers = new List<AgentWorker>(_dRigidBodyIdToAttachedAgents.Count);
            }

            // Kill any extra workers
            if (_rigidbodyWorkers.Count > _dRigidBodyIdToAttachedAgents.Count)
            {
                _rigidbodyWorkers.RemoveRange(_dRigidBodyIdToAttachedAgents.Count, _rigidbodyWorkers.Count - _dRigidBodyIdToAttachedAgents.Count);
            }

            int rigidbdyIndex = 0;
            foreach (var kvp in _dRigidBodyIdToAttachedAgents)
            {
                if (_rigidbodyWorkers.Count <= rigidbdyIndex)
                {
                    _rigidbodyWorkers.Add(new AgentWorker());
                }

                var worker = _rigidbodyWorkers[rigidbdyIndex];
                worker.SetupBeforeThreading(kvp.Value);

                switch (Threading)
                {
                    case ThreadStyle.NoThreads:
                        worker.DoRigidbody(null);
                        break;
                    case ThreadStyle.ThreadPool:
                        ThreadPool.QueueUserWorkItem(worker.DoRigidbody);
                        break;
                    case ThreadStyle.CustomThreadPool:
                        _threadWorkQueue.Enqueue(worker);
                        break;
                }
                ++rigidbdyIndex;
            }
        }

        private IEnumerator SimulateLateFixedUpdateCoroutine()
        {
            while (true)
            {
                yield return new WaitForFixedUpdate();

                WaitForWorkersToFinish();

                for (int i = 0; i < _rigidbodyWorkers.Count; ++i)
                {
                    var rbw = _rigidbodyWorkers[i];
                    rbw.FinishAfterThreading();
                }
            }
        }

        private static void WaitForWorkersToFinish()
        {
            if (null == _rigidbodyWorkers || _rigidbodyWorkers.Count < 1)
            {
                return;
            }

            int numWaitPasses = 1 + (_rigidbodyWorkers.Count / _manualResetEventsBuffer.Length);
            int nextWaitIndex = 0;
            for (int i = 0; i < numWaitPasses; ++i)
            {
                for (int j = 0; j < _manualResetEventsBuffer.Length; ++j)
                {
                    if (nextWaitIndex < _rigidbodyWorkers.Count)
                    {
                        _manualResetEventsBuffer[j] = _rigidbodyWorkers[nextWaitIndex]._resetEvent;
                    }
                    else
                    {
                        _manualResetEventsBuffer[j] = _rigidbodyWorkers[0]._resetEvent;
                    }
                    ++nextWaitIndex;
                }
                WaitHandle.WaitAll(_manualResetEventsBuffer, 100);
            }
        }

        private void DestroyCustomThreadPool()
        {
            _killCustomThreads = true;
            if (_threads.Count > 0)
            {
                Thread.Sleep(1);  // give them a chance to die off naturally
                foreach (Thread t in _threads)
                {
                    // That's long enough.  Kill them all!
                    if (t.IsAlive)
                    {
                        t.Abort();
                    }
                }
                _threads.Clear();
            }
        }

        private void UpdateCustomThreadPool()
        {
            if (Threading != ThreadStyle.CustomThreadPool)
            {
                DestroyCustomThreadPool();
            }
            else
            {
                // Run the threads!
                _killCustomThreads = false;
                if (CustomPoolNumThreads > 64) CustomPoolNumThreads = 64;  // Sanity check, don't make too many

                while (_threads.Count < this.CustomPoolNumThreads)
                {
                    // Create new threads up to the desired #
                    Thread newThread = new Thread(ExecuteRigidbodyWorkerQueueThread);
                    newThread.Start();
                    _threads.Add(newThread);
                }

                while (_threads.Count > this.CustomPoolNumThreads)
                {
                    // Kill extra threads
                    Thread last = _threads[_threads.Count - 1];
                    if (last.IsAlive)
                    {
                        last.Abort();
                    }
                    _threads.RemoveAt(_threads.Count - 1);
                }
            }
        }

        void ExecuteRigidbodyWorkerQueueThread()
        {
            TimeSpan epsilon = new TimeSpan(100);  // Sleep(0) doesn't necessarily give up the chip.  Want a non-zero sleep time, but a ms is too long.
            try
            {
                while (!_killCustomThreads)
                {
                    if (_threadWorkQueue.Count > 0)
                    {
                        AgentWorker worker = null;
                        try
                        {
                            worker = _threadWorkQueue.Dequeue() as AgentWorker;
                        }
                        catch (InvalidOperationException)
                        {
                            // No more queue elements.  Some other thread must have taken the last one.
                            continue;
                        }

                        worker.DoRigidbody(null);
                    }
                    else
                    {
                        Thread.Sleep(epsilon);
                    }
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("Error in rigidbody worker thread!  " + e.Message);
            }
        }

        private void RefreshPointsAffectingVirtualObjects()
        {
            for (int i = 0; i < _pointPool.Count; i++)
            {
                ScanPoint scanPoint = _pointPool[i];
                int numColliders = CopyCollidersNearPointIntoBuffer(_collideWithLayers, ref scanPoint);

                for (int j = 0; j < numColliders; j++)
                {
                    Collider c = _colliderBuffer[j];
                    if (c.isTrigger)
                    { continue; }

                    AddPointToVirtualObject(scanPoint, c);
                }
            }
        }

        /// <summary>
        /// Copies colliders near the scanpoint into _colliderBuffer.
        /// Returns number of colliders found.
        /// Modifies scanPoint Magnitude to be the extra distance queried beyond the physics default.
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="scanPoint"></param>
        /// <returns></returns>
        private int CopyCollidersNearPointIntoBuffer(LayerMask mask, ref ScanPoint scanPoint)
        {
            //.forceRadius is base size, add the magnitude of points movement multiplied by 2.1f
            //because magnitude represents radius, added 1f is for additional scan radius
            //float scanRadius = _physicsProperties.forceRadius + (scanPoint.Magnitude * 4f);

            Vector3 velocityAtPoint;
            // Do we have velocity here?
            if (VelocityAPI.GetPointCloudVelocityAt(scanPoint.Point, out velocityAtPoint))
            {
                // Yes!  Query along a spherical path.
                float speed = velocityAtPoint.magnitude;
                Vector3 velocityDirection = velocityAtPoint / speed;

                // How far will the point move in a FixedUpdate?
                float extraQueryDistance = speed * _physicsProperties.extraVelocityRadiusMultiplier * (Time.fixedDeltaTime * 50);  // normalized to 50fps, since that's what we debugged with.
                scanPoint.Magnitude = extraQueryDistance;

                int numHits = UnityEngine.Physics.SphereCastNonAlloc(scanPoint.Point, _physicsProperties.forceRadius, velocityDirection, _rayCastHitBuffer, extraQueryDistance, mask, QueryTriggerInteraction.Ignore);
                for(int i = 0; i < numHits; ++i)
                {
                    _colliderBuffer[i] = _rayCastHitBuffer[i].collider;
                }
                return numHits;
            }
            else
            {
                // No!  Query just a sphere around the point.
                // How far will the point move in a FixedUpdate?
                float scanRadius = _physicsProperties.forceRadius;
                scanPoint.Magnitude = 0;

                return UnityEngine.Physics.OverlapSphereNonAlloc(scanPoint.Point, scanRadius, _colliderBuffer, mask, QueryTriggerInteraction.Ignore);
            }
        }

        private void AddPointToVirtualObject(ScanPoint point, Collider c)
        {
            MetaPhysicsAgent colliderVO = GetVirtualObjectFor(c);
            colliderVO.AddRealBody(point);
        }

        private void RefreshPointPool()
        {
            GeneralInfo.numberOfVisiblePoint = _pointProvider.GetPointPoolLength();

            var stride = GeneralInfo.numberOfVisiblePoint <= _maximumNumCloudPointsConsidered ? 1 : GeneralInfo.numberOfVisiblePoint / (float)_maximumNumCloudPointsConsidered;
            GeneralInfo.subsetAmount = stride;

            if (_priorUpdateId != _pointProvider.UpdateID)
            {
                _priorUpdateId = _pointProvider.UpdateID;
                _pointPool.Clear();
                _pointProvider.AddPointsToPool(_pointPool, stride);
            }
        }

        private MetaPhysicsAgent GetVirtualObjectFor(Collider c)
        {
            MetaPhysicsAgent agent = c.GetComponent<MetaPhysicsAgent>();
            if (!agent)
            {
                agent = c.gameObject.AddComponent<MetaPhysicsAgent>();
            }
            //this is really only being set every frame for debugging, letting you adjust it while running
            if (!agent.UseLocalPhysicsProperties)
            {
                agent.LocalPhysicsProperties = _physicsProperties;
            }
            return agent;
        }

        public static MetaPhysicsManager FindManagerForLayer(int layerIndex)
        {
            LayerMask mask = 1 << layerIndex;
            return FindManagerForLayer(mask);
        }

        public static MetaPhysicsManager FindManagerForCollider(Collider col)
        {
            return FindManagerForLayer(col.gameObject.layer);
        }

        protected static MetaPhysicsManager[] _managersCache;
        public static MetaPhysicsManager FindManagerForLayer(LayerMask mask)
        {
            if (null == _managersCache)
            {
                _managersCache = GameObject.FindObjectsOfType<MetaPhysicsManager>();
            }

            MetaPhysicsManager found = null;
            foreach (var manager in _managersCache)
            {
                int commonMask = manager._collideWithLayers.value & mask.value;
                if (commonMask != 0)
                {
                    if (found)
                    {
                        for (int i = 0; i < 32; ++i)
                        {
                            if (0 != (commonMask & 1 << i))
                            {
                                UnityEngine.Debug.LogWarningFormat("Two HPx2_Managers trying to control the same layer: {0} and {1}, for layer {2}", manager.name, found.name, LayerMask.LayerToName(i));
                            }
                        }

                    }
                    else
                    {
                        found = manager;
                    }
                }
            }
            return found;
        }
    }
}