﻿using UnityEngine;

namespace Meta.Physics
{
    [System.Serializable]
    public struct ScanPoint
    {
        public Vector3 Point;
        public float Magnitude;

        public ScanPoint(Vector3 point, float magnitude)
        {
            Point = point;
            Magnitude = magnitude;
        }
    }
}