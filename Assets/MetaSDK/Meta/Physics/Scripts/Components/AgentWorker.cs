﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;

namespace Meta.Physics
{
    /// <summary>
    /// Runs calculations on MetaPhysicsAgent to determine final forces.
    /// 
    /// Is a seperate class to support threading.
    /// </summary>
    public class AgentWorker
    {
        public Rigidbody _rigidbody;
        public List<MetaPhysicsAgent> _agentsForRigidbody;
        public ManualResetEvent _resetEvent = new ManualResetEvent(false);

        public void DoRigidbody(object unused)
        {
            float totalFractionOfMax = 0;

            // Calculate force per agent
            for (int i = 0; i < _agentsForRigidbody.Count; ++i)
            {
                var vo = _agentsForRigidbody[i];
                vo.CalculateForceAndReset();
                totalFractionOfMax += vo.SumSingleColliderFractionOfMax();
            }

            // Now want to normalize across colliders, too.
            for (int i = 0; i < _agentsForRigidbody.Count; ++i)
            {
                float fractionOfMaxForVO = _agentsForRigidbody[i].SumSingleColliderFractionOfMax();
                if (fractionOfMaxForVO > 0)
                {
                    // Divide the collider's contribution its share of the total contribution.
                    float multiplier = fractionOfMaxForVO/totalFractionOfMax;
                    _agentsForRigidbody[i].ScaleCalculatedForce(multiplier);
                }
            }

            _resetEvent.Set();
        }

        public void SetupBeforeThreading(List<MetaPhysicsAgent> vos)
        {
            _agentsForRigidbody = vos;
            _rigidbody = GetAttachedRigidbody(vos[0]);
            _resetEvent.Reset();

            for (int i = 0; i < this._agentsForRigidbody.Count; ++i)
            {
                this._agentsForRigidbody[i].SetupBeforeThreading();
            }
        }

        public void FinishAfterThreading()
        {
            for (int i = 0; i < _agentsForRigidbody.Count; ++i)
            {
                _agentsForRigidbody[i].ApplyCalculatedForce(_rigidbody);
            }
        }

        private static Rigidbody GetAttachedRigidbody(Component mb)
        {
            var coll = mb.GetComponent<Collider>();
            return coll ? coll.attachedRigidbody : (Rigidbody)null;
        }
    }
}