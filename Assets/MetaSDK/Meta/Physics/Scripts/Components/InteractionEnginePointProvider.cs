﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Meta.Extensions;

namespace Meta.Physics
{
    /// <summary>
    /// This class retrieves data from interactionEngine
    /// then does operations to prepares it for delivery
    /// to MetaPhysics and Velocity. This could theoretically
    /// be removed once InteractionEngine does everything this 
    /// does.
    /// </summary>
    public class InteractionEnginePointProvider : PointProvider
    {
        private PointCloudData<PointXYZConfidence> _currentFrame;
        private Transform _cloudTransform;
        private InteractionEngine _interactionEngine;
        private int _updateId;
        private bool _initialized;

        public override int UpdateID
        {
            get { return _updateId; }
        }

        public override bool Initialized
        {
            get { return _initialized; }
        }

        private IEnumerator Start()
        {
            _cloudTransform =
                FindObjectOfType<MetaManager>().transform.Find("MetaCameras/DepthOcclusion").transform;

            PointCloudMetaData metadata = new PointCloudMetaData();
            while (metadata.maxSize == 0)
            {
                metaContext.Get<InteractionEngine>().GetCloudMetaData(ref metadata);
                yield return null;
            }

            Debug.Log("Allocating current frame in point provider");
            _currentFrame = new PointCloudData<PointXYZConfidence>(metadata.maxSize);

            _interactionEngine = metaContext.Get<InteractionEngine>();
            _initialized = true;
        }

        public void Update()
        {
            if (_initialized)
            {
                _interactionEngine.GetCloudData(ref _currentFrame);
                _updateId = _currentFrame.frameId;
            }
        }

        public override void AddPointsToPool(List<Vector3> pool)
        {
            for (int i = 0; i < _currentFrame.size; ++i)
            {
                Vector3 point = _currentFrame.points[i].vertex;
                if (!point.IsNaN())
                {
                    point.Clamp(-10000f, 10000f);
                    point = _cloudTransform.TransformPoint(point);
                    pool.Add(point);
                }
            }
        }

        public override void AddPointsToPool(List<ScanPoint> pool, float stride)
        {
            for (float fi = 0; fi < _currentFrame.size; fi += stride)
            {
                int i = (int) fi;
                Vector3 point = _currentFrame.points[i].vertex;
                if (!point.IsNaN())
                {
                    point.Clamp(-10000f, 10000f);
                    point = _cloudTransform.TransformPoint(point);
                    float deltaMagnitude = 0f;
                    pool.Add(new ScanPoint(point, deltaMagnitude));
                }
            }
        }

        public override int GetPointPoolLength()
        {
            return _currentFrame.size;
        }
    }
}