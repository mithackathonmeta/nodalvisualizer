﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Meta.Physics
{
    /// <summary>
    /// MetaPhysics is an expirimental physics sytem to project forces directly
    /// from the pointcloud.
    /// 
    /// Recieves appropriate force information from MetaphysicsManager
    /// to be processed into forces to be applied to rigidbody.
    /// 
    /// Allows local physics properties to be configured by manually adding
    /// it to a GameObject.
    /// </summary>
    public class MetaPhysicsAgent : MonoBehaviour
    {
        [ThreadStatic] private static List<ApplyingForcePoint> _appliedForceClosestBuffer;

        [SerializeField] private bool _useLocalPhysicsProperties = false;

        [SerializeField] private PhysicsProperties _localPhysicsProperties;

        private const float MaxForceDiscrepency = 20f;
        private const float AcceptibleDiscrepency = 5f;
        private const float UpwardsSmoothing = 0.9f;
        private const float DownwardsSmoothing = 0.075f;
        private float _normalizedDistance;
        private float _totalAppliedForceMagnitude;
        private float _forceFromFinalForce;
        private float _forceFromVForce;
        private float _normalizedForceDiscrepency;
        private float _forceDiscrepency;
        private MetaPhysicsManager _physicsManager;

        private int _maxPointsToAverage = 5;
        private List<ApplyingForcePoint> _appliedForces;

        private Matrix4x4 _worldToLocalMatrix;
        private Matrix4x4 _localToWorldMatrix;
        private Bounds _localBounds;
        private float _fixedDeltaTime;

        private Collider _collider;
        private Vector3 _additiveForce;
        private Vector3 _appliedForce;
        private List<ScanPoint> _additivePointsWithinVisinity;
        private List<ScanPoint> _pointsWithinVisinity;
        private float _additiveForceMagnitude;
        private float _totalForceMagnitude;

        public PhysicsProperties LocalPhysicsProperties
        {
            get { return _localPhysicsProperties; }
            set { _localPhysicsProperties = value; }
        }

        public Vector3 AppliedForce
        {
            get { return _appliedForce; }
        }

        public bool UseLocalPhysicsProperties
        {
            get { return _useLocalPhysicsProperties; }
        }

        public List<ScanPoint> PointsWithinVisinity
        {
            get { return _pointsWithinVisinity; }
        }

        public void Awake()
        {
            _additivePointsWithinVisinity = new List<ScanPoint>(64);
            _pointsWithinVisinity = new List<ScanPoint>(64);

            _physicsManager = MetaPhysicsManager.FindManagerForLayer(gameObject.layer);
            _physicsManager.RegisterAgent(this);

            _appliedForces = new List<ApplyingForcePoint>(64);
                // we're likely to need this much (by experimentation, logic)
            // so let's preallocate it to avoid lots of little list expansion allocations 

            _collider = GetComponent<Collider>();
            _maxPointsToAverage = 5;
        }

        public virtual void CalculateForceAndReset()
        {
            if (_additivePointsWithinVisinity.Count == 0 && PointsWithinVisinity.Count == 0)
            {
                return;
            }

            // Could we just swap these lists rather than copying??
            PointsWithinVisinity.Clear();
            if (PointsWithinVisinity.Capacity < _additivePointsWithinVisinity.Capacity)
            {
                PointsWithinVisinity.Capacity = _additivePointsWithinVisinity.Capacity;
            }
            PointsWithinVisinity.AddRange(_additivePointsWithinVisinity);
            _additivePointsWithinVisinity.Clear();

            CalculateForce();

            _appliedForce = _additiveForce;
            _additiveForce = Vector3.zero;

            _totalForceMagnitude = _additiveForceMagnitude;
            _additiveForceMagnitude = 0;

            _forceFromVForce = AppliedForce.magnitude;
            _forceFromFinalForce = _totalForceMagnitude;

            _forceDiscrepency = (_forceFromFinalForce - _forceFromVForce);
            var targetDiscrepency = Mathf.Clamp((_forceDiscrepency - AcceptibleDiscrepency)/MaxForceDiscrepency, 0, 1);
            var smoothing = targetDiscrepency > _normalizedForceDiscrepency ? UpwardsSmoothing : DownwardsSmoothing;
            smoothing = 60f*_fixedDeltaTime;
            _normalizedForceDiscrepency = Mathf.Lerp(_normalizedForceDiscrepency, targetDiscrepency, smoothing);
        }

        private void CalculateForce()
        {
            _normalizedDistance = 0f;

            float stride = PointsWithinVisinity.Count <= _localPhysicsProperties.relevantPointCount
                ? 1
                : PointsWithinVisinity.Count/(float) _localPhysicsProperties.relevantPointCount;

            _appliedForces.Clear();

            if (PointsWithinVisinity.Count/(int) stride == 0)
            {
                return;
            }

            float totalNormalizedDistancesFromSurface =
                CalculateAppliedForceAndReturnTotalNormalizedDistanceFromSurface(stride);

            DetermineClosestPoints(_appliedForces, _maxPointsToAverage, ref _appliedForceClosestBuffer);

            float runningNormalizedDistance =
                CalculateRunningNormalizedDistanceAndDrawSpheres(_appliedForceClosestBuffer);

            _normalizedDistance = Mathf.Clamp(runningNormalizedDistance/_appliedForceClosestBuffer.Count, 0f, 1f);
            if (!float.IsNaN(_normalizedDistance))
            {
                _totalAppliedForceMagnitude =
                    (_localPhysicsProperties.normalizedForceCurve.Evaluate(_normalizedDistance)*
                     _localPhysicsProperties.maxAppliedForceMagnitude);
            }

            _forceFromVForce = 0;

            CalculateFinalColliderForceAndDrawLines(totalNormalizedDistancesFromSurface);
        }

        private void CalculateFinalColliderForceAndDrawLines(float totalNormalizedDistancesFromSurface)
        {
            float inverseMaxAppliedForceMagnitude = 1/_localPhysicsProperties.maxAppliedForceMagnitude;

            int count = _appliedForces.Count;
            for (int i = 0; i < count; i++)
            {
                ApplyingForcePoint applying = _appliedForces[i];
                float forceMultiplier = (applying.NormalizedDistanceFromSurface/totalNormalizedDistancesFromSurface)*
                                        _totalAppliedForceMagnitude;
                Vector3 vForceDirNormalized =
                    (applying.ClosestPointOnSurfacePosition - applying.ScanPoint.Point).normalized;

                if (applying.IsInside)
                {
                    vForceDirNormalized = -vForceDirNormalized;
                }

                Vector3 vForce = (vForceDirNormalized*forceMultiplier);

                applying.SingleColliderForceToApply = vForce;
                applying.SingleColliderFractionOfMax = forceMultiplier*inverseMaxAppliedForceMagnitude;

                _appliedForces[i] = applying;

                _additiveForce += vForce;
                _additiveForceMagnitude += vForce.magnitude;
            }
        }

        private float CalculateAppliedForceAndReturnTotalNormalizedDistanceFromSurface(float stride)
        {
            float totalNormalizedDistancesFromSurface = 0;
            //float inverseForceRadius = 1 / _localPhysicsProperties.forceRadius;
            //float forceRadiusSquared = _localPhysicsProperties.forceRadius * _localPhysicsProperties.forceRadius;

            for (float fi = 0; fi < PointsWithinVisinity.Count; fi += stride)
            {
                int i = (int) fi;

                Vector3 closestPointOnObject;
                bool pointIsInside = false;
                switch (_localPhysicsProperties.surfaceCalculationType)
                {
                    case SurfaceCalculationsType.ClosestPointOnBounds:
                        closestPointOnObject = _collider.ClosestPointOnBounds(PointsWithinVisinity[i].Point);
                        break;
                    case SurfaceCalculationsType.ClosestPointOnBox:
                        closestPointOnObject = ClosestPointOnBox(PointsWithinVisinity[i].Point, out pointIsInside);
                        break;
                    case SurfaceCalculationsType.ClosestPointOnSphere:
                        closestPointOnObject = ClosestPointOnSphere(PointsWithinVisinity[i].Point, out pointIsInside);
                        break;
                    case SurfaceCalculationsType.ClosestPointOnLine:
                        closestPointOnObject = ClosestPointOnLine(PointsWithinVisinity[i].Point);
                        break;
                    default:
                        closestPointOnObject = _collider.transform.position;
                        break;
                }

                float forceRadius = _localPhysicsProperties.forceRadius + PointsWithinVisinity[i].Magnitude;
                float forceRadiusSquared = forceRadius*forceRadius;
                float inverseForceRadius = 1/forceRadius;

                Vector3 vPointToSurface = closestPointOnObject - PointsWithinVisinity[i].Point;
                float distanceToSurfaceSquared = vPointToSurface.sqrMagnitude;
                if (distanceToSurfaceSquared > forceRadiusSquared)
                {
                    continue;
                }

                float distanceToSurface = Mathf.Sqrt(distanceToSurfaceSquared);

                float normalizedDistToSurface = pointIsInside ? 1 : (forceRadius - distanceToSurface)*inverseForceRadius;
                totalNormalizedDistancesFromSurface += normalizedDistToSurface;

                //collect all applying force point positions
                var applyingForce = new ApplyingForcePoint()
                {
                    ScanPoint = PointsWithinVisinity[i],
                    ClosestPointOnSurfacePosition = closestPointOnObject,
                    IsInside = pointIsInside,
                    NormalizedDistanceFromSurface = normalizedDistToSurface,
                    DistanceToSurface = distanceToSurface,
                };

                _appliedForces.Add(applyingForce); //  <-- Not significant time, as long as it's not allocating -AHG
            }

            return totalNormalizedDistancesFromSurface;
        }

        private float CalculateRunningNormalizedDistanceAndDrawSpheres(List<ApplyingForcePoint> closestPoints)
        {
            float runningNormalizedDistance = 0;
            for (int i = 0; i < closestPoints.Count; i++)
            {
                runningNormalizedDistance += closestPoints[i].NormalizedDistanceFromSurface;
            }
            return runningNormalizedDistance;
        }

        internal void ScaleCalculatedForce(float multiplier)
        {
            for (int i = 0; i < _appliedForces.Count; i++)
            {
                ApplyingForcePoint applying = _appliedForces[i];
                applying.SingleColliderForceToApply *= multiplier;
                _appliedForces[i] = applying;
            }
        }

        internal void SetupBeforeThreading()
        {
            _fixedDeltaTime = Time.fixedDeltaTime;
            _worldToLocalMatrix = transform.worldToLocalMatrix;
            _localToWorldMatrix = transform.localToWorldMatrix;
            _localBounds = new Bounds(Vector3.zero, Vector3.one);
            Collider collider = GetComponent<Collider>();
            if (collider)
            {
                BoxCollider boxCollider = collider as BoxCollider;
                if (boxCollider)
                {
                    _localBounds = new Bounds(boxCollider.center, boxCollider.size);
                }
                else
                {
                    SphereCollider sphereCollider = collider as SphereCollider;
                    if (sphereCollider)
                    {
                        _localBounds = new Bounds(sphereCollider.center, Vector3.one*2*sphereCollider.radius);
                    }
                }
            }
        }

        private class SortByDistanceFromSurfaceComparer : IComparer<ApplyingForcePoint>
        {
            public int Compare(ApplyingForcePoint one, ApplyingForcePoint two)
            {
                return two.NormalizedDistanceFromSurface.CompareTo(one.NormalizedDistanceFromSurface);
            }
        }

        private readonly SortByDistanceFromSurfaceComparer _sortByDistanceFromSurface =
            new SortByDistanceFromSurfaceComparer();

        private void DetermineClosestPoints(List<ApplyingForcePoint> source, int numPointsToInclude,
            ref List<ApplyingForcePoint> buffer)
        {
            if (null == buffer)
            {
                buffer = new List<ApplyingForcePoint>(numPointsToInclude);
            }

            buffer.Clear();

            float discardSmallerThan = float.MinValue;

            for (int i = 0; i < source.Count; ++i)
            {
                if (source[i].NormalizedDistanceFromSurface < discardSmallerThan)
                {
                    continue;
                }

                int insertLocation = buffer.BinarySearch(source[i], _sortByDistanceFromSurface);
                if (insertLocation < 0)
                {
                    insertLocation = ~insertLocation; // look up BinarySearch; it's goofy.
                }

                if (insertLocation <= buffer.Count && insertLocation < numPointsToInclude)
                {
                    buffer.Insert(insertLocation, source[i]);

                    if (buffer.Count > numPointsToInclude)
                    {
                        buffer.RemoveAt(numPointsToInclude);
                    }

                    discardSmallerThan = buffer[buffer.Count - 1].NormalizedDistanceFromSurface;
                }
            }
        }

        internal float SumSingleColliderFractionOfMax()
        {
            float total = 0;
            for (int i = 0; i < _appliedForces.Count; ++i)
            {
                total += _appliedForces[i].SingleColliderFractionOfMax;
            }

            return total;
        }

        public void ApplyCalculatedForce(Rigidbody attachedRigidbody)
        {
            float frozenDrag = 1/Time.fixedDeltaTime;

            for (int i = 0; i < _appliedForces.Count; i++)
            {
                ApplyingForcePoint applying = _appliedForces[i];

                // According to the end of this forum thread, Unity drag applies logic like this for each FixedUpdate:            //     http://forum.unity3d.com/threads/drag-factor-what-is-it.85504/
                //    velocity *= Mathf.Clamp01(1f - drag * Time.deltaTime);
                //  We can't counter that exactly with a simple impulse, but we can at least enforce our velocity/force desires for a frame.
                //    UNLESS the drag is >= 1/Time.fixedDeltaTime, in which case we can't move anything.
                if (attachedRigidbody.drag < frozenDrag)
                {
                    applying.SingleColliderForceToApply /= (1 - attachedRigidbody.drag*Time.fixedDeltaTime);
                }

                attachedRigidbody.AddForceAtPosition(applying.SingleColliderForceToApply,
                    applying.ClosestPointOnSurfacePosition, _localPhysicsProperties.forceMode);
            }
        }

        public virtual void AddRealBody(ScanPoint scanPoint)
        {
            _additivePointsWithinVisinity.Add(scanPoint);
        }

        private Vector3 ClosestPointOnSphere(Vector3 pointPosition, out bool pointIsInside)
        {
            // Unity sphere colliders don't do non-uniform scale.  Instead, they seem to use the largest scale dimension.
            //  We'll mimic that here so that the HPx2 shape matches the Unity shape.

            // Extract the scale part of the matrix
            float xScaleSqr = _localToWorldMatrix.GetColumn(0).sqrMagnitude;
            float yScaleSqr = _localToWorldMatrix.GetColumn(1).sqrMagnitude;
            float zScaleSqr = _localToWorldMatrix.GetColumn(2).sqrMagnitude;

            float maxScaleSqr = Mathf.Max(xScaleSqr, yScaleSqr);
            maxScaleSqr = Mathf.Max(maxScaleSqr, zScaleSqr);

            Vector3 worldSphereCenter = _localToWorldMatrix.MultiplyPoint3x4(_localBounds.center);
            float worldRadiusSqr = maxScaleSqr*_localBounds.extents.x*_localBounds.extents.x;
                // assuming an actual sphere, which dimension we choose here doesn't matter

            Vector3 toPointWorld = pointPosition - worldSphereCenter;
            float d2 = toPointWorld.sqrMagnitude;
            if (d2 == 0)
            {
                pointIsInside = true;
                return pointPosition; // actual point on sphere is ambiguous, so just return point position
            }
            else
            {
                toPointWorld = toPointWorld*Mathf.Sqrt(worldRadiusSqr/d2);
                pointIsInside = (d2 < worldRadiusSqr);
                return worldSphereCenter + toPointWorld;
            }
        }


        private Vector3 ClosestPointOnBox(Vector3 pointPosition, out bool isInside)
        {
            var localToObjectInitial = _worldToLocalMatrix.MultiplyPoint3x4(pointPosition);

            var localToObjectFinal = Vector3.Min(localToObjectInitial, _localBounds.max);
            localToObjectFinal = Vector3.Max(localToObjectFinal, _localBounds.min);

            if (localToObjectFinal == localToObjectInitial)
            {
                isInside = true;

                // Closest point on edge of box when we are inside....
                Vector3 minToPoint = localToObjectInitial - _localBounds.min;
                Vector3 pointToMax = _localBounds.max - localToObjectInitial;
                float smallestDistance = float.MaxValue;
                bool isCloserToMin = false;
                int smallestDimension = -1;
                for (int dimension = 0; dimension < 3; ++dimension)
                {
                    if (minToPoint[dimension] < smallestDistance)
                    {
                        smallestDistance = minToPoint[dimension];
                        isCloserToMin = true;
                        smallestDimension = dimension;
                    }

                    if (pointToMax[dimension] < smallestDistance)
                    {
                        smallestDistance = pointToMax[dimension];
                        isCloserToMin = false;
                        smallestDimension = dimension;
                    }
                }

                localToObjectFinal[smallestDimension] = isCloserToMin
                    ? _localBounds.min[smallestDimension]
                    : _localBounds.max[smallestDimension];
            }
            else
            {
                isInside = false;
            }

            return _localToWorldMatrix.MultiplyPoint3x4(localToObjectFinal);
        }


        private Vector3 ClosestPointOnBox(Vector3 pointPosition)
        {
            var localToObject = _worldToLocalMatrix.MultiplyPoint3x4(pointPosition);

            localToObject = Vector3.Min(localToObject, _localBounds.max);
            localToObject = Vector3.Max(localToObject, _localBounds.min);

            return _localToWorldMatrix.MultiplyPoint3x4(localToObject);
        }

        private Vector3 ClosestPointOnLine(Vector3 pointPosition)
        {
            Vector3 localToObject = _worldToLocalMatrix.MultiplyPoint3x4(pointPosition);

            localToObject.x = 0;
            localToObject.z = 0;

            return _localToWorldMatrix.MultiplyPoint3x4(localToObject);
        }

        public struct ApplyingForcePoint
        {
            public ScanPoint ScanPoint;
            public Vector3 ClosestPointOnSurfacePosition;
            public bool IsInside;
            public float DistanceToSurface;
            public float NormalizedDistanceFromSurface;
            public float Magnitude;

            public Vector3 SingleColliderForceToApply;
            public float SingleColliderFractionOfMax;
        }
    }
}