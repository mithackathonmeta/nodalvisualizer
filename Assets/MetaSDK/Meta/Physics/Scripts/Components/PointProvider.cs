﻿using System.Collections.Generic;
using UnityEngine;

namespace Meta.Physics
{
    [SerializeField]
    public abstract class PointProvider : MetaBehaviour
    {
        public abstract int UpdateID { get; }
        public abstract bool Initialized { get; }
        public abstract void AddPointsToPool(List<Vector3> pool);
        public abstract void AddPointsToPool(List<ScanPoint> pool, float stride);
        public abstract int GetPointPoolLength();
    }
}