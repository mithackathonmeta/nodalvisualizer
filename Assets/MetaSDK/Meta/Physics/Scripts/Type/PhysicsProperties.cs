﻿using UnityEngine;

namespace Meta.Physics
{
    [System.Serializable]
    public class PhysicsProperties 
    {
        [Tooltip("How to apply rigidbody forces.")]
        public ForceMode forceMode = ForceMode.VelocityChange;

        [Tooltip("Radius from hand to search for colliders.")]
        public float forceRadius = .02f;

        [Tooltip("Scale factor on amount of extra radius when using velocity system to expand colliders.")]
        public float extraVelocityRadiusMultiplier = 0.5f;

        [Tooltip("Maximum number of points that can influence a collider.")]
        public int relevantPointCount = 100;

        [Tooltip("Maximum force applied to rigidbody.")]
        public float maxAppliedForceMagnitude = 1;

        [Tooltip("Controls amount of force based on distance from surface. Normalized to the forceRadius.")]
        public AnimationCurve normalizedForceCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [Tooltip("Method to determine application of forces on collider.")]
        public SurfaceCalculationsType surfaceCalculationType = SurfaceCalculationsType.ClosestPointOnBox;
    }
}