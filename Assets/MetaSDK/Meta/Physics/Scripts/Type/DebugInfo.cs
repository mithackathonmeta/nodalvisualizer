﻿namespace Meta.Physics
{
    [System.Serializable]
    public class DebugInfo
    {
        public int numberOfVisiblePoint;
        public float subsetAmount;
        public bool drawLines;
    }
}