﻿using UnityEngine;
using System.Diagnostics;
using System;

namespace Meta.Analytics
{

    /// <summary>
    /// Static class to invoke function logging
    /// </summary>
    public static class MetaStackTracer
    {
        /// <summary>
        /// Contains static log information
        /// </summary>
        internal static string csvName;
        internal static string initSceneTimestamp;
        internal static bool doLog;
        internal static GameObject mainCamera;


        /// <summary>
        /// Record the current function call into the table
        /// </summary>
        public static void LogFunction()
        {
            if (!doLog)
            {
                return;
            }
            string currentFunctionName = getFunctionName(2);
            string timestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            PushToDataTable(timestamp, currentFunctionName);
            MetaAnalytics.Dispatch(csvName);
        }

        /// <summary>
        /// Record current function call into the table with object position
        /// </summary>
        /// <param name="g"></param>
        public static void LogFunction(GameObject g)
        {
            if (!doLog)
            {
                return;
            }
            string currentFunctionName = getFunctionName(2);
            string timestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            PushToDataTable(timestamp, currentFunctionName, g);
            MetaAnalytics.Dispatch(csvName);
        }

        /// <summary>
        /// Record current function call into the table with object and hands position
        /// </summary>
        /// <param name="g"></param>
        /// <param name="l"></param>
        /// <param name="r"></param>
        public static void LogFunction(GameObject g, HandData l, HandData r)
        {
            if (!doLog)
            {
                return;
            }
            string currentFunctionName = getFunctionName(2);
            string timestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            PushToDataTable(timestamp, currentFunctionName, g, l, r);
            MetaAnalytics.Dispatch(csvName);
        }

        /// <summary>
        /// Get the function name using reflection
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        private static string getFunctionName(int frame)
        {

            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(frame);

            string className = sf.GetMethod().ReflectedType.Name;
            return className + "." + sf.GetMethod().Name;
        }

        /// <summary>
        /// Push data to the datatable
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="function"></param>
        private static void PushToDataTable(string timestamp, string function)
        {
            String csv = csvName;
            Vector3 camTransform = mainCamera.transform.position;
            MetaAnalytics.LogEntry(csv, "scene_initialized", initSceneTimestamp);
            MetaAnalytics.LogEntry(csv, "timestamp", timestamp);
            MetaAnalytics.LogEntry(csv, "function_call", function);
            MetaAnalytics.LogEntry(csv, "pmdX", camTransform.x.ToString());
            MetaAnalytics.LogEntry(csv, "pmdY", camTransform.y.ToString());
            MetaAnalytics.LogEntry(csv, "pmdZ", camTransform.z.ToString());
        }

        /// <summary>
        /// Push data with gameobject position
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="function"></param>
        /// <param name="g"></param>
        private static void PushToDataTable(string timestamp, string function, GameObject g)
        {
            String csv = csvName;
            Transform t = g.transform;
            PushToDataTable(timestamp, function);
            MetaAnalytics.LogEntry(csv, "objPosX", t.position.x.ToString());
            MetaAnalytics.LogEntry(csv, "objPosY", t.position.y.ToString());
            MetaAnalytics.LogEntry(csv, "objPosZ", t.position.z.ToString());
            MetaAnalytics.LogEntry(csv, "objScaleX", t.localScale.x.ToString());
            MetaAnalytics.LogEntry(csv, "objScaleY", t.localScale.y.ToString());
            MetaAnalytics.LogEntry(csv, "objScaleZ", t.localScale.z.ToString());
        }

        /// <summary>
        /// Push data with gameobject and hand position
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="function"></param>
        /// <param name="g"></param>
        /// <param name="l"></param>
        /// <param name="r"></param>
        private static void PushToDataTable(string timestamp, string function, GameObject g, HandData l, HandData r)
        {
            String csv = csvName;
            PushToDataTable(timestamp, function, g);
            MetaAnalytics.LogEntry(csv, "lhPosX", l.center.x.ToString());
            MetaAnalytics.LogEntry(csv, "lhPosY", l.center.y.ToString());
            MetaAnalytics.LogEntry(csv, "lhPosZ", l.center.z.ToString());
            MetaAnalytics.LogEntry(csv, "rhPosX", r.center.x.ToString());
            MetaAnalytics.LogEntry(csv, "rhPosY", r.center.y.ToString());
            MetaAnalytics.LogEntry(csv, "rhPosZ", r.center.z.ToString());

        }
    }
}

