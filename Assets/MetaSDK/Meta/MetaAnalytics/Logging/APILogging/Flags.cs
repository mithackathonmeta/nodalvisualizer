﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta
{

    /// <summary>
    /// Bitmask for flags to activate loggers
    /// </summary>

    [Flags]
    internal enum FunctionFlags
    {
        None = 0,
        Grab = 1,
        Physics = 2,
        IMU = 4,
        Flag4 = 8,
        Flag5 = 16,
        Flag6 = 32,
        Flag7 = 64,
        Flag8 = 128,
    }

}
