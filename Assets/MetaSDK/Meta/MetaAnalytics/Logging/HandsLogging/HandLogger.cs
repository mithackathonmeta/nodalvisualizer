﻿using UnityEngine;
using System.Collections;
using Meta;
using System;

namespace Meta.Analytics
{
    /// <summary>
    /// HandPart position logging class
    /// </summary>
    internal static class HandLogger
    {
        internal static string initSceneTimestamp;
        internal static float timeSinceLastLog;
        internal static float timeThreshold;
        internal static string csvName;
        internal static GameObject mainCamera;

        /// <summary>
        /// Called every frame to check whether it is time to log again
        /// </summary>
        /// <param name="deltaTime"></param>
        /// <param name="dataFPS"></param>
        /// <param name="lh"></param>
        /// <param name="rh"></param>
        internal static void UpdateHandLogger(float deltaTime, float dataFPS, Vector3 lh, Vector3 rh)
        {
            timeThreshold = 1.0f / dataFPS;

            if (timeSinceLastLog > timeThreshold)
            {
                LogHands(lh, rh);
                timeSinceLastLog = 0;
            }
            timeSinceLastLog += deltaTime;
        }

        /// <summary>
        /// Log the hand and pmd data
        /// </summary>
        /// <param name="lh"></param>
        /// <param name="rh"></param>
        private static void LogHands(Vector3 lh, Vector3 rh)
        {
            string csv = csvName;
            string timestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            Vector3 camTransform = mainCamera.transform.position;
            MetaAnalytics.LogEntry(csv, "scene_initialized", initSceneTimestamp);
            MetaAnalytics.LogEntry(csv, "timestamp", timestamp);
            MetaAnalytics.LogEntry(csv, "pmdX", camTransform.x.ToString());
            MetaAnalytics.LogEntry(csv, "pmdY", camTransform.y.ToString());
            MetaAnalytics.LogEntry(csv, "pmdZ", camTransform.z.ToString());
            MetaAnalytics.LogEntry(csv, "lX", lh.x.ToString());
            MetaAnalytics.LogEntry(csv, "lY", lh.y.ToString());
            MetaAnalytics.LogEntry(csv, "lZ", lh.z.ToString());
            MetaAnalytics.LogEntry(csv, "rX", rh.x.ToString());
            MetaAnalytics.LogEntry(csv, "rY", rh.y.ToString());
            MetaAnalytics.LogEntry(csv, "rZ", rh.z.ToString());
            MetaAnalytics.Dispatch(csv);
        }


    }
}
