﻿using System;
using System.CodeDom;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Meta;

internal enum KernelLoggingVerbosity
{
    INFO = 0,

    WARNINGS = 1,

    ERROR = 2,

    FATAL = 3
};

internal class MetaKernelLogging
{

    [SerializeField]
    internal KernelLoggingVerbosity kernelLoggingVerbosity = KernelLoggingVerbosity.ERROR;

    [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "enableLogging")]
    internal static extern bool EnableKernelLogging(int logSeverity, string logFileLocation);

    [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "disableLogging")]
    internal static extern bool DisableKernelLogging();

    [SerializeField]
    internal string logFolderLocation;

    private bool _loggingEnabled = false;

    private string logFileLocation;

    public MetaKernelLogging(KernelLoggingVerbosity kernelLoggingVerbosity, string logFolderLocation)
    {
        this.kernelLoggingVerbosity = kernelLoggingVerbosity;
        this.logFolderLocation = logFolderLocation;


        string[] log_files = System.IO.Directory.GetFiles(logFolderLocation, "*kl*", SearchOption.TopDirectoryOnly).Where(file => !file.EndsWith(".meta")).ToArray();

        if (log_files.Length >= 5)
        {
            DateTime current_date_time;
            DateTime min = DateTime.MaxValue;
            int fileIndex = -1;
            for (int i = 0; i < log_files.Length; i++)
            {
                current_date_time = File.GetCreationTime(log_files[i]);
                if (current_date_time < min)
                {
                    fileIndex = i;
                    min = current_date_time;
                }
            }
            System.IO.File.Delete(log_files[fileIndex]);
        }
        
        logFileLocation = logFolderLocation + "/kl";
        EnableKernelLogging((int)kernelLoggingVerbosity, logFileLocation);
        _loggingEnabled = true;
    }

    internal void DuringOnDestroy ()
    {
        if (_loggingEnabled)
        {
            DisableKernelLogging();
        }
        _loggingEnabled = false;

        if (!Directory.GetFiles(logFileLocation).Any())
        {
            Directory.Delete(logFileLocation);
        }
    }
}


