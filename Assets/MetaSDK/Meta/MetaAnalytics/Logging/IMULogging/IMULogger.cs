﻿using UnityEngine;
using System;

namespace Meta.Analytics
{

    internal static class IMULogger
    {

        internal static string initSceneTimestamp;
        internal static float timeSinceLastLog;
        internal static float timeThreshold;
        internal static string csvName;

        /// <summary>
        ///  Called every frame to check whether it is time to log again
        /// </summary>
        /// <param name="deltaTime"></param>
        /// <param name="dataFPS"></param>
        /// <param name="imulocalizer"></param>
        internal static void UpdateIMULogger(float deltaTime, float dataFPS, IMULocalizer imulocalizer)
        {
            timeThreshold = 1.0f / dataFPS;

            if (timeSinceLastLog > timeThreshold)
            {
                LogIMU(imulocalizer);
                timeSinceLastLog = 0;
            }
            timeSinceLastLog += deltaTime;
        }


        /// <summary>
        /// Log the IMU data
        /// </summary>
        /// <param name="i"></param>
        private static void LogIMU(IMULocalizer i)
        {
            string csv = csvName;
            string timestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            Vector3 acc = i.accelerometerValues;
            Vector3 mag = i.magnetometerValues;
            Vector3 gyr = i.gyroscopeValues;
            MetaAnalytics.LogEntry(csv, "scene_initialized", initSceneTimestamp);
            MetaAnalytics.LogEntry(csv, "timestamp", timestamp);
            MetaAnalytics.LogEntry(csv, "accX", acc.x.ToString());
            MetaAnalytics.LogEntry(csv, "accY", acc.y.ToString());
            MetaAnalytics.LogEntry(csv, "accZ", acc.z.ToString());
            MetaAnalytics.LogEntry(csv, "magX", mag.x.ToString());
            MetaAnalytics.LogEntry(csv, "magY", mag.y.ToString());
            MetaAnalytics.LogEntry(csv, "magZ", mag.z.ToString());
            MetaAnalytics.LogEntry(csv, "gyroX", gyr.x.ToString());
            MetaAnalytics.LogEntry(csv, "gyroY", gyr.y.ToString());
            MetaAnalytics.LogEntry(csv, "gyroZ", gyr.z.ToString());
            MetaAnalytics.Dispatch(csv);
        }
    }
}
