﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Meta.Analytics;

[CustomEditor(typeof(MetaSDKLogger))]
internal class MetaSDKLoggerEditor : Editor{

    public bool functionAnalyticsToggle = false;
    public MetaSDKLogger _ctrl = null;

    void OnEnable()
    {
        _ctrl = (MetaSDKLogger)target;
        
    }
        
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Function Analytics", GUILayout.Width(160));
        _ctrl.logFunctions = EditorGUILayout.Toggle(_ctrl.logFunctions);
        GUILayout.EndHorizontal();
        if (_ctrl.logFunctions)
        {
            GUILayout.Space(0);
            GUILayout.BeginHorizontal();
            GUILayout.Label("       Log Grab", GUILayout.Width(160));
            _ctrl.logGrab = EditorGUILayout.Toggle(_ctrl.logGrab);
            GUILayout.EndHorizontal();
            GUILayout.Space(0);
            GUILayout.BeginHorizontal();
            GUILayout.Label("       Log Physics", GUILayout.Width(160));
            _ctrl.logPhysics = EditorGUILayout.Toggle(_ctrl.logPhysics);
            GUILayout.EndHorizontal();
        }
        GUILayout.BeginHorizontal();
        GUILayout.Label("HandPart Analytics", GUILayout.Width(160));
        _ctrl.logHands = EditorGUILayout.Toggle(_ctrl.logHands);
        GUILayout.EndHorizontal();
        
        if (_ctrl.logHands)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("       Hands Data Framerate", GUILayout.Width(160));
            _ctrl.handPositionFPS = EditorGUILayout.Slider(_ctrl.handPositionFPS, 0.1f, 120.0f);
            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        GUILayout.Label("IMU Analytics", GUILayout.Width(160));
        _ctrl.logIMU = EditorGUILayout.Toggle(_ctrl.logIMU);
        GUILayout.EndHorizontal();

        if (_ctrl.logIMU)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("       IMU Data Framerate", GUILayout.Width(160));
            _ctrl.imuFPS = EditorGUILayout.Slider(_ctrl.imuFPS , 0.1f, 120.0f);
            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        GUILayout.Label("Kernel Logger", GUILayout.Width(160));
        _ctrl.logKernel = EditorGUILayout.Toggle(_ctrl.logKernel);
        GUILayout.EndHorizontal();

        if (_ctrl.logKernel)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("       Logging Verbosity", GUILayout.Width(160));
            _ctrl.kernelLoggingVerbosity = (KernelLoggingVerbosity)EditorGUILayout.EnumPopup(_ctrl.kernelLoggingVerbosity);
            GUILayout.EndHorizontal();
        }
        EditorUtility.SetDirty(target);
    }

}
