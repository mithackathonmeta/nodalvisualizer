﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace Meta.Analytics
{
    internal class MetaSDKLogger : MetaBehaviour
    {

        public float handPositionFPS = 1.0f;

        public float imuFPS = 1.0f;

        public bool logFunctions = false;

        public bool logGrab = false;

        public bool logPhysics = false;

        public bool logHands = false;

        public bool logIMU = false;

        public bool logKernel = false;

        public KernelLoggingVerbosity kernelLoggingVerbosity = KernelLoggingVerbosity.INFO;

        private MetaKernelLogging metaKernelLogging;

        private bool functionLogInitialized = false;

        private bool handsLogInitialized = false;

        private bool imuLogInitialized = false;

        private static string initSceneTimestamp;

        private GameObject mainCamera;

        private HandData lh;

        private HandData rh;

        private IMULocalizer imulocalizer;

        private void Start()
        {
            //Get the main camera
            mainCamera = GameObject.FindWithTag("MainCamera");
            lh = metaContext.Get<InteractionEngine>().leftHand;
            rh = metaContext.Get<InteractionEngine>().rightHand;

            imulocalizer = gameObject.GetComponent<IMULocalizer>();
            //If user has attempted to log IMU data without IMU being used, warn user
            if (imulocalizer == null && logIMU)
            {
                Debug.Log("Warning: To log IMU data, the IMU localizer in the Meta Localization Settings must be selected before runtime. Currently not logging IMU data.");
            }

            //Get the initial unix timestamp
            initSceneTimestamp = Math.Floor(System.DateTime.Now.ToUniversalTime().Subtract(new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)).TotalMilliseconds).ToString();

            //If we want to log the kernel, create the path for logging, and instantiate new kernel logger
            if (logKernel)
            {
                if (!Directory.Exists(Application.dataPath + "/AnalyticsResults/KernelLog"))
                {
                    Directory.CreateDirectory(Application.dataPath + "/AnalyticsResults/KernelLog");
                }
                string kernelLogFolderLocation = Application.dataPath + "/AnalyticsResults/KernelLog";
                metaKernelLogging = new MetaKernelLogging(kernelLoggingVerbosity, kernelLogFolderLocation);
            }
        }


        // Update is called once per frame
        private void Update()
        {
            ///Update the options bitmask during runtime
            UpdateBitmask();

            //Only initialize each option's analytics upon the toggle being activated
            CheckToStartOptions();

            //Push data to the loggers
            UpdateLoggers();
        }

        /// <summary>
        /// Disable kernel logging
        /// </summary>
        private void OnDestroy()
        {
            if (logKernel && metaKernelLogging!=null)
            {
                metaKernelLogging.DuringOnDestroy();
            }
        }

        /// <summary>
        /// Update the options flags
        /// </summary>
        private void UpdateBitmask()
        {
            //If we toggle log ON, then flip the flag bit to 1; if we toggle log OFF, then flip the flag bit to 0
            metaContext.optionsBitmask = (logGrab) ? metaContext.optionsBitmask | FunctionFlags.Grab : metaContext.optionsBitmask & ~(FunctionFlags.Grab);
            metaContext.optionsBitmask = (logPhysics) ? metaContext.optionsBitmask | FunctionFlags.Physics : metaContext.optionsBitmask & ~(FunctionFlags.Physics);
            metaContext.optionsBitmask = (logIMU) ? metaContext.optionsBitmask | FunctionFlags.IMU : metaContext.optionsBitmask & ~(FunctionFlags.IMU);
        }

        /// <summary>
        /// Check whether to create the table and initialize the options' analytics
        /// </summary>
        private void CheckToStartOptions()
        {
            if (!functionLogInitialized && logFunctions)
            {
                // initialize the function logging analytics
                InitializeFunctionAnalytics();
                functionLogInitialized = true;
            }
            if (!handsLogInitialized && logHands)
            {
                //initialize the hand logging analytics
                InitializeHandAnalytics();
                handsLogInitialized = true;
            }
            if (!imuLogInitialized && logIMU)
            {
                InitializeIMUAnalytics();
                imuLogInitialized = true;
            }
        }


        /// <summary>
        /// Initialize function call logging
        /// </summary>
        private void InitializeFunctionAnalytics()
        {
            //Switch for whether to log functions
            MetaStackTracer.doLog = logFunctions;

            //Set the scene initialization time as an UNRELIABLE primary key
            MetaStackTracer.initSceneTimestamp = initSceneTimestamp;

            //Label the files by scene initialiaztion time
            MetaStackTracer.csvName = "FunctionLog/" + MetaStackTracer.initSceneTimestamp;

            //Set the main camera as the PMD reference
            MetaStackTracer.mainCamera = mainCamera;

            //Create the table
            try
            {
                MetaAnalytics.CreateTable(MetaStackTracer.csvName, "scene_initialized", "timestamp", "function_call",
                    "pmdX", "pmdY", "pmdZ",
                    "objPosX", "objPosY", "objPosZ", "objScaleX", "objScaleY", "objScaleZ",
                    "lhPosX", "lhPosY", "lhPosZ", "rhPosX", "rhPosY", "rhPosZ");
            }
            catch (TableAlreadyExistsException)
            {
                Debug.Log("ATTEMPTING TO MAKE FUNCTION TABLE WITH SAME NAME");
            }
        }

        /// <summary>
        /// Initialize hand position logging
        /// </summary>
        private void InitializeHandAnalytics()
        {
            //Frequency of data pull
            HandLogger.timeThreshold = 1.0f / handPositionFPS;

            //TODO: Should use GUID for filename in future
            HandLogger.initSceneTimestamp = initSceneTimestamp;

            //Label the files by scene initialiaztion time
            HandLogger.csvName = "HandsLog/" + initSceneTimestamp;

            //Set the main camera as the PMD reference
            HandLogger.mainCamera = mainCamera;

            //Create the table
            try
            {
                MetaAnalytics.CreateTable(HandLogger.csvName, "scene_initialized", "timestamp", "pmdX", "pmdY", "pmdZ", "lX", "lY", "lZ", "rX", "rY", "rZ");
            }
            catch (TableAlreadyExistsException)
            {
                Debug.Log("ATTEMPTING TO MAKE HANDS TABLE WITH SAME NAME");
            }
        }

        private void InitializeIMUAnalytics()
        {
            //Frequency of data pull
            IMULogger.timeThreshold = 1.0f / imuFPS;

            //TODO: Should use GUID for filename in future
            IMULogger.initSceneTimestamp = initSceneTimestamp;

            //Label the files by scene initialiaztion time
            IMULogger.csvName = "IMULog/" + initSceneTimestamp;

            //Create the table
            try
            {
                MetaAnalytics.CreateTable(IMULogger.csvName, "scene_initialized", "timestamp", "accX", "accY", "accZ", "magX", "magY", "magZ", "gyroX", "gyroY", "gyroZ");
            }
            catch (TableAlreadyExistsException)
            {
                Debug.Log("ATTEMPTING TO MAKE IMU TABLE WITH SAME NAME");
            }
        }

        /// <summary>
        /// Push data to the loggers
        /// </summary>
        private void UpdateLoggers()
        {
            if (logHands && handsLogInitialized)
            {
                HandLogger.UpdateHandLogger(Time.deltaTime, handPositionFPS, lh.center, rh.center);
            }
            //Check that IMULocalizer is not null
            if (logIMU && imuLogInitialized && imulocalizer!=null)
            {
                IMULogger.UpdateIMULogger(Time.deltaTime, imuFPS, imulocalizer);
            }
        }

    }
}
