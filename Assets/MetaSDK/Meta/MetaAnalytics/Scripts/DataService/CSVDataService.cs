﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

namespace Meta.Analytics
{

    /// <summary>
    /// A facade for logging data to csv files.
    /// </summary>
    internal class CSVDataService : IDataService
    {

        public static string FileExtension = ".csv";

        /// <summary>
        /// A manager that handles writing tables to files.
        /// </summary>
        protected ITableFileManager _tableFileManager;

        /// <summary>
        /// A manager that handles creation and data entry logging for the tables.
        /// </summary>
        protected ITableManager _tableManager;

        /// <summary>
        /// The directory where the files are to be written
        /// </summary>
        protected string _localPath;

        /// <summary>
        /// The directory where files are moved to for pushing to Amazon.
        /// </summary>
        protected string _pushPath;

        /// <summary>
        /// Constructs a new CSV data service.
        /// </summary>
        public CSVDataService()
        {
            _localPath = string.Empty;
            _pushPath = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableManager"></param>
        /// <param name="tableFileManager"></param>
        public CSVDataService(ITableManager tableManager, ITableFileManager tableFileManager)
        {
            this._tableManager = tableManager;
            this._tableFileManager = tableFileManager;
            this._localPath = string.Empty;
            this._pushPath = string.Empty;
        }
        
        /// <summary>
        /// Log an entry to a given data table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataEntry"></param>
        public virtual void LogEntry(string tableName, IDataEntry dataEntry)
        {
            _tableManager.LogEntry(tableName, dataEntry);
        }

        /// <summary>
        /// Logs multiple entries to the specified table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataEntries"></param>
        public void LogEntry(string tableName, params IDataEntry[] dataEntries)
        {
            for(int i = 0; i < dataEntries.Length; ++i)
            {
                _tableManager.LogEntry(tableName, dataEntries[i]);
            }
        }

        /// <summary>
        /// Log entry 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <param name="data"></param>
        public void LogEntry(string tableName, string column, string data)
        {
            _tableManager.LogEntry(tableName, column, data);
        }


        /// <summary>
        /// Validates the service.  
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// This should be surrounded with a try-catch.  If an exception is thrown then the service may not be functioning.
        /// </remarks>
        //public virtual void Initialize()
        //{

        //    _tableFileManager = new CSVTableFileManager();

        //    _tableManager = new TableManager();
        //}

        /// <summary>
        /// Create a data table with the given name.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <remarks>
        /// An exception will be thrown if there is already a data table in existence with the same name. 
        /// </remarks>
        public virtual void CreateTable(string tableName, params string[] columns)
        {
       
            _tableManager.CreateTable(Types.CSV, tableName, columns);

            _tableFileManager.CreateFile("", _tableManager.GetTable(tableName), ".csv");
        }


        /// <summary>
        /// Return the CSV type
        /// </summary>
        /// <returns></returns>
        public virtual string Type()
        {
            return Types.CSV;
        }

        /// <summary>
        /// Returns true if the specified table exists.  False otherwise. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public virtual bool ContainsTable(string tableName)
        {
            return _tableManager.ContainsTable(tableName);
        }

        /// <summary>
        /// Write or push a certain table.
        /// </summary>
        public virtual void Dispatch(string tableName)
        {

            _tableFileManager.WriteTable(_tableManager.GetTable(tableName));

            _tableManager.ClearTable(tableName);
        }

        /// <summary>
        /// Write all data tables to their csv files.
        /// </summary>
        public virtual void DispatchAll()
        {

            IList<IDataTable> tables = _tableManager.GetTables();

            for(int i = 0; i < tables.Count; ++i)
            {
                _tableFileManager.WriteTable(tables[i]);
            }

            // clear all of the tables of their data entries.
            _tableManager.ClearAllTables();

            
        }

 

        /// <summary>
        /// Set the path where the local files are to be written
        /// </summary>
        /// <param name="path"></param>
        public virtual void SetLocalPath(string path)
        {
            
            _localPath = path;
            _tableFileManager.SetPath(path);
        }

        /// <summary>
        /// Set the location where files will be moved to in order to be pushed to a server. 
        /// </summary>
        /// <param name="path"></param>
        public virtual void SetPushPath(string path)
        {
            _pushPath = path;
        }


        /// <summary>
        /// Close the service
        /// </summary>
        public virtual void Close()
        {
            DeallocateResources();   
        }

        /// <summary>
        /// Deallocate all resources
        /// </summary>
        protected virtual void DeallocateResources()
        {
            CloseFiles();
            FreeTables();
        }

        /// <summary>
        /// Free all of the data tables
        /// </summary>
        protected virtual void FreeTables()
        {
            _tableManager = null;
        }

        /// <summary>
        /// Close all of the open file streams
        /// </summary>
        protected virtual void CloseFiles()
        {
            IList<IDataTable> tables = _tableManager.GetTables();

            for(int i = 0; i < tables.Count; ++i)
            {
                _tableFileManager.CloseFile(tables[i]);
            }

            _tableFileManager = null;
        }

        /// <summary>
        /// Creates a file name using the table's name, the date and a counter for how many times it's already been pushed.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="date"></param>
        /// <param name="counter"></param>
        /// <returns></returns>
        protected string GenerateTableFileName(string tableName, System.DateTime date, int counter)
        {
            return "";
        }

        /// <summary>
        /// Configure the service
        /// </summary>
        public void Configure()
        {
            
        }




    }
    
}
