﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// A static class which contains methods to log data. 
    /// </summary>
    /// <remarks>
    /// This static wraps a DataService - a facade to multiple systems for logging and pushing data
    /// </remarks>
    internal static class MetaAnalytics
    {


        private static IDataService _service = null;
        private static bool _isInit = false;

        /// <summary>
        /// Log an entry to the given table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="entry"></param>
        public static void LogEntry(string tableName, IDataEntry entry)
        {
            if (!IsValid()) return;

            _service.LogEntry(tableName, entry);
        }

        /// <summary>
        /// Log an entry via strings.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <param name="data"></param>
        public static void LogEntry(string tableName, string column, string data)
        {
            if (!IsValid()) return;

            _service.LogEntry(tableName, column, data);
        }

        /// <summary>
        /// Create a new table.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="columns"></param>
        public static void CreateTable(string name, params string[] columns)
        {
            if (!IsValid()) return;

            _service.CreateTable(name, columns);
        }

        /// <summary>
        /// Dispatch a single table.
        /// </summary>
        /// <param name="tableName"></param>
        public static void Dispatch(string tableName)
        {
            if (!IsValid()) return;

            _service.Dispatch(tableName);
        }

        /// <summary>
        /// Dispatch all tables.
        /// </summary>
        public static void DispatchAll()
        {
            if (!IsValid()) return;

            _service.DispatchAll();
        }

        /// <summary>
        /// Set the local directory for where local files will be stored.
        /// </summary>
        public static void SetLocalPath(string path)
        {
            if (!IsValid()) return;

            _service.SetLocalPath(path);
        }

        /// <summary>
        /// Validate the service.  An exception will be thrown if it cannot start. 
        /// </summary>
        public static void InitializeService()
        {

            try
            {
                if (_service == null)
                {
                    ITableFileManager fileManager = new CSVTableFileManager();
                    ITableManager tableManager = new TableManager();
                    _service = new CSVDataService(tableManager, fileManager);
                    
                }
            }
            catch(System.Exception e)
            {
                System.Console.WriteLine(e);
                _service = null; // free memory
            }
            finally
            {
                if (_service == null)
                {
                    _isInit = false;
                }
                else
                {
                    _isInit = true;
                }
                
            }
        }

        /// <summary>
        /// Have the dataservice do any checks and perform any actions from the previous session.
        /// </summary>
        /// <remarks>
        /// This should be called after InitializeService and SetLocalPath
        /// </remarks>
        public static void CheckPreviousSession()
        {
            //_service.CheckPreviousSession();
        }

        /// <summary>
        /// Returns true if the analytics engine is functional.
        /// </summary>
        /// <returns></returns>
        public static bool IsValid()
        {
            return _isInit;
        }

        /// <summary>
        /// close the service
        /// </summary>
        public static void Close()
        {
            if(_service != null) _service.Close();
        }

    }

}
