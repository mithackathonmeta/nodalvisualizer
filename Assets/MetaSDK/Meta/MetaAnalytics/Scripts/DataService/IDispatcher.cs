﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta.Analytics
{
    internal interface IDispatcher
    {

        /// <summary>
        /// Dispatch the files. 
        /// </summary>
        /// <param name="uploadFolder"></param>
        /// <param name="tableManager"></param>
        /// <param name="fileManager"></param>
        void Dispatch(string uploadFolder, DateTime fileDate, ITableManager tableManager, ITableFileManager fileManager);

        /// <summary>
        /// An event to be called when dispatch occurs. 
        /// </summary>
        event EventHandler OnDispatch;

        /// <summary>
        /// Files should be uploaded after the given amount of seconds.
        /// </summary>
        /// <param name="seconds"></param>
        void SetUploadFrequency(int seconds);


    }

}