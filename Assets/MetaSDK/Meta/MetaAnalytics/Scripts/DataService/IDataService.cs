﻿using System.Collections;

namespace Meta.Analytics
{
    internal interface IDataService
    {

        /// <summary>
        /// Log an entry to a specific data table
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataEntry"></param>
        void LogEntry(string tableName, IDataEntry dataEntry);

        /// <summary>
        /// Log multiple entries to a specific data table. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dataEntries"></param>
        void LogEntry(string tableName, params IDataEntry[] dataEntries);

        /// <summary>
        /// Log a string of data to a specified column in a specified table. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <param name="data"></param>
        void LogEntry(string tableName, string column, string data);

        /// <summary>
        /// Creates a table from the given columns.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        void CreateTable(string tableName, params string[] columns);

        /// <summary>
        /// Ready the service
        /// </summary>
        /// <returns></returns>
        //void Initialize();

        /// <summary>
        /// Return a string stating which type of data service it is. 
        /// </summary>
        /// <returns></returns>
        string Type();

        /// <summary>
        /// Returns true if the service already has a table. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        bool ContainsTable(string tableName);

        /// <summary>
        /// Write or push a certain table of data
        /// </summary>
        void Dispatch(string tableName);

        /// <summary>
        /// Write or push all currently logged entries.
        /// </summary>
        void DispatchAll();

        /// <summary>
        /// Set the directory for local files.
        /// </summary>
        /// <param name="path"></param>
        void SetLocalPath(string path);

        /// <summary>
        /// Set the directory where the files will be copied to and then pushed to amazon.
        /// </summary>
        /// <param name="path"></param>
        void SetPushPath(string path);

        /// <summary>
        /// Check for anything left over from the previous section (incase of a crash etc) and handle appropriately.
        /// </summary>
        //void CheckPreviousSession();

        /// <summary>
        /// Shutdown the service cleanly.
        /// </summary>
        void Close();

        /// <summary>
        /// Configure the service.
        /// </summary>
        void Configure();

    }
}
