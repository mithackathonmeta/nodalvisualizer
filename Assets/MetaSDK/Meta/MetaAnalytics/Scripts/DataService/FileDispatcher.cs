﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta.Analytics
{
    internal class FileDispatcher : IDispatcher
    {

        private DateTime _lastDispatch = new DateTime();

        private int _dispatchFrequencySeconds = 60;

        /// <summary>
        /// Dispatches the files (in the perspective of the service).
        /// </summary>
        /// <param name="uploadFolder"></param>
        /// <param name="tableManager"></param>
        /// <param name="fileManager"></param>
        public void Dispatch(string uploadFolder, DateTime fileDate, ITableManager tableManager, ITableFileManager fileManager)
        {
            int interval = (DateTime.Now - _lastDispatch).Seconds;
            // check the time
            if(interval >= _dispatchFrequencySeconds)
            {
                // prepare files for upload
                fileManager.PrepareUpload(uploadFolder);
            }

            if(OnDispatch != null)
            {
                OnDispatch(this, EventArgs.Empty);
            }
        }

        public event System.EventHandler OnDispatch;

        public void SetUploadFrequency(int seconds)
        {
            throw new System.NotImplementedException();
        }
    }
}
