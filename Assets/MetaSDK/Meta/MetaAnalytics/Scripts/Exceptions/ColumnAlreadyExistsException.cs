﻿using System.Collections;
using System;

namespace Meta.Analytics
{
    /// <summary>
    /// An exception that should be raised in circumstances when a column already exists
    /// and a duplicate is attempting to be made or added. 
    /// </summary>
    internal class ColumnAlreadyExistsException : Exception
    {
        public ColumnAlreadyExistsException() : base("Column already exists. ")
        {
             
        }

        public ColumnAlreadyExistsException(string column) : base("Column already exists: " + column)
        {

        }

        public ColumnAlreadyExistsException(string message, Exception inner) : base(message, inner)
        {

        }

    }
    
}