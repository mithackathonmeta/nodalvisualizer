﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// An exception class for when the requested field isn't present in the data group.
    /// </summary>
    internal class ColumnNotFoundException : System.Exception
    {
        public ColumnNotFoundException() : base("Column not found. ")
        {

        }

        public ColumnNotFoundException(string column) : base("Column not found: " + column)
        {

        }

        public ColumnNotFoundException(string message, System.Exception inner) : base(message, inner)
        {

        }

    }
    
}