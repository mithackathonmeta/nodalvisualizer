﻿using System.Collections;

namespace Meta.Analytics
{
    internal class ElementAlreadyExistsException : System.Exception
    {
        /// <summary>
        /// Simply notifies that an element already exists.
        /// </summary>
        public ElementAlreadyExistsException() : base("Element already exists.")
        {

        }

        /// <summary>
        /// Create with a custom message.
        /// </summary>
        /// <param name="message"></param>
        public ElementAlreadyExistsException(string message) : base(message)
        {

        }

        /// <summary>
        /// Construct with a custom message and inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ElementAlreadyExistsException(string message, System.Exception inner)
            : base(message, inner)
        {

        }

    }

}