﻿using System.Collections;



namespace Meta.Analytics
{
    internal class TableNotFoundException : System.Exception
    {
        public TableNotFoundException() : base("Table not found. ")
        {

        }

        public TableNotFoundException(string name) : base("Table not found: " + name)
        {

        }

        public TableNotFoundException(string message, System.Exception inner) : base(message, inner)
        {

        }

    }

}