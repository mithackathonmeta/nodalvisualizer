﻿using System.Collections;


namespace Meta.Analytics
{
    internal class ElementNotFoundException : System.Exception
    {
        
        /// <summary>
        /// Simply notifies that an element can't be found.
        /// </summary>
        public ElementNotFoundException() : base("Element not found.")
        {

        }

        /// <summary>
        /// Create with a custom message.
        /// </summary>
        /// <param name="message"></param>
        public ElementNotFoundException(string message) : base(message)
        {

        }

        /// <summary>
        /// Construct with a custom message and inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ElementNotFoundException(string message, System.Exception inner) : base(message, inner)
        {

        }

    }

}