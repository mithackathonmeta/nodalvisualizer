﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// An exception that should be raised when a duplicate Table is being added or created 
    /// and a table already exists.
    /// </summary>
    internal class TableAlreadyExistsException : System.Exception
    {

        public TableAlreadyExistsException() : base("Table already exists. ")
        {

        }

        public TableAlreadyExistsException(string name) : base("Table already exists: " + name)
        {

        }

        public TableAlreadyExistsException(string message, System.Exception inner)
            : base(message, inner)
        {

        }

    }

}