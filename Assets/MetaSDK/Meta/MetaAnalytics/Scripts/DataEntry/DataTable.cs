﻿using System.Collections;
using System.Collections.Generic;

namespace Meta.Analytics
{
    internal class DataTable : IDataTable
    {
        /// Dictionary to hold data 
        protected IDictionary<string, IDataEntry> _table;
        protected List<string> _columns;

        /// <summary>
        /// The string that separates each data entry when printing data.
        /// </summary>
        protected string _dataSeparator;

        /// <summary>
        /// Name of the table.
        /// </summary>
        protected string _name;
        public string name { get {return _name;} set {_name = value; } }

        /// <summary>
        /// type of the data table
        /// </summary>
        protected string _type;

        /// <summary>
        /// Number of columns
        /// </summary>
        public int columnCount
        {
            get
            {
                return _columns.Count;
            }
        }

        /// <summary>
        /// Number of added entries
        /// </summary>
        public int entryCount
        {
            get
            {
                int count = 0;
                foreach(IDataEntry e in _table.Values)
                {
                    // entries that have a null or empty string are only added internally and shouldn't be counted
                    if (!string.IsNullOrEmpty(e.GetData()))
                    {
                        ++count;
                    }
                }

                return count;
            }
        }

        /// <summary>
        /// Create empty data table
        /// </summary>
        public DataTable()
        {
            _dataSeparator = " ";
            _table = new Dictionary<string, IDataEntry>();
            _columns = new List<string>();
            _name = string.Empty;
            _type = Types.Default;
        }

        /// <summary>
        /// Create an empty data table with a name
        /// </summary>
        /// <param name="name"></param>
        public DataTable(string name)
        {
            _dataSeparator = " ";
            _table = new Dictionary<string, IDataEntry>();
            _columns = new List<string>();
            _name = name;
            _type = Types.Default;

        }


        /// <summary>
        /// Add a set of columns to the data table.
        /// </summary>
        /// <param name="columns"></param>
        public virtual void AddColumns(params string[] columns)
        {
            foreach(string column in columns)
            {
                // if the column hasn't already been added then add it
                if(!ContainsColumn(column))
                {
                    _table[column] = DataEntry.Empty;
                    _columns.Add(column);
                }
                else
                {
                    // throw an exception if the column name already exists
                    ColumnAlreadyExistsException e = new ColumnAlreadyExistsException(column);
                    throw e;
                }
            }
        }

        /// <summary>
        /// Add one or more entries to the data table.  
        /// </summary>
        /// <param name="dataEntry"></param>
        /// <remarks>
        /// If there is a data entry already entered for the corresponding column name.  That data will be overwritten with the new data. 
        /// Throws and exception if a data entry is given but there hasn't been a column added for it previously. 
        /// </remarks>
        public virtual void AddDataEntries(params IDataEntry[] dataEntries)
        {
            foreach(IDataEntry entry in dataEntries)
            {
                // if the column doesn't exist throw an exception
                if (!ContainsColumn(entry.GetColumn()))
                {
                    ColumnNotFoundException e = new ColumnNotFoundException(entry.GetColumn());
                    throw e;
                }

                // add the entry
                _table[entry.GetColumn()] = entry;
            }
        }

        /// <summary>
        /// Set the data of the column. 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="data"></param>
        public virtual void SetColumnData(string column, string data)
        {
            AssertColumnExists(column);

            _table[column].SetData(data);
        }

        /// <summary>
        /// Return all of the data as a string.
        /// </summary>
        /// <returns></returns>
        public virtual string GetData()
        {
            string data = string.Empty;

            // retrieve each entry in order
            for(int i = 0; i < _table.Count; ++i)
            {
                data += _table[_columns[i]].GetData() + _dataSeparator;
            }

            // remove trailing separator
            if(data.Length > 0) data = data.Substring(0, data.Length - 1);

            return data;

        }

        /// <summary>
        /// Set the name of the table.
        /// </summary>
        /// <param name="name"></param>
        public virtual void SetName(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Returns a string of the column names in order
        /// </summary>
        /// <returns></returns>
        public virtual string GetHeader()
        {
            string data = string.Empty;

            // place all headers into a single string in order
            for(int i = 0; i < _columns.Count; ++i)
            {
                data += _columns[i] + _dataSeparator;
            }

            // remove trailing separator
            if(data.Length > 0) data = data.Substring(0, data.Length - 1);

            return data;
        }

        /// <summary>
        /// Returns true if the data table already contains the given column, otherwise returns false.
        /// </summary>
        /// <returns></returns>
        public virtual bool ContainsColumn(string column)
        {
            return _table.ContainsKey(column);
        }

        /// <summary>
        /// Should clear all of the data entries.
        /// </summary>
        public virtual void ClearData()
        {
            foreach(string column in _columns)
            {
                _table[column] = DataEntry.Empty;
            }
        }

        /// <summary>
        /// Return the default type.
        /// </summary>
        /// <returns></returns>
        public virtual string Type()
        {
            return Types.Default;
        }

        /// <summary>
        /// Assert that the column exist.
        /// </summary>
        /// <param name="column"></param>
        public void AssertColumnExists(string column)
        {
            if(!ContainsColumn(column))
            {
                ColumnNotFoundException e = new ColumnNotFoundException(column);
                throw e;
            }
        }
    }

}