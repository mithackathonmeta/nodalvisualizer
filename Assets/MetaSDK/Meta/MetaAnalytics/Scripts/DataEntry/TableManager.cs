﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Meta.Analytics
{
    internal class TableManager : ITableManager
    {

        /// <summary>
        /// Dictionary for fast access to data tables.  The key will be the name of the data table.
        /// </summary>
        protected Dictionary<string, IDataTable> _tables;

        /// <summary>
        /// A factory for creating tables.
        /// </summary>
        protected IDataTableFactory _tableFactory;

        /// <summary>
        /// Create an empty table manager.
        /// </summary>
        public TableManager()
        {
            // allocate a new dictionary.
            _tables = new Dictionary<string, IDataTable>();

            // create a factory for creating tables. 
            _tableFactory = new DataTableFactory();
        }

        /// <summary>
        /// Create a table from the given table name and column names. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        public void CreateTable(string type, string tableName, params string[] columns)
        {
            AssertTableNotExist(tableName);

            // create new table object. 
            IDataTable table = _tableFactory.Create(type,tableName);
            table.AddColumns(columns);

            // add to dictionary with table name being the key. 
            _tables[table.name] = table;
        }

        /// <summary>
        /// Log an entry to a specific table. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="entry"></param>
        public void LogEntry(string tableName, IDataEntry entry)
        {
            AssertTableExsits(tableName);

            IDataTable table = _tables[tableName];

            table.AddDataEntries(entry);
        }

        /// <summary>
        /// Add an string of data to the specified table and column.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <param name="data"></param>
        public void LogEntry(string tableName, string column, string data)
        {
            AssertTableExsits(tableName);

            IDataTable table = _tables[tableName];

            table.SetColumnData(column, data);

        }

        /// <summary>
        /// Informs if the specified table exists in the manager. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public bool ContainsTable(string tableName)
        {
            return _tables.ContainsKey(tableName);
        }

        /// <summary>
        /// Get an array of tables. 
        /// </summary>
        /// <returns></returns>
        public IList<IDataTable> GetTables()
        {
            IDataTable[] tables = new IDataTable[_tables.Count];
            

            Dictionary<string, IDataTable>.Enumerator enumerator = _tables.GetEnumerator();
            
            int index = 0;
            while(enumerator.MoveNext())
            {
                IDataTable current = enumerator.Current.Value;
                tables[index] = current;
                ++index;
            }

            return Array.AsReadOnly(tables); 
        }

        /// <summary>
        /// Throw exception if the table doesn't exist. 
        /// </summary>
        /// <param name="tableName"></param>
        private void AssertTableNotExist(string tableName)
        {
            if (ContainsTable(tableName)) throw new TableAlreadyExistsException(tableName);
        }

        /// <summary>
        /// Throw exception if the table doesn't exist.  
        /// </summary>
        /// <param name="tableName"></param>
        private void AssertTableExsits(string tableName)
        {
            if (!ContainsTable(tableName)) throw new TableNotFoundException(tableName);
        }

        /// <summary>
        /// Get the table object for the given name. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns>Returns an IDataTable corresponding to the given name. </returns>
        public IDataTable GetTable(string tableName)
        {
            AssertTableExsits(tableName);
            return _tables[tableName];
        }

        /// <summary>
        /// Clear the data entries in the specified table.  
        /// </summary>
        /// <param name="tableName"></param>
        public void ClearTable(string tableName)
        {
            AssertTableExsits(tableName);

            _tables[tableName].ClearData();

        }

        /// <summary>
        /// Clear all of the tables of their data entries.
        /// </summary>
        public void ClearAllTables()
        {
            Dictionary<string, IDataTable>.Enumerator tableIterator = _tables.GetEnumerator();

            while(tableIterator.MoveNext())
            {
                tableIterator.Current.Value.ClearData();
            }
        }
    } 
}
