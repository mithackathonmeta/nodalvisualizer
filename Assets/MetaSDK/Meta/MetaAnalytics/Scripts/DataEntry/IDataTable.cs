﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// An interface for classes that create data tables.
    /// </summary>
    /// <remarks>
    /// A data table is a group of column names and one entry associated with each field.  Similar to a row in a data base query.  
    /// At an abstract level however, this acts as a table.
    /// </remarks>
    internal interface IDataTable
    {
        /// <summary>
        /// Add a column name to the current table
        /// </summary>
        /// <param name="fieldName"></param>
        void AddColumns(params string[] columns);

        /// <summary>
        /// Add an entry of data to the current "row".
        /// </summary>
        /// <param name="dataEntry"></param>
        void AddDataEntries(params IDataEntry[] dataEntries);

        /// <summary>
        /// Will set the column entry to the given data.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="data"></param>
        void SetColumnData(string column, string data);

        /// <summary>
        /// Get the data of the current "row" in string form
        /// </summary>
        /// <returns></returns>
        string GetData();

        /// <summary>
        /// Set the name of the table. 
        /// </summary>
        /// <param name="name"></param>
        void SetName(string name);

        /// <summary>
        /// Returns a list of column names in the order that they were added to the table
        /// </summary>
        /// <returns></returns>
        string GetHeader();

        /// <summary>
        /// Returns true if the given column name is already apart of the data table
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        bool ContainsColumn(string column);

        /// <summary>
        /// Should clear all of the data entries.
        /// </summary>
        void ClearData();

        /// <summary>
        /// The name for the table
        /// </summary>
        string name { get; set; }

        /// <summary>
        /// Return the number of columns in the data table
        /// </summary>
        int columnCount { get;}

        /// <summary>
        /// Return the number of entries in the data table
        /// </summary>
        int entryCount { get; }

        /// <summary>
        /// Returns a string of what type of data table this object is. 
        /// </summary>
        /// <returns></returns>
        string Type();

    }
    
}