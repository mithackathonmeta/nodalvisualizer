﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Meta.Analytics
{
    internal interface ITableManager
    {

        /// <summary>
        /// Create a table with the given name and from the given fields.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        void CreateTable(string type, string tableName, params string[] columns);

        /// <summary>
        /// Log an entry to a table. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="entry"></param>
        void LogEntry(string tableName, IDataEntry entry);

        /// <summary>
        /// Log the given data to the specified table
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <param name="data"></param>
        void LogEntry(string tableName, string column, string data);

        /// <summary>
        /// Informs if the mananger contains the specified table.  
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns>Returns true if the table exists in the manager. </returns>
        bool ContainsTable(string tableName);

        /// <summary>
        /// Provides an array of all the tables in the manager. 
        /// </summary>
        /// <returns>
        /// Returns an array
        /// </returns>
        IList<IDataTable> GetTables();

        /// <summary>
        /// Returns a table object for the given name. 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        IDataTable GetTable(string tableName);

        /// <summary>
        /// Clear the data in a specific table.
        /// </summary>
        /// <param name="tableName"></param>
        void ClearTable(string tableName);

        /// <summary>
        /// Clear the data in all tables.
        /// </summary>
        void ClearAllTables();
    }

}