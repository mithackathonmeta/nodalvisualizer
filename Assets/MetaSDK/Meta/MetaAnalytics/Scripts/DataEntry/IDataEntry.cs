﻿using System.Collections;


namespace Meta.Analytics
{
    /// <summary>
    /// An interface for the most basic unit for data analytics - a single entry.
    /// </summary>
    /// <remarks>
    /// This interface has methods for a column name which is to be used for grouping data.  It's a string as this is what most analytics work with. 
    /// The Data type states what kind of data is being stored (in string form) e.g Vector3, int, float etc. 
    /// The data is the string of data itself.  Strings are used due to their flexibility and conversion aspects. It's also what mose analytics use. 
    /// </remarks>
    internal interface IDataEntry
    {
        /// <summary>
        /// Get the name of the column in which the given data will be grouped by
        /// </summary>
        /// <returns></returns>
        string GetColumn();

        /// <summary>
        /// Set the name of the column in which the given data will be grouped by
        /// </summary>
        /// <returns></returns>
        void SetColumn(string column);

        /// <summary>
        /// Get the type of data stored, as a string. This can be customized to any custom class.
        /// </summary>
        /// <returns></returns>
        string GetDataType();

        /// <summary>
        /// Set the type of data stored, as a string
        /// </summary>
        void SetDataType(string dataType);

        /// <summary>
        /// Retrieve the data in string form
        /// </summary>
        /// <returns></returns>
        string GetData();

        /// <summary>
        /// Set's the data string 
        /// </summary>
        /// <param name="data"></param>
        void SetData(string data);



    }
}


