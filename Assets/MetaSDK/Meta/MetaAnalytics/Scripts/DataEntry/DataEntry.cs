﻿using System.Collections;

namespace Meta.Analytics
{
    internal class DataEntry : IDataEntry
    {

        /// <summary>
        /// The name of the column that the data belongs to.  Data will most likely be grouped by this column.
        /// </summary>
        protected string _column;

        /// <summary>
        /// The type of data being stored.
        /// </summary>
        protected string _dataType;

        /// <summary>
        /// A string containing the data.
        /// </summary>
        protected string _data;

        /// <summary>
        /// Returns an empty Data Entry.  
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// This returns a newly allocated but empty data object.  
        /// </remarks>
        public static DataEntry Empty
        {
            get
            {
                DataEntry empty = new DataEntry();
                empty.SetData(string.Empty);
                empty.SetDataType(string.Empty);
                empty.SetColumn(string.Empty);

                return empty;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DataEntry()
        {
            _column = string.Empty;
            _data = string.Empty;
            _dataType = string.Empty;
        }

        /// <summary>
        /// Construct from column name and data type
        /// </summary>
        /// <param name="column"></param>
        /// <param name="dataType"></param>
        public DataEntry(string column, string dataType)
        {
            _column = column;
            _dataType = dataType;
            _data = string.Empty;
        }

        /// <summary>
        /// Constructs with all 3 core pieces - column name, data type and, data string.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="dataType"></param>
        /// <param name="data"></param>
        public DataEntry(string column, string dataType, string data)
        {
            _column = column;
            _dataType = dataType;
            _data = data;
        }

        /// <summary>
        /// Return the column name.
        /// </summary>
        /// <returns></returns>
        public virtual string GetColumn()
        {
            return _column;
        }

        /// <summary>
        /// Set the column name.
        /// </summary>
        /// <param name="column"></param>
        public virtual void SetColumn(string column)
        {
            _column = column;
        }

        /// <summary>
        /// Get the data type.
        /// </summary>
        /// <returns></returns>
        public virtual string GetDataType()
        {
            return _dataType;
        }

        /// <summary>
        /// Set data type.
        /// </summary>
        /// <param name="dataType"></param>
        public virtual void SetDataType(string dataType)
        {
            _dataType = dataType;
        }

        /// <summary>
        /// Get the data in string form.  This is most likely what will be posted to the data/analytics service.
        /// </summary>
        /// <returns></returns>
        public virtual string GetData()
        {
            return _data;
        }

        /// <summary>
        /// Set the data string.
        /// </summary>
        /// <param name="data"></param>
        public virtual void SetData(string data)
        {
            _data = data;
        }
    }

}

