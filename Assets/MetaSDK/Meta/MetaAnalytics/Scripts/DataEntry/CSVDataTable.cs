﻿using System.Collections;

namespace Meta.Analytics
{
    internal class CSVDataTable : DataTable
    {

        /// <summary>
        /// Create's an empty CSV data table.
        /// </summary>
        public CSVDataTable()
        {
            _dataSeparator = ",";
            
        }

        /// <summary>
        /// Creates a CSV data table with the given name
        /// </summary>
        /// <param name="name"></param>
        public CSVDataTable(string tableName) : base(tableName)
        {
            _dataSeparator = ",";
        }

        /// <summary>
        /// Return the CSV data type of the table.
        /// </summary>
        /// <returns></returns>
        public override string Type()
        {
            return Types.CSV;
        }
        
    }

}