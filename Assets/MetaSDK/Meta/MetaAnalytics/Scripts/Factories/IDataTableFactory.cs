﻿using System.Collections;

namespace Meta.Analytics
{
    internal interface IDataTableFactory 
    {
        /// <summary>
        /// Create an empty, nameless data table of the specified type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IDataTable Create(string type);

        /// <summary>
        /// Create an empty data table with a name.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        IDataTable Create(string type, string name);

        /// <summary>
        /// Create a data table of the specified type and given fieldNames.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fieldNames"></param>
        /// <returns></returns>
        IDataTable Create(string type, string name, params string[] fieldNames);
    }

}