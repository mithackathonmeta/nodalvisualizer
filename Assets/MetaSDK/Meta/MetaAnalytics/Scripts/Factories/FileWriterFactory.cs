﻿using System.Collections;

namespace Meta.Analytics
{
    internal class FileWriterFactory : IFileWriterFactory
    {

        /// <summary>
        /// default constructor
        /// </summary>
        public FileWriterFactory()
        {

        }

        /// <summary>
        /// Create a new file writer based on the type. 
        /// </summary>
        /// <param name="fileWriterType"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public virtual IFileWriter Create(string fileWriterType, string fileName)
        {
            IFileWriter newWriter;

            // select which type if filewriter to create
            switch(fileWriterType.ToLower())
            {
                // create a csv file writer
                case Types.CSV:
                    newWriter = new CSVFileWriter(fileName);
                    break;
                // by default make a CSVfilewriter
                default:
                    newWriter = new CSVFileWriter(fileName);
                    break;
            }

            return newWriter;

        }
    }

}