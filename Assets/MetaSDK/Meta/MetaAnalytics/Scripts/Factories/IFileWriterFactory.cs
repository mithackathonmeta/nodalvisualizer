﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// An abstract factory for creating file writers
    /// </summary>
    internal interface IFileWriterFactory
    {
        /// <summary>
        /// Create a file writer given the type of file writer to create and the file name
        /// </summary>
        /// <param name="fileWriterType"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        IFileWriter Create(string fileWriterType, string fileName);
    }

}