﻿using System.Collections;

namespace Meta.Analytics
{
    /// <summary>
    /// Factory for creating different data tables
    /// </summary>
    internal class DataTableFactory : IDataTableFactory
    {

        /// <summary>
        /// Create a data table given the type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual IDataTable Create(string type)
        {

            IDataTable newTable;

            switch(type)
            {
                // create a csv type
                case Types.CSV:
                    newTable = new CSVDataTable();
                    break;
                // create the default data table - base datatable class.
                default:
                    newTable = new DataTable();
                    break;
            }

            return newTable;

        }

        /// <summary>
        /// Create a data table of the given type with a name. 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual IDataTable Create(string type, string name)
        {
            IDataTable newTable = Create(type);
            newTable.SetName(name);

            return newTable;
        }

        /// <summary>
        /// Create a data table of the given type with a name and the fields the data table will contain. 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public virtual IDataTable Create(string type, string name, params string[] columns)
        {
            IDataTable newTable = Create(type, name);
            newTable.AddColumns(columns);

            return newTable;
        }
    } 
}
