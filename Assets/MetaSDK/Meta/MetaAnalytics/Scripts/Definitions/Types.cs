﻿using System.Collections;

namespace Meta.Analytics
{
    internal static class Types
    {
        /// <summary>
        /// A type for the CSV data classes.
        /// </summary>
        public const string CSV = "csv";

        public const string Default = "default";

    }
    
}