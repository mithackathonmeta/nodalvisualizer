﻿using System.Collections;

/// <summary>
/// Generic singleton class
/// </summary>
/// <typeparam name="T"></typeparam>
internal class Singleton<T> where T : new()
{
    /// <summary>
    /// Instance to only have one of
    /// </summary>
    private static T _instance;

    /// <summary>
    /// Get the instance
    /// </summary>
    public T Instance
    {
        get
        {
            /// check that the instance exists, if not create a new one.
            if (_instance == null)
            {
                _instance = new T();
            }

            return _instance;
        }
    }
}
