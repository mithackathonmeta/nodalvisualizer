﻿using System.Collections;
using System.Collections.Generic;

namespace Meta.Analytics
{
    internal interface ITableFileManager
    {
        /// <summary>
        /// Create a file based off a Data table
        /// </summary>
        /// <param name="table"></param>
        void CreateFile(string prefix, IDataTable table, string extension);

        /// <summary>
        /// Write a table to a file.
        /// </summary>
        /// <param name="table"></param>
        void WriteTable(IDataTable table);

        /// <summary>
        /// Write a set of tables to their corresponding files. 
        /// </summary>
        /// <param name="tables"></param>
        void WriteTable(params IDataTable[] tables);

        /// <summary>
        /// Close a specific table's file.
        /// </summary>
        /// <param name="table"></param>
        void CloseFile(IDataTable table);

        /// <summary>
        /// Prepare the files for upload.
        /// </summary>
        void PrepareUpload(string uploadFolderPath);

        /// <summary>
        /// Set the path as to where the files will be stored. 
        /// </summary>
        /// <param name="path"></param>
        void SetPath(string path);

        
    }

}