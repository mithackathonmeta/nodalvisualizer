﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Meta.Analytics
{
    internal class CSVTableFileManager : ITableFileManager
    {

        /// <summary>
        /// Factory for creating file writers.
        /// </summary>
        IFileWriterFactory _fileFactory;

        /// <summary>
        /// Maps file name to file writer.
        /// </summary>
        Dictionary<string, IFileWriter> _files;


        /// <summary>
        /// Location for the files.
        /// </summary>
        private string _path;

        /// <summary>
        /// Construct a table file manager.  
        /// </summary>
        public CSVTableFileManager()
        {
            _fileFactory = new FileWriterFactory();
            _files = new Dictionary<string, IFileWriter>();
        }

        /// <summary>
        /// Construct with a given path to store the files.
        /// </summary>
        /// <param name="path"></param>
        public CSVTableFileManager(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Create file from the prefix and the table
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="table"></param>
        public void CreateFile(string prefix, IDataTable table, string extension)
        {
        
            // ensure that it doesn't already exist - throw exception if it does.
            AssertFileNotExist(table.name);

            // create a file name
            string filename = _path + prefix + table.name + extension;

            // create a new file writer and add it to the dictionary. 
            _files[table.name] = _fileFactory.Create(Types.CSV, filename);
            _files[table.name].Write(table.GetHeader());
        }

        /// <summary>
        /// Write a table to its file.
        /// </summary>
        /// <param name="table"></param>
        public void WriteTable(IDataTable table)
        {
            AssertFileExists(table.name);
            
            // if there aren't any entries don't write the table
            if (table.entryCount == 0) return;

            // write the data
            _files[table.name].Write(table.GetData());
        }

        /// <summary>
        /// Close a table's file.
        /// </summary>
        /// <param name="table"></param>
        public void CloseFile(IDataTable table)
        {
            AssertFileExists(table.name);

            _files[table.name].Close();
        }

        /// <summary>
        /// Prepare the files for uploading.  
        /// </summary>
        /// <param name="uploadFolderPath"></param>
        /// <remarks>
        /// This operation will delete all table files and so each will need to be recreated with a different prefix.  
        /// </remarks>
        public void PrepareUpload(string uploadFolderPath)
        {
            // move the files
            MoveFiles(uploadFolderPath);

            // clear the dictionary
            _files.Clear();
        }

        /// <summary>
        /// Throw an exception if the file doesn't exist and should.
        /// </summary>
        /// <param name="name"></param>
        private void AssertFileExists(string name)
        {
            if(!_files.ContainsKey(name))
            {
                throw new ElementNotFoundException(name);
            }
        }

        /// <summary>
        /// Throw an exception if a file already exists and shouldn't.
        /// </summary>
        /// <param name="name"></param>
        private void AssertFileNotExist(string name)
        {
            if(_files.ContainsKey(name))
            {
                throw new ElementAlreadyExistsException(name);
            }
        }

        /// <summary>
        /// Set the path as to where the files will be stored. 
        /// </summary>
        /// <param name="path"></param>
        public void SetPath(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Move all of the files. 
        /// </summary>
        /// <param name="path"></param>
        private void MoveFiles(string path)
        {

            Dictionary<string, IFileWriter>.Enumerator enumerator = _files.GetEnumerator();

            // iterate over all files
            while(enumerator.MoveNext())
            {
                IFileWriter current = enumerator.Current.Value;

                // move the file
                current.MoveTo(path + current.GetFileName());
            }
        }

        /// <summary>
        /// Write all of the tables to file. 
        /// </summary>
        /// <param name="tables"></param>
        public void WriteTable(params IDataTable[] tables)
        {
            foreach(IDataTable table in tables)
            {
                WriteTable(table);
            }
        }
    }

}