﻿using System.Collections;
using System.IO;

namespace Meta.Analytics
{
    /// <summary>
    /// A class for writing to a csv file
    /// </summary>
    internal class CSVFileWriter : IFileWriter
    {

        /// <summary>
        /// Name of table that this file writer will be writing for.  Used for file name.
        /// </summary>
        protected string _filename;

        /// <summary>
        /// The type of file writer this class is. 
        /// </summary>
        protected string _fileWriterType = Types.CSV;

        /// <summary>
        /// A stream that writes to a file.
        /// </summary>
        protected StreamWriter _writer;

        /// <summary>
        /// A writer is valid if it can write to a file. 
        /// </summary>
        protected bool _isValid = true;

        /// <summary>
        /// Default constructor
        /// </summary>
        public CSVFileWriter()
        {
           
        }

        /// <summary>
        /// Create file using the given name
        /// </summary>
        /// <param name="name"></param>
        public CSVFileWriter(string name)
        {
            _filename = name;
        }


        /// <summary>
        /// Returns the type of file writer via string
        /// </summary>
        /// <returns></returns>
        public new virtual string GetType()
        {
            return _fileWriterType;
        }

        /// <summary>
        /// Validate that the file is ready for writing
        /// </summary>
        protected virtual void Validate()
        {

            if(_writer == null)
            {
                try
                {
                    CreateStream();
                }
                catch(System.IO.IOException )
                {
                    _isValid = false;
                }
                catch(System.Exception )
                {
                    _isValid = false;
                }

            }
        }

        /// <summary>
        /// Write the string to a file
        /// </summary>
        /// <param name="entry"></param>
        public void Write(string entry)
        {
            Validate();

            if (!IsValid()) return;

            _writer.WriteLine(entry);
            _writer.Flush();
            
        }

        /// <summary>
        /// Close the file
        /// </summary>
        public void Close()
        {
            _writer.Close();
            _writer = null;
        }

        /// <summary>
        /// IF the writer can write to the file, it is valid. 
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return _isValid;
        }

        /// <summary>
        /// Create the stream
        /// </summary>
        protected void CreateStream()
        {
           
            _isValid = true; // assume that it will work
            FileInfo file = new FileInfo(_filename);
            file.Directory.Create();

            _writer = new StreamWriter(_filename, false);
        }

        /// <summary>
        /// Move the file to the specified directory (includes file name).  
        /// </summary>
        /// <param name="path"></param>
        public void MoveTo(string path)
        {
            _writer.Close();
            File.Move(_filename, path);
            _isValid = false;
            _writer = null;
        }

        /// <summary>
        /// Retrieves the name of the file currently being written to. 
        /// </summary>
        /// <returns></returns>
        public string GetFileName()
        {
            return _filename;
        }
    }
    
}