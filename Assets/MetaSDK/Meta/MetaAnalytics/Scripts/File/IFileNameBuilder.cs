﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta.Analytics
{
    internal interface IFileNameBuilder
    {
        /// <summary>
        /// Return the file name
        /// </summary>
        /// <returns></returns>
        string GetFileName();

        /// <summary>
        /// Set the date/time for the file name.
        /// </summary>
        /// <param name="time"></param>
        void SetTime(DateTime time);

        /// <summary>
        /// Set the table name.
        /// </summary>
        /// <param name="table"></param>
        void SetTableName(string tableName);

        /// <summary>
        /// Set the count/version of the file.
        /// </summary>
        /// <param name="counter"></param>
        void SetCount(int counter);
    } 
}
