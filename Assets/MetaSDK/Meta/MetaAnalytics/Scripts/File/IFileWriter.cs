﻿using System.Collections;


namespace Meta.Analytics
{
    internal interface IFileWriter
    {   
        /// <summary>
        /// Write the given string to the file.
        /// </summary>
        /// <param name="entry"></param>
        void Write(string entry);

        /// <summary>
        /// Return the type of file writer
        /// </summary>
        /// <returns></returns>
        string GetType();

        /// <summary>
        /// Close the file. 
        /// </summary>
        void Close();

        /// <summary>
        /// Returns true if the file writer can keep writing.  
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Should be set to false internally if there's an IO exception
        /// </remarks>
        bool IsValid();

        /// <summary>
        /// Move the file to a new location (includes file name). 
        /// </summary>
        /// <param name="path"></param>
        void MoveTo(string path);

        /// <summary>
        /// Get the file name. 
        /// </summary>
        /// <returns></returns>
        string GetFileName();
    }

}