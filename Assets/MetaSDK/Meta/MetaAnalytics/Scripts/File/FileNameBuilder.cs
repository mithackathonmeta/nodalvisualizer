﻿using System.Collections;
using System;

namespace Meta.Analytics
{
    internal class FileNameBuilder: IFileNameBuilder
    {
        /// <summary>
        /// Used to separate the different fields
        /// </summary>
        private string _delimiter = "_";

        /// <summary>
        /// Counter/Version of the file.  As we send in pieces, this keeps track of the order.
        /// </summary>
        private int _counter = 1;

        /// <summary>
        /// The time - used for ticks 
        /// </summary>
        private DateTime _time;

        /// <summary>
        /// The table name which we're creating a file for.
        /// </summary>
        private string _tableName = "";

        /// <summary>
        /// Return the format of the date for the file name
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        protected string FormatDate(System.DateTime date)
        {
            return date.Ticks.ToString();
        }

        public FileNameBuilder()
        {
            _tableName = string.Empty;
            _time = DateTime.MinValue;
            _counter = 1;
        }

        /// <summary>
        /// Get the file name after the other pieces have been set. 
        /// </summary>
        /// <returns></returns>
        public string GetFileName()
        {
            string fileName = "";

            fileName += (_time != DateTime.MinValue ? FormatDate(_time) : "0") + _delimiter;
            fileName += _tableName + _delimiter;
            fileName += _counter;

            return fileName;
        }

        /// <summary>
        /// The time - used for ticks.  
        /// </summary>
        /// <param name="time"></param>
        public void SetTime(System.DateTime time)
        {
            _time = time;
        }

        /// <summary>
        /// Set the table name.
        /// </summary>
        /// <param name="tableName"></param>
        public void SetTableName(string tableName)
        {
            _tableName = tableName;
        }

        /// <summary>
        /// Set the counter/version of the file.  
        /// </summary>
        /// <param name="counter"></param>
        public void SetCount(int counter)
        {
            // bounds checking
            if (counter < 1) counter = 1;

            _counter = counter;
        }
    }

}