﻿using UnityEngine;
using System.Collections;

namespace Meta.Analytics
{
    internal interface IEnvironmentConfigurer
    {
        /// <summary>
        /// Handle any existing files in the way specific to the data service
        /// </summary>
        void MoveExistingFiles();

        /// <summary>
        /// Sets up directories if they don't exist
        /// </summary>
        /// <param name="current">Refers to where the files will be stored</param>
        /// <param name="existing">Referes to where to place files that already existed upon start up (and shouldn't)</param>
        void SetUpDirectories(string current, string existing);

        /// <summary>
        /// Creates a properties file - filled with data that the service will make use of.
        /// </summary>
        /// <param name="name"></param>
        void CreatePropertiesFile(string name);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool CheckPropertiesFile();
    }

}