﻿using System.Collections;
using System.Collections.Generic;

namespace Meta.Analytics
{

    internal class Properties : IProperties
    {
        private static string _notFoundMessage = "Property not found: ";
        private static string _alreadyExistsMessage = "Property already exists: ";

        /// <summary>
        /// Maps properties to value.
        /// </summary>
        protected Dictionary<string, string> _properties;

        /// <summary>
        /// Constructs an empty properties class.
        /// </summary>
        public Properties()
        {
            _properties = new Dictionary<string, string>();
        }

        /// <summary>
        /// Add a property.  The value will be an empty string.
        /// </summary>
        /// <param name="property"></param>
        public void AddProperty(string property)
        {
            CheckNotExists(property);

            _properties[property] = string.Empty;
        }

        /// <summary>
        /// Add a property and a value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        public void AddProperty(string property, string value)
        {
            // Check that it doesn't exist - will throw exception if it does. 
            CheckNotExists(property);

            _properties[property] = value;
        }

        /// <summary>
        /// Set a property's value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        public void SetProperty(string property, string value)
        {
            // Check that it can be found - throws exception if it can't. 
            CheckFound(property);

            _properties[property] = value;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public string GetPropertyValue(string property)
        {
            // check that the property exists. 
            CheckFound(property);

            return _properties[property];
        }

        /// <summary>
        /// Retrieve an unordered array of all the properites. 
        /// </summary>
        /// <returns></returns>
        public string[] GetAllProperties()
        {
            Dictionary<string, string>.KeyCollection keys = _properties.Keys;
            Dictionary<string, string>.KeyCollection.Enumerator enumerator = keys.GetEnumerator();

            string[] names = new string[keys.Count];
            int counter = 0;
            
            // iterate over the keys and add each one to the array. 
            while(enumerator.MoveNext())
            {
                names[counter] = enumerator.Current;
                counter++;
            }

            return names;
        }

        /// <summary>
        /// Determines if the given property exists. 
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public bool ContainsProperty(string property)
        {
           return _properties.ContainsKey(property);
        }

        /// <summary>
        /// Checks if the property exists and throws an exception if it does.
        /// </summary>
        /// <param name="property"></param>
        private void CheckNotExists(string property)
        {
            if (ContainsProperty(property)) throw new ElementAlreadyExistsException(_alreadyExistsMessage + property);
        }

        /// <summary>
        /// Checks to see if the element can't be found and throws exception if it can't be found.
        /// </summary>
        /// <param name="property"></param>
        private void CheckFound(string property)
        {
            if (!ContainsProperty(property)) throw new ElementNotFoundException(_notFoundMessage + property);
        }

        /// <summary>
        /// Return an enumerator that goes over the property and value pairs. 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _properties.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _properties.GetEnumerator();
        }
    }

}