﻿
using System.Collections;
using System.Collections.Generic;

namespace Meta.Analytics
{
    /// <summary>
    /// An interface for the properties family. 
    /// </summary>
    internal interface IProperties : IEnumerable<KeyValuePair<string, string>>
    {

        /// <summary>
        /// Add a property
        /// </summary>
        /// <param name="property"></param>
        void AddProperty(string property);

        /// <summary>
        /// Add a property and an value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        void AddProperty(string property, string value);

        /// <summary>
        /// Give a property a value - the property must exist
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        void SetProperty(string property, string value);

        /// <summary>
        /// Get a property's value.
        /// </summary>
        /// <param name="property"></param>
        string GetPropertyValue(string property);

        /// <summary>
        /// Returns an array of all the property names.
        /// </summary>
        /// <returns></returns>
        string[] GetAllProperties();

        /// <summary>
        /// Tests to see if the given property exists.
        /// </summary>
        /// <param name="property"></param>
        /// <returns>
        /// Returns true if the specified property exists, false otherwise.
        /// </returns>
        bool ContainsProperty(string property);

        
        

        

    }

}