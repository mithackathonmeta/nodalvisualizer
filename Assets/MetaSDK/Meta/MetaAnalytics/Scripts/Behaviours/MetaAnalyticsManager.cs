﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using Meta;

namespace Meta.Analytics
{

    internal class MetaAnalyticsManager : IEventReceiver
    {
        //[SerializeField]
        public string localPath;

        public MetaAnalyticsManager()
        {
            localPath = "AnalyticsResults/";
        }

        // Use this for initialization
        void Awake()
        {

            // initialize the analytics
            InitializeAnalytics();

            // set the local path for storing files
            SetLocalPath();

        }

        // Update is called once per frame
        void Update()
        {
            try
            {
                MetaAnalytics.DispatchAll();
            }
            catch (System.IO.IOException e)
            {
                CloseAnalytics();

                Debug.LogError(e);
            }
            catch (System.Exception e)
            {
                CloseAnalytics();

                Debug.LogError(e);
            }

        }

        /// <summary>
        /// Close the analytics.
        /// </summary>
        /// <remarks>
        /// This should be called if the analytics become invalid, exceptions occur or the user chooses to opt out. 
        /// </remarks>
        void CloseAnalytics()
        {
            MetaAnalytics.Close();

            // turn this off
            //gameObject.SetActive(false);
        }

        /// <summary>
        /// Verify that local path ends with a backslash; if not, append backslash
        /// </summary>
        private void VerifyTrailingBackslash()
        {
            if (localPath[localPath.Length - 1] != '/')
            {
                localPath = localPath.Insert(localPath.Length, "/");
            }
        }

        /// <summary>
        /// Begin the initialization phase for analytics.
        /// </summary>
        private void InitializeAnalytics()
        {
            // initialize the analytics
            MetaAnalytics.InitializeService();

            // close if 
            if (!MetaAnalytics.IsValid()) CloseAnalytics();
        }

        /// <summary>
        /// Set the local path for storing files etc.
        /// </summary>
        private void SetLocalPath()
        {
            if (string.IsNullOrEmpty(localPath))
            {
                localPath = Application.dataPath;
            }
            else
            {
                localPath = Application.dataPath + "/" + localPath;
            }
            // verify that final backslash exists
            VerifyTrailingBackslash();

            MetaAnalytics.SetLocalPath(localPath);
        }

        /// <summary>
        /// Close the analytics when the application closes. 
        /// </summary>
        void OnApplicationQuit()
        {
            MetaAnalytics.Close();
        }

        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.awakeEvent += Awake;
            eventHandlers.updateEvent += Update;
        }
    }

}