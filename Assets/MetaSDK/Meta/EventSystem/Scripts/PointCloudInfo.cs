﻿using UnityEngine;

namespace Meta
{
    public struct PointCloudInfo
    {
        public readonly Vector3 Centroid;
        //TODO THIS MUST BE AN ORIENTED BOUNDS
        public readonly Bounds Bounds;
        /// <summary>
        /// Distance from front of MetaEventVolume
        /// </summary>
        public readonly float FrontDistance;

        public PointCloudInfo(Vector3 centroid, Bounds bounds, float frontDistance)
        {
            Centroid = centroid;
            Bounds = bounds;
            FrontDistance = frontDistance;
        }

        public static PointCloudInfo SmoothDamp( PointCloudInfo a, PointCloudInfo b, ref PointCloudInfoVelocity velocity)
        {
            Vector3 position = Vector3.SmoothDamp(a.Centroid, b.Centroid, ref velocity._centroidVelocity, velocity._mainDamp);
            float depth = Mathf.SmoothDamp(a.FrontDistance, b.FrontDistance, ref velocity._depthVelocity, velocity._depthDamp);
            Bounds bounds = new Bounds
            {
                size = Vector3.SmoothDamp(a.Bounds.size, b.Bounds.size, ref velocity._boundsSizeVelocity, velocity._mainDamp),
                center = Vector3.SmoothDamp(a.Bounds.center, b.Bounds.center, ref velocity._boundsCenterVelocity, velocity._mainDamp)
            };
            return new PointCloudInfo(position, bounds, depth);
        }
    }
}