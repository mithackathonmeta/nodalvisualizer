﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    public struct PointCloudInfoVelocity
    {
        //Breaking encapsulation because these are meant to be passed by Ref into SmoothDamp functions
        public Vector3 _centroidVelocity;
        public Vector3 _boundsSizeVelocity;
        public Vector3 _boundsCenterVelocity;
        public float _depthVelocity;
        public float _mainDamp;
        public float _depthDamp;
    }
}