﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using Meta.Events;
using Meta.HandInput;

namespace Meta.UI
{
    [RequireComponent(typeof(BoxCollider))]
    public class MetaEventVolume : MonoBehaviour
    {
        [Header("Optional Event Cursor.")]
        [SerializeField]
        private EventCursor _eventCursor = null;

        //[SerializeField]
        private float _reticleDepth = 1f;

        [Header("Offset from front of collider which will activate pressed.")]
        [SerializeField]
        private float _downTriggerOffset = 4f;

        [Header("DownTriggerOffset multiplied this to produce offset which will activate released.")]
        [SerializeField]
        private float _upTriggerOffset = 0.5f;

        [SerializeField]
        private PressStateEvent _eventVolumeState = new PressStateEvent();

        private readonly List<RaycastResult> _resultsCache = new List<RaycastResult>();

        /// <summary>
        /// ID for event
        /// </summary>
        private static int _nextId = 1000;

        /// <summary>
        /// Collider indicating the bounds of MetaEventVolume
        /// </summary>
        private BoxCollider _collider;
        private int _pointerId;
        private MetaHandEventData _pointerData;
        private HandFeature _handInVolume;
        private MetaHandGraphicsRaycaster _raycaster;
        internal PressState pressState;
        private float _handHoldExitTimestamp = -1;
        private HandFeature _handThatExitedWhileHeld = null;
        private Vector3 _touchPosition;
        private float _touchTime;
        private float _handHoldExitTimeout = 0.25f;
        private bool _enableUpDownClick = true;
        private bool _enableTouchClick = false;
        private bool _checkXY = true;
        private bool _checkXYZ = true;
        private float _acceptableXYZClickDistance = 10;
        private float _acceptableXYClickDistance = 10;
        private float _minAcceptableClickTime = 0.5f;
        private float _maxAcceptableClickTime = 2f;



        private void Awake()
        {
            _pointerId = _nextId++;

            //Get the collider defining the bounds
            _collider = GetComponent<BoxCollider>();

            if (!_collider.isTrigger)
            {
                Debug.LogWarning(name + " EventVolume collider must be trigger.");
            }

            _raycaster = GetComponentInParent<MetaHandGraphicsRaycaster>();
            _raycaster.Register(this);

            _pointerData = new MetaHandEventData(_raycaster.EventSystem);
            _pointerData.pointerId = _pointerId;
        }

        private void OnTriggerEnter(Collider collider)
        {
            //Check if the collider represents a hand
            if (_handInVolume == null)
            {
                HandFeature hand = collider.GetComponent<HandFeature>();

                if (hand != _handThatExitedWhileHeld || Time.time - _handHoldExitTimestamp > _handHoldExitTimeout)
                {
                    //Check if the hand has a top point
                    if (hand != null && hand is TopHandFeature)
                    {
                        _handInVolume = hand;

                        if (_eventCursor != null)
                        {
                            _eventCursor.Enable();
                        }
                    }
                }
            }
        }

        private void OnTriggerExit(Collider collider)
        {
            //Check if the collider represents a hand
            HandFeature hand = collider.GetComponent<HandFeature>();

            //Check if the first hand exited, in case multiple hands are in the EventVolume
            if (_handInVolume == hand)
            {
                _handInVolume = null;

                if (_eventCursor != null)
                {
                    _eventCursor.Disable();
                }

                //Release on EventVolume exit if being held
                if (pressState == PressState.Held)
                {
                    _handThatExitedWhileHeld = hand;
                    _handHoldExitTimestamp = Time.time;

                    _pointerData.Reset();
                    _pointerData.pointerCurrentRaycast = new RaycastResult();
                    pressState = PressState.Released;
                    _raycaster.ProcessHandEvent(_pointerData, false, true);
                }
            }
        }

        private void LateUpdate()
        {
            if (_handInVolume != null && _handInVolume.ParentHand.Data.center.z > 0f)
            {
                //Get the front center position of the collider
                Vector3 colliderForwardPoint = _collider.bounds.center + (transform.forward * (_collider.size.z / 2 * transform.lossyScale.z));
                Plane forwardPlane = new Plane(-transform.forward, colliderForwardPoint);
                float depth = forwardPlane.GetDistanceToPoint(_handInVolume.transform.position);
                _pointerData.PointCloudInfo = new PointCloudInfo(_handInVolume.transform.position, new Bounds(), depth);

                UpdatePressStateAndFireEvents();
                UpdateMetaHandEventData();

                if (_eventCursor != null)
                {
                    UpdateCursor();
                }

                ProcessRaycasts();
            }
        }

        private void UpdatePressStateAndFireEvents()
        {
            //Determine how the hand is interacting with the EventVolume.
            //When the hand is past the down trigger, the user is pressing. This is like the user pressing the left mouse button down.
            //Once the user is pressing, if the hand returns past the up trigger, the press has been released. This is like the user releasing the left mouse button.
            //Once the user is pressing, if the hand does not pass the up trigger, the press becomes a hold. This is like the user holding down the left mouse button.
            if (pressState == PressState.None)
            {
                //plane click
                if (_pointerData.PointCloudInfo.FrontDistance < _downTriggerOffset * transform.lossyScale.z)
                {
                    pressState = PressState.Pressed;
                    _touchPosition = transform.InverseTransformPoint(_pointerData.PointCloudInfo.Centroid);
                    _touchTime = Time.time;
                }
            }
            else if (pressState == PressState.Pressed)
            {
                pressState = PressState.Held;
            }
            else if (pressState == PressState.Held && _enableUpDownClick &&
                _pointerData.PointCloudInfo.FrontDistance > ((_downTriggerOffset * transform.lossyScale.z) + (_upTriggerOffset * transform.lossyScale.z)))
            {
                pressState = PressState.Released;
            }
            else if (pressState == PressState.Held && _enableTouchClick && checkTouchClickTime(_touchTime) &&
                checkTouchClickDistance(_touchPosition, _pointerData.PointCloudInfo.Centroid))
            {
                pressState = PressState.Released;
            }
            else if (pressState == PressState.Released && _enableTouchClick)
            {
                pressState = PressState.Releasing;
            }
            else if (pressState == PressState.Releasing &&
                _pointerData.PointCloudInfo.FrontDistance > ((_downTriggerOffset * transform.lossyScale.z) + (_upTriggerOffset * transform.lossyScale.z)))
            {
                pressState = PressState.None;
            }
            else if (pressState == PressState.Released)
            {
                pressState = PressState.None;
            }

            if (_eventVolumeState != null)
            {
                _eventVolumeState.Invoke(pressState);
            }

            if (_eventCursor != null)
            {
                _eventCursor.SetEventVolumeState(pressState);
            }
        }

        private void UpdateMetaHandEventData()
        {
            //Pointer needs to be reset each update
            _pointerData.Reset();
            Vector2 screenPoint = _raycaster.eventCamera.WorldToScreenPoint(_pointerData.PointCloudInfo.Centroid);
            _pointerData.delta = screenPoint - _pointerData.position;
            _pointerData.position = screenPoint;
        }

        private void UpdateCursor()
        {
            //Update cursor with hand position and EventVolume orientation
            _eventCursor.SetHandPosition(_pointerData.PointCloudInfo.Centroid);
            _eventCursor.SetForward(transform.forward);

            Ray screenRay = _raycaster.eventCamera.ScreenPointToRay(_pointerData.position);
            Plane plane = new Plane(-transform.forward, _eventCursor.transform.position);
            float distance;

            if (plane.Raycast(screenRay, out distance))
            {
                _eventCursor.transform.position = screenRay.GetPoint(distance);
            }

            if (pressState == PressState.None)
            {
                float downDepth = _pointerData.PointCloudInfo.FrontDistance - (_downTriggerOffset * transform.lossyScale.z);
                _eventCursor.NormalizedDistance = downDepth / (_reticleDepth * transform.lossyScale.z);
            }
            else
            {
                float upDepth = _pointerData.PointCloudInfo.FrontDistance - (_downTriggerOffset + _upTriggerOffset) * transform.lossyScale.z;
                _eventCursor.NormalizedDistance = upDepth / (_reticleDepth * transform.lossyScale.z);
            }
        }

        private void ProcessRaycasts()
        {
            _resultsCache.Clear();
            _raycaster.Raycast(_pointerData, _resultsCache);

            if (_resultsCache.Count > 0)
            {
                _resultsCache.Sort(RaycastComparer);
                _pointerData.pointerCurrentRaycast = FindFirstRaycast(_resultsCache);
            }
            else
            {
                _pointerData.pointerCurrentRaycast = new RaycastResult();
            }

            _raycaster.ProcessHandEvent(_pointerData, pressState == PressState.Pressed, pressState == PressState.Released);

            if (pressState == PressState.Pressed ||
                pressState == PressState.Held ||
                pressState == PressState.None)
            {
                _raycaster.ProcessMove(_pointerData);
                _raycaster.ProcessDrag(_pointerData);
            }
        }

        private int RaycastComparer(RaycastResult lhs, RaycastResult rhs)
        {
            if (lhs.module != rhs.module)
            {
                if (lhs.module.eventCamera != null && rhs.module.eventCamera != null && lhs.module.eventCamera.depth != rhs.module.eventCamera.depth)
                {
                    // need to reverse the standard compareTo
                    if (lhs.module.eventCamera.depth < rhs.module.eventCamera.depth)
                        return 1;
                    if (lhs.module.eventCamera.depth == rhs.module.eventCamera.depth)
                        return 0;

                    return -1;
                }

                if (lhs.module.sortOrderPriority != rhs.module.sortOrderPriority)
                    return rhs.module.sortOrderPriority.CompareTo(lhs.module.sortOrderPriority);

                if (lhs.module.renderOrderPriority != rhs.module.renderOrderPriority)
                    return rhs.module.renderOrderPriority.CompareTo(lhs.module.renderOrderPriority);
            }

            if (lhs.sortingLayer != rhs.sortingLayer)
            {
                // Uses the layer value to properly compare the relative order of the layers.
                var rid = SortingLayer.GetLayerValueFromID(rhs.sortingLayer);
                var lid = SortingLayer.GetLayerValueFromID(lhs.sortingLayer);
                return rid.CompareTo(lid);
            }


            if (lhs.sortingOrder != rhs.sortingOrder)
                return rhs.sortingOrder.CompareTo(lhs.sortingOrder);

            if (lhs.depth != rhs.depth)
                return rhs.depth.CompareTo(lhs.depth);

            if (lhs.distance != rhs.distance)
                return lhs.distance.CompareTo(rhs.distance);

            return lhs.index.CompareTo(rhs.index);
        }

        private RaycastResult FindFirstRaycast(List<RaycastResult> candidates)
        {
            for (var i = 0; i < candidates.Count; ++i)
            {
                if (candidates[i].gameObject == null)
                    continue;

                return candidates[i];
            }
            return new RaycastResult();
        }

        private bool checkTouchClickTime(float touchTime)
        {
            float deltaTime = Time.time - touchTime;

            return deltaTime >= _minAcceptableClickTime && deltaTime <= _maxAcceptableClickTime;
        }

        private bool checkTouchClickDistance(Vector3 startPosition, Vector3 position)
        {
            Vector3 localPosition = transform.InverseTransformPoint(position);

            return (_checkXYZ && Vector3.Distance(startPosition, localPosition) <= _acceptableXYZClickDistance) ||
                   (_checkXY && Vector2.Distance(startPosition, localPosition) < _acceptableXYClickDistance);
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying)
            {
                Collider collider = GetComponent<Collider>();

                //Calculate the front limit of the pressed area
                Vector3 pressPoint = collider.bounds.center + (transform.forward * (collider.bounds.extents.z - (_downTriggerOffset * transform.lossyScale.z)));
                //Calculate the rear limit of the released area
                Vector3 releasePoint = collider.bounds.center + (transform.forward * (collider.bounds.extents.z - (_downTriggerOffset + _upTriggerOffset) * transform.lossyScale.z));

                Color color = Gizmos.color;
                //Draw the front limit of the pressed area
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireCube(pressPoint, new Vector3(collider.bounds.size.x, collider.bounds.size.y, 0f));
                //Draw the rear limit of the released area
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(releasePoint, new Vector3(collider.bounds.size.x, collider.bounds.size.y, 0f));
                Gizmos.color = color;
            }
        }
    }
}
