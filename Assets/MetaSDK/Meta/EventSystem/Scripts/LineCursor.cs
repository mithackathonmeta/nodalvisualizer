﻿using UnityEngine;
using System.Collections;
using Meta.Extensions;
using System;

namespace Meta.UI
{
    public class LineCursor : EventCursor
    {
        [SerializeField]
        private SpriteRenderer[] _renderers = null;

        [SerializeField]
        private Transform _lineStartTransform = null;

        private const float FadeInSeconds = 0.5f;
        private const float FadeOutSeconds = 0.5f;
        private float _normalizedDistance = 1f;
        private Color[] _normalColors;

        private Coroutine _fadeInCoroutine = null;
        private Coroutine _fadeOutCoroutine = null;

        public override float NormalizedDistance
        {
            get { return _normalizedDistance; }
            set { _normalizedDistance = value; }
        }

        public Transform LineStartTransform
        {
            get { return _lineStartTransform; }
        }

        private void Awake()
        {
            if (_renderers != null && _renderers.Length > 0)
            {
                _normalColors = new Color[_renderers.Length];

                for (int i = 0; i < _renderers.Length; i++)
                {
                    _normalColors[i] = _renderers[i].color;
                }
            }
        }

        public override void Enable()
        {
            if (_fadeOutCoroutine != null)
            {
                StopCoroutine(_fadeOutCoroutine);
                _fadeOutCoroutine = null;
            }

            _fadeInCoroutine = StartCoroutine(FadeIn(FadeInSeconds));
        }

        public override void Disable()
        {
            if (_fadeInCoroutine != null)
            {
                StopCoroutine(_fadeInCoroutine);
                _fadeInCoroutine = null;
            }

            _fadeOutCoroutine = StartCoroutine(FadeOut(FadeOutSeconds));
        }

        public override void SetForward(Vector3 forward)
        {
            //Do nothing
        }

        public override void SetHandPosition(Vector3 position)
        {
            if (_lineStartTransform != null)
            {
                _lineStartTransform.position = position;
            }
        }

        public override void SetColor(Color c)
        {
            if (_renderers != null)
            {
                for (int i = 0; i < _renderers.Length; i++)
                {
                    setColor(c, i, true);
                }
            }
        }

        public override void SetHover(bool hover)
        { }

        private void setColor(Color c, int index, bool leaveAlpha = false)
        {
            if (_renderers != null && index >= 0 && index < _renderers.Length && _renderers[index] != null)
            {
                _renderers[index].color = leaveAlpha ? c.SetAlpha(_renderers[index].color.a) : c;
            }
        }

        public override void ResetColor()
        {
            if (_renderers != null)
            {
                for (int i = 0; i < _renderers.Length; i++)
                {
                    setColor(_normalColors[i], i, true);
                }
            }
        }

        private IEnumerator FadeIn(float duration)
        {
            float elapsed = GetAlpha() * duration;

            while (elapsed <= duration)
            {
                yield return null;

                elapsed += Time.deltaTime;

                float alpha = Mathf.SmoothStep(0, 1, Mathf.Clamp01(elapsed / duration));
                SetAlpha(alpha);
            }

            //An overabundance of caution
            SetAlpha(1);
            _fadeInCoroutine = null;
        }

        private IEnumerator FadeOut(float duration)
        {
            float elapsed = (1 - GetAlpha()) * duration;

            while (elapsed <= duration)
            {
                yield return null;

                elapsed += Time.deltaTime;

                float alpha = Mathf.SmoothStep(1, 0, Mathf.Clamp01(elapsed / duration));
                SetAlpha(alpha);
            }

            //An overabundance of caution
            SetAlpha(0);
            _fadeOutCoroutine = null;
        }

        private float GetAlpha()
        {
            float alpha = 0;

            if (_renderers != null && _renderers.Length > 0 && _renderers[0] != null)
            {
                alpha = _renderers[0].color.a;
            }

            return alpha;
        }

        private void SetAlpha(float alpha)
        {
            if (_renderers != null)
            {
                for (int i = 0; i < _renderers.Length; i++)
                {
                    Color c;

                    if (GetColor(i, out c))
                    {
                        setColor(c.SetAlpha(alpha), i);
                    }
                }
            }
        }

        private bool GetColor(int index, out Color c)
        {
            c = new Color();

            if (_renderers != null && index >= 0 && index < _renderers.Length && _renderers[index] != null)
            {
                c = _renderers[index].color;

                return true;
            }

            return false;
        }

        public override void SetEventVolumeState(PressState state)
        { }
    }
}