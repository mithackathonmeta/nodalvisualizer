﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Meta.Events
{
    public class MetaHandEventData : PointerEventData
    {
        public PointCloudInfo PointCloudInfo { get; set; }
        //public new Camera enterEventCamera { get; set; }
        //public new Camera pressEventCamera { get; set; }

        public MetaHandEventData(EventSystem eventSystem) : base(eventSystem)
        {
            eligibleForClick = false;

            pointerId = -1;
            position = Vector2.zero; // Current position of the mouse or touch event
            delta = Vector2.zero; // Delta since last update
            pressPosition = Vector2.zero; // Delta since the event started being tracked
            clickTime = 0.0f; // The last time a click event was sent out (used for double-clicks)
            clickCount = 0; // Number of clicks in a row. 2 for a double-click for example.

            scrollDelta = Vector2.zero;
            useDragThreshold = true;
            dragging = false;
            button = InputButton.Left;
        }
    }
}