﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Meta.Events
{
    static public class EventSystemUtility
    {
        static public Camera GetMetaHandEventDataPressEventCamera(PointerEventData pointerEventData)
        {
            Camera eventCamera = pointerEventData.pressEventCamera;
            if (pointerEventData is MetaHandEventData)
            {
                eventCamera = ((MetaHandEventData)pointerEventData).pressEventCamera;
            }
            return eventCamera;
        }
    }
}