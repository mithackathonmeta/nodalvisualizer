﻿using UnityEngine;

namespace Meta.UI
{
    public abstract class EventCursor : MonoBehaviour
    {
        public abstract float NormalizedDistance { get; set; }

        public abstract void SetForward(Vector3 forward);
        public abstract void SetHandPosition(Vector3 position);
        public abstract void SetEventVolumeState(PressState state);
        public abstract void SetColor(Color color);
        public abstract void SetHover(bool hover);
        public abstract void ResetColor();

        public abstract void Enable();
        public abstract void Disable();
    }
}