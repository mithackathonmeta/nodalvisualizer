﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    [ExecuteInEditMode]
    public class ScaleSpriteRendererToTransform : MonoBehaviour
    {
        [SerializeField]
        private Transform _target = null;

        [SerializeField]
        private BoxCollider _collider = null;
        private float _length = 0;

        void Start()
        {
            if (transform.localScale.x == 0)
            {
                transform.localScale = transform.localScale + Vector3.right;
            }

            if (_collider != null)
            {
                _length = _collider.size.x * transform.lossyScale.x; //_collider.transform.lossyScale.x;
            }

            //Debug.Log("Renderer bounds: " + _collider.size.x + ", " + _collider.size.y + ", " + _collider.size.z);
            //Debug.Log("Length: " + _length);
        }

        void Update()
        {
            scaleToReachTarget();
        }

        private void scaleToReachTarget()
        {
            if (_target != null && _length > 0)
            {
                //Calculate distance to target
                float distance = Vector3.Distance(_target.transform.position, transform.position); //_target.transform.position.z - transform.position.z;

                //if (distance > 0)
                //{
                //    Debug.Log("Distance: " + distance);
                //}

                //Calculate ratio of distance to length
                float ratio = distance / _length;

                //Set scale
                Vector3 localScale = transform.localScale;
                localScale.x = ratio;
                transform.localScale = localScale;
            }
        }
    }
}