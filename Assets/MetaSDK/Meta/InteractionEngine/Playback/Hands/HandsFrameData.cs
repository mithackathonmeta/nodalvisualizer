﻿namespace Meta.Internal.Playback
{
    /// <summary>
    /// Wrapper for holding hands data for a single frame.
    /// </summary>
    public class HandsFrameData {

        public HandData left { get; set; }
        public HandData right { get; set; }
        public int frameId { get; set; }

	    public HandsFrameData(HandData left, HandData right)
        {
            this.left = left;
            this.right = right;
        }

        public HandsFrameData()
        {
            left = new HandData();
            right = new HandData();
        }
    }
}

