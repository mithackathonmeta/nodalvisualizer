﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using Meta.Extensions;

namespace Meta.Internal.Playback
{
    /// <summary>
    /// Parses files containing hands data in an appended log format.
    /// </summary>
    internal class HandsParser : IFileParser<HandsFrameData>
    {
        private const int FieldsPerHand = 15;

        public HandsFrameData ParseFile(FileInfo f)
        {
            return ParseFile(f, 0);
        }

        public HandsFrameData ParseFile(FileInfo f, int id)
        {
            string[] lines = File.ReadAllLines(f.FullName);
            return ParseLine(lines[0]);
        }

        public List<HandsFrameData> ParseFileIntoList(FileInfo f, int id, ref List<HandsFrameData> list)
        {
            ParseLinesToList(f, id, ref list);
            return list;
        }

        private void ParseLinesToList(FileInfo f, int id, ref List<HandsFrameData> list)
        {
            try
            {
                string[] file = File.ReadAllLines(f.FullName);
                if (file.Length == 0)
                {
                    throw new InvalidDataException("The Hands file was empty");
                }
                foreach (string line in file)
                {
                    HandsFrameData frame = ParseLine(line);
                    if (list != null)
                    {
                        list.Add(frame);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        private HandsFrameData ParseLine(string line)
        {
            // Parse hand data
            HandsFrameData currHands = new HandsFrameData();
            string[] values = line.Split(',');
            if (values.Length != FieldsPerHand * 2)
            {
                throw new FormatException("Values count did not match expected format.");
            }

            // Take the ID for the left hand as the container's ID (should both be the same).
            currHands.frameId = currHands.left.frameID = int.Parse(values[2]);
            currHands.right.frameID = int.Parse(values[2 + FieldsPerHand]);

            // Hand is valid
            currHands.left.detected = bool.Parse(values[3]);
            currHands.right.detected = bool.Parse(values[3 + FieldsPerHand]);

            // Hand top vector
            currHands.left.top = Vector3Extensions.Parse(values[4], values[5], values[6]);
            currHands.right.top = Vector3Extensions.Parse(
                values[4 + FieldsPerHand],
                values[5 + FieldsPerHand],
                values[6 + FieldsPerHand]
            );

            // Hand center vector
            currHands.left.center = Vector3Extensions.Parse(values[7], values[8], values[9]);
            currHands.right.center = Vector3Extensions.Parse(values[7 + FieldsPerHand], values[8 + FieldsPerHand], values[9 + FieldsPerHand]);

            // Grab detect, palm radius
            currHands.left.grabDetected = int.Parse(values[10]);
            currHands.left.palmRadius = int.Parse(values[11]);
            currHands.left.grabValue = float.Parse(values[12]);
            currHands.left.grabResidual = float.Parse(values[13]);
            currHands.left.type = (HandType) int.Parse(values[14]);

            currHands.right.grabDetected = int.Parse(values[10 + FieldsPerHand]);
            currHands.right.palmRadius = int.Parse(values[11 + FieldsPerHand]);
            currHands.right.grabValue = float.Parse(values[12 + FieldsPerHand]);
            currHands.right.grabResidual = float.Parse(values[13 + FieldsPerHand]);
            currHands.right.type = (HandType)int.Parse(values[14 + FieldsPerHand]);
            return currHands;
        }
    }
}

