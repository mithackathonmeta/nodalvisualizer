﻿namespace Meta.Internal.Playback
{
    /// <summary>
    /// Hands source which uses recorded hands data.
    /// </summary>
    internal class HandsPlaybackSource : GenericFilePlayback<HandsFrameData>, IHandDataSource
    {
        private readonly object _handDataLock;

        public HandsPlaybackSource() : base()
        {
            _handDataLock = new object();
            _parser = new HandsParser();
        }

        public HandsPlaybackSource(string filepath) : base(filepath)
        {
            _handDataLock = new object();
            _parser = new HandsParser();
        }

        public bool GetHandDataFromSensor(ref HandData leftHandData, ref HandData rightHandData)
        {
            lock (_handDataLock)
            {
                if (_currFrame != null)
                {
                    _currFrame.left.CopyTo(ref leftHandData);
                    _currFrame.right.CopyTo(ref rightHandData);
                    return true;
                }
                return false;
            }
        }

        public void InitHandDataSource()
        {
            if (HasValidSource())
            {
                LoadFrameFiles();
            }
        }

        public void SetHandOptions(HandProcessorOptions handOptions)
        {
        }
    }
}

