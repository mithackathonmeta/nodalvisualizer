﻿using Meta.Internal.Playback;
using UnityEngine;

namespace Meta
{
    /// <summary>   The interaction engine factory. </summary>
    internal static class InteractionEngineFactory
    {
        /// <summary>   Creates the sources. </summary>
        /// <typeparam name="TPoint">   Type of the point. </typeparam>
        /// <param name="handDataSource">           The hand data source. </param>
        /// <param name="pointCloudSource">         The point cloud source. </param>
        /// <param name="playbackSource">           The playback source. </param>
        /// <param name="handConsumerType">         Type of the hand consumer. </param>
        /// <param name="interactionDataSource">    The interaction data source. </param>
        /// <param name="playbackFolder">           Pathname of the playback folder. </param>
        internal static void CreateSources<TPoint>(out IHandDataSource handDataSource,
                                                    out IPointCloudSource<TPoint> pointCloudSource,
                                                    out IPlaybackSource<PointCloudData<TPoint>> pcdPlaybackSource,
                                                    out IPlaybackSource<HandsFrameData> handsPlaybackSource,
                                                    string handConsumerType,
                                                    string interactionDataSource,
                                                    string playbackFolder = "") where TPoint : PointXYZ, new()
        {
            switch (interactionDataSource)
            {
                case "Playback":
                    CreatePlaybackSources(out handDataSource, out pointCloudSource, out pcdPlaybackSource, out handsPlaybackSource, playbackFolder);
                    break;
                default:
                    CreateRealdataSources(out handDataSource, out pointCloudSource);
                    pcdPlaybackSource = null;
                    handsPlaybackSource = null;
                    break;
            }
        }

        /// <summary>   Creates playback sources. </summary>
        /// <typeparam name="TPoint">   Type of the point. </typeparam>
        /// <param name="handDataSource">   The hand data source. </param>
        /// <param name="pointCloudSource"> The point cloud source. </param>
        /// <param name="pcdPlaybackSource">   The playback source. </param>
        /// <param name="playbackFolder">   Pathname of the playback folder. </param>
        internal static void CreatePlaybackSources<TPoint>(out IHandDataSource handDataSource,
                                                            out IPointCloudSource<TPoint> pointCloudSource,
                                                            out IPlaybackSource<PointCloudData<TPoint>> pcdPlaybackSource,
                                                            out IPlaybackSource<HandsFrameData> handsPlaybackSource,
                                                            string playbackFolder) where TPoint : PointXYZ, new()
        {
            if (System.IO.Directory.Exists(playbackFolder))
            {
                try
                {
                    handDataSource = new HandsPlaybackSource(playbackFolder + "hands");
                }
                catch (System.IO.FileNotFoundException e)
                {
                    UnityEngine.Debug.Log("The hands file was not found");
                    handDataSource = new HandsPlaybackSource();
                }
                handsPlaybackSource = handDataSource as IPlaybackSource<HandsFrameData>;
                ThreadedPlaybackPointCloudSource src;
                try
                {
                    src = new ThreadedPlaybackPointCloudSource(playbackFolder);
                }
                catch (System.IO.FileNotFoundException e)
                {
                    src = new ThreadedPlaybackPointCloudSource();
                }
                pointCloudSource = src as IPointCloudSource<TPoint>;
                pcdPlaybackSource = src as IPlaybackSource<PointCloudData<TPoint>>;
            }
            else
            {
                Debug.Log("Playback folder did not exist. Creating blank sources.");
                handDataSource = new HandsPlaybackSource();
                handsPlaybackSource = handDataSource as IPlaybackSource<HandsFrameData>;
                ThreadedPlaybackPointCloudSource pcdSrc = new ThreadedPlaybackPointCloudSource();
                pointCloudSource = pcdSrc as IPointCloudSource<TPoint>;
                pcdPlaybackSource = pcdSrc as IPlaybackSource<PointCloudData<TPoint>>;
            }
        }

        /// <summary>   Creates realdata sources. </summary>
        /// <typeparam name="TPoint">   Type of the point. </typeparam>
        /// <param name="handDataSource">   The hand data source. </param>
        /// <param name="pointCloudSource"> The point cloud source. </param>
        public static void CreateRealdataSources<TPoint>(out IHandDataSource handDataSource, out IPointCloudSource<TPoint> pointCloudSource) where TPoint : PointXYZ, new()
        {
            handDataSource = new HandProcessorInterop();
            pointCloudSource = new PointCloudInterop<PointXYZConfidence>() as IPointCloudSource<TPoint>;
        }

        /// <summary>   Constructs the interaction Engine. </summary>
        /// <param name="interactionEngine">        The interaction engine. </param>
        /// <param name="handConsumerType">         Type of the hand consumer. </param>
        /// <param name="interactionDataSource">    The interaction data source. </param>
        /// <param name="playbackFolder">           Pathname of the playback folder. </param>
        public static void Construct(out InteractionEngine interactionEngine, string handConsumerType, string interactionDataSource, Transform depthOcclusionTransform, string playbackFolder = "")
        {
            IHandDataSource handDataSource;
            IPointCloudSource<PointXYZConfidence> pointCloudSource; //todo: maybe support other stuff?
            IPlaybackSource<PointCloudData<PointXYZConfidence>> pcdPlaybackSource;
            IPlaybackSource<HandsFrameData> handsPlaybackSource;
            CreateSources(out handDataSource, out pointCloudSource, out pcdPlaybackSource, out handsPlaybackSource, handConsumerType, interactionDataSource, playbackFolder);
            interactionEngine = new InteractionEngine(handDataSource, pointCloudSource, handConsumerType, depthOcclusionTransform, pcdPlaybackSource, handsPlaybackSource);
        }
    }
}
