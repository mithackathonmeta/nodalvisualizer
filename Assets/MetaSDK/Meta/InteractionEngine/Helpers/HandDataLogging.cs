﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Meta
{
    /// <summary>   A hand data logging. </summary>
    public class HandDataLogging
    {
        /// <summary>   The output file. </summary>
        readonly string outputFile;

    
        /// <summary>   Initializes a new instance of the Meta.HandDataLogging class. </summary>
        /// <param name="recordDataFolder"> Pathname of the record data folder. </param>
    
        public HandDataLogging(string recordDataFolder)
        {
            outputFile = recordDataFolder + "\\handOutputData.txt";
            if (File.Exists(outputFile))
            {
                outputFile = recordDataFolder + "\\handOutputData_" + MetaUtils.GetCurrentSystemTime("{0:HHmmssffff}") + ".txt";
            }
        }

    
        /// <summary>   Updates this object with new hand datums. </summary>
        /// <param name="leftHand">     The left hand. </param>
        /// <param name="rightHand">    The right hand. </param>
    
        public void Update(HandData leftHand, HandData rightHand)
        {
            string handDataString = leftHand.ToOutputString() + ", " + rightHand.ToOutputString();

            string systemTime = MetaUtils.GetCurrentSystemTime();

            handDataString = systemTime + ", " + handDataString + "\n";

            File.AppendAllText(outputFile, handDataString);
        }
    }
}
