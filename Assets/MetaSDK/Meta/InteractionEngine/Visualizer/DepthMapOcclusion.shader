﻿Shader "Meta/DepthOcclusionShader" {
	Properties {
		_Color ("Color", Color) = (0,0,0,1)
		_ParticleTex ("Particle Texture", 2D) = "black" {}
		_DepthTex ("Depth Texture", 2D) = "white" {}
		_DepthScale ("Depth Scale", float) = 0.001
		_DepthWidth("_DepthWidth", float) = 320
		_DepthHeight("_DepthHeight", float) = 240
		_DepthFocalLengthX("_DepthFocalLengthX", float) = 368.096588
		_DepthFocalLengthY("_DepthFocalLengthy", float) = 368.096588
		_DepthPrincipalPointX("_DepthPrincipalPointX", float) = 261.696594
		_DepthPrincipalPointY("_DepthPrincipalPointy", float) = 202.522202
		_XGAP("X Gap", float) = 0.0
		_YGAP("Y Gap", float) = 0.0
		_QUAD_WIDTH("Quad Width", float) = 1
		_QUAD_HEIGHT("Quad Height", float) = 1
	}
	SubShader {

		Tags { "RenderType" = "Opaque"} // "Queue" = "Transparent-1" }

		LOD 100

		Pass {

			// Begin CG shader
			CGPROGRAM

			// Unity variables
			uniform float4 _Color;
			uniform sampler2D _ParticleTex;
			uniform sampler2D _DepthTex;
			uniform float _DepthScale;
			uniform float _DepthWidth;
			uniform float _DepthHeight;
			uniform float _DepthFocalLengthX;
			uniform float _DepthFocalLengthY;
			uniform float _DepthPrincipalPointX;
			uniform float _DepthPrincipalPointY;
			uniform float _XGAP;
			uniform float _YGAP;
			uniform float _QUAD_WIDTH;
			uniform float _QUAD_HEIGHT;

			// user variables
			uniform float4 _ParticleTex_ST;

			// settings
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			// Vertex input data
			struct VertexInput {
				float4 vertexPosition : POSITION;
				float4 particleTexCoord : TEXCOORD0;
				float4 depthTexCoord : TEXCOORD1;
			};

			// Vertex output data
			struct VertexOutput {
				float4 vertexPosition : SV_POSITION;
				float4 particleTexCoord : TEXCOORD0;
			};

			// vertex shader 
			VertexOutput vert(VertexInput vi) {
				// output vertex declaration
				VertexOutput vo;

				// get the depth which is returned as a color
				float4 depthColor = tex2Dlod(_DepthTex, vi.depthTexCoord);

                float indexX = depthColor.r;
				float indexY = depthColor.g;
                float x = depthColor.r + ((_QUAD_WIDTH - _XGAP) * indexX + _QUAD_WIDTH * vi.particleTexCoord.x) * _DepthScale;
                float y = - depthColor.g + ((_QUAD_HEIGHT - _YGAP) * indexY + _QUAD_HEIGHT * vi.particleTexCoord.y) * _DepthScale;
				//_QUAD_WIDTH * (indexX + vi.particleTexCoord.x) + (indexX * _XGAP);
				//float x = depthColor.r + (_QUAD_WIDTH * (indexX + vi.particleTexCoord.x) + (indexX * _XGAP)) * _DepthScale;
				//float y = depthColor.g + (_QUAD_HEIGHT * (indexY + vi.particleTexCoord.y) + (indexY * _YGAP)) * _DepthScale;
				float z = depthColor.b;

				float4 finalPos = float4(x, y, z, 1);

//				vo.vertexPosition = finalPos;

				// convert to unity space
				vo.vertexPosition = mul(UNITY_MATRIX_MVP, finalPos);

				// apply the texture coordinates
				vo.particleTexCoord = float4(TRANSFORM_TEX(vi.particleTexCoord, _ParticleTex), 0, 0);

				return vo;
			}

			// fragment shader
			float4 frag(VertexOutput vo) : COLOR {
				
				float4 finalColor = tex2D(_ParticleTex, vo.particleTexCoord) * _Color;

				return finalColor;
			}

			// End CG shader
			ENDCG

		}
	} 
}
