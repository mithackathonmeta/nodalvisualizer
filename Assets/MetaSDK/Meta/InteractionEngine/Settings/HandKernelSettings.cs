﻿using UnityEngine;

namespace Meta
{
    //todo: make this inacceesbible to external devs before release
    /// <summary>   A hand kernel settings. </summary>
    ///
    /// <seealso cref="T:Meta.MetaBehaviour"/>
    public class HandKernelSettings : MetaBehaviour
    {
        public enum HandKernelType
        {
            NONE,
            META1,
            META1LEGACY,
            META1LEGACY_NOHANDPROCESSOR,
            META1LEGACY_NODEPTHPOINTCLOUD,
            META1LEGACY_NODEPTHVISUALIZER
        }

        [SerializeField]
        public HandKernelType handKernelType = HandKernelType.META1LEGACY;

        /// <summary>   The minimum confidence. </summary>
        [Range(1, 255)]
        public int minimumConfidence = 20;

        /// <summary>   The maximum noise. </summary>
        [Range(0.001f, 1.0f)]
        public float maximumNoise = 0.035f;

        /// <summary>   The minimum depth to perform hand physics interactions. </summary>
        [Range(1, 500)]
        public int minimumDepth = 50;

        /// <summary>   The maximum depth to perform hand physics interactions. </summary>
        [Range(500, 1200)]
        public int maximumDepth = 1200;

        /// <summary>   Size of the median filter. </summary>
        [Range(3, 5)]
        public int medianFilterSize = 3;

        /// <summary>   Size of the morphological filter. </summary>
        [Range(0, 3)]
        public int morphologicalFilterSize = 3;

        /// <summary>   The morpholical iteration. </summary>
        [Range(1, 3)]
        public int morpholicalIteration = 1;

        /// <summary>   true to debug display. </summary>
        public bool debugDisplay = true;

        /// <summary>   The subsample factor. </summary>
        [Range(0.1f, 1)]
        public float subsampleFactor = 1;

        /// <summary>   The grab threshold. </summary>
        [Range(-20, 20)]
        public int grabThresh = -10;

        /// <summary>   true to enable, false to disable the kalman. </summary>
        public bool enableKalman = true;

        /// <summary>   true to use default values. </summary>
        public bool UseDefaultValues = true;

        private InteractionEngine _engine;

        void Awake()
        {
            if (UseDefaultValues)
            {
                minimumConfidence = 10;
                maximumNoise = 0.07f;
                minimumDepth = 50;
                maximumDepth = 1000;
                medianFilterSize = 3;
                morphologicalFilterSize = 3;
                morpholicalIteration = 1;
                debugDisplay = true;
                subsampleFactor = 0.5f;
                grabThresh = -10;
                enableKalman = true;
            }
        }

        // Use this for initialization
        void Start()
        {
            _engine = metaContext.Get<InteractionEngine>();
            UpdateSensorValues();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateSensorValues();
        }

        void UpdateSensorValues()
        {
            _engine.cloudGeneratorOptions.subsampleFactor = subsampleFactor;
            _engine.cloudGeneratorOptions.debugDisplay = debugDisplay;

            _engine.depthDataCleanerOptions.maximumNoise = maximumNoise;
            _engine.depthDataCleanerOptions.minimumDepth = minimumDepth;
            _engine.depthDataCleanerOptions.maximumDepth = maximumDepth;
            _engine.depthDataCleanerOptions.minimumConfidence = minimumConfidence;
            _engine.depthDataCleanerOptions.medianFilterSize = medianFilterSize;
            _engine.depthDataCleanerOptions.morphologicalFilterSize = morphologicalFilterSize;
            _engine.depthDataCleanerOptions.morpholicalIteration = morpholicalIteration;
            _engine.depthDataCleanerOptions.debugDisplay = debugDisplay;
            _engine.depthDataCleanerOptions.cameraRotated180 = debugDisplay;

            _engine.handProcessorOptions.grabThresh = grabThresh;
            _engine.handProcessorOptions.debugDisplay = debugDisplay;
            _engine.handProcessorOptions.enableKalman = enableKalman;
        }

    }

}
