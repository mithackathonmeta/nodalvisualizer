﻿using System.Runtime.InteropServices;

namespace Meta
{
    /// <summary>
    ///     Options for cloud generation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 0, CharSet = CharSet.Ansi)]
    public struct CloudGeneratorOptions
    {
        /// <summary>
        ///     The subsampling factor. The point is subsmpled by this factor.
        /// </summary>
        public float subsampleFactor;

        /// <summary>   turn on for debug display from kernel. </summary>
        public bool debugDisplay;
    }
}