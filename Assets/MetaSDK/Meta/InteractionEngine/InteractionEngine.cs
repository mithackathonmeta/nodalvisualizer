﻿using Meta.Internal.Playback;

using UnityEngine;

namespace Meta
{
    /// <summary>   The interaction engine Module. Encapsulates HandData source and PointCloud data source </summary>
    /// Should also eventualyl handle more functionalities regarding interaactions
    /// <seealso cref="T:Meta.IEventReceiver" />
    public class InteractionEngine : IEventReceiver
    {
        /// <summary>   Type of the hand consumer. </summary>
        private readonly string _handConsumerType;
        /// <summary>   The hand data source. </summary>
        private readonly IHandDataSource _handDataSource;
        /// <summary>   The point cloud source. </summary>
        private readonly IPointCloudSource<PointXYZConfidence> _pointCloudSource;
        /// <summary>   Information describing the cloud. </summary>
        private PointCloudData<PointXYZConfidence> _cloudData;
        /// <summary>   Information describing the cloud meta. </summary>
        private PointCloudMetaData _cloudMetaData;
        /// <summary>   Left hand data, with a grab source, which has been normalized using the occlusion transform. </summary>
        private HandData _normalizedLeft = new HandData(HandType.LEFT);
        /// <summary>   Right hand data, with a grab source, which has been normalized using the occlusion transform. </summary>
        private HandData _normalizedRight = new HandData(HandType.RIGHT);
        /// <summary>   True to point cloud data valid. </summary>
        private bool _pointCloudDataValid;
        /// <summary>   True to point cloud meta data valid. </summary>
        private bool _pointCloudMetaDataValid;
        /// <summary>   Raw data describing the left hand from the hand source. </summary>
        internal HandData rawLeftHandData = new HandData(HandType.LEFT);
        /// <summary>   Raw data describing the right hand from the hand source. </summary>
        internal HandData rawRightHandData = new HandData(HandType.RIGHT);
        /// <summary>   Options for controlling the cloud generator. </summary>
        internal CloudGeneratorOptions cloudGeneratorOptions;
        /// <summary>   Options for controlling the depth data cleaner. </summary>
        internal DepthDataCleanerOptions depthDataCleanerOptions;
        /// <summary>   Options for controlling the hand processor. </summary>
        internal HandProcessorOptions handProcessorOptions;

        /// <summary>   Gets the left hand. </summary>
        /// <value> The left hand. </value>
        public HandData leftHand
        {
            get { return rawLeftHandData; }
        }
        /// <summary>   Gets the right hand. </summary>
        /// <value> The right hand. </value>
        public HandData rightHand
        {
            get { return rawRightHandData; }
        }

        /// <summary>   Gets the playback source. </summary>
        /// <value> The playback source. </value>
        internal IPlaybackSource<PointCloudData<PointXYZConfidence>> pcdPlaybackSource { get; private set; }

        internal IPlaybackSource<HandsFrameData> handsPlaybackSource { get; private set; }

        /// <summary>   Initializes a new instance of the Meta.InteractionEngine class. </summary>
        /// <param name="handDataSource">   The hand data source. </param>
        /// <param name="pointCloudSource"> The point cloud source. </param>
        /// <param name="handConsumerType"> Type of the hand consumer. </param>
        /// <param name="depthOcclusionTransform"> The transform of the DepthOcclusion object. </param>
        /// <param name="playbackSource">   The playback source. </param>
        internal InteractionEngine(IHandDataSource handDataSource,
                                 IPointCloudSource<PointXYZConfidence> pointCloudSource,
                                 string handConsumerType,
                                 Transform depthOcclusionTransform,
                                 IPlaybackSource<PointCloudData<PointXYZConfidence>> pcdPlaybackSource = null, IPlaybackSource<HandsFrameData> handsPlaybackSource = null)
        {
            _handConsumerType = handConsumerType;
            _handDataSource = handDataSource;
            _pointCloudSource = pointCloudSource;
            _handConsumerType = handConsumerType;
            _handDataSource = handDataSource;
            _pointCloudSource = pointCloudSource;
            _pointCloudDataValid = false;
            _pointCloudMetaDataValid = false;
            this.pcdPlaybackSource = pcdPlaybackSource;
            this.handsPlaybackSource = handsPlaybackSource;
            rawLeftHandData.SetTransformForHands(depthOcclusionTransform);
            rawRightHandData.SetTransformForHands(depthOcclusionTransform);
        }

        /// <summary>   Initialises this object. </summary>
        /// <param name="eventHandlers">    The event handlers. </param>
        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.awakeEvent += Awake;
            eventHandlers.startEvent += Start;
            eventHandlers.updateEvent += Update;
            eventHandlers.lateUpdateEvent += LateUpdate;
            eventHandlers.onDestroyEvent += OnDestroy;
        }

        /// <summary>   Gets cloud data. </summary>
        /// <param name="cloudData">    Information describing the cloud. </param>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        public bool GetCloudData(ref PointCloudData<PointXYZConfidence> cloudData)
        {
            if (!_pointCloudDataValid)
            {
                return false;
            }

            return _pointCloudSource.GetPointCloudData(ref cloudData);
        }

        /// <summary>
        /// Gets a deep copy of the point cloud data. Used for persisting point cloud data after the frame it is played.
        /// </summary>
        /// <param name="pcd">The point cloud object to store the data in.</param>
        /// <param name="metadata">The metadata object to store the data in.</param>

        internal void GetDeepCopyOfPointData(ref PointCloudData<PointXYZConfidence> pcd, ref PointCloudMetaData metadata)
        {
            _pointCloudSource.SetPointCloudDataFromInteropData(pcd, metadata);
        }


        /// <summary>   Gets cloud meta data. </summary>
        /// <param name="cloudMetaData">    Information describing the cloud meta. </param>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        public bool GetCloudMetaData(ref PointCloudMetaData cloudMetaData)
        {
            if (!_pointCloudMetaDataValid)
            {
                return false;
            }
            return _pointCloudSource.GetPointCloudMetaData(ref cloudMetaData);
        }

        /// <summary>   Starts this object. </summary>
        private void Start()
        {
            if (_pointCloudSource != null)
            {
                _pointCloudSource.InitPointCloudSource();
            }
            if (_handDataSource != null)
            {
                _handDataSource.InitHandDataSource();
            }
        }

        /// <summary>   Awakes this object. Build Hand Consumer </summary>
        private void Awake()
        {
            if (!IsUsingHandsPlayback())
            {
                //todo : generalize this
                HandKernelInterop.BuildHandConsumer(_handConsumerType);
            }
        }

        /// <summary>   Updates this object. </summary>
        private void Update()
        {
            //if point meta data is invlaid then set the meta data
            if (!IsPointCloudMetaDataValid())
            {
                //if setting fails that implies that the meta data is not ready. lets keep trying
                if (SetPointCloudMetaData())
                {
                    //meta data is now valid
                    _pointCloudMetaDataValid = true;
                    return;
                }
            }

            /// is point cloud data is valid, then set point cloud data
            else if (!IsPointCloudDataValid())
            {
                //if set fails that implies that the data is not ready. lets keep trying
                if (SetPointCloudData())
                {
                    // point cloud data isnow valid
                    _pointCloudDataValid = true;
                }
            }

            // if the hand data is valid, lets update
            if (IsHandDataValid())
            {
                SetHandDataFromSource();
                //SetNormalizedHandData();
            }
        }

        /// <summary>
        ///     Normalizes and transforms raw hand data values into global coordinates.
        /// </summary>
        private void SetNormalizedHandData()
        {
            rawLeftHandData.CopyToAdjusted(ref _normalizedLeft);
            rawRightHandData.CopyToAdjusted(ref _normalizedRight);
        }

        /// <summary>   Query if this object is hand data valid. </summary>
        /// <returns>   true if hand data valid, false if not. </returns>
        private bool IsHandDataValid()
        {
            if ((rawLeftHandData == null) || (rawRightHandData == null))
            {
                return false;
            }
            return true;
        }

        /// <summary>   Whether a playback source is currently being used for interaction data. </summary>
        private bool IsUsingHandsPlayback()
        {
            return handsPlaybackSource != null &&  pcdPlaybackSource != null;
        }

        /// <summary>   Query if this object is point cloud data valid. </summary>
        /// <returns>   true if point cloud data valid, false if not. </returns>
        private bool IsPointCloudDataValid()
        {
            if (_pointCloudDataValid)
            {
                return true;
            }
            if (_cloudData == null)
            {
                return false;
            }
            return _cloudData.points.Length != 0;
        }

        /// <summary>   Query if this object is point cloud meta data valid. </summary>
        /// <returns>   true if point cloud meta data valid, false if not. </returns>
        private bool IsPointCloudMetaDataValid()
        {
            if (_pointCloudMetaDataValid)
            {
                return true;
            }
            if (_cloudMetaData == null)
            {
                return false;
            }
            return _cloudMetaData.fieldCount.Length != 0 && _cloudMetaData.IsValid();
        }

        /// <summary>   Sets point cloud data. </summary>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        private bool SetPointCloudData()
        {
            _cloudData = new PointCloudData<PointXYZConfidence>(_cloudMetaData.maxSize, _cloudMetaData);
            return true;
        }

        /// <summary>   Sets point cloud meta data. </summary>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        private bool SetPointCloudMetaData()
        {
            return _pointCloudSource.GetPointCloudMetaData(ref _cloudMetaData);
        }

        /// <summary>   Updates the raw hand data values from the engine source. </summary>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        private bool SetHandDataFromSource()
        {
            if (_handDataSource.GetHandDataFromSensor(ref rawLeftHandData, ref rawRightHandData))
            {
                return true;
            }
            return false;
        }

        /// <summary>   Updates the point cloud data. </summary>
        /// <returns>   true if it succeeds, false if it fails. </returns>
        private bool UpdatePointCloudData()
        {
            if (_pointCloudSource.GetPointCloudData(ref _cloudData))
            {
                return true;
            }
            return false;
        }

        /// <summary>   Late update. </summary>
        private void LateUpdate()
        {
            if (!IsUsingHandsPlayback())
            {
                HandKernelInterop.SetDepthDataCleanerOptions(ref depthDataCleanerOptions);
            }

            if (_pointCloudSource != null)
            {
                _pointCloudSource.SetPointCloudGeneratorOptions(cloudGeneratorOptions);
            }
            if (_handDataSource != null)
            {
                _handDataSource.SetHandOptions(handProcessorOptions);
            }
        }

        /// <summary>   Executes the destroy action. </summary>
        private void OnDestroy()
        {
            if (_pointCloudSource != null)
            {
                _pointCloudSource.DeinitPointCloudSource();
            }
            if (!IsUsingHandsPlayback())
            {
                HandKernelInterop.DestroyHandConsumer();
            }
        }
    }
}
