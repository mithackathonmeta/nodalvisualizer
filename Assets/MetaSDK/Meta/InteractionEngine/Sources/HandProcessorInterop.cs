using System.Diagnostics;

namespace Meta
{

    /// <summary>  The interop class for HandPart Data. </summary>
    /// <seealso cref="T:Meta.IHandDataSource" />

    public class HandProcessorInterop : IHandDataSource
    {
        /// <summary>   The cloud data lock. </summary>
        private readonly object _handDataLock = new object();

        /// <summary>   The handler for new hand data. </summary>
        private HandKernelInterop.NewDataHandler _newHandDataHandler;
        /// <summary>   Information describing the left hand from kernel. </summary>
        private HandInteropData _leftHandInteropData;
        /// <summary>   Information describing the right hand from kernel. </summary>
        private HandInteropData _rightHandInteropData;

        /// <summary>   Information describing the left hand. </summary>
        private HandData _leftHandData;
        /// <summary>   Information describing the right hand. </summary>
        private HandData _rightHandData;


        /// <summary>   Initialises the hand data source. </summary>
        public void InitHandDataSource()
        {
            _newHandDataHandler = this.NewHandDataHandler;
            HandKernelInterop.RegisterNewHandDataEventHandler(_newHandDataHandler);
            _leftHandData = new HandData(HandType.LEFT);
            _rightHandData = new HandData(HandType.RIGHT);
        }

    
        /// <summary>   Gets the hand data. </summary>
        /// <param name="leftHandData">     Information describing the left hand. </param>
        /// <param name="rightHandData">    Information describing the right hand. </param>
        /// <returns>   true if it succeeds, false if it fails. </returns>
    
        public bool GetHandDataFromSensor(ref HandData leftHandData, ref HandData rightHandData)
        {
            lock (_handDataLock)
            {
                _leftHandData.CopyTo(ref leftHandData);
                _rightHandData.CopyTo(ref rightHandData);
            }
            return true;
        }

    
        /// <summary>   Sets hand options. </summary>
        /// <param name="handOptions">  Options for controlling the hand. </param>
    
        public void SetHandOptions(HandProcessorOptions handOptions)
        {
            HandKernelInterop.SetHandProcessorOptions(ref handOptions);
        }

        /// <summary>   Handler, called when the new hand data is available in the kernel. </summary>
        public void NewHandDataHandler()
        {
            if (!HandKernelInterop.GetHandData(ref _rightHandInteropData, ref _leftHandInteropData))
            {
                return;
            }
            lock (_handDataLock)
            {
                HandProcessorInterop.HandDataConverter(_leftHandInteropData, ref _leftHandData);
                HandProcessorInterop.HandDataConverter(_rightHandInteropData, ref _rightHandData);
            }
        }

    
        /// <summary>   Converts HandPart data from InteropData to HandPart Data. </summary>
        /// <param name="handInteropData">  Information describing the hand interop. </param>
        /// <param name="handData">         Information describing the hand. </param>
    
        private static void HandDataConverter(HandInteropData handInteropData, ref HandData handData)
        {
            if (handData == null)
            {
                return;
            }

            if (handInteropData.center != null)
            {
                handData.center.Set(handInteropData.center[0], handInteropData.center[1], handInteropData.center[2]);
            }
            if (handInteropData.top != null)
            {
                handData.top.Set(handInteropData.top[0], handInteropData.top[1], handInteropData.top[2]);
            }
            if (handInteropData.farthestZ != null)
            {

                handData.farthestZ.Set(handInteropData.farthestZ[0], handInteropData.farthestZ[1], handInteropData.farthestZ[2]);
            }
            handData.grabDetected = handInteropData.grabDetected;
            handData.palmRadius = handInteropData.palmRadius;
            handData.grabValue = handInteropData.grabValue;
            handData.grabResidual = handInteropData.grabResidual;
            handData.frameID = handInteropData.frameID;
            if (handData.timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp"))
            {
                handData.timestamps["arrivalOfCleanSensorDataTimeStamp"] = handInteropData.arrivalOfCleanSensorDataTimeStamp;
            }
            if (handData.timestamps.ContainsKey("completionOfHandProcessingTimeStamp"))
            {
                handData.timestamps["completionOfHandProcessingTimeStamp"] = handInteropData.completionOfHandProcessingTimeStamp;
            }
            handData.detected = handInteropData.valid;
        }
    }
}
