﻿namespace Meta
{
    /// <summary>
    /// Represents predefined hand gestures.
    /// </summary>
    public enum GestureType
    {
        NONE = 0,
        GRAB = 1,
    }
}

