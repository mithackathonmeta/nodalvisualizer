using System.Runtime.InteropServices;

namespace Meta
{
    /// <summary>   A hand interop data. </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 0, CharSet = CharSet.Ansi)]
    internal struct HandInteropData
    {
        /// <summary>   The top. </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] top;

        /// <summary>   The center. </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] center;

        /// <summary>   The farthest point in Z. </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] farthestZ;

        /// <summary>   The grab detected. </summary>
        public int grabDetected;

        /// <summary>   The palm radius. </summary>
        public int palmRadius;

        /// <summary>   The grab value. </summary>
        public float grabValue;

        /// <summary>   The grab residual. </summary>
        public float grabResidual;

        /// <summary>   The type. </summary>
        public HandType type;

        /// <summary>   The arrival of clean sensor data time stamp. </summary>
        public long arrivalOfCleanSensorDataTimeStamp;

        /// <summary>   The completion of hand processing time stamp. </summary>
        public long completionOfHandProcessingTimeStamp;

        /// <summary>   Identifier for the frame. </summary>
        public int frameID;

        /// <summary>   true to valid. </summary>
        public bool valid;
    }
}