using System.Collections.Generic;
using UnityEngine;

namespace Meta
{
    using System;

    /// <summary>
    ///     Values that represent Type of HandPart.
    /// </summary>
    public enum HandType
    {
        /// <summary>
        ///     An unknown hand type.
        /// </summary>
        UNKNOWN = -1,
        /// <summary>
        ///     A left hand.
        /// </summary>
        LEFT = 1,
        /// <summary>
        ///     A right hand.
        /// </summary>
        RIGHT = 2
    }

    /// <summary>  HandPart data type. </summary>
    public class HandData
    {
        /// <summary>   The center position of the hand. </summary>
        public Vector3 center;
        /// <summary>   The farthest z position of the hand. </summary>
        public Vector3 farthestZ;
        /// <summary>   Identifier for the frame. </summary>
        public int frameID;
        /// <summary>   Is grab detected. </summary>
        public int grabDetected;
        /// <summary>   The grab residual. </summary>
        public float grabResidual;
        /// <summary>   The grab value. </summary>
        public float grabValue;
        /// <summary>   The palm radius. </summary>
        public int palmRadius;
        /// <summary>   The timestamps. </summary>
        public Dictionary<string, long> timestamps;
        /// <summary>   The topmost position of the hand. </summary>
        public Vector3 top;

        /// <summary>   The type. </summary>
        public HandType type;

        /// <summary>   The gesture currently being performed by the hand. /// </summary>
        public GestureType gesture { get; protected set; }

        /// <summary>   Whether a hand was detected. </summary>
        public bool detected;

        /// <summary>   If the hand was detected and has a valid position. </summary>
        public bool isValid { get { return detected && !float.IsNaN(center[0]); } }

        /// <summary>   Transform matrix for normalising hand values. </summary>
        private Matrix4x4 _localToWorldMatrix;

        private Transform _shaderOcclusionTransform;

        /// <summary>   Initializes a new instance of the HandData class. </summary>
        public HandData(HandType initType = HandType.UNKNOWN, Transform occlusionTransform = null)
        {
            detected = false;
            top = Vector3.zero;
            center = Vector3.zero;
            farthestZ = Vector3.zero;
            grabDetected = 0;
            palmRadius = 0;
            grabValue = 0;
            grabResidual = int.MaxValue;
            type = initType;
            frameID = 0;
            timestamps = new Dictionary<string, long>();
            timestamps.Add("arrivalOfCleanSensorDataTimeStamp", 0);
            timestamps.Add("completionOfHandProcessingTimeStamp", 0);
            _shaderOcclusionTransform = occlusionTransform;
        }

        /// <summary>
        ///     Sets the transform for hand data for coordinate conversions.
        /// </summary>
        /// <param name="depthOcclusion">The depth occlusion transform for adjustment of coordinates. </param>
        internal void SetTransformForHands(Transform depthOcclusion)
        {
            _shaderOcclusionTransform = depthOcclusion.Find("ShaderOcclusion");
        }

        /// <summary>   Converts this object to an output string. </summary>
        /// <returns>   This object as a string. </returns>

        public string ToOutputString()
        {
            string dataString = "";
            if (timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp"))
            {
                dataString += timestamps["arrivalOfCleanSensorDataTimeStamp"] + ",";
            }
            if (timestamps.ContainsKey("completionOfHandProcessingTimeStamp"))
            {
                dataString += timestamps["completionOfHandProcessingTimeStamp"];
            }

            //todo: Fix garbage values being fed for grabDetected, palmRadius, grabValue, grabResidual etc
            return dataString + "," + frameID + "," + detected + "," + top.x + "," + top.y + "," + top.z + "," + center.x + "," + center.y + "," + center.z + "," + grabDetected + "," +
                   palmRadius + "," + grabValue + "," + grabResidual + "," + (int)type;
        }

        public void CopyTo(ref HandData handData)
        {
            handData.center = center;
            handData.top = top;
            handData.farthestZ = farthestZ;
            handData.frameID = frameID;
            handData.grabDetected = grabDetected;
            handData.grabResidual = grabResidual;
            handData.grabValue = grabValue;
            handData.palmRadius = palmRadius;
            handData.type = type;
            handData.detected = detected;
            if (handData.timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp") && timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp"))
            {
                handData.timestamps["arrivalOfCleanSensorDataTimeStamp"] = timestamps["arrivalOfCleanSensorDataTimeStamp"];
            }
            if (handData.timestamps.ContainsKey("completionOfHandProcessingTimeStamp") && timestamps.ContainsKey("completionOfHandProcessingTimeStamp"))
            {
                handData.timestamps["completionOfHandProcessingTimeStamp"] = timestamps["completionOfHandProcessingTimeStamp"];
            }
        }

        /// <summary>
        /// Copies data to the normalized and coordinate converted hand data object.
        /// </summary>
        /// <param name="hand">The hand normalized hand data source to copy to.</param>
        internal void CopyToAdjusted(ref HandData handData)
        {
            _localToWorldMatrix = _shaderOcclusionTransform.localToWorldMatrix;
            //handData.center = _localToWorldMatrix.MultiplyPoint3x4(center); //TODO: Push down PointFeature.cs coordinate transformation duplicate
            handData.center = center;
            handData.top = top;
            handData.farthestZ = farthestZ;
            handData.frameID = frameID;
            handData.grabDetected = grabDetected;
            handData.grabResidual = grabResidual;
            handData.grabValue = grabValue;
            handData.palmRadius = palmRadius;
            handData.type = type;
            handData.detected = detected;
            if (handData.timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp") && timestamps.ContainsKey("arrivalOfCleanSensorDataTimeStamp"))
            {
                handData.timestamps["arrivalOfCleanSensorDataTimeStamp"] = timestamps["arrivalOfCleanSensorDataTimeStamp"];
            }
            if (handData.timestamps.ContainsKey("completionOfHandProcessingTimeStamp") && timestamps.ContainsKey("completionOfHandProcessingTimeStamp"))
            {
                handData.timestamps["completionOfHandProcessingTimeStamp"] = timestamps["completionOfHandProcessingTimeStamp"];
            }
        }
    }
}
