﻿using UnityEngine;
using System.Collections;
using System;

namespace Meta
{

    public class MouseLocalizer : MetaBehaviour, ILocalizer
    {

        [SerializeField]
        private float _sensitivity = 0.5f;

        private float _deltaX;

        private float _deltaY;

        [SerializeField]
        private bool _invertVerticalMovement = false;

        public void ResetLocalizer(GameObject targetGO)
        {
            targetGO.transform.localRotation = Quaternion.identity;
        }

        public void UpdateLocalizer(GameObject targetGO)
        {
            Vector3 rotEuler = targetGO.transform.localRotation.eulerAngles;
            targetGO.transform.localRotation = Quaternion.Euler(rotEuler.x + _deltaY, rotEuler.y + _deltaX, rotEuler.z);
            _deltaX = _deltaY = 0f; //Once input has been used, it shouldn't be used again
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                _sensitivity *= Input.GetAxis("Mouse ScrollWheel") * .1f + 1f;
                int direction = _invertVerticalMovement ? 1 : -1;

                //Update if the cursor is locked or confined
                if (Cursor.lockState != CursorLockMode.None)
                {
                    _deltaX = Input.GetAxis("Mouse X") * _sensitivity;
                    _deltaY = Input.GetAxis("Mouse Y") * _sensitivity * direction;
                }
            }

            //Handle grab/releasing of mouse
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                if (metaContext.ContainsModule<CursorState>())
                {
                    var cursorState = metaContext.Get<CursorState>();
                    Cursor.lockState = cursorState.CursorLockMode;
                    Cursor.visible = cursorState.IsCursorVisible;
                }
                else
                {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }
            }
        }
    }
}