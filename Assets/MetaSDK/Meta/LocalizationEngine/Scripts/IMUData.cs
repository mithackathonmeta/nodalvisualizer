﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

namespace Meta
{

    public class IMUData
    {
        /// <summary>
        /// 64 bit IMU gyro and accelerometer timestamp.
        /// </summary>
        [StructLayoutAttribute(LayoutKind.Sequential)]
        internal struct Timestamp64
        {
            /// <summary>
            /// Most significant timestamp bits.
            /// </summary>
            UInt32 timestamp_msb;
            /// <summary>
            /// Least significant timestamp bits.
            /// </summary>
            UInt32 timestamp_lsb;
        };

        /// <summary>
        /// IMU device type. Not sure of its use.
        /// </summary>
        internal enum MotionDeviceType
        {
            META_ACCELEROMETER = 0,
            META_GYROSCOPE = 1,
            INVALID_DEVICE = 2
        };

        /// <summary> Motion sensor data.</summary>
        [StructLayoutAttribute(LayoutKind.Sequential)]
        internal struct MotionSensorData
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] gyroscopeValues;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] accelerometerValues;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] magnetometerValues;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public double[] fusedAngle;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public double[] orientation;

            private double timestamp;
            public UInt32 hardware_time_stamp;
            public UInt32 hardware_counter;
            public Timestamp64 gyro_hw_timestamp;
            public UInt32 gyro_hw_frame_id;
            public Timestamp64 accel_hw_timestamp;
            public UInt32 accel_hw_frame_id;
            public MotionDeviceType device_type;
        };

        /// <summary> Gets motion sensor data.</summary>
        ///
        /// <param name="motionSensorData"> [in,out] Information describing the motion sensor.</param>
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "getMotionSensorData")]
        internal static extern void GetMotionSensorData(ref MotionSensorData motionSensorData);

        /// <summary>
        /// Last updated motion sensor data.
        /// </summary>
        private MotionSensorData motionSensorData;

        /// <summary> Gets the correction orientation.</summary>
        ///
        /// <value> The correction orientation.</value>
        private Quaternion _orientation;
        /* <summary> The correction. </summary> */
        private Quaternion _correctionOrientation;
        /* <summary> The correction. </summary> */
        private Vector3 _correctionVector;
        /* <summary> The fused angle. </summary> */
        private Vector3 _imuOrientation;
        /* <summary> The set angle. </summary> */
        private Vector3 _setAngle;
        /* <summary> The accelerometer values. </summary> */
        private Vector3 _accelerometerValues;
        /* <summary> The magnetometer values. </summary> */
        private Vector3 _magnetometerValues;
        /* <summary> The gyroscope values. </summary> */
        private Vector3 _gyroscopeValues;
        /// <summary>
        /// Running average of gravity values.
        /// </summary>
        private Vector3 _smoothedGravity = new Vector3();
        /// <summary>
        /// Exponential weighting factor.  New smoothed gravity = lam * smoothed + new obs gravity / (1 + lam)
        /// </summary>
        private float _lambdaGravity = 0.5f;
        /// <summary>
        /// Is this the first valid observation of gravity?
        /// </summary>
        private bool _firstGravity = true;
        /// <summary>
        /// Return orientation.
        /// </summary>
        /*internal Quaternion Orientation
        {
            get { return _orientation; }
        }*/

        /// <summary>
        /// Return correction in orientation.
        /// </summary>
        internal Quaternion CorrectionOrientation
        {
            get { return _correctionOrientation; }
        }

        /// <summary>
        /// Return correction vector.
        /// </summary>
        internal Vector3 CorrectionVector
        {
            get { return _correctionVector; }
            set { _correctionVector = value; }
        }

        /// <summary>
        /// Gets the raw IMU orientation.
        /// </summary>
        public Vector3 orientation
        {
            get { return _imuOrientation; }
        }

        /// <summary>
        /// Return the localizer orientation.
        /// </summary>
        internal Vector3 SetAngle
        {
            get { return _setAngle; }
        }

        /// <summary>
        /// Gets or sets the accelerometer values in hardware units (short).
        /// </summary>
        /// <remarks>
        /// To convert to m/s^2, you need to multiply by g / 8192 (ie. 9.81/8192 = ~0.0012).
        /// </remarks>
        public Vector3 accelerometerValues
        {
            get { return _accelerometerValues; }
        }

        /// <summary> 
        /// Gets the magnetometer values in hardware units (short).
        /// </summary>
        /// <remarks>
        /// To convert to microtesla, you need to multiply by 0.3.
        /// </remarks>
        internal Vector3 magnetometerValues
        {
            get { return _magnetometerValues; }
        }

        /// <summary>
        /// Gets or sets the gyroscope values in hardware units (short).
        /// </summary>
        /// <remarks>
        /// To convert to degrees per second (DPS), you need to divide the values by 16.4 (~0.061x).
        /// </remarks>
        public Vector3 gyroscopeValues
        {
            get { return _gyroscopeValues; }
        }

        /// <summary>
        /// Return current smoothed gravity.
        /// </summary>
        internal Vector3 SmoothedGravity
        {
            get { return _smoothedGravity; }
        }

        /// <summary>
        /// Change the value of lambda.  will put in range of (0, 1.]
        /// </summary>
        internal float LambdaGravity
        {
            get { return _lambdaGravity; }
            set
            {
                _lambdaGravity = Math.Min(1.0f, Math.Max(value, 0.001f));
            }
        }
        /// <summary>
        /// Compute the direction of gravity.  Pitch y, roll x, yaw z
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/2986628/accelerometer-gravity-components"/>
        internal Vector3 Gravity
        {
            get
            {
                Vector3 grav = new Vector3();

                double roll, pitch;

                double ax = motionSensorData.accelerometerValues[0];
                double ay = motionSensorData.accelerometerValues[1];
                double az = motionSensorData.accelerometerValues[2];

                double yz = Math.Sqrt(ay * ay + az * az);
                double sz = (az < 0.0) ? -1.0 : 1.0;

                double azd = (Math.Abs(az) < 1.0e-06) ? sz * 1.0e-06 : az;
                roll = Math.Atan(ay / azd);
                pitch = Math.Atan(ax / (yz + 1.0e-06));

                double c1 = Math.Cos(pitch);
                double s1 = Math.Sin(pitch);

                double c0 = Math.Cos(roll);
                double s0 = Math.Sin(roll);

                // Rx(r) Ry (p) Rx(y) rotated by 90 degrees about x axis --> LHS for unity

                grav[0] = (float)(s1);
                grav[1] = -(float)(c0 * c1); // - to change to LHS
                grav[2] = (float)(c1 * s0);

                grav.Normalize();

                return grav;
            }
        }

        internal IMUData()
        {
            motionSensorData.accelerometerValues = new double[3];
            motionSensorData.gyroscopeValues = new double[3];
            motionSensorData.magnetometerValues = new double[3];
            motionSensorData.orientation = new double[3];
            motionSensorData.fusedAngle = new double[3];

            InitDataStructs();
        }

        /// <summary> Initialises the data structs.</summary>
        private void InitDataStructs()
        {
            _orientation = _correctionOrientation = Quaternion.identity;

            _correctionVector = _imuOrientation = _setAngle = _accelerometerValues =
                _magnetometerValues = _gyroscopeValues = Vector3.zero;

            for (int i = 0; i < 3; i++)
            {
                motionSensorData.accelerometerValues[i] =
                    motionSensorData.gyroscopeValues[i] =
                    motionSensorData.magnetometerValues[i] =
                    motionSensorData.orientation[i] =
                    motionSensorData.fusedAngle[i] = 0.0;
            }
            _firstGravity = true;
        }

        /// <summary>
        /// Update the internal IMU state.
        /// </summary>
        internal void Update()
        {
            GetMotionSensorData(ref motionSensorData);

            ////Change order to accommodate the change in coordinate system of unity and IMU
            _orientation.w = (float)motionSensorData.orientation[0];
            _orientation.x = (float)motionSensorData.orientation[1];
            _orientation.y = -(float)motionSensorData.orientation[3];
            _orientation.z = (float)motionSensorData.orientation[2];

            // orientation data in Euler Angles
            _imuOrientation.x = (float)(motionSensorData.fusedAngle[0] * 180 / Math.PI);
            _imuOrientation.y = -(float)(motionSensorData.fusedAngle[2] * 180 / Math.PI);
            _imuOrientation.z = (float)(motionSensorData.fusedAngle[1] * 180 / Math.PI);

            //Raw Values from the Accelerometer
            _accelerometerValues.x = (float)motionSensorData.accelerometerValues[0];
            _accelerometerValues.y = -(float)motionSensorData.accelerometerValues[2];
            _accelerometerValues.z = (float)motionSensorData.accelerometerValues[1];

            //Raw Values from the Gyroscope
            _gyroscopeValues.x = (float)motionSensorData.gyroscopeValues[0];
            _gyroscopeValues.y = -(float)motionSensorData.gyroscopeValues[2];
            _gyroscopeValues.z = (float)motionSensorData.gyroscopeValues[1];

            //Raw Values from the Magnetometer
            _magnetometerValues.x = (float)motionSensorData.magnetometerValues[0];
            _magnetometerValues.y = -(float)motionSensorData.magnetometerValues[2];
            _magnetometerValues.z = (float)motionSensorData.magnetometerValues[1];

            // Update the smoothed value of gravity. as long as IMU reading is valid.

            Quaternion imuq = Compute();
            if (isNormalizedQuaternionValid(imuq))
            {
                if (_firstGravity)
                {
                    _firstGravity = false;
                    _smoothedGravity = Gravity;
                }
                else
                {
                    _smoothedGravity = (_lambdaGravity * _smoothedGravity + Gravity) / (1.0f + _lambdaGravity);
                }
            }
        }

        /// <summary>
        /// Compute the quaternion for the IMU.  Exposed
        /// as internal so that SLAM++ localizer can also
        /// use.
        /// </summary>
        /// <returns></returns>
        internal Quaternion Compute()
        {
            _setAngle = _imuOrientation - _correctionVector;
            //_setAngle.z = -SetAngle.z;
            return Quaternion.Euler(_setAngle);
        }

        // To reset the IMU to bring the unity world back to horizontal direction (Will be deprecated once the modeling of the Glasses is fixed)
        internal void Reset()
        {
            if (isNormalizedQuaternionValid(_orientation))
                _correctionOrientation = _orientation;
            else
                _correctionOrientation = Quaternion.identity;

            _correctionVector = _imuOrientation;
            _firstGravity = true;
        }

        /// <summary>
        /// Check if magnitude of normalized quaternion is 1.
        /// </summary>
        /// <param name="Q">quaternion.</param>
        /// <returns>true if magnitude between .9 and 1.1.</returns>
        static internal bool isNormalizedQuaternionValid(Quaternion Q)
        {
            float magnitude = (float)Math.Sqrt(Q.x * Q.x + Q.y * Q.y + Q.z * Q.z + Q.w * Q.w);
            if (Mathf.Abs(magnitude - 1) < 0.1f)
                return true;
            else
                return false;
        }

        /// <summary>
        ///  Ensure that the square is bounded.  If we assume this
        ///  is a normalized quaternion, the magnitude should be 1.
        /// </summary>
        /// <seealso cref="http://en.wikipedia.org/wiki/Quaternion"/>
        /// <param name="Q">quaternion.</param>
        /// <returns>true if valid; false otherwise.</returns>
        static internal bool isQuaternionValid(Quaternion Q)
        {
            float magnitude = (float)Math.Sqrt(Q.x * Q.x + Q.y * Q.y + Q.z * Q.z + Q.w * Q.w);
            if (Mathf.Abs(magnitude) > Quaternion.kEpsilon)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Latch the current IMU value if IMU data is valid.  If not valid,
        /// return waitIMU state.  TODO - move axis computation to IMUMotionData
        /// </summary>
        internal bool LatchIMU(ref Quaternion oVirt2Gravity)
        {
            Quaternion imuq = Compute();
            if (!IMUData.isNormalizedQuaternionValid(imuq)) return false;

            // compute angle of rotation between gravity and vertical in camera frame

            Vector3 grav_camera = SmoothedGravity;
            grav_camera.Normalize();

            Vector3 virt = new Vector3(0, -1, 0);
            Vector3 axis = Vector3.Cross(virt, grav_camera);

            if (axis.sqrMagnitude < 1.0e-07) return false;
            Vector3 forward = Vector3.Cross(grav_camera, axis);
            forward.Normalize();

            float cth = Vector3.Dot(virt, grav_camera);
            double th = Math.Acos((double)cth);
            th *= 180 / Math.PI;
            axis.Normalize();

            oVirt2Gravity = Quaternion.AngleAxis((float)th, axis);

            //Debug.Log("IMU latched: grav c " + grav_camera + " th " + th + " axis " + axis + " w rot " + oVirt2Gravity);
            return true;
        }
    };

}