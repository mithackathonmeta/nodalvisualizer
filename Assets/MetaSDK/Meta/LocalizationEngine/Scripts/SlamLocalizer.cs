// using System.Net.Mime;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;

// using System.Collections;
// using System.Threading;
// using System.IO;

namespace Meta
{
    ///<summary>
    /// This module uses MetaSLAM as a localizer.
    /// </summary>
    [Serializable]
    internal class SlamLocalizer : MetaBehaviour, ILocalizer
    {
        /// <summary>
        /// The types of SLAM represented as strings. These are used by the SLAM interop and passed to c++.
        /// </summary>
        private static readonly string[] _slamTypes = { "vslam", "vislam" };

        /// <summary>
        /// The types of SLAM. This is used for presentation purposes in the inspector.
        /// </summary>
        private enum SLAMType
        {
            VSLAM = 0,
            VISLAM
        }
        #region Public Events

        [System.Serializable]
        public class SLAMSensorsReadyEvent : UnityEvent { }
        [System.Serializable]
        public class SLAMMappingInProgressEvent : UnityEvent<float> { }
        [System.Serializable]
        public class SLAMMappingCompleteEvent : UnityEvent { }
        [System.Serializable]
        public class SLAMTrackingLostEvent : UnityEvent { }
        [System.Serializable]
        public class SLAMTrackingRelocalizedEvent : UnityEvent { }


        public SLAMSensorsReadyEvent onSlamSensorsReady = null;
        public SLAMMappingInProgressEvent onSlamMappingInProgress = null;
        public SLAMMappingCompleteEvent onSlamMappingComplete = null;
        public SLAMTrackingLostEvent onSlamTrackingLost = null;
        public SLAMTrackingRelocalizedEvent onSlamTrackingRelocalized = null;

        #endregion

        /// <summary>The SLAM interop; this wraps implementations of ISlam interface.</summary>
        private SlamInterop slamInterop;

        /// <summary>
        /// The mode of slam used
        /// </summary>
        [SerializeField]
        private SLAMType SLAM_Mode = SLAMType.VSLAM;

        /// <summary>Feedback from the Slam algorithm.</summary>
        /// \todo Populate this and make it public.
        public SlamFeedback slamFeedback = null;
        SlamFeedback lastSlamFeedback = new SlamFeedback();

        /// <summary>The parameters that can control the behavior of the slam system.  </summary>
        /// \todo Populate this and make it public.
        private SlamControlParameters slamControlParameters = null;

        /// <summary>Is this for debugging?  Can we remove it?</summary>
        protected SlamInterop.SLAMLocalizerState state = SlamInterop.SLAMLocalizerState.atStart;  // marked protected to avoid warning

        private bool enableJsonFeedback = true;

        private string jsonFeedback = "";

        [SerializeField]
        private bool _enableRelocalization = true;

        public bool showCalibrationUI = true;

        /// <summary>
        /// The instance of CalibrationProcess responsible for executing the SLAM calibration process.
        /// This is the top-level controller of the SLAM calibration procedure.
        /// </summary>
        private SLAMInitializationProcess _calibration;

        IEnumerator SetBasePositionContinuously(float duration) {
            float startTime = Time.time;
            while(Time.time < startTime + duration)
            {
                slamInterop.SetBasePosition();
                yield return null;
            }
        }

        /// <summary> Starts the monobehaviour.</summary>
        private void Start()
        {
            slamInterop = new SlamInterop(_slamTypes[(int)SLAM_Mode]);
            slamInterop.UpdateParameters(slamControlParameters, slamFeedback);
            onSlamMappingComplete.AddListener( () => { StartCoroutine(SetBasePositionContinuously(1f)); Debug.Log("mapping listener"); } );
            // onSlamMappingComplete.AddListener(new UnityAction(() => { slamInterop.SetBasePosition(); Debug.Log("mapping listener"); }));
            

            if (showCalibrationUI)
            {
                _calibration = GameObject.FindObjectOfType<SLAMInitializationProcess>();

                if (_calibration == null)
                {
                    GameObject slamUiRef = (GameObject)Resources.Load("Prefabs/SLAM Calibration UI");
                    if (slamUiRef == null)
                    {
                        Debug.LogError("Could not locate SLAM UI resource.");
                    }
                    else
                    {
                        GameObject slamInitUI = Instantiate(slamUiRef);
                        slamInitUI.name = "Meta 2 SLAM Init UI";
                    }
                }
            }
        }
        
        /// <summary> Override of ILocalizer. </summary>
        public void UpdateLocalizer(GameObject targetGO)
        {
            bool slamIsReady = (slamFeedback.tracking_ready == 1) && (slamFeedback.scale_quality_percent == 100);

            if (slamInterop != null)
            {
                slamInterop.Update(slamIsReady);
                slamInterop.TargetGO = targetGO;
                if (!slamIsReady)
                {
                       
                }
                

                //if (!slamIsReady)
                //    targetGO.transform.position = Vector3.zero;

                slamInterop.UpdateParameters(slamControlParameters, slamFeedback);
                state = slamInterop.State;

                if (enableJsonFeedback && slamFeedback != null)
                {
                    lastSlamFeedback.ParseJson(jsonFeedback);

                    slamInterop.GetSlamFeedback(out jsonFeedback);
                    slamFeedback.ParseJson(jsonFeedback);

                    FireEvents(slamFeedback, lastSlamFeedback);
                }                
            }
        }

        void FireEvents(SlamFeedback thisFrame, SlamFeedback previousFrame)
        {
            // slam mapping started: camera ready, tracking just got ready, scale quality = 0
            // slam mapping in progress: camera ready, tracking ready, 0 < scale_quality < 100
            // slam mapping complete: scale_quality just got 100, tracking ready
            // slam tracking lost: scale_qualiy = 100, !tracking ready

            if(thisFrame.CameraReady && !previousFrame.CameraReady && thisFrame.scale_quality_percent == 0)
            {
                // Debug.Log("Slam Sensors Ready");
                onSlamSensorsReady.Invoke();
            }

            if(thisFrame.CameraReady && thisFrame.TrackingReady && thisFrame.scale_quality_percent > 0 && thisFrame.scale_quality_percent < 100)
            {
                // Debug.Log("Slam Mapping In Progress, " + thisFrame.scale_quality_percent + " percent");
                onSlamMappingInProgress.Invoke(thisFrame.scale_quality_percent / 100f);
            }

            if(thisFrame.scale_quality_percent == 100 && previousFrame.scale_quality_percent < 100)
            {
                // Debug.Log("Slam Mapping Complete");
                onSlamMappingComplete.Invoke();
            }

            if(thisFrame.scale_quality_percent == 100 && !thisFrame.TrackingReady && previousFrame.scale_quality_percent == 100 && previousFrame.TrackingReady)
            {
                // Debug.Log("Slam Tracking Lost");
                onSlamTrackingLost.Invoke();
            }

            if (thisFrame.scale_quality_percent == 100 && thisFrame.TrackingReady && previousFrame.scale_quality_percent == 100 && !previousFrame.TrackingReady)
            {
                // Debug.Log("Slam Tracking Relocalized");
                onSlamTrackingRelocalized.Invoke();
            }
        }

        /// <summary> Override of ILocalizer. </summary>
        public void ResetLocalizer(GameObject targetGO)
        {
            slamInterop.ResetLocalizer();
        }

        private void Update()
        {
            slamInterop.EnableRelocalization = _enableRelocalization;
        }
    }
}