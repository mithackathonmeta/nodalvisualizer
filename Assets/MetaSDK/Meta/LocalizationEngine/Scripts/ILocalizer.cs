﻿using System;
using UnityEngine;

namespace Meta
{

    /// <summary>
    ///     Interface for localizers which update the position and rotation of a specified object.
    /// </summary>
    public interface ILocalizer
    {
        /// <summary>
        ///     Updates the transform values for an object.
        /// </summary>
        /// <param name="targetGO">The object to be updated.</param>
        void UpdateLocalizer(GameObject targetGO);

        /// <summary>
        ///     Resets the state of the localizer.
        /// </summary>
        /// <param name="targetGO">The object to be updated by the localizer.</param>
        void ResetLocalizer(GameObject targetGO);

    }

}