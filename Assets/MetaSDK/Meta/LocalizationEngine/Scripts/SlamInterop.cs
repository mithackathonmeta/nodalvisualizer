using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Linq;
using UnityEngine.UI;

namespace Meta
{
    ///<summary>
    /// This module uses any implementation of ISlam as a localizer.
    /// </summary>
    /// <remarks>
    // <para>Notes</para>
    ///</remarks>
    public class SlamInterop
    {
        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "registerSLAM")]
        protected static extern void RegisterSLAM([MarshalAs(UnmanagedType.BStr)] string iSlamName, float iRelThresh);

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "enableSLAM")]
        protected static extern void EnableSLAM();

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "relocalizeSLAM")]
        protected static extern void RelocalizeSLAM();

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "resetSLAM")]
        protected static extern void ResetSLAM();

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "cameraSLAMMetaWorldToCamera")]
        protected static extern int CameraSlamMetaWorldToCamera([MarshalAs(UnmanagedType.LPArray, SizeConst = 3)] double[] oTrans,
                                                                 [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] double[] oQuaternion);

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "cameraSLAMToMetaWorld")]
        protected static extern int CameraSlamToMetaWorld([MarshalAs(UnmanagedType.LPArray, SizeConst = 3)] double[] oTrans,
                                                           [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] double[] oQuaternion);

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "deltaSLAMMetaWorldCamera")]
        protected static extern int DeltaSlamMetaWorldCamera([MarshalAs(UnmanagedType.LPArray, SizeConst = 3)] double[] oTrans,
                                                              [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] double[] oQuaternion);

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "deltaSLAMMetaWorld")]
        protected static extern int DeltaSlamMetaWorld([MarshalAs(UnmanagedType.LPArray, SizeConst = 3)] double[] oTrans,
                                                        [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)] double[] oQuaternion);

        [DllImport(DllReferences.MetaVisionDLLName, EntryPoint = "getSlamFeedback")]
        protected static extern int getSlamFeedback([MarshalAs(UnmanagedType.BStr), Out] out string json);

        /// <summary>
        /// Transform as vector data buffer.
        /// </summary>
        private double[] _trans = new double[3];
        /// <summary>
        /// Quaternion as vector data buffer.
        /// </summary>
        private double[] _quat = new double[4];
        /// <summary>
        /// Current quaternion data.
        /// </summary>
        private Quaternion _quaternion;

        /// <summary>
        /// Latched initial IMU orientation.
        /// </summary>
        private Quaternion _imuOrientation;

        /// <summary>
        /// Latched SLAM world axis to IMU axis.  This is _imuOrientation * _initSlam2World^-1
        /// </summary>
        protected Quaternion _slam2Gravity;  // TODO: unused variable.  Are we keeping it for debugging?

        private bool _hasRelocalized = false;
        private float _firstInTrackingTimestamp = 0;
        private Vector3 _basePosition = Vector3.zero;
        private Quaternion _baseRotationInverse = Quaternion.identity;
        private bool _resetLocalizer = false;
        private float _initializationTime = 0;

        /// <summary>
        ///  SLAM localizer state.
        /// </summary>
        /// <remarks>
        /// atStart:
        /// wait
        /// </remarks>
        public enum SLAMLocalizerState
        {
            atStart, // When Unity starts or you lost localization long enough that it is restarting.
            waitIMU, // Waiting for good IMU data, only should happen at the very start unless IMU drops
            inError, // Error states other than just not localizing, see their error codes
            inTracking, // SLAM is tracking
            inRelocalization // While SLAM has lost localization
        };

        /// <summary>
        /// Current SLAM localizer state.
        /// </summary>
        private SLAMLocalizerState _state = SLAMLocalizerState.atStart;

        /// <summary>
        /// Start time of not tracking state.
        /// </summary>
        private DateTime _timeNotTracking = DateTime.Now;

        /// <summary>
        /// Maximum amount of time we are in the not tracking state (seconds).
        /// </summary>
        public int m_maxNotTracking = 3;

        /// <summary>
        /// Use or do not use the IMU.  This is mostly for testing.
        /// </summary>
        public bool m_useIMU = false;

        /// <summary>
        /// Allow auto relocalization or not.
        /// </summary>
        public bool m_autoRelocalize = true;

        /// <summary>
        /// Returns true when SLAM has had enough time to fully relocalize.
        /// </summary>
        public bool LocalizationDone
        {
            get { return _hasRelocalized; }
        }

        // Externally set GameObjects and Images

        /// <summary>
        /// Direction of gravity in world coordinates.  For debugging purposes.
        /// </summary>
        private GameObject targetGO = null; // Target game object

        // ---------------- Accessors ----------------------

        /// <summary>
        /// Return current state to caller.
        /// </summary>
        public SLAMLocalizerState State
        {
            get { return _state; }
        }

        /// <summary>
        /// Are we tracking state yet?  Do not track until can latch
        /// IMU values AND SLAM is tracking.
        /// </summary>
        public bool AreTracking
        {
            get { return _state == SLAMLocalizerState.inTracking; }
        }

        /// <summary>
        /// Return current time we have not been tracking to caller.
        /// </summary>
        public int TimeNotTracking
        {
            get
            {
                switch (_state)
                {
                    case SLAMLocalizerState.atStart:
                    case SLAMLocalizerState.inTracking:
                        return 0;
                    default:
                        break;
                }
                TimeSpan diff = DateTime.Now - _timeNotTracking;
                return diff.Seconds;
            }
        }

        public float TimeNotTrackingFloat
        {
            get
            {
                switch (_state)
                {
                    case SLAMLocalizerState.atStart:
                    case SLAMLocalizerState.inTracking:
                        return 0;
                    default:
                        break;
                }
                TimeSpan diff = DateTime.Now - _timeNotTracking;
                return (float)diff.TotalSeconds;
            }
        }

        public GameObject TargetGO
        {
            get { return targetGO; }
            set { targetGO = value; }
        }

        #region DebugGameObjects

        // TODO this whole region should go away, all debugging stuff.

        private Image slamTimerImage = null;
        private GameObject slamArrowGO = null;
        private GameObject gravity_arrow = null;

        public Image SlamTimerImage
        {
            get { return slamTimerImage; }
            set { slamTimerImage = value; }
        }

        public GameObject SlamArrowGO
        {
            get { return slamArrowGO; }
            set { slamArrowGO = value; }
        }

        public GameObject GravityArrow
        {
            get { return gravity_arrow; }
            set { gravity_arrow = value; }
        }


        public void UpdateSLAMTimer()
        {
            if (SlamTimerImage == null || SlamArrowGO == null)
            {
                return;
            }
            if (_state == SLAMLocalizerState.inRelocalization)
            {
                SlamTimerImage.enabled = true;
                SlamTimerImage.fillAmount = TimeNotTrackingFloat / (m_maxNotTracking + 1);
                if (!SlamArrowGO.activeSelf)
                {
                    SlamArrowGO.SetActive(true);
                }
            }
            else
            {
                SlamTimerImage.enabled = false;
                if (SlamArrowGO.activeSelf)
                {
                    SlamArrowGO.SetActive(false);
                }
            }
        }

        #endregion

        public bool EnableRelocalization { get; set; }

        /// <summary>Internal start method that can be used for all specializations of the SLAM localizer.</summary>
        public SlamInterop(string iSlamName)
        {
            //Debug.Log("Starting " + iSlamName + " Localizer...");
            RegisterSLAM(iSlamName, 0.10f); // for now

            _imuOrientation = Quaternion.identity;
            _slam2Gravity = Quaternion.Inverse(_imuOrientation);
        }

        /// <summary>Internal update method that can be used for all specializations of the SLAM localizer.</summary>
        virtual public void Update(bool isScaleEstimated)
        {
            UpdateTargetGOTransform(isScaleEstimated);
            UpdateSLAMTimer();
        }


        public void GetSlamFeedback(out string json)
        {
            getSlamFeedback(out json);
        }

        /// <summary>
        /// Fetches the feedback from the Slam algorithm and sets the control parameters.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="feedback"></param>
        public void UpdateParameters(SlamControlParameters control, SlamFeedback feedback)
        {
            // noop for now
        }

        #region State Handling

        /// <summary>
        /// Return whether or not we are in the state of relocalization.  If state is
        /// in error or in relocalization, we are in the relocalization state.
        /// </summary>
        /// <param name="iState">SLAM state.</param>
        /// <returns></returns>
        public static bool StateInRelocalization(SLAMLocalizerState iState)
        {
            return iState == SLAMLocalizerState.inRelocalization ||
                iState == SLAMLocalizerState.inError;
        }

        public static bool StateAtStart(SLAMLocalizerState iState)
        {
            return iState == SLAMLocalizerState.atStart ||
                iState == SLAMLocalizerState.waitIMU;
        }

        private void StateTransition(SLAMLocalizerState iNewState)
        {
            // If new state is in a relocalization equivalent state,
            // update all IMU values

            bool nowRelocal = StateInRelocalization(iNewState);

            // If states are not equivalent, transition states.
            if (_state != iNewState && StateInRelocalization(_state) != nowRelocal)
            {
                if (nowRelocal)
                {
                    // Stay in start state until we start to track
                    if (StateAtStart(_state))
                    {
                        return;
                    }

                    //Debug.Log("Relocalizing SLAM");
                    // Transitioning to a relocalization state.
                    _timeNotTracking = DateTime.Now;
                }
            }


            // If our time elapsed exceeds our maximum time AND we are
            // auto relocalizing, handle that here.  Put ourselves
            // into start state if this is the case
            if (m_autoRelocalize && nowRelocal)
            {
                if (TimeNotTracking > m_maxNotTracking)
                {
                    ResetLocalizer();
                    return;
                }
            }

            // If we are in the start state, we are about to transition to
            // tracking state.  Latch initial transformations.  If either
            // the IMU or the SLAM transformations are not valid,
            // do not transition to tracking state just yet.

            SLAMLocalizerState nextState;
            if (StateAtStart(_state) && !nowRelocal)
            {
                // If we are at start state : waiting for IMU, just try and latch a
                // valid IMU valid.  If fails, we stay in the waitIMU state.

                if (_state == SLAMLocalizerState.waitIMU)
                    nextState = LatchIMU(iNewState);
                else
                {
                    // If latch current fails, then start the not-tracking timer.
                    nextState = LatchCurrent(iNewState);
                    if (StateInRelocalization(nextState)) _timeNotTracking = DateTime.Now;
                }
            }
            else
            {
                nextState = iNewState;
            }
            _state = nextState;
        }


        #endregion


        /// <summary>
        /// Latch all current values.  public so can exterally force
        /// latching of current values.
        /// </summary>
        /// <see cref="LatchIMU"/>
        /// <returns>true if we current quaternions are valid; false otherwise.</returns>
        internal SLAMLocalizerState LatchCurrent(SLAMLocalizerState iReqState)
        {
            // Ensure that SLAM quaternion is valid
            SLAMLocalizerState ret = iReqState;

            return ret;
        }

        /// <summary>
        /// Latch the current IMU value if IMU data is valid.  If not valid,
        /// return waitIMU state.
        /// </summary>
        public SLAMLocalizerState LatchIMU(SLAMLocalizerState iReqState)
        {
            Quaternion curOrient = Quaternion.identity;

            _imuOrientation = curOrient;
            _slam2Gravity = Quaternion.Inverse(_imuOrientation);

            if (GravityArrow != null) GravityArrow.transform.rotation = _imuOrientation;
            //Debug.Log("IMU latched: grav c " + grav_camera + " th " + th + " axis " + axis + " w rot " + _imuOrientation);

            return iReqState;
        }

        /// <summary> Updates the target game object transform.</summary>
        public void UpdateTargetGOTransform(bool isScaleEstimated)
        {
            int status = CameraSlamToMetaWorld(_trans, _quat);
            if (status == 0)
            {
                _quaternion = FromDouble();

                if (TargetGO != null && isScaleEstimated)
                {
                    setPosition();
                }
            
                if (EnableRelocalization && !_hasRelocalized && _state == SLAMLocalizerState.inTracking)
                {
                    if (Mathf.Approximately(_firstInTrackingTimestamp, 0))
                    {
                        _firstInTrackingTimestamp = Time.time;
                        _initializationTime = _firstInTrackingTimestamp + (_resetLocalizer ? 0.05f : 0.05f);
                        _resetLocalizer = false;
                    }

                    if (Time.time >= _initializationTime)
                    {
                        _hasRelocalized = true;
                        SetBaseRotation();
                    }
                }

                StateTransition(SLAMLocalizerState.inTracking);

                if (TargetGO != null)
                {
                    if (SlamArrowGO != null) SlamArrowGO.transform.localPosition = -TargetGO.transform.localPosition;

                    //TargetGO.transform.rotation = _slam2Gravity * _quaternion;
                    if (!EnableRelocalization)
                    {
                        TargetGO.transform.localRotation = _quaternion;
                    }
                    else if (_hasRelocalized && _state == SLAMLocalizerState.inTracking)
                    {
                        setRotationAndPosition(isScaleEstimated);
                    }
                }
            }
        }

        // To reset the IMU to bring the unity world back to horizontal direction (Will be deprecated once the modeling of the Glasses is fixed)
        virtual public void ResetLocalizer()
        {
            ResetSLAM();

            _state = SLAMLocalizerState.atStart;
            _firstInTrackingTimestamp = 0;
            _hasRelocalized = false;
            _resetLocalizer = true;
        }

        // temporary offsets from CV - not used right now, but SLAM position/rotation should be applied in the correct coordinate frame
        // TODO: use or remove these variables.  (Marked protected to avoid warnings)
        protected Vector3 cvPositionOffset = new Vector3(0.03f, 0.0089f, 0.052f);
        protected Quaternion cvRotationOffset = Quaternion.Euler(8.436f, -7.443f, -2.301f);

        private void setPosition()
        {
            TargetGO.transform.localPosition = new Vector3((float)_trans[0], (float)_trans[1], (float)_trans[2]) - transformPosition(_basePosition);
        }

        private void setRotationAndPosition(bool isScaleEstimated)
        {
            Quaternion unsmoothedRotation = transformRotation(_quaternion);
            TargetGO.transform.localRotation = unsmoothedRotation;    

            if (isScaleEstimated)
            {
                Vector3 unsmoothedPosition =
                    transformPosition(new Vector3((float) _trans[0], (float) _trans[1], (float) _trans[2]) -
                                      _basePosition);
                
                TargetGO.transform.localPosition = unsmoothedPosition;
            }
        }

        private Vector3 transformPosition(Vector3 position)
        {
            return _baseRotationInverse * position;
        }

        private Quaternion transformRotation(Quaternion q)
        {
            return _baseRotationInverse * q;
        }

        public void SetBasePosition()
        {
            _basePosition = new Vector3((float)_trans[0], (float)_trans[1], (float)_trans[2]);
        }

        public void SetBaseRotation()
        {
            _baseRotationInverse = _externalCorrection * Quaternion.Inverse(_quaternion);
        }

        Quaternion _externalCorrection = Quaternion.identity;
        public void SetRotationCorrection(Quaternion externalCorrection)
        {
            _externalCorrection = externalCorrection;
        }

        /// <summary>
        /// Convert _quat to a quaternion.
        /// </summary>
        /// <see cref="_quat"/>
        /// <see cref="From(double[])"/>
        /// <returns>quaternion.</returns>
        internal Quaternion FromDouble()
        {
            return FromDouble(_quat);
        }

        /// <summary>
        /// Convert specified array of 4 to quaternion.
        /// </summary>
        /// <param name="iQuat">array of 4 doubles.</param>
        /// <returns>quaternion.</returns>
        static internal Quaternion FromDouble(double[] iQuat)
        {
            Quaternion ret;
            ret.x = (float)iQuat[0];
            ret.y = (float)iQuat[1];
            ret.z = (float)iQuat[2];
            ret.w = (float)iQuat[3];
            return ret;
        }
    }
}