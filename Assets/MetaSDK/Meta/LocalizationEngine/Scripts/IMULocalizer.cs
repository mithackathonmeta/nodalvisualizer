﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

namespace Meta
{
    /// <summary>
    /// This is the localizer class which update the position and rotation of a specified object based on the IMU sensor data.
    /// </summary>
    [Serializable]
    internal class IMULocalizer :  MetaBehaviour, ILocalizer
    {
        /// <summary> Gets motion sensor data.</summary>
        ///
        /// <param name="motionSensorData"> [in,out] Information describing the motion sensor.</param>
        [DllImport("MetaVisionDLL", EntryPoint = "isMotionSensorConnected")]
        internal static extern bool IsMotionSensorConnected(); 

        /// <summary>
        /// Whether gravity should be taken into account
        /// </summary>
		[SerializeField]
        private bool _useGravity = false;

        /// <summary>
        /// Current IMU values.
        /// </summary>
        private IMUData _IMUData;

        /// <summary>
        /// Latched IMU world axis to Gravity axis.
        /// </summary>
        private Quaternion _imu2Gravity;

        /// <summary>
        /// Is the IMU gravity valid.
        /// </summary>
        private bool _imu2GravityValid = false;

        /// <summary>
        /// Whether the IMU has been reset to center on start
        /// </summary>
        private bool _imuResetOnStart = false;

        /// <summary>
        /// funtion to check if IMU is connected or not
        /// </summary>
        /// <returns></returns>
        public bool IsIMUConnected()
        {
            return IsMotionSensorConnected();
        }

        /// <summary> Gets the raw imu orientation. </summary>
        /// <value> The imu orientation. </value>
        public Vector3 imuOrientation
        {
            get
            {
                return _IMUData.orientation;
            }

        }

        /// <summary> Gets the localizer orientation. This is affected by resets. </summary>
        public Vector3 localizerOrientation
        {
            get
            {
                return _IMUData.SetAngle;
            }
        }


        /// <summary>
        /// Gets or sets the accelerometer values in m/s^2.
        /// </summary>
        public Vector3 accelerometerValues
        {
            get
            {
                return _IMUData.accelerometerValues;
            }
        }

        /// <summary> Gets or sets the gyroscope values in degrees per second (DPS). </summary>
        public Vector3 gyroscopeValues
        {
            get
            {
                return _IMUData.gyroscopeValues;
            }
        }

        /// <summary> Gets the magnetometer values normalized from (-2048 to 2048). </summary>
        public Vector3 magnetometerValues
        {
            get
            {
                return _IMUData.magnetometerValues;
            }
        }

        private void Start()
        {
            _IMUData = metaContext.Get<IMUData>();
        }

        public void UpdateLocalizer(GameObject targetGO)
        {
            if (IsIMUConnected())
            {
                _IMUData.Update();
                UpdateTargetGOTransform(targetGO);
            }
        }

        /// <summary> Updates the target game object transform.</summary>
        private void UpdateTargetGOTransform(GameObject targetGO)
        {
            if (!_imu2GravityValid) LatchIMU();

            Quaternion imuOrient;
            if (_imu2GravityValid && _useGravity)
            {
                imuOrient = _imu2Gravity * _IMUData.Compute();
            }
            else
            {
                imuOrient = _IMUData.Compute();
            }
            if (!_imuResetOnStart && imuOrient != Quaternion.identity)
            {
                ResetLocalizer(targetGO);
                _imuResetOnStart = true;
            }
            targetGO.transform.localRotation = imuOrient;

        }

        /// <summary>
        /// Latch the current IMU value if IMU data is valid.  If not valid,
        /// return waitIMU state.  TODO - move axis computation to IMUMotionData
        /// </summary>
        public bool LatchIMU()
        {
            Quaternion curOrient = Quaternion.identity;
            if (!_IMUData.LatchIMU(ref curOrient)) return false;

            _imu2Gravity = Quaternion.Inverse(curOrient);
            _imu2GravityValid = true;

            return true;
        }

        // Checking to see if it is the quaternion is Valid or not. (TODO: Could be moved to a static Util Class Later)
        // Note - I do not believe this.  The magnitude of a valid normalized quaternion is 1.
        static internal bool isQuaternionValid(Quaternion Q)
        {
            float magnitude = (float)Math.Sqrt(Q.x * Q.x + Q.y * Q.y + Q.z * Q.z + Q.w * Q.w);
            if (Mathf.Abs(magnitude - 1) > 0.1f)
                return true;
            else
                return false;
        }

        // To reset the IMU to bring the unity world back to horizontal direction (Will be deprecated once the modeling of the Glasses is fixed)
        public void ResetLocalizer(GameObject targetGO)
        {
            //Debug.Log("Reset IMU localizer");
            _IMUData.Reset();
            _imu2GravityValid = false;
        }

    }

}