﻿using UnityEngine;
using System;

namespace Meta
{
    /// <summary>
    ///     Handles setup and execution of localization for the MetaWorld prefab.
    /// </summary>
    public class MetaLocalization : IEventReceiver
    {

        private GameObject _targetGO;
        private ILocalizer _currentLocalizer;

        /// <summary>
        ///     Constructor for the localization module.
        /// </summary>
        /// <param name="targetGO">The object to be updated with values from the localizer.</param>
        /// <param name="currentLocalizer">The localization method to be used.</param>
        internal MetaLocalization(GameObject targetGO)
        {
            _targetGO = targetGO;
        }

        /// <summary>
        ///     Initalises the events for the module.
        /// </summary>
        /// <param name="eventHandlers"></param>
        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.updateEvent += Update;
        }

        /// <summary>
        /// Calls the update loop to get new values from the localizer.
        /// </summary>
        private void Update()
        {
            if (_currentLocalizer != null)
            {
                if (Input.GetKeyDown(KeyCode.F4))
                {
                    ResetLocalization();
                }
                _currentLocalizer.UpdateLocalizer(_targetGO);
            }
        }

        /// <summary>
        ///     Sets the localizer to be used for position and rotation tracking.
        /// </summary>
        /// <param name="localizerType">The type of localizer to be used.</param>
        public void SetLocalizer(Type localizerType)
        {
            if (_currentLocalizer != null && localizerType == _currentLocalizer.GetType())
            {
                return; //The same type of localizer is already assigned
            }
            //Get the components that are ILocalizers from the target object
            var oldComponents = _targetGO.GetComponents<ILocalizer>();
            if (oldComponents != null && oldComponents.Length > 0 && oldComponents[0].GetType() == localizerType)
            {
                _currentLocalizer = oldComponents[0];
                return; //Avoid reassignment and loss of set script values in editor
            }
            if (!(typeof(ILocalizer).IsAssignableFrom(localizerType)))
            {
                return; // type was not a ILocalizer
            }

            if (oldComponents != null)
            {
                foreach (var component in oldComponents)
                {
                    if (component != null)
                    {
                        GameObject.DestroyImmediate((UnityEngine.Object)component);
                    }
                }
            }
            ILocalizer newComponent = _targetGO.AddComponent(localizerType) as ILocalizer;
            _currentLocalizer = newComponent;
        }

        public ILocalizer GetLocalizer()
        {
            return _currentLocalizer;
        }

        /// <summary>
        ///     Resets the currently enabled localizer.
        /// </summary>
        public void ResetLocalization()
        {
            if (_currentLocalizer != null)
            {
                _currentLocalizer.ResetLocalizer(_targetGO);
            }
        }

    }

}