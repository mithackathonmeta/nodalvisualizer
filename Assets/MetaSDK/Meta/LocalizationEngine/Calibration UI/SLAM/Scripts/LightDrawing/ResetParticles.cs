﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Meta
{
    public class ResetParticles : MonoBehaviour
    {

        public ParticleSystem[] systems;
        public UnityEngine.Events.UnityEvent OnReset;

        Dictionary<ParticleSystem, float> originalSettings;

        void Start()
        {
            originalSettings = new Dictionary<ParticleSystem, float>();
            foreach (var s in systems)
                originalSettings.Add(s, s.emission.rate.constantMax);
        }

        public void Reset()
        {
            foreach (var system in systems)
            {
                system.Simulate(0, true, true);
                system.Play(true);
            }

            Debug.Log("Resetting particles");
            OnReset.Invoke();
        }


        public void Pause()
        {
            // Debug.Log("Frame for pause: " + Time.frameCount);
            foreach (var system in systems)
            {
                // Replacing enableEmission/emissionRate with new calls:
                // http://forum.unity3d.com/threads/what-is-the-unity-5-3-equivalent-of-the-old-particlesystem-emissionrate.373106/
                ParticleSystem.EmissionModule e = system.emission;
                e.enabled = false;
                ParticleSystem.MinMaxCurve rate = new ParticleSystem.MinMaxCurve();
                rate.constantMax = 0;
                e.rate = rate;

                // system.enableEmission = false;
                // system.emissionRate = 0;
            }
        }

        IEnumerator _Play(ParticleSystem system)
        {
            yield return null;

            ParticleSystem.EmissionModule e = system.emission;
            e.enabled = true;
            ParticleSystem.MinMaxCurve rate = new ParticleSystem.MinMaxCurve();
            rate.constantMax = originalSettings[system];
            e.rate = rate;

            // system.enableEmission = true;
            // system.emissionRate = originalSettings[system]; // HACK get the values properly
        }

        public void Play()
        {
            // Debug.Log("Frame for play: " + Time.frameCount); 
            foreach (var system in systems)
            {
                StartCoroutine(_Play(system));
            }
        }
    }


}