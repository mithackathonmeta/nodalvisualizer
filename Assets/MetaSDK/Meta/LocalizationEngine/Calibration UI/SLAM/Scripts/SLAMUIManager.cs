﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Meta
{
    public class SLAMUIManager : MonoBehaviour
    {

        public SLAMInitializationGazePoint slamInitPrefab;
        public Transform eyeCamera;

        [Header("Head Rotation Settings")]
        //public Camera EyeCamera;
        public float fromAngle = -100;
        public float toAngle = 100;
        public int stepCountHalfCircle = 15;

        [Header("Procedure Settings")]
        public float lookAtTime = .2f;
        public float waitTimeForHelpText = 1f;
        public float waitTimeForHelpArrows = 1f;
        public SLAMParticles particles;

        [Header("Help Animations / Texts")]
        public Animator slamMoveHeadHelp;
        public Animator slamDuringTrackingHelp;
        public Animator slamTrackingDoneHelp;
        public bool useDirectionArrows = true;
        public Animator leftArrows, rightArrows;

        [Header("Blinking Arrow Animation")]
        public bool useArrowAnimation = false;
        public float blinkSpeed = 0.3f;
        public float blinkShift = 0.5f;

        [Header("Output")]
        public float slamDonePercentage = 0;
        public float timeSinceLastPoint = 0;
        public bool isCalibrating = false;

        // SLAM Particles need these
        [HideInInspector]
        public Transform currentTarget;
        [HideInInspector]
        public int direction;
        SLAMInitializationGazePoint[] _points;

        /// <summary>
        /// Generate the points for SLAM initialization that the user has to look at.
        /// TODO: If we go with the current version, only one point will be needed. If we go with the arrow version, all points are needed.
        /// </summary>
        void Start()
        {
            // build all slam init points for the specified angle and step settings
            // "halfCircleCount * 2" pieces for "fromAngle" to "toAngle"
            // save them in an array
            // then do calibration fromIndex toIndex and use these points (activate/deactivate/show correct graphics/animation/light)

            if (eyeCamera == null)
                eyeCamera = GameObject.Find("StereoCameras").transform;

            var pointCount = useArrowAnimation ? stepCountHalfCircle * 2 : 1;

            _points = new SLAMInitializationGazePoint[pointCount];
            for (int i = 0; i < pointCount; i++)
            {
                var angle = Mathf.Lerp(fromAngle, toAngle, (float)i / (pointCount + 1));

                var pf = Instantiate(slamInitPrefab);
                pf.EyeCamera = eyeCamera;
                pf.transform.parent = this.transform;
                pf.transform.localPosition = Vector3.zero;
                pf.transform.localRotation = Quaternion.Euler(0, angle, 0);

                pf.requiredLookAtTime = lookAtTime;

                // initialize and hide these points
                // in non-arrow mode, they are invisible anyways
                pf.direction = 1;
                pf.Activate(false);
                _points[i] = pf;
            }
        }

        /// <summary>
        /// Hide all messages, cleanup work.
        /// </summary>
        void OnDisable()
        {
            if (slamMoveHeadHelp != null && slamMoveHeadHelp.isActiveAndEnabled) slamMoveHeadHelp.SetBool("Show", false);
            if (slamDuringTrackingHelp != null && slamDuringTrackingHelp.isActiveAndEnabled) slamDuringTrackingHelp.SetBool("Show", false);
            if (slamTrackingDoneHelp != null && slamTrackingDoneHelp.isActiveAndEnabled) slamTrackingDoneHelp.SetBool("Show", false);
        }


        // Use this for initialization
        public IEnumerator RunFullCalibration()
        {
            isCalibrating = true;

            gameObject.SetActive(true);
            timeSinceLastPoint = 100;

            // helper coroutines -
            // displays text whenever user gets stuck (movement is not smooth)
            // display direction arrows whenever the user gets stuck
            StartCoroutine(ShowDuringTrackingHelp());

            if (useDirectionArrows)
                StartCoroutine(ShowArrows());

            // wait for first SLAM initialization
            // yield return StartCoroutine(WaitForHeadMovement());

            // activate lightband particles
            if (particles)
                particles.DoStart();

            int first = 0;
            int midStart = stepCountHalfCircle + 5; // offset the start position a bit to the right so it is not activated in the beginning
            int end = stepCountHalfCircle * 2 - 1;

            // start with turning your head from the middle to the right
            yield return StartCoroutine(CalibrateDirectional(midStart, end));

            // while SLAM is not finished, make the user look left and right and vice versa
            // at any point, the SLAM subsystem might be "done", and then we need some early-out. Or we need to repeat the process!
            while (!CheckForSLAMInitComplete())
            {
                // turn your head from right to left
                yield return StartCoroutine(CalibrateDirectional(end, first));

                // turn your head from left to right
                yield return StartCoroutine(CalibrateDirectional(first, end));
            }

            Debug.Log("SLAM initialization done.");

            currentTarget = null;
            isCalibrating = false;

            // hide direction arrows
            if (useDirectionArrows && leftArrows && rightArrows)
            {
                leftArrows.SetBool("IsOn", false);
                rightArrows.SetBool("IsOn", false);
            }

            // show done help text
            slamDuringTrackingHelp.SetBool("Show", false);
            slamTrackingDoneHelp.SetBool("Show", true);

            // end animation with lightband drawing a circle at high speed
            if (particles)
            {
                particles.GoCrazy();
                yield return new WaitForSeconds(3);
                particles.DoStop();
            }

            // fade "well done" message out
            yield return new WaitForSeconds(4);
            slamTrackingDoneHelp.SetBool("Show", false);

            // wait for UI to fade out
            yield return new WaitForSeconds(0.7f);

            // deactivate this step
            gameObject.SetActive(false);
        }

        bool CheckForSLAMInitComplete()
        {
            var slamLocalizer = GameObject.FindObjectOfType<Meta.SlamLocalizer>();
            var feedback = slamLocalizer.slamFeedback;

            return ((feedback.tracking_ready > 0) && (feedback.camera_ready > 0) && (feedback.scale_quality_percent >= 100));
        }

        #region HelperCoroutines

        IEnumerator CalibrateDirectional(int fromIndex, int toIndex)
        {
            // check for early-out if slam calibration is fully done already
            if (CheckForSLAMInitComplete())
            {
                yield break;
            }

            if (useArrowAnimation)
                yield return StartCoroutine(CalibrateDirectional_Arrows(fromIndex, toIndex));
            else
                yield return StartCoroutine(CalibrateDirectional_SinglePoint(fromIndex, toIndex));
        }

        /// <summary>
        /// Activates a single (invisible) point at a time and moves that around to have the user look at it.
        /// </summary>
        IEnumerator CalibrateDirectional_SinglePoint(int fromIndex, int toIndex)
        {
            timeSinceLastPoint = 0;
            direction = fromIndex > toIndex ? -1 : 1;
            leftArrows.SetBool("IsOn", direction > 0);
            rightArrows.SetBool("IsOn", direction < 0);

            int i = fromIndex;
            while (i != toIndex)
            {
                SLAMInitializationGazePoint pCurrent = _points[0];

                // set point rotation
                var angle = Mathf.Lerp(fromAngle, toAngle, (float)i / (stepCountHalfCircle * 2 + 1));
                pCurrent.transform.localRotation = Quaternion.Euler(0, angle, 0);

                // set up point for gazing
                pCurrent.direction = -direction;
                pCurrent.allowGazing = true;
                pCurrent.Init();
                pCurrent.Activate(true);

                currentTarget = pCurrent.r.transform;

                // wait until point has been gazed at for long enough
                while (pCurrent.isActive)
                {
                    // check for early-out if slam calibration is fully done already
                    if (CheckForSLAMInitComplete())
                    {
                        timeSinceLastPoint = 0;
                        yield break;
                    }

                    timeSinceLastPoint += Time.deltaTime;
                    yield return null;
                }

                timeSinceLastPoint = 0;
                i += direction;
            }
        }

        /// <summary>
        /// Shows multiple points at the same time and animates their blinking.
        /// </summary>
        IEnumerator CalibrateDirectional_Arrows(int fromIndex, int toIndex)
        {
            timeSinceLastPoint = 0;
            direction = fromIndex > toIndex ? -1 : 1;

            if (useDirectionArrows && leftArrows && rightArrows)
            {
                leftArrows.SetBool("IsOn", direction > 0);
                rightArrows.SetBool("IsOn", direction < 0);
            }

            int i = fromIndex;
            while (i != toIndex)
            {
                SLAMInitializationGazePoint pCurrent = _points[i];
                SLAMInitializationGazePoint pNext1 = null;
                SLAMInitializationGazePoint pNext2 = null;
                SLAMInitializationGazePoint pNext3 = null;

                pCurrent.direction = -direction;
                pCurrent.allowGazing = true;
                pCurrent.Init();
                pCurrent.Activate(true);

                currentTarget = pCurrent.r.transform;

                // activate the next two future points if they exist
                if (direction * (i + direction) < direction * toIndex)
                {
                    pNext1 = _points[i + direction];
                    pNext1.allowGazing = false;
                    pNext1.Activate(true);
                }
                if (direction * (i + 2 * direction) < direction * toIndex)
                {
                    pNext2 = _points[i + 2 * direction];
                    pNext2.allowGazing = false;
                    pNext2.Activate(true);
                }
                if (direction * (i + 3 * direction) < direction * toIndex)
                {
                    pNext3 = _points[i + 3 * direction];
                    pNext3.allowGazing = false;
                    pNext3.Activate(true);
                }

                // wait until this point is detected (looked at for a minimum time set in the point prefab)
                while (pCurrent.isActive)
                {
                    // check for early-out if slam calibration is fully done already
                    if (CheckForSLAMInitComplete())
                    {
                        timeSinceLastPoint = 0;
                        yield break;
                    }

                    timeSinceLastPoint += Time.deltaTime;

                    // change the colors of the three points here (every frame) based on
                    // - their index
                    // - direction.
                    pCurrent.time = Mathf.Sin(Time.time * blinkSpeed + i * blinkShift);
                    if (pNext1) pNext1.time = Mathf.Sin(Time.time * blinkSpeed + (i + direction) * blinkShift * -direction);
                    if (pNext2) pNext2.time = Mathf.Sin(Time.time * blinkSpeed + (i + 2 * direction) * blinkShift * -direction);
                    if (pNext3) pNext3.time = Mathf.Sin(Time.time * blinkSpeed + (i + 3 * direction) * blinkShift * -direction);

                    yield return null;
                }

                timeSinceLastPoint = 0;
                i += direction;
            }
        }

        /// <summary>
        /// User has to move his head to get SLAM from "sensors initialized" to "tracking initialized".
        /// </summary>
        IEnumerator WaitForHeadMovement()
        {
            // wait for event from SLAM system that tracking has started.
            // what to do if no event comes?

            // for testing: wait for some total angular movement (here 20°)
            var startVector = eyeCamera.transform.forward;
            var angle = 0f;
            while (angle < 10)
            {
                angle += Vector3.Angle(eyeCamera.transform.forward, startVector) * Time.deltaTime;
                yield return null;
            }
        }

        /// <summary>
        /// Show help during tracking at the beginning and if the user hasn't gotten rid of any target points for some seconds.
        /// </summary>
        IEnumerator ShowDuringTrackingHelp()
        {
            slamMoveHeadHelp.SetBool("Show", true);
            yield return new WaitForSeconds(4);

            slamMoveHeadHelp.SetBool("Show", false);

            while (isCalibrating)
            {
                // user needs help if he didnt calibrate a point in the last couple seconds
                var userNeedsHelp = timeSinceLastPoint > waitTimeForHelpText;
                // also show help for the first couple seconds
                // userNeedsHelp |= (Time.time - startTime < 3);

                slamDuringTrackingHelp.SetBool("Show", userNeedsHelp);
                yield return new WaitForSeconds(0.5f);
            }
        }

        /// <summary>
        /// Show arrow animation pointing the user into the right direction when he gets stuck.
        /// </summary>
        IEnumerator ShowArrows()
        {
            while (isCalibrating)
            {
                var userNeedsHelp = timeSinceLastPoint > waitTimeForHelpArrows;

                if (currentTarget == null)
                    yield return null;
                else
                {
                    // see whether we are left or right of the next target point
                    // show arrows accordingly

                    var eyeRightVector = Vector3.Cross(eyeCamera.transform.forward, -Vector3.up).normalized;
                    var objectForwardVector = (currentTarget.position - eyeCamera.transform.position).normalized;

                    // get dot product of these
                    var dir = Vector3.Dot(objectForwardVector, eyeRightVector);

                    leftArrows.SetBool("IsOn", userNeedsHelp && (dir < 0));
                    rightArrows.SetBool("IsOn", userNeedsHelp && (dir > 0));

                    yield return null;
                }
            }

            leftArrows.SetBool("IsOn", false);
            rightArrows.SetBool("IsOn", false);
        }

        /// <summary>
        /// Make sure that all sensors needed for this procedure are up and running.
        /// TODO: Not only wait for IMU
        /// </summary>
        IEnumerator WaitForSensorInitialization()
        {
            yield break;
        }

        #endregion
    }

}