﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    public class SLAMInitializationProcess : MonoBehaviour
    {
        private Coroutine _calibration;

        void Start()
        {
            BeginCalibration();
        }

        /// <summary>
        /// Begins the calibration process.
        /// </summary>
        public void BeginCalibration()
        {
            //prevent runaway calibration process
            if (_calibration != null)
            {
                StopCoroutine(_calibration);
            }

            _calibration = StartCoroutine(DoFullCalibration());
        }

        /// <summary>
        /// Cleanly destroy the UI so that it does not leave unused game objects behind.
        /// </summary>
        public void Cleanup()
        {
            // getting a weird AccessViolation error on actual destruction,
            // so lets hide the object and pretend it is not here :)
            //this.gameObject.SetActive(false);
            this.gameObject.hideFlags = HideFlags.HideAndDontSave;
        }

        /// <summary>
        /// Prevent errors from appearing in the console when Unity stops playing. 
        /// </summary>
        public void OnApplicationQuit()
        {
            //The errors are produced by the hide-flags being set. Remove the hide-flags
            this.gameObject.hideFlags = HideFlags.None; 
        }

        /// <summary>
        /// Run UI
        /// </summary>
        public IEnumerator DoFullCalibration() 
        {

            yield return StartCoroutine(SensorInitialization());

            var slamInitManager = GetComponentInChildren<SLAMUIManager>();
            yield return StartCoroutine(slamInitManager.RunFullCalibration());
            // yield return StartCoroutine(canvas.FindCanvasOrigin());

            yield return null;

            Cleanup();
        }

        IEnumerator SensorInitialization()
        {
            // wait for SLAM cameras to be initialized
            var localizer = GameObject.FindObjectOfType<Meta.MetaManager>().GetComponent<Meta.SlamLocalizer>();

            if (localizer != null && localizer.slamFeedback != null)
            {
                while (!(localizer.slamFeedback.camera_ready > 0))
                    yield return null;
            }

            yield break;
        }
    }


}