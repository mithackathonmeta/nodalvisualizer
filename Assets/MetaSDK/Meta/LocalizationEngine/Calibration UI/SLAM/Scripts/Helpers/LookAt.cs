﻿using UnityEngine;
using System.Collections;

namespace Meta
{
    public class LookAt : MonoBehaviour
    {

        public Transform target;

        // Update is called once per frame
        void LateUpdate()
        {
            if (!target) target = GameObject.Find("StereoCameras").transform;

            transform.LookAt(target);
            transform.rotation *= Quaternion.Euler(180, 0, 0);
        }
    }

}
