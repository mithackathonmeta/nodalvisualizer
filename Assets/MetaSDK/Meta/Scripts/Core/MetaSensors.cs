﻿using System.Runtime.InteropServices;

namespace Meta
{
    internal class MetaSensors: IEventReceiver
    {
        /// <summary>
        /// DLL call to start the camera, IMU and the Metavision DLL
        /// </summary>
        [DllImport("MetaVisionDLL", EntryPoint = "initMetaVisionCamera")]
        internal static extern int InitMetaVisionCamera(DataAcquisitionSystem iDAQ, ref DeviceInfo cameraInfo, IMUModel imuModel);

        ///<summary>
        /// DLL call to stop the Metavision DLL, camera and IMU
        ///</summary>
        [DllImport("MetaVisionDLL", EntryPoint = "deinitMeta")]
        private static extern void DeinitMeta();

        /// <summary>
        /// Retrieve the device information. This is only for DS325.
        /// </summary>
        /// <param name="cameraInfo">DS325 device information data structure.</param>
        /// <returns>true if camera ready; false if not.</returns>
        [DllImport("MetaVisionDLL", EntryPoint = "GetDeviceInfo")]
        internal static extern bool GetDeviceInfo(ref DeviceInfo cameraInfo);

        /// <summary>
        /// Enables the virtual webcam feed. 
        /// </summary>
        [DllImport("MetaVisionDLL", EntryPoint = "enableVirtualWebcam")]
        internal static extern void EnableVirtualWebcam();

        private DeviceInfo _deviceInfo;
        private DataAcquisitionSystem _dataAcquisitionSystem;

        public MetaSensors(DeviceInfo deviceInfo, DataAcquisitionSystem dataAcquisitionSystem)
        {
            _deviceInfo = deviceInfo;
            _dataAcquisitionSystem = dataAcquisitionSystem;
        }

        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.awakeEvent += Awake;
            eventHandlers.onDestroyEvent += OnDestroy;
        }

        private void Awake()
        {
            if (_dataAcquisitionSystem == DataAcquisitionSystem.Meta1)
            {
                InitMetaVisionCamera(_dataAcquisitionSystem, ref _deviceInfo, IMUModel.MPU9150Serial);
            }
            else
            {
                InitMetaVisionCamera(_dataAcquisitionSystem, ref _deviceInfo, IMUModel.UnknownIMU);
                EnableVirtualWebcam();
            }
        }

        public bool GetDeviceInfoValid()
        {
            return GetDeviceInfo(ref _deviceInfo);
        }

        private void OnDestroy()
        {
            DeinitMeta();
        }
    }
}