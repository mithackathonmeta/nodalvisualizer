﻿using UnityEngine;
using System.Collections.Generic;

namespace Meta
{
    using Meta.Internal;
    using Analytics;
    internal class MetaFactory
    {

        internal MetaFactory(DataAcquisitionSystem dataAcquisitionSystem, GameObject metaPrefab, float effectiveScale, string playbackPath = null)
        {
            _dataAcquisitionSystem = dataAcquisitionSystem;
            _metaPrefab = metaPrefab;
            _meterToUnityScale = effectiveScale;
            _playbackPath = playbackPath;
            _depthOcclusionTransform = metaPrefab.transform.Find("MetaCameras/DepthOcclusion");
        }

        private GameObject _metaPrefab;
        private Transform _depthOcclusionTransform;
        private float _meterToUnityScale;
        private string _playbackPath;
        private bool _usingPlayback { get { return !string.IsNullOrEmpty(_playbackPath); } }

        private List<IEventReceiver> _eventReceivers = new List<IEventReceiver>();

        private DeviceInfo _deviceInfo;
        private MetaSensors _metaSensors;

        private DeviceTextureSource _metaPOV;
        private InteractionEngine _interactionEngine;
        private DepthOcclusionHandler _depthOcclusionHandler;

        private MetaLocalization _metaLocalization;
        private IMUData _imuData;

        private Gaze _gaze;
        private HudLock _hudLock;
        private OrbitalLock _orbitalLock;
        private MetaAnalyticsManager _metaAnalyticsManager;

        private MetaContext _metaContext;

        private DataAcquisitionSystem _dataAcquisitionSystem;

        public List<IEventReceiver> ConstructAll(ref MetaContext metaContext)
        {
            if (!_usingPlayback)
            {
                ConstructSensors();
                ConstructPOV();
                ConstructDepthOcclusion();
            }
            ConstructLocalization();
            ConstructDefaultInteractionEngine();
            ConstructGaze();
            ConstructLocking();
            ConstructMetaContext();
            ConstructUserSettings();
            ConstructMarkers();
            metaContext = _metaContext;
            ConstructAnalytics();
            ConstructCalibrationParameters();
            ConstructAlignmentHandler();
            return _eventReceivers;
        }

        /// <summary>
        /// Constructs the AlignmentHandler. This will load an 
        /// </summary>
        private void ConstructAlignmentHandler()
        {
            AlignmentHandler alignmentHandler = new AlignmentHandler();
            _eventReceivers.Add(alignmentHandler);
            _metaContext.Add(alignmentHandler);
        }

        /// <summary>
        /// Constructs the calibration parameters object.
        /// No calibration data is guaranteed until the DLL which supplies the data does.
        /// </summary>
        private void ConstructCalibrationParameters()
        {
            CalibrationParameters pars = new CalibrationParameters(new CalibrationParameterLoaderAdditionalMatrices());
            _metaContext.Add(pars);
            _eventReceivers.Add(pars);
        }


        private void ConstructMarkers()
        {
            //var markers = new Markers(_metaPrefab.transform);
            //_eventReceivers.Add(markers);
            //_metaContext.Add(markers);
        }

        private void ConstructAnalytics()
        {
            _metaAnalyticsManager = new MetaAnalyticsManager();
            _eventReceivers.Add(_metaAnalyticsManager);
        }

        private void ConstructDepthOcclusion()
        {
            GameObject depthOcclusionGO = _metaPrefab.transform.Find("MetaCameras/DepthOcclusion/ShaderOcclusion").gameObject;
            if (depthOcclusionGO == null)
            {
                Debug.LogWarning("Meshrenederer missing from depthOcclusion GameObject");
                return;
            }
            else if (depthOcclusionGO.GetComponent<DepthOcclusionManager>() == null)
            {
                Debug.LogWarning("DepthOcclusionManager missing from depthOcclusion GameObject");
                return;
            }
            else if (depthOcclusionGO.GetComponent<Renderer>() == null)
            {
                Debug.LogWarning("Renderer missing from depthOcclusion GameObject");
                return;
            }
            else if (depthOcclusionGO.GetComponent<Renderer>().material.shader.name != "Meta/DepthOcclusionShader")
            {
                Debug.LogWarning("Renderer on depthOcclusion GameObject does not have the right shader set up");
                return;
            }
            _depthOcclusionHandler = new DepthOcclusionHandler(depthOcclusionGO);
            //Hack; becuase Cant run Coroutines outside of MonoBehvaiour. 
            //todo: Maybe should think of using a MetaBehaviour Script to get around this. 
            depthOcclusionGO.GetComponent<DepthOcclusionManager>().depthOcclusionHandler = _depthOcclusionHandler;
            _eventReceivers.Add(_depthOcclusionHandler);
        }

        private void ConstructUserSettings()
        {
            //This will be how the username is passed around
            Credentials creds = new Credentials("default", null);
            _metaContext.Add(creds);
            var userSettings = new EventReceivingUserSettings(creds);
            _eventReceivers.Add(userSettings);
            _metaContext.Add((IUserSettings)userSettings);
        }

        private void ConstructDefaultInteractionEngine()
        {
            //todo: redundant and conflicting options possible. Needs to be refactored

            HandKernelSettings handSettignsGO = GameObject.FindObjectOfType<HandKernelSettings>();
            string handkernelType = handSettignsGO.handKernelType.ToString();
            if (!_usingPlayback)
            {
                InteractionEngineFactory.Construct(out _interactionEngine, handkernelType, "Sensors", _depthOcclusionTransform);
            }
            else
            {
                InteractionEngineFactory.Construct(out _interactionEngine, handkernelType, "Playback", _depthOcclusionTransform, _playbackPath);
            }
            _eventReceivers.Add(_interactionEngine);
        }

        private void ConstructSensors()
        {
            _deviceInfo = new DeviceInfo();
            _deviceInfo.imuModel = IMUModel.MPU9150Serial;
            _deviceInfo.cameraModel = CameraModel.DS325;
            _deviceInfo.depthFps = 60;
            _deviceInfo.depthHeight = 240;
            _deviceInfo.depthWidth = 320;
            _deviceInfo.colorFps = 30;
            _deviceInfo.colorHeight = 720;
            _deviceInfo.colorWidth = 1280;

            _metaSensors = new MetaSensors(_deviceInfo, _dataAcquisitionSystem);
            _eventReceivers.Add(_metaSensors);
        }

        private void ConstructPOV()
        {
            _metaPOV = new DeviceTextureSource(_metaSensors, _deviceInfo);
            _eventReceivers.Add(_metaPOV);
            MonoBehaviour.DontDestroyOnLoad(GameObject.FindObjectOfType<MetaManager>());
        }

        private void ConstructLocalization()
        {
            _imuData = new IMUData();
            _metaLocalization = new MetaLocalization(_metaPrefab);
            _eventReceivers.Add(_metaLocalization);
        }

        private void ConstructGaze()
        {
            _gaze = new Gaze();
            _eventReceivers.Add(_gaze);
        }

        private void ConstructLocking()
        {
            _hudLock = new HudLock();
            _orbitalLock = new OrbitalLock();
            _eventReceivers.Add(_hudLock);
            _eventReceivers.Add(_orbitalLock);
        }

        private void ConstructMetaContext()
        {
            _metaContext = new MetaContext();
            _metaContext.Add(_metaLocalization);
            _metaContext.Add(_imuData);
            _metaContext.Add(_gaze);
            _metaContext.Add(_hudLock);
            _metaContext.Add(_orbitalLock);
            _metaContext.meterToUnityScale = _meterToUnityScale;
            _metaContext.Add(_metaPOV);
            _metaContext.Add(new RGBData(_metaPOV));
            _metaContext.Add(_interactionEngine);
            _metaContext.Add(_depthOcclusionHandler);
        }

    }

}