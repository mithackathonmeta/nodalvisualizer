﻿namespace Meta
{

    internal interface IEventReceiver
    {
        void Init(ref EventHandlers eventHandlers);
    }

}