﻿namespace Meta
{

    public delegate void AwakeHandler();
    public delegate void StartHandler();
    public delegate void UpdateHandler();
    public delegate void FixedUpdateHandler();
    public delegate void LateUpdateHandler();
    public delegate void OnDestroyHandler();
    public delegate void OnApplicationQuitHandler();

    /// <summary>
    /// Contains event delegates to allow control over execution of registered modules.
    /// </summary>
    public struct EventHandlers
    {
        public AwakeHandler awakeEvent;
        public StartHandler startEvent;
        public UpdateHandler updateEvent;
        public FixedUpdateHandler fixedUpdateEvent;
        public LateUpdateHandler lateUpdateEvent;
        public OnDestroyHandler onDestroyEvent;
        public OnApplicationQuitHandler onApplicationQuitEvent;
    }

}