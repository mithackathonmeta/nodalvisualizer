﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Meta-Editor")]
[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]

namespace Meta
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Handles setup of the Meta scene environment and provides reference to Meta context classes.
    /// </summary>
    public class MetaManager : MonoBehaviour
    {

        #region Member Variables

        private MetaFactory _metaFactory;
        private MetaContext _metaContext;

        /// <summary>
        ///     The context object containing references to modules providing Meta functionality.
        /// </summary>
        public MetaContext metaContext
        {
            get
            {
                return _metaContext;
            }
        }

        [SerializeField]
        private DataAcquisitionSystem _dataAcquisitionSystem = new DataAcquisitionSystem();

        /// <summary>
        ///     The scale mapping of 1 meter to the number of unity units. This affects the simulation parameters. As this value increases, 1 unity unit will become smaller in
        ///     real world terms. eg To use a simulation scale of 1 = 1cm, change this variable to 100. To use 1 = 100cm, change
        ///     this variable to 1.
        /// </summary>
        [SerializeField]
        internal float globalScaleMetresToUnity = 1f; // TODO: will be moved to a settings class

        protected string playbackDir;

        private List<IEventReceiver> _eventReceivers = new List<IEventReceiver>();

        private EventHandlers _eventHandlers = new EventHandlers();

        #endregion

        #region MonoBehaviour Methods

        protected virtual void Awake()
        {
            new MetaPathVariables().AddPathVariables();
            _metaFactory = new MetaFactory(_dataAcquisitionSystem, gameObject, globalScaleMetresToUnity, playbackDir);
            _eventReceivers = _metaFactory.ConstructAll(ref _metaContext);
            foreach (IEventReceiver eventReceiver in _eventReceivers)
            {
                eventReceiver.Init(ref _eventHandlers);
            }
            if (_eventHandlers.awakeEvent != null)
            {
                _eventHandlers.awakeEvent();
            }
        }

        protected virtual void Start()
        {
            if (_eventHandlers.startEvent != null)
            {
                _eventHandlers.startEvent();
            }
        }

        private void Update()
        {
            CheckForShortcuts();
            if (_eventHandlers.updateEvent != null)
            {
                _eventHandlers.updateEvent();
            }
        }

        private void FixedUpdate()
        {
            if (_eventHandlers.fixedUpdateEvent != null)
            {
                _eventHandlers.fixedUpdateEvent();
            }
        }

        private void LateUpdate()
        {
            if (_eventHandlers.lateUpdateEvent != null)
            {
                _eventHandlers.lateUpdateEvent();
            }
        }

        private void OnDestroy()
        {
            if (_eventHandlers.onDestroyEvent != null)
            {
                _eventHandlers.onDestroyEvent();
            }
            
        }

        private void OnApplicationQuit()
        {
            if (_eventHandlers.onApplicationQuitEvent != null)
            {
                _eventHandlers.onApplicationQuitEvent();
            }
        }

        protected virtual void CheckForShortcuts()
        {
            if (Input.GetKeyDown(KeyCode.F4))
            {
                metaContext.Get<MetaLocalization>().ResetLocalization();
            }
        }

        #endregion

    }

}