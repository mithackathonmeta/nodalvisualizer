﻿using System;
using Meta.Internal.HandPhysics;
using UnityEngine;

namespace Meta {

	/*================================================================================================*/
	public class MetaCalibrationSwitcher : MonoBehaviour {

		public enum ProfileName {
			Default,
			Zach
		}

		public ProfileName ActiveProfile;

		private Transform vLeftCameraTx;
		private Transform vRightCameraTx;
		private Transform vCloudTx;
		private HandPointCloudFromRealPoints vCloud;
		private ProfileName vPrevProfile;
		private int vNameCount;


		////////////////////////////////////////////////////////////////////////////////////////////////
		/*--------------------------------------------------------------------------------------------*/
		public void Awake() {
			vLeftCameraTx = gameObject.transform.FindChild("MetaCameras/StereoCameras/LeftCamera");
			vRightCameraTx = gameObject.transform.FindChild("MetaCameras/StereoCameras/RightCamera");
			vCloudTx = gameObject.transform.FindChild("MetaCameras/DepthOcclusion");
			vCloud = vCloudTx.GetComponentInChildren<HandPointCloudFromRealPoints>(true);
			vNameCount = Enum.GetNames(typeof(ProfileName)).Length;
		}

		/*--------------------------------------------------------------------------------------------*/
		public void Update() {
			bool isControl = (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl));
			bool isShift = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));

			if ( isControl && isShift && Input.GetKeyDown(KeyCode.W) ) {
				ActiveProfile = (ProfileName)(((int)ActiveProfile+1)%vNameCount); //cycle calibrations
			}

			if ( ActiveProfile != vPrevProfile ) {
				Debug.Log("Calibrating Meta2 with profile: "+ActiveProfile);
				ResetCalibration();
				SetPersonalCalibration();
				vPrevProfile = ActiveProfile;
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////////////
		/*--------------------------------------------------------------------------------------------*/
		private void ResetCalibration() {
			//Reset everything to the same starting point. Any property that is changed in the ...
			//... SetPersonalCalibration() method should be given a default value here.

			vCloudTx.localPosition = new Vector3(0, 0.03398f, 0.09796f);
			vCloudTx.localRotation = Quaternion.Euler(22, -5, 0);
			vCloudTx.localScale = new Vector3(1, -1, 1);

			vCloud.particleColor = new Color32(231, 198, 198, 255);
			vCloud.particleSize = 0.25f;
			vCloud.ColorWithCharge = false;
			vCloud.ColorWithChargeMultiplier = 16;
			SetIpd(61);
		}

		/*--------------------------------------------------------------------------------------------*/
		private void SetIpd(float pDistanceInMillimeters) {
			vLeftCameraTx.transform.localPosition = new Vector3(-pDistanceInMillimeters/2000, 0, 0);
			vRightCameraTx.transform.localPosition = new Vector3(pDistanceInMillimeters/2000, 0, 0);
		}

		/*--------------------------------------------------------------------------------------------*/
		private void SetPersonalCalibration() {
			switch ( ActiveProfile ) {
				case ProfileName.Zach:
					vCloudTx.localPosition = new Vector3(0.05f, 0.095f, 0.1f);
					vCloudTx.localRotation = Quaternion.Euler(37.5f, 350, 0);
					vCloudTx.localScale = new Vector3(1, -1.1f, 0.94f);
					vCloud.ColorWithCharge = true;
					SetIpd(57);
					break;
			}
		}

	}

}
