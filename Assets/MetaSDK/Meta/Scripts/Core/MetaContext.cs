﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Meta
{
    using Meta.Internal;

    /// <summary>
    ///     Handles setup and references to modules for access to different components of the Meta SDK
    /// </summary>
    public class MetaContext
    {
        /// <summary>
        ///     Returns the effective scale factor applied to Meta objects, based on the default scale of 1m to 100 Unity units.
        /// </summary>
        internal float meterToUnityScale { get; set; }

        internal FunctionFlags optionsBitmask { get; set; }

        /// <summary>
		/// Dictionary used to keep all the various modules accessible via MetaContext.
        /// </summary>
        private Dictionary<Type, Object> modules = new Dictionary<Type, object>();

        /// <summary>
        /// Returns a list of all the modules currently available in MetaContext.
        /// </summary>
        /// <returns>A list of strings of the names of the types.</returns>
        public Type[] GetModuleList()
        {
            return modules.Keys.ToArray();
        }

        /// <summary>
        /// Returns True if MetaContext contains a module of Type T.
        /// </summary>
        /// <typeparam name="T">Type to check for.</typeparam>
        /// <returns>True if a module of the type exists.</returns>
        public bool ContainsModule<T>()
        {
            return modules.ContainsKey(typeof (T));
        }

        /// <summary>
        /// Get the module of type T. If no such module exists, returns null.
        /// </summary>
        /// <typeparam name="T">Type of module to return.</typeparam>
        /// <returns>Module of type T if it exists, otherwise null.</returns>
        public T Get<T>()
        {
            if (!modules.ContainsKey(typeof(T)))
            {
                throw new KeyNotFoundException("No module of type " + typeof(T)
                                               +
                                               " exists. Please check using MetaContext.ContainsModule<T>() before using MetaContext.Get<T>()");
            }
            return (T) modules[typeof (T)];
        }

        /// <summary>
        /// The IUserSettings interface is not exposed, developers may have
        /// access to part of it- inherited from IUserSettingsDeveloper.
        /// </summary>
        /// <returns></returns>
        public IUserSettingsDeveloper GetUserSettings()
        {
            return Get<IUserSettings>();
        }

        /// <summary>
        /// Add a module to MetaContext.
        /// </summary>
        /// <typeparam name="T">Type of the module to be added.</typeparam>
        /// <param name="module">Actual object to add.</param>
        internal void Add <T>(T module)
        {
            modules[typeof (T)] = module;
        }
    }
}