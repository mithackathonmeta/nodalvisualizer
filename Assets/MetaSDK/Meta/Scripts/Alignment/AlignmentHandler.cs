﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Handles the loading and application of alignment data.
    /// This module observes the alignment-directory-file and 
    /// is alerted when updates occur to it (i.e when the 'active'
    /// profile changes). 
    /// </summary>
    public class AlignmentHandler : IEventReceiver
    {
        /// <summary>
        /// The Alignment update listeners. These will receive events when the alignment is updated.
        /// </summary>
        public readonly List<IAlignmentUpdateListener> AlignmentUpdateListeners = new List<IAlignmentUpdateListener>();

        /// <summary>
        /// A reference to the main metaContext instance
        /// </summary>
        private MetaContext _metaContext;

        /// <summary>
        /// The path to the alignment-directory-file's directory. 
        /// </summary>
        private readonly string _alignmentDirFilePathDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                               @"\Meta\UserSettings\";

        private readonly string _alignmentDirFileName = "meta_calibration.cdb";

        /// <summary>
        /// The path to the alignment-directory-file. 
        /// </summary>
        private string _fullADFPath
        {
            get { return (_alignmentDirFilePathDir + _alignmentDirFileName); }
        }

        private FileSystemWatcher _fsWatcher;

        /// <summary>
        /// The index of the alignment profile that is active (retrieved from the alignment-directory-file)
        /// </summary>
        private int _alignmentIndexFromFile = -1;

        /// <summary>
        /// The index of the alignment profile that is active (retrieved from the alignment-directory-file)
        /// </summary>
        public int AlignmentProfileIndex
        {
            get { return _alignmentIndexFromFile; }
        }

        /// <summary>
        /// Set up the file system watcher, add to the delegate 'changed' an anonymous function which
        /// loads the updates and updates AlignmentUpdateListeners.
        /// </summary>
        /// <param name="eventHandlers"></param>
        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.startEvent += () =>
            {
                if (File.Exists(_fullADFPath))
                {
                    _fsWatcher = new FileSystemWatcher(_alignmentDirFilePathDir);
                    _fsWatcher.Filter = _alignmentDirFileName;
                    _fsWatcher.NotifyFilter = NotifyFilters.LastWrite;
                    _fsWatcher.EnableRaisingEvents = true;
                    _fsWatcher.Changed += (object o, FileSystemEventArgs e) =>
                    {
                        Debug.Log(string.Format("the file \"{0}\" changed.", _fullADFPath));
                        var profile = _updateAlignmentProfile();
                        foreach (IAlignmentUpdateListener listener in AlignmentUpdateListeners)
                        {
                            listener.OnAlignmentUpdate(profile);
                        }
                    };
                }

                //Run this at least once so that the index is loaded from the alignment-directory-file.
                MetaManager manager = GameObject.FindObjectOfType<MetaManager>();
                if (manager != null)
                {
                    _metaContext = manager.metaContext;
                }
                _updateAlignmentProfile();
            };

            //Allow for the delegates to be deleted.
            eventHandlers.onApplicationQuitEvent += () => { _fsWatcher = null; };
        }

        /// <summary>
        /// Updates the alignment profile, if necessary.
        /// </summary>
        private AlignmentProfile _updateAlignmentProfile()
        {
            var oldAlignmentIndex = _alignmentIndexFromFile;
            _alignmentIndexFromFile = _getActiveAlignmentIndexFromFile();

            if (_alignmentIndexFromFile != oldAlignmentIndex
                && _metaContext != null
                && _metaContext.ContainsModule<IUserSettings>() //Get the more permissive user settings
                && _metaContext.Get<IUserSettings>().HasKey(MetaConfiguration.AlignmentProfile, _alignmentIndexFromFile)) //Check that the index from file correctly indexes an alignment profile
            {
                var newProfile =
                    _metaContext.Get<IUserSettings>()
                        .GetSetting<AlignmentProfile>(MetaConfiguration.AlignmentProfile, _alignmentIndexFromFile);

                //May intentionally overwrite the existing profile
                _metaContext.Add<AlignmentProfile>(newProfile);
                return newProfile;
            }
            return null;
        }

        /// <summary>
        /// Loads the active alignment profile index from the alignment-directory-file. 
        /// </summary>
        private int _getActiveAlignmentIndexFromFile()
        {
            string[] lines = null;
            if (File.Exists(_fullADFPath))
            {
                lines = File.ReadAllLines(_fullADFPath);
            }
            
            //just enough for header, and one tuple of name-index across lines.
            //The name of the last tuple should be empty signifying that it is the 'active' profile.
            if (lines != null && lines.Length >= 3 && lines[lines.Length - 2].Equals(""))
            {
                try
                {
                    //The index from the last name-index tuple
                    return Int32.Parse(lines[lines.Length - 1]);
                }
                catch (Exception)
                {}
            }
            return -1;
        }
    }

}
