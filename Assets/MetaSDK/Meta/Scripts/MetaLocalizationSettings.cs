﻿using UnityEngine;
using System.Collections;
using Meta;
using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace Meta
{
    /// <summary>
    /// To be attached to any gameObject in a Scene containing the meta2 gameObject.
    /// This script allows the user to select from a drop-down menu the localizer to be used.
    /// </summary>
    [Serializable]
    public class MetaLocalizationSettings : MetaBehaviour
    {
        /// <summary>
        /// Stores the index of selected localizer in the list of localizers.
        /// </summary>
        [SerializeField]
        public int m_listIdx;

        public void Start()
        {
            //Debug.Log("at start: " + m_listIdx); //manual test
            AssignLocalizationType(GetLocalizationTypes()[m_listIdx]);
            ILocalizer localizerMember = GetComponent<ILocalizer>();
            if (localizerMember != null)
            {
                metaContext.Get<MetaLocalization>().SetLocalizer(localizerMember.GetType());
            }
        }

        /// <summary>
        /// Gets a list of all class types that are descendants of the interface 'ILocalizer'
        /// </summary>
        /// <returns>List of Type, which contains all the types descending from ILocalizer </returns>
        public List<Type> GetLocalizationTypes()
        {
            Type baseType = typeof(ILocalizer);
            var assembly = Assembly.GetExecutingAssembly();
            var types = assembly.GetTypes().Where(baseType.IsAssignableFrom).Where(t => baseType != t).ToList();
            //do some juggling of the IMULocalizer in the list (since it is the default localizer)
            types.Remove(typeof(IMULocalizer));
            types.Insert(0, typeof(IMULocalizer));
            return types;
        }

        public int GetIndexForLocalizationType(Type localizationType)
        {
            var types = GetLocalizationTypes();
            for (int i = 0; i < types.Count; ++i)
            {
                if (localizationType == types[i])
                {
                    return i;
                }
            }
            return 0;
        }

        /// <summary>
        /// Sets the localizer currently used by the metaContext's MetaLocalization instance.
        /// </summary>
        /// <param name="localizationType">the type of the localizer to instantiate.</param>
        public void SetLocalizationType(Type localizationType)
        {
            ConstructorInfo[] constructorInfos = localizationType.GetConstructors();
            if (constructorInfos.Length > 0 && constructorInfos[0].GetParameters().Length > 0)
            {
                Debug.LogError(localizationType.ToString() + " needs to contain an empty constructor.");
            }
            else
            {
                //ILocalizer instance = localizationType.GetConstructor(new Type[] { }).Invoke(new object[] { }) as ILocalizer;
                //metaContext.Get<MetaLocalization>().SetLocalizer(instance);
            }
        }

        /// <summary>
        /// Get the localizer assigned as a component
        /// </summary>
        /// <returns></returns>
        public ILocalizer GetAssignedLocalizer()
        {
            if (Application.isPlaying)
            {
                return metaContext.Get<MetaLocalization>().GetLocalizer();
            }

            var oldComponents = GetComponents<ILocalizer>();
            if (oldComponents != null && oldComponents.Length > 0)
            {
                return oldComponents[0];
            }

            return null;
        }

        /// <summary>
        /// Assigns the localization type as a component member of the GameObject
        /// </summary>
        /// <param name="localizationType"></param>
        public void AssignLocalizationType(Type localizationType)
        {
            if (Application.isPlaying)
            {
                metaContext.Get<MetaLocalization>().SetLocalizer(localizationType);
                m_listIdx = GetIndexForLocalizationType(localizationType);
            }
            else
            {
                //Record a list of existing localizers assigned
                var oldComponents = GetComponents<ILocalizer>();
                if (oldComponents != null && oldComponents.Length > 0 && oldComponents[0].GetType() == localizationType)
                {
                    return; //Avoid reassignment and loss of set script values
                }

                //Assign the new localizer
                gameObject.AddComponent(localizationType);
                
                //Support for hotswapping the localizer method
                if (oldComponents != null)
                {
                    foreach (var component in oldComponents)
                    {
                        if (component != null)
                        {
                            DestroyImmediate((UnityEngine.Object)component);
                        }
                    }
                }
            }
        }
    }
}