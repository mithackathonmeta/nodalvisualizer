using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using Microsoft.Win32;
#endif
using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.InteropServices;

// TODO !!! The whole DirectMode stack right now is a proof of concept. Alpha version, use at your own risk!
// TODO Move rendering to Compositor (Conner)
// TODO Send separate textures for left and right, do unwarping in the Compositor (Conner/Felix)
// TODO Get rid of flickering (Conner/Felix)
// TODO Remove MenuItems and Debug Messages in the Editor when not needed anymore (Felix)

/// <summary>
/// Put this on the UnwarpingCamera to enable DirectMode.
/// </summary>
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class MetaDirectMode : MonoBehaviour
{
	#region Menu Items for Direct Mode Alpha Testing

	#if UNITY_EDITOR

	[MenuItem("Meta 2/DirectMode/Enable DirectMode on Meta2")]
	static void EnableDirectMode() {
		if(EditorUtility.DisplayDialog ("Are you sure?", "Unity might crash. Please save before doing this. Continue?", "Yes", "No")) {
			// call batch file to enable it
			CallMetaDirectMode ("-enable");
			// TODO Unity might crash on calling InitDirectMode from inside Unity.
			// InitDirectMode();
			InitDirectMode ();
		}
	}

	[MenuItem("Meta 2/DirectMode/Disable DirectMode on Meta2")]
	static void DisableDirectMode() {
		// turn the plugin off (free DirectMode handle)
		DestroyDirectMode();
		// call the batch file to turn direct mode on. show the result in the console output.
		CallMetaDirectMode ("-disable");
	}

	static void CallMetaDirectMode(string argument) {
		// check for registry key
		if (pathToDirectModeTool.StartsWith ("/")) {
			EditorUtility.DisplayDialog ("DirectMode Utilities not found", "To be able to use DirectMode, please restart your computer.", "Got it");
		}
		else
		if (!RegistryKeyExists ()) {
			// warning
				EditorUtility.DisplayDialog("DirectMode registry entry not found", "DirectMode is not yet enabled on your computer. You might need to restart your machine.", "Got it");
		}
		else
		{
			// call the batch file to turn direct mode on. show the result in the console output.
			var proc = new Process {
				StartInfo = new ProcessStartInfo {
					FileName = pathToDirectModeTool,
					Arguments = argument,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					RedirectStandardError = true,
					CreateNoWindow = true
				}
			};
			
			proc.Start ();
			
			// show both errors and normal output
			UnityEngine.Debug.Log(proc.StandardOutput.ReadToEnd ());
			var error = proc.StandardError.ReadToEnd ();
			if (error != "") {
				UnityEngine.Debug.LogError (error);
				EditorUtility.DisplayDialog ("Could not enable DirectMode", "Please make sure that SDK2 is installed correctly, you have the newest NVidia graphics drivers, and you restarted your machine after installing.", "Got it");
			}					
			
			proc.WaitForExit ();
		}
	}

	static bool RegistryKeyExists() {
		var regPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\nvlddmkm";
		var regAttribute = "1641970VRWhiteList";

		// HACK does not contain the real value, but at least not null if the value is present
		var registryValue = Registry.GetValue (regPath, regAttribute, "").ToString();
		return (registryValue != "");
	}

	static string pathToDirectModeTool {
		get {
			return Environment.GetEnvironmentVariable ("META_2_DIRECTMODE") + "bin/MetaDirectModeUtil.exe";
		}
	}

	static string pathToWhitelist {
		get {
			return Environment.GetEnvironmentVariable ("META_2_DIRECTMODE") + "whitelist.reg";
		}
	}

	#endif

	#endregion

	// Camera to render to device
    Camera MainCamera;

    private delegate void DebugCallback(string message);

	// Native plugin rendering events are only called if a plugin is used
	// by some script. This means we have to DllImport at least
	// one function in some active script.
	// For this example, we'll call into plugin's SetTimeFromUnity
	// function and pass the current time so the plugin can animate.

	[DllImport("MetaDirectMode")]
	private static extern void SetTimeFromUnity(float t);

	// We'll also pass native pointer to a texture in Unity.
	// The plugin will fill texture data from native code.
	[DllImport("MetaDirectMode")]
	private static extern void SetTextureFromUnity(System.IntPtr texture);

	[DllImport("MetaDirectMode")]
	private static extern void SetUnityStreamingAssetsPath([MarshalAs(UnmanagedType.LPStr)] string path);

	[DllImport("MetaDirectMode")]
	private static extern IntPtr GetRenderEventFunc();

    [DllImport("MetaDirectMode")]
    private static extern void InitDirectMode();

    [DllImport("MetaDirectMode")]
    private static extern void DestroyDirectMode();

    [DllImport("MetaDirectMode")]
    private static extern void RegisterDebugCallback(DebugCallback callback);

    public enum DirectModeMode
    {
        WaitForEndOfFrame,
        OnPostRender
    }

    public DirectModeMode mode = DirectModeMode.WaitForEndOfFrame;

    private static void DebugMethod(string message)
    {
		UnityEngine.Debug.Log("Meta Direct Mode: " + message);
    }

	IEnumerator Start()
	{
        RegisterDebugCallback(new DebugCallback(DebugMethod));
        
        SetCameraTexture();
        
		if (Application.isPlaying && mode == DirectModeMode.WaitForEndOfFrame) { 
            yield return StartCoroutine(CallPluginAtEndOfFrames());
        }

		yield break;
    }

    private void SetCameraTexture()
    {
        MainCamera = GetComponent<Camera>();
        var rT = new RenderTexture(2560, 1440, 24, RenderTextureFormat.ARGB32);
        rT.generateMips = false;
        rT.filterMode = FilterMode.Point;
        rT.Create();
        MainCamera.targetTexture = rT;
        SetTextureFromUnity(rT.GetNativeTexturePtr());
    }

    void Check()
    {
        if(MainCamera == null || MainCamera.targetTexture == null)
            SetCameraTexture();
    }
	// TODO figure out which is the correct way to submit frames. Currently, this flickers even more than the OnPostRender method.
	/// <summary>
	/// Used in PlayMode and in Builds
	/// </summary>
	
	private IEnumerator CallPluginAtEndOfFrames()
	{
	    if (mode != DirectModeMode.WaitForEndOfFrame) yield break;

	    Check();

		while (true)
        {
			// Wait until all frame rendering is done
			yield return new WaitForEndOfFrame();
			SetTimeFromUnity (Time.timeSinceLevelLoad);
			GL.IssuePluginEvent(GetRenderEventFunc(), 1);
		}
	}

	// for debugging purposes
    bool saveImage = false;

	/// <summary>
	/// Used in the Editor when not using PlayMode
	/// </summary>
	void OnPostRender()
	{
        if (mode == DirectModeMode.OnPostRender || (mode == DirectModeMode.WaitForEndOfFrame && Application.isEditor && !Application.isPlaying))
        {
            Check();
            SetTimeFromUnity(Time.timeSinceLevelLoad);
            GL.IssuePluginEvent(GetRenderEventFunc(), 1);
        }
    }

    void OnEnable()
    {
        // turn DirectMode on
        // ...

#if UNITY_EDITOR
        UnityEditor.SceneView.RepaintAll();
#endif
    }

    void OnDisable()
    {
        // turn DirectMode off
        // ...

        var rT = MainCamera.targetTexture;
        MainCamera.targetTexture = null;

        if (rT)
        {
            rT.Release();
            rT = null;
        }

#if UNITY_EDITOR
        UnityEditor.SceneView.RepaintAll();
#endif
    }
}
