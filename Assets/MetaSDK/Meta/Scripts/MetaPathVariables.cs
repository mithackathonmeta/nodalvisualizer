﻿using System.IO;
using System;
using UnityEngine;

namespace Meta
{

    ///// <summary>
    ///// MetaPlugin adds dll path to the programs path.
    ///// </summary>
    ///// <remarks>
    ///// It adds Assets/Plugins/x86 to the path in the editor, and ApplicationDataFolder\Plugins to the build path.
    ///// *NOTE*The static constructor for this class needs to be loaded before the assembly tris to load the dlls. therfore, changing the MetaWorld script exxecution order will create problems for builds.*NOTE*
    ///// </remarks>
    internal class MetaPathVariables
    {
        public void AddPathVariables()
        {
            string depthsenseEnvironmentVar, sdkEnvironmentVar;
            string depthsensePath, librariesPath, pluginsPath;

            pluginsPath = Application.dataPath + Path.DirectorySeparatorChar + (Application.isEditor ? "MetaSDK" + Path.DirectorySeparatorChar : "") + "Plugins";

            if (MetaUtils.IsBeta())
            {
                sdkEnvironmentVar = "META_SDK_BETA";
            }
            else
            {
                sdkEnvironmentVar = "META_SDK";
            }

            librariesPath = Environment.GetEnvironmentVariable(sdkEnvironmentVar) + "Libraries" + Path.DirectorySeparatorChar;

            if (IntPtr.Size == 8)
            {
                if (Application.isEditor)
                {
                    pluginsPath += (Path.DirectorySeparatorChar + "x86_64");
                }
                librariesPath += "x64";
                depthsenseEnvironmentVar = "DEPTHSENSESDK64";
            }
            else
            {
                if (Application.isEditor)
                {
                    pluginsPath += (Path.DirectorySeparatorChar + "x86");
                }
                librariesPath += "x86";
                depthsenseEnvironmentVar = "DEPTHSENSESDK32";
            }
            depthsensePath = Environment.GetEnvironmentVariable(depthsenseEnvironmentVar) + Path.DirectorySeparatorChar + "bin";

            AddPathVariable(depthsensePath);
            AddPathVariable(librariesPath);
            pluginsPath = pluginsPath.Replace("/", "\\");
            AddPathVariable(pluginsPath);

            string metaConfigPath = pluginsPath + Path.DirectorySeparatorChar + "META_CONFIG";

            if (Environment.GetEnvironmentVariable("META_CONFIG", EnvironmentVariableTarget.Machine) == null)
            {
                Environment.SetEnvironmentVariable("META_CONFIG", metaConfigPath, EnvironmentVariableTarget.Process);
            }

            //Debug.Log(Environment.GetEnvironmentVariable("PATH"));
        }

        /// <summary>
        /// Add From lowest precedence to highest precedence.
        /// </summary>
        /// <param name="dllPath">directory to add to the path.</param>
        private void AddPathVariable(string dllPath)
        {
            String currentPath = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Process);

            // There is no harm if a directory appears twice on the path.
            // This is to be able to change precedence of search path

            Environment.SetEnvironmentVariable("PATH", dllPath + Path.PathSeparator + currentPath, EnvironmentVariableTarget.Process);
        }

    }
}
