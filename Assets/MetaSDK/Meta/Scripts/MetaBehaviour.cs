﻿using UnityEngine;

namespace Meta
{
    /// <summary>
    /// Base class for simplying access to Meta context.
    /// </summary>
    public class MetaBehaviour : MonoBehaviour
    {

        private MetaContext _metaContext;

        /// <summary>
        /// Contains references to modules used to provide Meta functionality in the scene.
        /// </summary>
        protected MetaContext metaContext
        {
            get
            {
                if (_metaContext == null)
                {
                    MetaManager metaManager = FindObjectOfType<MetaManager>();
                    if (metaManager != null)
                    {
                        _metaContext = metaManager.metaContext;
                    }
                    else
                    {
                        Debug.LogError("Error: The Meta2 prefab is missing from the scene");
                    }
                }
                return _metaContext;
            }
        }

    }

}