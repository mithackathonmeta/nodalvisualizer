﻿namespace Meta
{
    /// <summary>
    /// A flavour of the UserSettings which uses the event receiver to serialize user settings.
    /// </summary>
    internal class EventReceivingUserSettings : UserSettings, IEventReceiver
    {
        public void Init(ref EventHandlers eventHandlers)
        {
            eventHandlers.onDestroyEvent += () => { SerializePersistentSettings();};
        }

        public EventReceivingUserSettings(Credentials creds) : base(creds)
        {
        }
    }

}
