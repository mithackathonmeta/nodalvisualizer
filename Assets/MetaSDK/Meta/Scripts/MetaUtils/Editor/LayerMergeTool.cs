﻿using UnityEngine;
using UnityEditor;
using System.Linq;

namespace Meta
{
    /// <summary>
    /// This script allows the user to merge the layers used by Assets/Meta/ prefabs 
    /// with their own layers. 
    /// User layers should not change. Meta layers are assigned to layer slots that
    /// are vacant in the user's layer list. Meta prefabs are updated to use the new
    /// layer indices.
    /// 
    /// Masks of Meta prefabs see all user layers.
    /// </summary>
    [InitializeOnLoad]
    internal class LayerMergeTool : EditorWindow
    {
        /// <summary>
        /// The merger used to resolve layer conflicts
        /// </summary>
        private LayerMerger _merger = new LayerMerger();

        /// <summary>
        /// Whether this instance of the LayerMergeTool has run.
        /// </summary>
        private bool _hasRun;

        /// <summary>
        /// The key used to store the version in which the layer merge tool was last run.
        /// </summary>
        private static readonly string _persistantVersionKey = "layermergetool-latest-run-version";

        [MenuItem("Meta 2/Merge Layer Configurations")]
        public static void OpenPopup()
        {
            EditorWindow window = GetWindow(typeof(LayerMergeTool)); 
            window.titleContent = new GUIContent("Merge layer configurations");
            window.minSize = new Vector2(240, 280);
            window.ShowPopup();
        }

        public void OnGUI()
        {
            //This may change as the user adds/removes layers in the editor
            _merger.LoadLayerNames("ProjectSettings/TagManager.asset");

            if (_merger.GetLayerList() == null)
            {
                EditorGUILayout.LabelField("Could not load layer lists.");
                return;
            }
            
            //Get the number of layers needed by performing a union of old and new lists.
            var layers = LayerMerger.GetLayerFeatures(_merger.GetLayerList()).Select(t => t.Name);
            bool bHasDuplicateLayerNames = layers.GroupBy(n => n).Any(c => c.Count() > 1); 
            //Perform a union to get the set of layer names (not including duplicate names) to estimate how 
            // many layer slots will be needed.
            var oldLayers = LayerMerger.GetLayerFeatures(_merger.GetOldLayerList()).Select(t => t.Name);
            int nLayersAfter = layers.Union(oldLayers).ToList().Count + 8; //eight unassignable layers
            bool bHasEnoughVacantLayers = nLayersAfter <= 32; //The number of layers is assumed to be 32

            GUIStyle style = new GUIStyle(EditorStyles.label);
            style.wordWrap = true;
            
            //Describe the tool in a style with word-wrap
            EditorGUILayout.LabelField("This tool can be used to merge the layers used by the Meta "
                +"prefabs with user defined layers. Conflicts involving layer slot indices are resolved"
                +" by relocating Meta layers to vacant slots in the user layer list.", style);

            //Determine the colour of any messages to follow based on criteria
            Color styleTextColor = style.normal.textColor;
            style.normal.textColor = (bHasEnoughVacantLayers && !bHasDuplicateLayerNames) ? new Color(0f, .5f, 0f) : Color.red;

            if (bHasDuplicateLayerNames)
            {
                EditorGUILayout.LabelField("You may not have two or more layers with the same name.", style);
                return;
            }

            EditorGUILayout.LabelField("You " + (bHasEnoughVacantLayers ? "" : "do not ") + "have enough free layer slots to merge.", style);

            if (!bHasEnoughVacantLayers)
            {
                return;
            }

            if (GUILayout.Button("Merge Layers"))
            {
                _hasRun = true;
                _merger.MergeLayers();
                //Record the version of the SDK in which the Layer Merge Tool was run.
                EditorPrefs.SetString(_persistantVersionKey, MetaUtils.SDKVersion);
            }

            style.normal.textColor = styleTextColor;
            style.fontStyle = FontStyle.Bold;
            VersionFeedbackMessage(style);
            style.fontStyle = FontStyle.Normal;
            EditorGUILayout.LabelField("Important: To apply changes to Meta prefabs currently in the scene,"
                + " you must revert the prefab by pressing 'Revert' in the top of the inspector.", style);

            EditorGUILayout.LabelField("It is recommended that you do not rename the layers used by the Meta prefabs.", style);
        }

        /// <summary>
        /// A GUI method which provides feedback to the user including the version of the SDK that the Layer Merge Tool was last run.
        /// </summary>
        public void VersionFeedbackMessage(GUIStyle style)
        {
            string lastRunSDKVersion = EditorPrefs.GetString(_persistantVersionKey);



            string currentSDKVersion = MetaUtils.SDKVersion;
            string message = "";

            if (string.IsNullOrEmpty(lastRunSDKVersion))
            {
                message = "You have not run the tool.";
            }
            else if (lastRunSDKVersion.Equals(currentSDKVersion))
            {
                message = _hasRun?"You have run the tool.":"You have already run the tool for this SDK version.";
            }
            else
            {
                message = string.Format("You last ran the tool for SDK {0}.", lastRunSDKVersion);
            }

            EditorGUILayout.LabelField(message, style);
        }
    }
}