﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Meta
{
    /// <summary>
    /// Includes methods for merging layers.
    /// </summary>
    internal class LayerMerger 
    {

        /// <summary>
        /// Tuple of string and int.
        /// </summary>
        internal struct NameIndexTuple
        {
            public string Name;
            public int Index;
        }

        /// <summary>
        /// Information generated after systematically merging layer lists.
        /// </summary>
        internal struct MergedLayersInfo
        {
            public string[] MergedLayerNames;
            public int[] LayerOldNewMapping;
        }

        /// <summary>
        /// The lowest index of the User-definable Layer sublist.
        /// </summary>
        private static readonly int _minLayerIndex = 8;

        private string[] _oldLayerNameList =
        {
            "Default", //Layer 0 (Not user defined)
            "TransparentFX", //Layer 1 (Not user defined)
            "Ignore Raycast", //Layer 2 (Not user defined)
            "", //Layer 3 (Not user defined)
            "Water", //Layer 4 (Not user defined)
            "UI", //Layer 5 (Not user defined)
            "", //Layer 6 (Not user defined)
            "", //Layer 7 (Not user defined)
            "Unwarping", //Layer 8 (User defined)
            "PointCloud", //Layer 9 (User defined)
            "", //Layer 10 (User defined)
            "Grid", //Layer 11 (User defined)
            "", //Layer 12 (User defined)
            "", //Layer 13 (User defined)
            "", //Layer 14 (User defined)
            "", //Layer 15 (User defined)
            "Compositor", //Layer 16 (User defined)
            "IgnorePointCloud", //Layer 17 (User defined)
            "", //Layer 18 (User defined)
            "", //Layer 19 (User defined)
            "", //Layer 20 (User defined)
            "", //Layer 21 (User defined)
            "", //Layer 22 (User defined)
            "", //Layer 23 (User defined)
            "", //Layer 24 (User defined)
            "", //Layer 25 (User defined)
            "", //Layer 26 (User defined)
            "", //Layer 27 (User defined)
            "Display3", //Layer 28 (User defined)
            "", //Layer 29 (User defined)
            "", //Layer 30 (User defined)
            "Invisible" //Layer 31 (User defined)
        };

        private string[] _layerNameList;

        /// <summary>
        /// Get tuples of Layer-Name and Layer-Index for only the layers that are used. 
        /// </summary>
        /// <param name="layerNameList"></param>
        /// <returns></returns>
        internal static List<NameIndexTuple> GetLayerFeatures(string[] layerNameList)
        {
            var features = new List<NameIndexTuple>();
            for (int i = _minLayerIndex; i < layerNameList.Length; ++i)
            {
                if (layerNameList[i] != "")
                {
                    features.Add(new NameIndexTuple {Index = i, Name = layerNameList[i]});
                }
            }
            return features;
        }

        /// <summary>
        /// Load layer names from a TagManager file
        /// </summary>
        /// <param name="fpath">path to the TagManager file</param>
        /// <returns>list of layer names</returns>
        internal void LoadLayerNames(string fpath)
        {
            var layersAscii = AssetDatabase.LoadAllAssetsAtPath(fpath)[0];
            SerializedObject layersManager = new SerializedObject(layersAscii);
            SerializedProperty layersProp = layersManager.FindProperty("layers");
            string[] layerNames = new string[layersProp.arraySize];
            for (int i = 0; i < layersProp.arraySize; ++i)
            {
                SerializedProperty sp = layersProp.GetArrayElementAtIndex(i);
                layerNames[i] = sp.stringValue;
            }
            _layerNameList = layerNames;
        }

        internal string[] GetLayerList()
        {
            return _layerNameList;
        }

        internal string[] GetOldLayerList()
        {
            return _oldLayerNameList;
        }

        /// <summary>
        /// Find the number of layers used from an array of layer names.
        /// Layers that are used do not have an empty ("") name.
        /// </summary>
        /// <param name="layerNameList">list of layer names</param>
        /// <returns>number of used layers</returns>
        private static int CountUsedLayers(string[] layerNameList)
        {
            int nLayerNamesUsed = 0;
            for (int i = _minLayerIndex; i < layerNameList.Length; ++i)
            {
                if (layerNameList[i] != "")
                {
                    nLayerNamesUsed++;
                }
            }
            return nLayerNamesUsed;
        }

        /// <summary>
        /// Merge old layers into the current layer list
        /// </summary>
        internal void MergeLayers()
        {
            //merge old into current. Begin with a copy of the current one.
            var mergeLayersInfo = MergeLayerFiles(_oldLayerNameList, _layerNameList);
            //Now apply to prefabs
            foreach (var prefabPath in GetPrefabPathsInDirectory("Assets/MetaSDK/Meta"))
            {
                LayerConvertPrefab(prefabPath, mergeLayersInfo.LayerOldNewMapping);
            }

            //Seriralize the layers array from UnityEngine.Object read from file
            var tagManagerAsset = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0];
            SerializedObject layersManager = new SerializedObject(tagManagerAsset);
            SerializedProperty layersArrayProp = layersManager.FindProperty("layers");
            //Apply names to the layers
            for (int i = _minLayerIndex; i < mergeLayersInfo.MergedLayerNames.Length; ++i)
            {
                SerializedProperty sp = layersArrayProp.GetArrayElementAtIndex(i);
                sp.stringValue = mergeLayersInfo.MergedLayerNames[i];
            }
            //Save changes
            layersManager.ApplyModifiedProperties();
        }

        /// <summary>
        /// Merge a list of old layer names into a new list
        /// </summary>
        /// <param name="oldLayerNameList">the array of old layer names</param>
        /// <param name="layerNameList">the array of new layer names</param>
        /// <returns>The merged layers AND the map from old layer index to new layer index</returns>
        private static MergedLayersInfo MergeLayerFiles(string[] oldLayerNameList, string[] layerNameList)
        {
            var mergeLayerNameList = (string[]) layerNameList.Clone();
            //Get tuples of Name,Index for the old list
            var features = GetLayerFeatures(oldLayerNameList);
            //Initialize the mapping array of old index to new
            var oldNewLayerMappings = new int[oldLayerNameList.Length];
            //Unchangable layers should map to each other as usual.
            for (int i = 0; i < _minLayerIndex; ++i)
            {
                oldNewLayerMappings[i] = i;
            }
            //User defined layers should point to undefined for now.
            for (int i = _minLayerIndex; i < oldLayerNameList.Length; ++i)
            {
                oldNewLayerMappings[i] = -1;
            }
            //A list of tuples that did not experience name conflicts
            var unmergedFeatures = new List<NameIndexTuple>();
            //Merge old with current if there is a name conflict
            foreach (var feature in features)
            {
                bool b_added = false;
                for (int i = _minLayerIndex; i < layerNameList.Length; ++i)
                {
                    if (layerNameList[i] == feature.Name)
                    {
                        oldNewLayerMappings[feature.Index] = i;
                        b_added = true;
                    }
                }
                if (!b_added)
                {
                    unmergedFeatures.Add(feature);
                }
            }

            int freeIndex = _minLayerIndex;
            foreach (var feature in unmergedFeatures)
            {
                while (mergeLayerNameList[freeIndex] != "")
                {
                    freeIndex++;
                }

                if (freeIndex >= mergeLayerNameList.Length)
                {
                    Debug.Log("Not enough vacant layers");
                    break;
                }

                mergeLayerNameList[freeIndex] = feature.Name;
                oldNewLayerMappings[feature.Index] = freeIndex;
            }
            return new MergedLayersInfo()
            {
                MergedLayerNames = mergeLayerNameList,
                LayerOldNewMapping = oldNewLayerMappings
            };
        }

        /// <summary>
        /// Convert the layer indices used by the prefab located at the path to the new indices specified by the lookup table.
        /// </summary>
        /// <param name="prefabPath">Path to the prefab on disk to be converted</param>
        /// <param name="layersOldNewMap">lookup table of old layer index to new index</param>
        private static void LayerConvertPrefab(string prefabPath, int[] layersOldNewMap)
        {
            var prefabAssets = AssetDatabase.LoadAllAssetsAtPath(prefabPath);

            foreach (var unityObj in prefabAssets)
            {
                if (unityObj == null)
                {
                    continue;
                }

                SerializedObject assetNode = null;
                try
                {
                    assetNode = new SerializedObject(unityObj);
                }
                catch (Exception)
                {
                    Debug.LogError(string.Format("Layer Merger could not process the prefab: '{0}'", unityObj.name));
                    continue;
                }
                 
                var objType = assetNode.targetObject.GetType();
                if (objType != typeof(Camera) && objType != typeof(GameObject) &&
                    !objType.IsSubclassOf(typeof(MonoBehaviour)))
                {
                    continue; //Asset does not use layermask or cannot have layer assigned
                }

                bool b_expandChild = true;
                //iterate over the prefab using the GetIterator method.
                for (var prop = assetNode.GetIterator(); prop.Next(b_expandChild);)
                {
                    if (objType == typeof(GameObject) && prop.name == "m_Layer")
                    {
                        //refer to layer mapping structure to transform old layer index to new layer index
                        prop.intValue = layersOldNewMap[prop.intValue] < 0
                            ? prop.intValue
                            : layersOldNewMap[prop.intValue];
                    }
                    else if ((objType == typeof(Camera) || objType.IsSubclassOf(typeof(MonoBehaviour))) &&
                             prop.propertyType == SerializedPropertyType.LayerMask)
                    {
                        prop.Next(true); // get to "m_Bits" child of the LayerMask
                        if (prop.propertyType != SerializedPropertyType.Integer)
                        {
                            Debug.Log("Assumption that m_Bits follows from LayerMask went bad");
                        }

                        //It is important to assign to longValue here because although 'intValue' can store 32 flags, ints < 0 set the property to 0.
                        prop.longValue = MakeNewBitfield((uint) prop.intValue, layersOldNewMap);
                    }
                    //Save some computation by preventing traversal of some types
                    var propType = prop.propertyType;
                    b_expandChild = prop.hasChildren && !prop.isArray;
                    b_expandChild &= propType != SerializedPropertyType.String;
                    b_expandChild &= propType != SerializedPropertyType.Quaternion;
                    b_expandChild &= propType != SerializedPropertyType.Vector2;
                    b_expandChild &= propType != SerializedPropertyType.Vector3;
                    b_expandChild &= propType != SerializedPropertyType.Vector4;
                    b_expandChild &= propType != SerializedPropertyType.AnimationCurve;
                    b_expandChild &= propType != SerializedPropertyType.Rect;
                    b_expandChild &= propType != SerializedPropertyType.Color;
                    //probably better at this point to specify which types are allowed.
                }
                assetNode.ApplyModifiedProperties();
            }
        }

        private static uint MakeNewBitfield(uint oldBitfield, int[] oldNewMapping)
        {
            uint bitfield = 0xffffffff; //assume everything is seen by default
            //Assume 32 layers
            for (int i = 0; i < 32; ++i)
            {
                //old bitfield is marked <off> for that layer AND there exists a old->new mapping for that layer
                if ((oldBitfield & (1 << i)) == 0 && oldNewMapping[i] >= 0)
                {
                    bitfield &= (uint) ~(1 << oldNewMapping[i]); //should turn off that layer in the new layer list
                }
            }
            return bitfield;
        }


        private void TestMergeLayers()
        {
            Debug.Log("Testing...");
            string[] oldLayerTest = new string[]
            {
                "Default", "TransparentFX", "Ignore Raycast", "", "Water", "UI", "", "", "Unwarping", "PointCloud", "", "Grid", "", "",
                "", "", "Compositor", "", "", "", "", "", "", "", "", "", "", "", "Display3", "", "", "Invisible"
            };
            string[] layerTest = new string[]
            {
                "Default", "TransparentFX", "Ignore Raycast", "", "Water", "UI", "", "", "test1", "test2", "test3", "test4",
                "test5", "test6", "test7", "test8", "test9", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
            };

            if (oldLayerTest.Length != 32 || layerTest.Length != 32)
            {
                throw new Exception("Layer name lists did not have the correct size.");
            }

            var info = MergeLayerFiles(oldLayerTest, layerTest);

            //Check layers were merged correctly
            string[] expectedResult = new string[]
            {
                "Default", "TransparentFX", "Ignore Raycast", "", "Water", "UI", "", "", "test1", "test2", "test3", "test4",
                "test5", "test6", "test7", "test8", "test9", "Unwarping", "PointCloud", "Grid", "Compositor", "Display3", "Invisible",
                "", "", "", "", "", "", "", "", ""
            };
            for (int i = 0; i < 32; ++i)
            {
                if (info.MergedLayerNames[i] != expectedResult[i])
                {
                    throw new Exception("Names differed at index: " + i + " ::" + info.MergedLayerNames[i] + " != " +
                                        expectedResult[i]);
                }
            }

            var pointsOfInterest = new int[] {8, 16, 28, 31};
            var expectedMapping = new int[] {17, 18, 19, 20};
            //Check mapping from old indices to new indices worked
            for (int i = 0; i < pointsOfInterest.Length; ++i)
            {
                if (info.LayerOldNewMapping[pointsOfInterest[i]] != expectedMapping[i])
                {
                    throw new Exception("mapping did not match expected for old: " +
                                        info.LayerOldNewMapping[pointsOfInterest[i]]);
                }
            }
            //Check bitfields are correctly converted:
            int bitfield_All = (1 << 8) | (1 << 16) | (1 << 28) | (1 << 31);
            int bitfield_Some = (1 << 8);
            int bitfield_Some2 = (1 << 16) | (1 << 28) | (1 << 31);
            int bitfield_Some3 = (1 << 8) | (1 << 28) | (1 << 31);
            int bitfield_Some4 = (1 << 8) | (1 << 16) | (1 << 31);
            int bitfield_Some5 = (1 << 8) | (1 << 16) | (1 << 28);
            int bitfield_Some6 = (1 << 28) | (1 << 31);
            int bitfield_Some7 = (1 << 8) | (1 << 16);
            int bitfield_Some8 = (1 << 8) | (1 << 28);
            int bitfield_Some9 = (1 << 16) | (1 << 28);

            int result_All = 0xffffff << 8;
            int result_Some = ~((1 << 18) | (1 << 19) | (1 << 20) | 0xff);
            int result_Some2 = ~((1 << 17) | 0xff);
            int result_Some3 = ~((1 << 18) | 0xff);
            int result_Some4 = ~((1 << 19) | 0xff);
            int result_Some5 = ~((1 << 20) | 0xff);
            int result_Some6 = ~((1 << 17) | (1 << 18) | 0xff);
            int result_Some7 = ~((1 << 19) | (1 << 20) | 0xff);
            int result_Some8 = ~((1 << 18) | (1 << 20) | 0xff);
            int result_Some9 = ~((1 << 17) | (1 << 20) | 0xff);

            //Additional tests:
            int test1 = 1878982399;
            int result1 = ~((1 << 18) | (1 << 19) | (1 << 20) | (1 << 17));

            var bitfields = new int[]
            {
                bitfield_All, bitfield_Some, bitfield_Some2, bitfield_Some3, bitfield_Some4, bitfield_Some5, bitfield_Some6,
                bitfield_Some7, bitfield_Some8, bitfield_Some9, test1
            };
            var results = new int[]
            {
                result_All, result_Some, result_Some2, result_Some3, result_Some4, result_Some5, result_Some6, result_Some7,
                result_Some8, result_Some9, result1
            };

            for (int i = 0; i < bitfields.Length; ++i)
            {
                uint bitfieldConversion = MakeNewBitfield((uint) bitfields[i], info.LayerOldNewMapping);
                if ((int) bitfieldConversion != results[i])
                {
                    throw new Exception("Bitfields did not match for trial " + i + ": in=" + (uint) bitfields[i] +
                                        " got processed to= " + bitfieldConversion + ", expected=" + (uint) results[i]);

                }
            }
            Debug.Log("Finished testing.");
        }

        /// <summary>
        /// Find paths to all prefabs in directory or subdirectory of path.
        /// </summary>
        /// <param name="path">the </param>
        /// <returns></returns>
        private static IEnumerable<string> GetPrefabPathsInDirectory(string path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.LogError(ex.Message);
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path).Where(f => f.EndsWith(".prefab")).ToArray();
                }
                catch (System.Exception ex)
                {
                    Debug.LogError(ex.Message);
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }

    }
}