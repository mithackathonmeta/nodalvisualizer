﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MetaDirectMode))]
public class MetaDirectModeEditor : Editor {
	public override void OnInspectorGUI ()
	{
	    var metaDirectMode = (target as MetaDirectMode);

		EditorStyles.label.wordWrap = true;

		EditorGUILayout.Space ();

		EditorGUILayout.HelpBox ("DirectMode is currently an Alpha version.", MessageType.None);

        EditorGUI.BeginDisabledGroup(Application.isPlaying);
        metaDirectMode.mode = (MetaDirectMode.DirectModeMode) EditorGUILayout.EnumPopup("Render Mode", metaDirectMode.mode);
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space ();

		if (metaDirectMode.enabled) {
			EditorGUILayout.HelpBox ("Make sure that DirectMode is enabled. Go to \"Meta 2 > DirectMode > Enable DirectMode on Meta2\".", MessageType.Warning);
		} else {
			EditorGUILayout.HelpBox ("Make sure that DirectMode is disabled. Go to \"Meta 2 > DirectMode > Disable DirectMode on Meta2\".", MessageType.Warning);
		}

		EditorStyles.label.fontStyle = FontStyle.Normal;

		EditorGUILayout.HelpBox ("If your display is not working properly, please unplug power and HDMI, then replug. You might need to restart Unity.", MessageType.Info);
		EditorGUILayout.HelpBox ("If you experience lots of flickering with DirectMode enabled, try hiding all open Game views. If it still flickers, also hide all Scene views.", MessageType.Info);
		EditorGUILayout.HelpBox ("Options for turning DirectMode on and off are under \"Meta 2 > DirectMode\".\nBefore using them, make sure that you have\n-a recent NVidia graphics card\n-newest graphics drivers installed.", MessageType.Info);
	}
}
