﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Collections.Generic;
using Meta;
using UnityEditor.SceneManagement;

/// <summary>
/// Changes the appearance of the the Inspector used for the script 
/// 'MetaLocalizationSettings'. See script 'MetaLocalizationSettings'
/// for more information.
/// </summary>
[CustomEditor(typeof(MetaLocalizationSettings))]
[Serializable]
public class MetaLocalizationSettingsInspector : Editor {


    public override void OnInspectorGUI()
    {
        MetaLocalizationSettings mls = target as MetaLocalizationSettings;
        List<Type> types = mls.GetLocalizationTypes();

        EditorGUI.BeginChangeCheck();
        mls.m_listIdx = EditorGUILayout.Popup("Current Localizer: ", mls.GetIndexForLocalizationType(mls.GetAssignedLocalizer().GetType()), types.ConvertAll(t => t.ToString()).ToArray());

        if (EditorGUI.EndChangeCheck())
        {
            // This instructs Unity to produce a new snapshot of the 'MetaLocalizationSettings' instance 
            // so that it is maintained across from the editor to playing in the editor.
            EditorUtility.SetDirty(mls); 
            mls.AssignLocalizationType(types[mls.m_listIdx]);
            if (!Application.isPlaying)
            {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
            

            //IMPORTANT: Nothing after ExitGUI will be called!
            EditorGUIUtility.ExitGUI(); // prevent the GUI from being drawn with a null member
            //Nothing after ExitGUI will be called.
        }
    }
}
