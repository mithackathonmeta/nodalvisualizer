﻿using UnityEngine;
using System.Collections;

namespace Meta.UI
{
    public enum PressState
    {
        None,
        Pressed,
        Held,
        Releasing,
        Released,
    }
}