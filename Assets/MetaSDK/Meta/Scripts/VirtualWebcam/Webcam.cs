﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

namespace Meta
{
    public class Webcam : MonoBehaviour
    {
        /// <summary>
        /// The camera component of the gameobject that this is connected to.
        /// </summary>
        private Camera _camera;

        /// <summary>
        /// Vector used to scale the projection matrix
        /// </summary>
        private Vector3 _scaleVector;

        public void Awake()
        {
            _camera = GetComponent<Camera>();

            _scaleVector = new Vector3(1, -1, 1);
        }

        public void OnEnable()
        {
            
            if (_camera.targetTexture == null)
            {
                _camera.targetTexture = new RenderTexture(1280, 720, 0, RenderTextureFormat.ARGB32);
                _camera.targetTexture.depth = 24; // turn on depth buffer with 24 bit precision. 
                _camera.targetTexture.Create();
            }

            Plugin.Webcam.Initalize(_camera.targetTexture.GetNativeTexturePtr(), 30.0f);
        }

        public void Start()
        {
            Plugin.Webcam.Run();

            // Matrix4x4 mat = _camera.projectionMatrix;
            // mat *= Matrix4x4.Scale(_scaleVector);
            // _camera.projectionMatrix = mat;
        }

        public void OnDisable()
        {
            Plugin.Webcam.Stop();
        }

        public void OnPreRender()
        {
            GL.invertCulling = true;
        }

        public void OnPostRender()
        {
            GL.invertCulling = false;
            GL.IssuePluginEvent(Plugin.Webcam.GetRenderCallback(), 0);
        }
    }
}
