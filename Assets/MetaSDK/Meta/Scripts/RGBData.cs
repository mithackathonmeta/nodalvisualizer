﻿using System;

namespace Meta
{
    /// <summary>
    ///     Provides access to data from the RGB camera.
    /// </summary>
    public class RGBData {

        private DeviceTextureSource _devices;

        internal RGBData(DeviceTextureSource devices)
        {
            _devices = devices;
        }

        /// <summary>
        ///     Whether the RGB device has been registered as an input source device.
        /// </summary>
        /// <returns></returns>
        public bool IsRegistered()
        {
            return _devices.IsDeviceTextureRegistered(0);
        }

        /// <summary>
        ///     Registers the RGB feed to begin receiving of frame data.
        /// </summary>
        public void RegisterFeed()
        {
            _devices.registerTextureDevice(0);
        }

        /// <summary>
        ///     Gets the pointer to the location of the RGB data in memory.
        /// </summary>
        /// <returns>Integer memory pointer of RGB data.</returns>
        public IntPtr GetTexturePtr()
        {
            return _devices.GetRGBSource().GetTextureData().data;
        }

        /// <summary>
        ///     Gets the pixel height of the RGB feed.
        /// </summary>
        /// <returns>The pixel height of the RGB feed.</returns>
        public int GetTextureHeight()
        {
            return _devices.GetRGBSource().GetTextureData().height;
        }

        /// <summary>
        ///     Gets the pixel width of the RGB feed.
        /// </summary>
        /// <returns>The pixel width of the RGB feed.</returns>
        public int GetTextureWidth()
        {
            return _devices.GetRGBSource().GetTextureData().width;
        }

        /// <summary>
        ///     Gets the size of the memory allocated for a frame of RGB data.
        /// </summary>
        /// <returns>The size of memory to store a frame of data.</returns>
        public int GetTextureSize()
        {
            return _devices.GetRGBSource().GetTextureSize();
        }
    }
}

