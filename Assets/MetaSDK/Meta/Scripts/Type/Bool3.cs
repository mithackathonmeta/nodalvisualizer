﻿namespace Meta
{
    [System.Serializable]
    public struct Bool3
    {
        public bool X;
        public bool Y;
        public bool Z;
    }
}
