﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Meta
{
    [Serializable]
    public class Vector2Event : UnityEvent<Vector2> {}
}