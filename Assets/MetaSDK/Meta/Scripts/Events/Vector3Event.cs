﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Meta.Events
{
    [Serializable]
    public class Vector3UnityEvent : UnityEvent<float> { }
}