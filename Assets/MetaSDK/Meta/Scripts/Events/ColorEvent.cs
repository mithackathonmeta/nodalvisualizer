﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Meta
{
    [Serializable]
    public class ColorEvent : UnityEvent<Color> {}
}