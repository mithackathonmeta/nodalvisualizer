﻿using System;
using UnityEngine.Events;

namespace Meta.UI
{
    [Serializable]
    public class PressStateEvent : UnityEvent<PressState> { }
}