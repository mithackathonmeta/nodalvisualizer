﻿using System;
using  UnityEngine;
using UnityEngine.Events;

namespace Meta.UI
{
    [Serializable]
    public class ColliderEvent : UnityEvent<Collider> { }
}