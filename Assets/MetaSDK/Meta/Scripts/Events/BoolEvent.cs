﻿using System;
using UnityEngine.Events;

namespace Meta
{
    [Serializable]
    public class BoolEvent : UnityEvent<bool> {}
}