﻿using UnityEngine;

namespace Meta.Extensions
{
    public static class Vector3Extensions
    {
        public static Vector3 Abs(this Vector3 vector3)
        {
            vector3.x = Mathf.Abs(vector3.x);
            vector3.y = Mathf.Abs(vector3.y);
            vector3.z = Mathf.Abs(vector3.z);
            return vector3;
        }

        public static Vector3 Clamp(this Vector3 vector3, float min, float max)
        {
            vector3.x = Mathf.Clamp(vector3.x, min, max);
            vector3.y = Mathf.Clamp(vector3.y, min, max);
            vector3.z = Mathf.Clamp(vector3.z, min, max);
            return vector3;
        }

        /// <summary>
        /// Checks if all three components of the two Vector are approximately equal
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Approximately(this Vector3 a, Vector3 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
        }

        public static Vector3 Parse(string x, string y, string z)
        {
            return new Vector3(float.Parse(x), float.Parse(y), float.Parse(z));
        }
    }
}