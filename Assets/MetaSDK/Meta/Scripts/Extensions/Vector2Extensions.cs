﻿using UnityEngine;

namespace Meta.Extensions
{
    public static class Vector2Extensions
    {
        public static Vector2 Abs(this Vector2 vector2)
        {
            vector2.x = Mathf.Abs(vector2.x);
            vector2.y = Mathf.Abs(vector2.y);
            return vector2;
        }

        /// <summary>
        /// Checks if all three components of the two Vector are approximately equal
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Approximately(this Vector2 a, Vector2 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
        }
    }
}