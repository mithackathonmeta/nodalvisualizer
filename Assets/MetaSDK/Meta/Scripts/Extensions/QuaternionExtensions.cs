﻿using UnityEngine;

namespace Meta.Extensions
{
    public class QuaternionExtensions : MonoBehaviour
    {
        public static Quaternion Parse(string x, string y, string z, string w)
        {
            return new Quaternion(float.Parse(x), float.Parse(y), float.Parse(z), float.Parse(w));
        }
    }
}

