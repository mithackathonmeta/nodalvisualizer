﻿using UnityEngine;
using System.Collections;

namespace Meta.Gizmo
{
    public class AxisGizmo : MonoBehaviour
    {
        [SerializeField]
        private float _length = .2f;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.right * _length);
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, transform.up * _length);
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, transform.forward * _length);
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(transform.position, _length / 50f);
        }
    }
}